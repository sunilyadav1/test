(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error('Cannot find module "' + req + '".');
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/AboutUs/blogs/blogs.component.css":
/*!***************************************************!*\
  !*** ./src/app/AboutUs/blogs/blogs.component.css ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/AboutUs/blogs/blogs.component.html":
/*!****************************************************!*\
  !*** ./src/app/AboutUs/blogs/blogs.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<!--page title start-->\r\n<section class=\"page-title ptb-30\">\r\n  <div class=\"container padding-top-40\">\r\n    <div class=\"row\">\r\n      <div class=\"col-md-12\">\r\n        <h6 class=\"pagetitles\">Services</h6>\r\n        <ol class=\"breadcrumb\">\r\n          <li>\r\n            <a href=\"#\">Home</a>\r\n          </li>\r\n          <li>\r\n            <a href=\"#\">Services</a>\r\n          </li>\r\n          <li class=\"active\">Cross Platform Mobile Application Development with Xamarin</li>\r\n        </ol>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</section>\r\n<!--page title end-->\r\n<div class=\"blog container padding-top-20 blog\">\r\n  <div class=\"col-md-8\">\r\n    <div>\r\n      <h1 class=\"text-bold mb-11\">Google Increases Meta Description Length: Here’s What It Means For SEO Experts</h1>\r\n      <img src=\"assets/img/services/Google-Increases-Meta-Description-Length.png\" class=\"img-responsive  \" alt=\"Image\">\r\n        <div class=\"blog-auth\">\r\n          <img src=\"https://png.icons8.com/ios/20/000000/user-filled.png\">\r\n            <a href=\"\">Virendra Yadav</a>\r\n          </div>\r\n          <hr>\r\n            <!-- \r\n            <p class=\"text-bold padding-top-40\" style=\"text-align: center;font-size:8px\"> Pic credits: Xamarin.com </p> -->\r\n            <h2 class=\"text-justified padding-top-40\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis pharetra varius quam sit amet vulputate. Quisque mauris augue, molestie tincidunt condimentum vitae, gravida a libero. Aenean sit amet felis dolor, in sagittis nisi.</h2>\r\n          </div>\r\n          <div class=\"col-md-12 single-blog padding-top-10\">\r\n            <div class=\"well\">\r\n              <div class=\"media\">\r\n                <div class=\"col-md-4\">\r\n                  <a class=\"pull-left\" href=\"#\">\r\n                    <div class=\"img-heading pull-left\">\r\n                      <h5>APP DEVELOPMENT</h5>\r\n                    </div>\r\n                    <img class=\"media-object\" src=\"assets/img/services/5-Simple-Tips-To-Make-Your-Android-App-Successful_bigperl.jpg\">\r\n                    </a>\r\n                  </div>\r\n                  <div class=\"col-md-8\">\r\n                    <div class=\"media-body\">\r\n                      <h2 class=\"media-heading\">5 Simple Tips To Make Your Android App Successful</h2>\r\n                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis pharetra varius quam sit amet vulputate. Quisque mauris augue, molestie tincidunt condimentum vitae .</p>\r\n                      <a class=\" pull-right marginBottom10\" href=\"#\">READ MORE</a>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <div class=\"col-md-12 single-blog padding-top-20\">\r\n              <div class=\"well\">\r\n                <div class=\"media\">\r\n                  <div class=\"col-md-4\">\r\n                    <a class=\"pull-left\" href=\"#\">\r\n                      <div class=\"img-heading pull-left\">\r\n                        <h5>APP DEVELOPMENT</h5>\r\n                      </div>\r\n                      <img class=\"media-object\" src=\"assets/img/services/5-Simple-Tips-To-Make-Your-Android-App-Successful_bigperl.jpg\">\r\n                      </a>\r\n                    </div>\r\n                    <div class=\"col-md-8\">\r\n                      <div class=\"media-body\">\r\n                        <h2 class=\"media-heading\">5 Simple Tips To Make Your Android App Successful</h2>\r\n                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis pharetra varius quam sit amet vulputate. Quisque mauris augue, molestie tincidunt condimentum vitae .</p>\r\n                        <a class=\" pull-right marginBottom10\" href=\"#\">READ MORE</a>\r\n                      </div>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n              <div class=\"col-md-12 single-blog padding-top-20\">\r\n                <div class=\"well\">\r\n                  <div class=\"media\">\r\n                    <div class=\"col-md-4\">\r\n                      <a class=\"pull-left\" href=\"#\">\r\n                        <div class=\"img-heading pull-left\">\r\n                          <h5>APP DEVELOPMENT</h5>\r\n                        </div>\r\n                        <img class=\"media-object\" src=\"assets/img/services/5-Simple-Tips-To-Make-Your-Android-App-Successful_bigperl.jpg\">\r\n                        </a>\r\n                      </div>\r\n                      <div class=\"col-md-8\">\r\n                        <div class=\"media-body\">\r\n                          <h2 class=\"media-heading\">5 Simple Tips To Make Your Android App Successful</h2>\r\n                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis pharetra varius quam sit amet vulputate. Quisque mauris augue, molestie tincidunt condimentum vitae .</p>\r\n                          <a class=\" pull-right marginBottom10\" href=\"#\">READ MORE</a>\r\n                        </div>\r\n                      </div>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n                <div class=\"col-md-12 single-blog padding-top-20\">\r\n                  <div class=\"well\">\r\n                    <div class=\"media\">\r\n                      <div class=\"col-md-4\">\r\n                        <a class=\"pull-left\" href=\"#\">\r\n                          <div class=\"img-heading pull-left\">\r\n                            <h5>APP DEVELOPMENT</h5>\r\n                          </div>\r\n                          <img class=\"media-object\" src=\"assets/img/services/5-Simple-Tips-To-Make-Your-Android-App-Successful_bigperl.jpg\">\r\n                          </a>\r\n                        </div>\r\n                        <div class=\"col-md-8\">\r\n                          <div class=\"media-body\">\r\n                            <h2 class=\"media-heading\">5 Simple Tips To Make Your Android App Successful</h2>\r\n                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis pharetra varius quam sit amet vulputate. Quisque mauris augue, molestie tincidunt condimentum vitae .</p>\r\n                            <a class=\" pull-right marginBottom10\" href=\"#\">READ MORE</a>\r\n                          </div>\r\n                        </div>\r\n                      </div>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"col-md-12 single-blog padding-top-20\">\r\n                    <div class=\"well\">\r\n                      <div class=\"media\">\r\n                        <div class=\"col-md-4\">\r\n                          <a class=\"pull-left\" href=\"#\">\r\n                            <div class=\"img-heading pull-left\">\r\n                              <h5>APP DEVELOPMENT</h5>\r\n                            </div>\r\n                            <img class=\"media-object\" src=\"assets/img/services/5-Simple-Tips-To-Make-Your-Android-App-Successful_bigperl.jpg\">\r\n                            </a>\r\n                          </div>\r\n                          <div class=\"col-md-8\">\r\n                            <div class=\"media-body\">\r\n                              <h2 class=\"media-heading\">5 Simple Tips To Make Your Android App Successful</h2>\r\n                              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis pharetra varius quam sit amet vulputate. Quisque mauris augue, molestie tincidunt condimentum vitae .</p>\r\n                              <a class=\" pull-right marginBottom10\" href=\"#\">READ MORE</a>\r\n                            </div>\r\n                          </div>\r\n                        </div>\r\n                      </div>\r\n                    </div>\r\n                    <div class=\"col-md-12 single-blog padding-top-20\">\r\n                      <div class=\"well\">\r\n                        <div class=\"media\">\r\n                          <div class=\"col-md-4\">\r\n                            <a class=\"pull-left\" href=\"#\">\r\n                              <div class=\"img-heading pull-left\">\r\n                                <h5>APP DEVELOPMENT</h5>\r\n                              </div>\r\n                              <img class=\"media-object\" src=\"assets/img/services/5-Simple-Tips-To-Make-Your-Android-App-Successful_bigperl.jpg\">\r\n                              </a>\r\n                            </div>\r\n                            <div class=\"col-md-8\">\r\n                              <div class=\"media-body\">\r\n                                <h2 class=\"media-heading\">5 Simple Tips To Make Your Android App Successful</h2>\r\n                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis pharetra varius quam sit amet vulputate. Quisque mauris augue, molestie tincidunt condimentum vitae .</p>\r\n                                <a class=\" pull-right marginBottom10\" href=\"#\">READ MORE</a>\r\n                              </div>\r\n                            </div>\r\n                          </div>\r\n                        </div>\r\n                      </div>\r\n                      <div class=\"col-md-12 single-blog padding-top-20\">\r\n                        <div class=\"well\">\r\n                          <div class=\"media\">\r\n                            <div class=\"col-md-4\">\r\n                              <a class=\"pull-left\" href=\"#\">\r\n                                <div class=\"img-heading pull-left\">\r\n                                  <h5>APP DEVELOPMENT</h5>\r\n                                </div>\r\n                                <img class=\"media-object\" src=\"assets/img/services/5-Simple-Tips-To-Make-Your-Android-App-Successful_bigperl.jpg\">\r\n                                </a>\r\n                              </div>\r\n                              <div class=\"col-md-8\">\r\n                                <div class=\"media-body\">\r\n                                  <h2 class=\"media-heading\">5 Simple Tips To Make Your Android App Successful</h2>\r\n                                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis pharetra varius quam sit amet vulputate. Quisque mauris augue, molestie tincidunt condimentum vitae .</p>\r\n                                  <a class=\" pull-right marginBottom10\" href=\"#\">READ MORE</a>\r\n                                </div>\r\n                              </div>\r\n                            </div>\r\n                          </div>\r\n                        </div>\r\n                        <div class=\"col-md-12 single-blog padding-top-20\">\r\n                          <div class=\"well\">\r\n                            <div class=\"media\">\r\n                              <div class=\"col-md-4\">\r\n                                <a class=\"pull-left\" href=\"#\">\r\n                                  <div class=\"img-heading pull-left\">\r\n                                    <h5>APP DEVELOPMENT</h5>\r\n                                  </div>\r\n                                  <img class=\"media-object\" src=\"assets/img/services/5-Simple-Tips-To-Make-Your-Android-App-Successful_bigperl.jpg\">\r\n                                  </a>\r\n                                </div>\r\n                                <div class=\"col-md-8\">\r\n                                  <div class=\"media-body\">\r\n                                    <h2 class=\"media-heading\">5 Simple Tips To Make Your Android App Successful</h2>\r\n                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis pharetra varius quam sit amet vulputate. Quisque mauris augue, molestie tincidunt condimentum vitae .</p>\r\n                                    <a class=\" pull-right marginBottom10\" href=\"#\">READ MORE</a>\r\n                                  </div>\r\n                                </div>\r\n                              </div>\r\n                            </div>\r\n                          </div>\r\n                        </div>\r\n                        <!-- Sidebar Widgets Column -->\r\n                        <div class=\"col-md-4\">\r\n                          <!-- Search Widget -->\r\n                          <form action=\"#\" method=\"get\">\r\n                            <div class=\"input-group\">\r\n                              <!-- USE TWITTER TYPEAHEAD JSON WITH API TO SEARCH -->\r\n                              <input class=\"form-control\" id=\"system-search\" name=\"q\" placeholder=\"Search for\" required>\r\n                                <span class=\"input-group-btn\" >\r\n                                  <button type=\"submit\" class=\"btn-a btn-a-default\">\r\n                                    <i class=\"glyphicon glyphicon-search\"></i>\r\n                                  </button>\r\n                                </span>\r\n                              </div>\r\n                            </form>\r\n                            <h2 class=\"text-bold mb-11 padding-top-20\">CATEGORIES</h2>\r\n                            <form>\r\n                              <div class=\"form-group\">\r\n                                <select class=\"form-control\" id=\"sel1\">\r\n                                  <option>Select Category</option>\r\n                                  <option>2</option>\r\n                                  <option>3</option>\r\n                                  <option>4</option>\r\n                                </select>\r\n                              </div>\r\n                            </form>\r\n                            <h2 class=\"text-bold mb-11 padding-top-20\">RECENT POSTS</h2>\r\n                            <ul TYPE = \"SQUARE\">\r\n                              <li>\r\n                                <a>\r\n                                  <p>Google Increases Meta Description Length: Here’s What It Means For SEO Experts</p>\r\n                                </a>\r\n                              </li>\r\n                              <li>\r\n                                <a>\r\n                                  <p>5 Simple Tips To Make Your Android App Successful</p>\r\n                                </a>\r\n                              </li>\r\n                              <li>\r\n                                <a>\r\n                                  <p>Google Increases Meta Description Length: Here’s What It Means For SEO Experts</p>\r\n                                </a>\r\n                              </li>\r\n                              <li>\r\n                                <a>\r\n                                  <p>5 Simple Tips To Make Your Android App Successful</p>\r\n                                </a>\r\n                              </li>\r\n                              <li>\r\n                                <a>\r\n                                  <p>Google Increases Meta Description Length: Here’s What It Means For SEO Experts</p>\r\n                                </a>\r\n                              </li>\r\n                              <li>\r\n                                <a>\r\n                                  <p>5 Simple Tips To Make Your Android App Successful</p>\r\n                                </a>\r\n                              </li>\r\n                              <li>\r\n                                <a>\r\n                                  <p>Google Increases Meta Description Length: Here’s What It Means For SEO Experts</p>\r\n                                </a>\r\n                              </li>\r\n                              <li>\r\n                                <a>\r\n                                  <p>5 Simple Tips To Make Your Android App Successful</p>\r\n                                </a>\r\n                              </li>\r\n                            </ul>\r\n                          </div>\r\n                        </div>\r\n                        <nav >\r\n                          <ul class=\"pagination justify-content-center\">\r\n                            <li class=\"page-item disabled\">\r\n                              <a class=\"page-link\" href=\"#\" tabindex=\"-1\">Previous</a>\r\n                            </li>\r\n                            <li class=\"page-item\">\r\n                              <a class=\"page-link\" href=\"#\">1</a>\r\n                            </li>\r\n                            <li class=\"page-item\">\r\n                              <a class=\"page-link\" href=\"#\">2</a>\r\n                            </li>\r\n                            <li class=\"page-item\">\r\n                              <a class=\"page-link\" href=\"#\">3</a>\r\n                            </li>\r\n                            <li class=\"page-item\">\r\n                              <a class=\"page-link\" href=\"#\">Next</a>\r\n                            </li>\r\n                          </ul>\r\n                        </nav>"

/***/ }),

/***/ "./src/app/AboutUs/blogs/blogs.component.ts":
/*!**************************************************!*\
  !*** ./src/app/AboutUs/blogs/blogs.component.ts ***!
  \**************************************************/
/*! exports provided: BlogsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BlogsComponent", function() { return BlogsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var BlogsComponent = /** @class */ (function () {
    function BlogsComponent() {
    }
    BlogsComponent.prototype.ngOnInit = function () {
    };
    BlogsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-blogs',
            template: __webpack_require__(/*! ./blogs.component.html */ "./src/app/AboutUs/blogs/blogs.component.html"),
            styles: [__webpack_require__(/*! ./blogs.component.css */ "./src/app/AboutUs/blogs/blogs.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], BlogsComponent);
    return BlogsComponent;
}());



/***/ }),

/***/ "./src/app/AboutUs/our-clients/our-clients.component.css":
/*!***************************************************************!*\
  !*** ./src/app/AboutUs/our-clients/our-clients.component.css ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/AboutUs/our-clients/our-clients.component.html":
/*!****************************************************************!*\
  !*** ./src/app/AboutUs/our-clients/our-clients.component.html ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n        <!--page title start-->\r\n        <section class=\"page-title ptb-30\">\r\n          <div class=\"container padding-top-40\">\r\n              <div class=\"row\">\r\n                  <div class=\"col-md-12\">\r\n                      <h6 class=\"pagetitles\">Clients</h6>\r\n                      <ol class=\"breadcrumb\">\r\n                          <li><a href=\"#\">Home</a></li>\r\n                          <li><a href=\"#\">our clients</a></li>\r\n                      </ol>\r\n                  </div>\r\n              </div>\r\n          </div>\r\n      </section>\r\n      <!--page title end-->\r\n\r\n      <section class=\"section-padding\">\r\n          <div class=\"container\">\r\n\r\n            <div class=\"text-center mb-80\">\r\n                <h2 class=\"section-title text-uppercase\">Awesome clients</h2>\r\n                <p class=\"section-sub\">Quisque non erat mi. Etiam congue et augue sed tempus. Aenean sed ipsum luctus, scelerisque ipsum nec, iaculis justo. Sed at vestibulum purus, sit amet viverra diam. Nulla ac nisi rhoncus,</p>\r\n            </div>\r\n            <div class=\" gutter\">\r\n              <div class=\"row\">\r\n                  <div class=\"owl-carousel owl-theme\" id=\"customers-carousel3\">\r\n                      <div class=\"item active\">\r\n                          <div class=\"border-box\">\r\n                              <a href=\"#\">\r\n                                  <img src=\"assets/img/client-logo/our_clients_akg_bigperl_2.png\" alt=\"our_clients_akg_bigperl\">\r\n                              </a>\r\n                          </div><br>\r\n                          <div class=\"border-box\">\r\n                              <a href=\"#\">\r\n                                  <img src=\"assets/img/client-logo/our_clients_ang_bigperl_2.png\" alt=\"our_clients_ang_bigperl\">\r\n                              </a>\r\n                          </div><br>\r\n                          <div class=\"border-box\">\r\n                              <a href=\"#\">\r\n                                  <img src=\"assets/img/client-logo/our_clients_archi_bigperl_2.png\" alt=\"our_clients_archi_bigperl\">\r\n                              </a>\r\n                          </div>\r\n                      </div>\r\n                      <div class=\"item\">\r\n                          <div class=\"border-box\">\r\n                              <a href=\"#\">\r\n                                  <img src=\"assets/img/client-logo/our_clients_arfma_bigperl_2.png\" alt=\"our_clients_arfma_bigperl\">\r\n                              </a>\r\n                          </div><br>\r\n                          <div class=\"border-box\">\r\n                              <a href=\"#\">\r\n                                  <img src=\"assets/img/client-logo/our_clients_barma_bigperl_2.jpg\" alt=\"our_clients_barma_bigperl\">\r\n                              </a>\r\n                          </div><br>\r\n                          <div class=\"border-box\">\r\n                              <a href=\"#\">\r\n                                  <img src=\"assets/img/client-logo/our_clients_ccat_bigperl_2.png\" alt=\"our_clients_ccat_bigperl\">\r\n                              </a>\r\n                          </div>\r\n                      </div>\r\n                      <div class=\"item\">\r\n                          <div class=\"border-box\">\r\n                              <a href=\"#\">\r\n                                  <img src=\"assets/img/client-logo/our_clients_fashion_bigperl_2.png\" alt=\"our_clients_fashion_bigperl\">\r\n                              </a>\r\n                          </div><br>\r\n                          <div class=\"border-box\">\r\n                              <a href=\"#\">\r\n                                  <img src=\"assets/img/client-logo/our_clients_gp_bigperl_2.png\" alt=\"our_clients_gp_bigperl\">\r\n                              </a>\r\n                          </div><br>\r\n                          <div class=\"border-box\">\r\n                              <a href=\"#\">\r\n                                  <img src=\"assets/img/client-logo/our_clients_he_bigperl_2.png\" alt=\"our_clients_he_bigperl\">\r\n                              </a>\r\n                          </div>\r\n                      </div>\r\n                      <div class=\"item\">\r\n                          <div class=\"border-box\">\r\n                              <a href=\"#\">\r\n                                  <img src=\"assets/img/client-logo/our_clients_jms_bigperl_2.png\" alt=\"our_clients_jms_bigperl\">\r\n                              </a>\r\n                          </div><br>\r\n                          <div class=\"border-box\">\r\n                              <a href=\"#\">\r\n                                  <img src=\"assets/img/client-logo/our_clients_kuch_bigperl_2.png\" alt=\"our_clients_kuch_bigperl\">\r\n                              </a>\r\n                          </div><br>\r\n                          <div class=\"border-box\">\r\n                              <a href=\"#\">\r\n                                  <img src=\"assets/img/client-logo/our_clients_kvrc_bigperl_2.png\" alt=\"our_clients_kvrc_bigperl\">\r\n                              </a>\r\n                          </div>\r\n                      </div>\r\n                      <div class=\"item\">\r\n                          <div class=\"border-box\">\r\n                              <a href=\"#\">\r\n                                  <img src=\"assets/img/client-logo/our_clients_lg_bigperl.png\" alt=\"our_clients_lg_bigperl\">\r\n                              </a>\r\n                          </div><br>\r\n                          <div class=\"border-box\">\r\n                              <a href=\"#\">\r\n                                  <img src=\"assets/img/client-logo/our_clients_mp_bigperl.png\" alt=\"our_clients_mp_bigperl\">\r\n                              </a>\r\n                          </div><br>\r\n                          <div class=\"border-box\">\r\n                              <a href=\"#\">\r\n                                  <img src=\"assets/img/client-logo/our_clients_mut_bigperl.png\" alt=\"our_clients_mut_bigperl\">\r\n                              </a>\r\n                          </div>\r\n                      </div>\r\n                      <div class=\"item\">\r\n                          <div class=\"border-box\">\r\n                              <a href=\"#\">\r\n                                  <img src=\"assets/img/client-logo/our_clients_omf_bigperl.png\" alt=\"our_clients_omf_bigperl\">\r\n                              </a>\r\n                          </div><br>\r\n                          <div class=\"border-box\">\r\n                              <a href=\"#\">\r\n                                  <img src=\"assets/img/client-logo/our_clients_pg_bigperl.png\" alt=\"our_clients_pg_bigperl\">\r\n                              </a>\r\n                          </div><br>\r\n                          <div class=\"border-box\">\r\n                              <a href=\"#\">\r\n                                  <img src=\"assets/img/client-logo/our_clients_pre_bigperl.png\" alt=\"our_clients_pre_bigperl\">\r\n                              </a>\r\n                          </div>\r\n                      </div>\r\n                      <div class=\"item\">\r\n                          <div class=\"border-box\">\r\n                              <a href=\"#\">\r\n                                  <img src=\"assets/img/client-logo/our_clients_q8_bigperl.jpg\" alt=\"our_clients_q8_bigperl\">\r\n                              </a>\r\n                          </div><br>\r\n                          <div class=\"border-box\">\r\n                              <a href=\"#\">\r\n                                  <img src=\"assets/img/client-logo/our_clients_rb_bigperl.png\" alt=\"our_clients_rb_bigperl\">\r\n                              </a>\r\n                          </div><br>\r\n                          <div class=\"border-box\">\r\n                              <a href=\"#\">\r\n                                  <img src=\"assets/img/client-logo/our_clients_rbcaterers_bigperl.png\" alt=\"our_clients_rbcaterers_bigperl\">\r\n                              </a>\r\n                          </div>\r\n                      </div>\r\n                      <div class=\"item\">\r\n                          <div class=\"border-box\">\r\n                              <a href=\"#\">\r\n                                  <img src=\"assets/img/client-logo/our_clients_rel_bigperl.png\" alt=\"our_clients_rel_bigperl\">\r\n                              </a>\r\n                          </div><br>\r\n                          <div class=\"border-box\">\r\n                              <a href=\"#\">\r\n                                  <img src=\"assets/img/client-logo/our_clients_rkcatring_bigperl.png\" alt=\"our_clients_rkcatring_bigperl\">\r\n                              </a>\r\n                          </div><br>\r\n                          <div class=\"border-box\">\r\n                              <a href=\"#\">\r\n                                  <img src=\"assets/img/client-logo/our_clients_sai_bigperl.png\" alt=\"our_clients_sai_bigperl\">\r\n                              </a>\r\n                          </div>\r\n                      </div>\r\n                      <div class=\"item\">\r\n                          <div class=\"border-box\">\r\n                              <a href=\"#\">\r\n                                  <img src=\"assets/img/client-logo/our_clients_saiele_bigperl.png\" alt=\"our_clients_saiele_bigperl\">\r\n                              </a>\r\n                          </div><br>\r\n                          <div class=\"border-box\">\r\n                              <a href=\"#\">\r\n                                  <img src=\"assets/img/client-logo/our_clients_sig_bigperl.png\" alt=\"our_clients_sig_bigperl\">\r\n                              </a>\r\n                          </div><br>\r\n                          <div class=\"border-box\">\r\n                              <a href=\"#\">\r\n                                  <img src=\"assets/img/client-logo/our_clients_smv_bigperl.png\" alt=\"our_clients_smv_bigperl\">\r\n                              </a>\r\n                          </div>\r\n                      </div>\r\n                      <div class=\"item\">\r\n                          <div class=\"border-box\">\r\n                              <a href=\"#\">\r\n                                  <img src=\"assets/img/client-logo/our_clients_svassa_bigperl.jpg\" alt=\"our_clients_svassa_bigperl\">\r\n                              </a>\r\n                          </div><br>\r\n                          <div class=\"border-box\">\r\n                              <a href=\"#\">\r\n                                  <img src=\"assets/img/client-logo/our_clients_tp_bigperl.png\" alt=\"our_clients_tp_bigperl\">\r\n                              </a>\r\n                          </div><br>\r\n                          <div class=\"border-box\">\r\n                              <a href=\"#\">\r\n                                  <img src=\"assets/img/client-logo/our_clients_vb_bigperl.png\" alt=\"our_clients_vb_bigperl\">\r\n                              </a>\r\n                          </div>\r\n                      </div>\r\n                      <div class=\"item\">\r\n                          <div class=\"border-box\">\r\n                              <a href=\"#\">\r\n                                  <img src=\"assets/img/client-logo/our_clients_vstrack_bigperl.png\" alt=\"our_clients_vstrack_bigperl\">\r\n                              </a>\r\n                          </div><br>\r\n                          <div class=\"border-box\">\r\n                              <a href=\"#\">\r\n                                  <img src=\"assets/img/client-logo/our_clients_yecons_bigperl.png\" alt=\"our_clients_yecons_bigperl\">\r\n                              </a>\r\n                          </div><br>\r\n                          <div class=\"border-box\">\r\n                              <a href=\"#\">\r\n                                  <img src=\"assets/img/client-logo/our_clients_del_bigperl.png\" alt=\"our_clients_envato_bigperl\">\r\n                              </a>\r\n                          </div>\r\n                      </div>\r\n                  </div>\r\n              </div><!-- /.row -->\r\n            </div><!-- /.clients-grid -->\r\n          </div><!-- /.container -->\r\n      </section><br>\r\n"

/***/ }),

/***/ "./src/app/AboutUs/our-clients/our-clients.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/AboutUs/our-clients/our-clients.component.ts ***!
  \**************************************************************/
/*! exports provided: OurClientsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OurClientsComponent", function() { return OurClientsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var OurClientsComponent = /** @class */ (function () {
    function OurClientsComponent() {
    }
    OurClientsComponent.prototype.ngOnInit = function () {
        $('#customers-carousel3').owlCarousel({ responsive: { 0: { items: 1 }, 600: { items: 3 }, 1000: { items: 4 } }, loop: true, margin: 30, nav: true, smartSpeed: 2000, navText: ["<i class='fa fa-chevron-left position'></i>", "<i class='fa fa-chevron-right position1'></i>"], autoplay: true, autoplayTimeout: 3000 });
    };
    OurClientsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-our-clients',
            template: __webpack_require__(/*! ./our-clients.component.html */ "./src/app/AboutUs/our-clients/our-clients.component.html"),
            styles: [__webpack_require__(/*! ./our-clients.component.css */ "./src/app/AboutUs/our-clients/our-clients.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], OurClientsComponent);
    return OurClientsComponent;
}());



/***/ }),

/***/ "./src/app/AboutUs/sucess-stories/sucess-stories.component.css":
/*!*********************************************************************!*\
  !*** ./src/app/AboutUs/sucess-stories/sucess-stories.component.css ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/AboutUs/sucess-stories/sucess-stories.component.html":
/*!**********************************************************************!*\
  !*** ./src/app/AboutUs/sucess-stories/sucess-stories.component.html ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--page title start-->\r\n<section class=\"page-title ptb-30\">\r\n  <div class=\"container padding-top-40\">\r\n      <div class=\"row\">\r\n          <div class=\"col-md-12\">\r\n              <h6 style=\"float: right;margin-bottom: -5px;\">About Us</h6>\r\n              <ol class=\"breadcrumb\">\r\n                  <li><a href=\"#\">Home</a></li>\r\n                  <li class=\"active\">about us</li>\r\n              </ol>\r\n          </div>\r\n      </div>\r\n  </div>\r\n</section>\r\n<!--page title end-->\r\n  <div class=\"container\">\r\n    <div class=\"row\">\r\n      <div class=\"col-md-12\">\r\n\r\n          <div class=\"border-bottom-tab\">\r\n\r\n            <!-- Nav tabs -->\r\n            <ul class=\"nav nav-tabs\" >\r\n              <li  class=\"active\"><a href=\"#tab-1\"  class=\"waves-effect waves-dark\" data-toggle=\"tab\">About us</a></li>\r\n              <li ><a href=\"#tab-2\" class=\"waves-effect waves-dark\" data-toggle=\"tab\">What We Do</a></li>\r\n              <li ><a href=\"#tab-3\" class=\"waves-effect waves-dark\" data-toggle=\"tab\">Our Mission</a></li>\r\n              <li ><a href=\"#tab-4\" class=\"waves-effect waves-dark\" data-toggle=\"tab\">Setps</a></li>\r\n            </ul>\r\n\r\n            <!-- Tab panes -->\r\n            <div class=\"panel-body\">\r\n              <div class=\"tab-content\">\r\n                <div  class=\"tab-pane fade in active\" id=\"tab-1\">\r\n                    <img class=\"alignleft\" src=\"assets/img/about_us_working_man_bigperl.png\" alt=\"about_us_working_man_bigperl\">\r\n                    <p>Duis senectus mus condimentum nunc ac habitasse duis consectetur a arcu a accumsan cras et metus ultricies justo cum a bibendum. <a href=\"#\">Egestas vestibulum blandit sem vestibulum curabitur</a> a vel aliquet gravida ullamcorper amet dictumst vestibulum a elementum proin id volutpat magna parturient. Id ac dui libero a ullamcorper euismod himenaeos a nam condimentum a adipiscing dapibus lobortis iaculis morbi.</p>\r\n\r\n                    <p>Himenaeos a vestibulum morbi. <a href=\"#\">Ullamcorper cras scelerisque</a> taciti lorem metus feugiat est lacinia facilisis id nam leo condimentum praesent id diam. Vestibulum amet porta odio elementum convallis parturient tempor tortor tempus a mi ad parturient ad nulla mus amet in penatibus nascetur at vulputate euismod a est tristique scelerisque. Aliquet facilisis nisl vel vestibulum dignissim gravida ullamcorper hac quisque ad at nisl egestas adipiscing vel blandit.</p>\r\n                </div>\r\n                <div  class=\"tab-pane fade\" id=\"tab-2\">\r\n                    <img class=\"alignleft\" src=\"assets/img/about_us_busy_man_bigperl.png\" alt=\"about_us_busy_man_bigperl\">\r\n                    <p>Duis senectus mus condimentum nunc ac habitasse duis consectetur a arcu a accumsan cras et metus ultricies justo cum a bibendum. <a href=\"#\">Egestas vestibulum blandit sem vestibulum curabitur</a> a vel aliquet gravida ullamcorper amet dictumst vestibulum a elementum proin id volutpat magna parturient. Id ac dui libero a ullamcorper euismod himenaeos a nam condimentum a adipiscing dapibus lobortis iaculis morbi.</p>\r\n\r\n                    <p>Himenaeos a vestibulum morbi. <a href=\"#\">Ullamcorper cras scelerisque</a> taciti lorem metus feugiat est lacinia facilisis id nam leo condimentum praesent id diam. Vestibulum amet porta odio elementum convallis parturient tempor tortor tempus a mi ad parturient ad nulla mus amet in penatibus nascetur at vulputate euismod a est tristique scelerisque. Aliquet facilisis nisl vel vestibulum dignissim gravida ullamcorper hac quisque ad at nisl egestas adipiscing vel blandit.</p>\r\n                </div>\r\n                <div  class=\"tab-pane fade\" id=\"tab-3\">\r\n                    <img class=\"alignleft\" src=\"assets/img/about_us_mission_bigperl.png\" alt=\"about_us_mission_bigperl\">\r\n                    <p>Duis senectus mus condimentum nunc ac habitasse duis consectetur a arcu a accumsan cras et metus ultricies justo cum a bibendum. <a href=\"#\">Egestas vestibulum blandit sem vestibulum curabitur</a> a vel aliquet gravida ullamcorper amet dictumst vestibulum a elementum proin id volutpat magna parturient. Id ac dui libero a ullamcorper euismod himenaeos a nam condimentum a adipiscing dapibus lobortis iaculis morbi.</p>\r\n\r\n                    <p>Himenaeos a vestibulum morbi. <a href=\"#\">Ullamcorper cras scelerisque</a> taciti lorem metus feugiat est lacinia facilisis id nam leo condimentum praesent id diam. Vestibulum amet porta odio elementum convallis parturient tempor tortor tempus a mi ad parturient ad nulla mus amet in penatibus nascetur at vulputate euismod a est tristique scelerisque. Aliquet facilisis nisl vel vestibulum dignissim gravida ullamcorper hac quisque ad at nisl egestas adipiscing vel blandit.</p>\r\n                </div>\r\n                <div  class=\"tab-pane fade\" id=\"tab-4\">\r\n                    <img class=\"alignleft\" src=\"assets/img/about_us_business_bigperl.png\" alt=\"about_us_business_bigperl\">\r\n                    <p>Duis senectus mus condimentum nunc ac habitasse duis consectetur a arcu a accumsan cras et metus ultricies justo cum a bibendum. <a href=\"#\">Egestas vestibulum blandit sem vestibulum curabitur</a> a vel aliquet gravida ullamcorper amet dictumst vestibulum a elementum proin id volutpat magna parturient. Id ac dui libero a ullamcorper euismod himenaeos a nam condimentum a adipiscing dapibus lobortis iaculis morbi.</p>\r\n\r\n                    <p>Himenaeos a vestibulum morbi. <a href=\"#\">Ullamcorper cras scelerisque</a> taciti lorem metus feugiat est lacinia facilisis id nam leo condimentum praesent id diam. Vestibulum amet porta odio elementum convallis parturient tempor tortor tempus a mi ad parturient ad nulla mus amet in penatibus nascetur at vulputate euismod a est tristique scelerisque. Aliquet facilisis nisl vel vestibulum dignissim gravida ullamcorper hac quisque ad at nisl egestas adipiscing vel blandit.</p>\r\n                </div>\r\n              </div>\r\n            </div>\r\n\r\n          </div><!-- /.border-bottom-tab -->\r\n\r\n      </div><!-- /.col-md-12 -->\r\n    </div><!-- /.row -->\r\n  </div><!-- /.container -->\r\n\r\n\r\n<hr>\r\n\r\n\r\n<section class=\"section-padding banner-6 bg-fixed parallax-bg overlay light-9\" data-stellar-background-ratio=\"0.5\">\r\n  <div class=\"container\">\r\n\r\n    <div class=\"text-center mb-80\">\r\n        <h2 class=\"section-title text-uppercase\">Work together</h2>\r\n        <p class=\"section-sub\">Quisque non erat mi. Etiam congue et augue sed tempus. Aenean sed ipsum luctus, scelerisque ipsum nec, iaculis justo. Sed at vestibulum purus, sit amet viverra diam nulla ac nisi rhoncus.</p>\r\n    </div>\r\n\r\n    <div class=\"featured-carousel brand-dot\">\r\n        <div class=\"featured-item border-box radius-4 hover brand-hover\">\r\n            <div class=\"icon mb-30\">\r\n                <i class=\"material-icons brand-icon\">security</i>\r\n            </div>\r\n            <div class=\"desc\">\r\n                <h2>We are creative</h2>\r\n                <p>Porttitor communicate pandemic data rather than enabled niche pandemic data rather markets neque pulvinar vitae.</p>\r\n            </div>\r\n        </div><!-- /.featured-item -->\r\n\r\n        <div class=\"featured-item border-box radius-4 hover brand-hover\">\r\n            <div class=\"icon mb-30\">\r\n                <i class=\"material-icons brand-icon\">account_box</i>\r\n            </div>\r\n            <div class=\"desc\">\r\n                <h2>We are awesome</h2>\r\n                <p>Porttitor communicate pandemic data rather than enabled niche pandemic data rather markets neque pulvinar vitae.</p>\r\n            </div>\r\n        </div><!-- /.featured-item -->\r\n\r\n        <div class=\"featured-item border-box radius-4 hover brand-hover\">\r\n            <div class=\"icon mb-30\">\r\n                <i class=\"material-icons brand-icon\">question_answer</i>\r\n            </div>\r\n            <div class=\"desc\">\r\n                <h2>We are Taltented</h2>\r\n                <p>Porttitor communicate pandemic data rather than enabled niche pandemic data rather markets neque pulvinar vitae.</p>\r\n            </div>\r\n        </div><!-- /.featured-item -->\r\n\r\n        <div class=\"featured-item border-box radius-4 hover brand-hover\">\r\n            <div class=\"icon mb-30\">\r\n                <i class=\"material-icons brand-icon\">pets</i>\r\n            </div>\r\n            <div class=\"desc\">\r\n                <h2>We are secured</h2>\r\n                <p>Porttitor communicate pandemic data rather than enabled niche pandemic data rather markets neque pulvinar vitae.</p>\r\n            </div>\r\n        </div><!-- /.featured-item -->\r\n\r\n        <div class=\"featured-item border-box radius-4 hover brand-hover\">\r\n            <div class=\"icon mb-30\">\r\n                <i class=\"material-icons brand-icon\">shopping_basket</i>\r\n            </div>\r\n            <div class=\"desc\">\r\n                <h2>We are Pationate</h2>\r\n                <p>Porttitor communicate pandemic data rather than enabled niche pandemic data rather markets neque pulvinar vitae.</p>\r\n            </div>\r\n        </div><!-- /.featured-item -->\r\n\r\n        <div class=\"featured-item border-box radius-4 hover brand-hover\">\r\n            <div class=\"icon mb-30\">\r\n                <i class=\"material-icons brand-icon\">thumb_up</i>\r\n            </div>\r\n            <div class=\"desc\">\r\n                <h2>We are Developer</h2>\r\n                <p>Porttitor communicate pandemic data rather than enabled niche pandemic data rather markets neque pulvinar vitae.</p>\r\n            </div>\r\n        </div><!-- /.featured-item -->\r\n\r\n        <div class=\"featured-item border-box radius-4 hover brand-hover\">\r\n            <div class=\"icon mb-30\">\r\n                <i class=\"material-icons brand-icon\">movie</i>\r\n            </div>\r\n            <div class=\"desc\">\r\n                <h2>We are Designer</h2>\r\n                <p>Porttitor communicate pandemic data rather than enabled niche pandemic data rather markets neque pulvinar vitae.</p>\r\n            </div>\r\n        </div><!-- /.featured-item -->\r\n\r\n        <div class=\"featured-item border-box radius-4 hover brand-hover\">\r\n            <div class=\"icon mb-30\">\r\n                <i class=\"material-icons brand-icon\">email</i>\r\n            </div>\r\n            <div class=\"desc\">\r\n                <h2>We are Expert</h2>\r\n                <p>Porttitor communicate pandemic data rather than enabled niche pandemic data rather markets neque pulvinar vitae.</p>\r\n            </div>\r\n        </div><!-- /.featured-item -->\r\n    </div>\r\n\r\n  </div><!-- /.container -->\r\n</section>\r\n\r\n\r\n<hr>\r\n\r\n\r\n<section class=\"section-padding\">\r\n  <div class=\"container\">\r\n\r\n      <div class=\"text-center mb-80\">\r\n          <h2 class=\"section-title text-uppercase\">Awesome Team</h2>\r\n          <p class=\"section-sub\">Quisque non erat mi. Etiam congue et augue sed tempus. Aenean sed ipsum luctus, scelerisque ipsum nec, iaculis justo. Sed at vestibulum purus, sit amet viverra diam nulla ac nisi rhoncus.</p>\r\n      </div>\r\n\r\n    <div class=\"team-tab\">\r\n        <!-- Nav tabs -->\r\n        <ul class=\"nav nav-tabs nav-justified\">\r\n            <li class=\"active\">\r\n                <a href=\"#team_1_bigperl\" data-toggle=\"tab\">\r\n                  <img src=\"assets/img/team/about_us_team_1_bigperl.jpg\" class=\"img-responsive\" alt=\"about_us_team_1_bigperl\">\r\n                </a>\r\n            </li>\r\n\r\n            <li>\r\n                <a href=\"#team_2_bigperl\" data-toggle=\"tab\">\r\n                    <img src=\"assets/img/team/about_us_team_2_bigperl.jpg\" class=\"img-responsive\" alt=\"about_us_team_2_bigperl\">\r\n                </a>\r\n            </li>\r\n\r\n            <li>\r\n                <a href=\"#team_3_bigperl\" data-toggle=\"tab\">\r\n                    <img src=\"assets/img/team/about_us_team_3_bigperl.jpg\" class=\"img-responsive\" alt=\"about_us_team_3_bigperl\">\r\n                </a>\r\n            </li>\r\n\r\n            <li>\r\n                <a href=\"#team_4_bigperl\" data-toggle=\"tab\">\r\n                    <img src=\"assets/img/team/about_us_team_4_bigperl.jpg\" class=\"img-responsive\" alt=\"about_us_team_4_bigperl\">\r\n                </a>\r\n            </li>\r\n        </ul>\r\n\r\n        <!-- Tab panes -->\r\n        <div class=\"panel-body\">\r\n          <div class=\"tab-content\">\r\n            <div  class=\"tab-pane fade in active\" id=\"team_1_bigperl\">\r\n                <div class=\"row\">\r\n                    <div class=\"col-md-4 col-sm-3\">\r\n                        <figure class=\"team-img text-center\">\r\n                            <img src=\"assets/img/team/about_us_team_large_1_bigperl.png\" class=\"img-responsive\" alt=\"about_us_team_large_1_bigperl\">\r\n\r\n                            <ul class=\"team-social-links list-inline\">\r\n                                <li><a href=\"#\"><i class=\"fa fa-facebook\"></i></a></li>\r\n                                <li><a href=\"#\"><i class=\"fa fa-twitter\"></i></a></li>\r\n                                <li><a href=\"#\"><i class=\"fa fa-google-plus\"></i></a></li>\r\n                                <li><a href=\"#\"><i class=\"fa fa-linkedin\"></i></a></li>\r\n                                <li><a href=\"#\"><i class=\"fa fa-dribbble\"></i></a></li>\r\n                                <li><a href=\"#\"><i class=\"fa fa-envelope-o\"></i></a></li>\r\n                            </ul>\r\n                        </figure>\r\n                    </div><!-- /.col-md-4 -->\r\n\r\n                    <div class=\"col-md-8 col-sm-9\">\r\n\r\n                        <div class=\"team-intro\">\r\n                            <h3>Elita Chow <small>Product Designer</small></h3>\r\n                            <p>A id a torquent tortor at lacus et donec platea eu scelerisque maecenas ac eros a adipiscing id lobortis cum lacus erat. Parturient eleifend adipiscing ultrices a cursus est feugiat porta a at condimentum fames adipiscing odio in nisi venenatis suspendisse suspendisse parturient. Leo congue sociosqu maecenas ligula eu penatibus at suscipit mus scelerisque.</p>\r\n                        </div>\r\n\r\n\r\n                        <div class=\"team-skill\">\r\n                            <div class=\"row\">\r\n\r\n                              <div class=\"col-sm-6\">\r\n\r\n                                  <div class=\"progress-section\">\r\n                                      <span class=\"progress-title\">Web Design</span>\r\n                                      <div class=\"progress\">\r\n                                          <div class=\"progress-bar brand-bg six-sec-ease-in-out\" >\r\n                                              <span>90%</span>\r\n                                          </div>\r\n                                      </div><!-- /.progress -->\r\n                                  </div> <!-- progress-section end -->\r\n\r\n\r\n                                  <div class=\"progress-section\">\r\n                                      <span class=\"progress-title\">Mobile Interface</span>\r\n                                      <div class=\"progress\">\r\n                                          <div class=\"progress-bar brand-bg six-sec-ease-in-out\"  >\r\n                                              <span>86%</span>\r\n                                          </div>\r\n                                      </div>\r\n                                  </div> <!-- progress-section end -->\r\n\r\n                                  <div class=\"progress-section\">\r\n                                      <span class=\"progress-title\">Mobile Interface</span>\r\n                                      <div class=\"progress\">\r\n                                          <div class=\"progress-bar brand-bg six-sec-ease-in-out\">\r\n                                              <span>86%</span>\r\n                                          </div>\r\n                                      </div>\r\n                                  </div> <!-- progress-section end -->\r\n                              </div><!-- /.col-md-6 -->\r\n\r\n                              <div class=\"col-sm-6\">\r\n                                  <div class=\"progress-section\">\r\n                                      <span class=\"progress-title\">Web Design</span>\r\n                                      <div class=\"progress\">\r\n                                          <div class=\"progress-bar brand-bg six-sec-ease-in-out\" >\r\n                                              <span>90%</span>\r\n                                          </div>\r\n                                      </div>\r\n                                  </div> <!-- progress-section end -->\r\n\r\n                                  <div class=\"progress-section\">\r\n                                      <span class=\"progress-title\">Mobile Interface</span>\r\n                                      <div class=\"progress\">\r\n                                          <div class=\"progress-bar brand-bg six-sec-ease-in-out\" >\r\n                                              <span>86%</span>\r\n                                          </div>\r\n                                      </div>\r\n                                  </div> <!-- progress-section end -->\r\n\r\n                                  <div class=\"progress-section\">\r\n                                      <span class=\"progress-title\">Mobile Interface</span>\r\n                                      <div class=\"progress\">\r\n                                          <div class=\"progress-bar brand-bg six-sec-ease-in-out\">\r\n                                              <span>86%</span>\r\n                                          </div>\r\n                                      </div>\r\n                                  </div> <!-- progress-section end -->\r\n                              </div><!-- /.col-md-6 -->\r\n\r\n                            </div><!-- /.row -->\r\n\r\n                        </div> <!--team-skill end -->\r\n                    </div> <!-- col-md-8 -->\r\n                </div> <!-- row -->\r\n            </div> <!--team-1 end-->\r\n\r\n              <div  class=\"tab-pane fade\" id=\"team_2_bigperl\">\r\n                <div class=\"row\">\r\n                    <div class=\"col-md-4 col-sm-3\">\r\n                        <figure class=\"team-img text-center\">\r\n                            <img src=\"assets/img/team/about_us_team_large_2_bigperl.png\" class=\"img-responsive\" alt=\"about_us_team_large_2_bigperl\">\r\n\r\n                            <ul class=\"team-social-links list-inline\">\r\n                                <li><a href=\"#\"><i class=\"fa fa-facebook\"></i></a></li>\r\n                                <li><a href=\"#\"><i class=\"fa fa-twitter\"></i></a></li>\r\n                                <li><a href=\"#\"><i class=\"fa fa-google-plus\"></i></a></li>\r\n                                <li><a href=\"#\"><i class=\"fa fa-linkedin\"></i></a></li>\r\n                                <li><a href=\"#\"><i class=\"fa fa-dribbble\"></i></a></li>\r\n                                <li><a href=\"#\"><i class=\"fa fa-envelope-o\"></i></a></li>\r\n                            </ul>\r\n                        </figure>\r\n                    </div><!-- /.col-md-4 -->\r\n\r\n                    <div class=\"col-md-8 col-sm-9\">\r\n\r\n                        <div class=\"team-intro\">\r\n                            <h3>Jhon Doe <small>Product Developer</small></h3>\r\n                            <p>A id a torquent tortor at lacus et donec platea eu scelerisque maecenas ac eros a adipiscing id lobortis cum lacus erat. Parturient eleifend adipiscing ultrices a cursus est feugiat porta a at condimentum fames adipiscing odio in nisi venenatis suspendisse suspendisse parturient. Leo congue sociosqu maecenas ligula eu penatibus at suscipit mus scelerisque.</p>\r\n                        </div>\r\n\r\n\r\n                        <div class=\"team-skill\">\r\n                            <div class=\"row\">\r\n\r\n                              <div class=\"col-sm-6\">\r\n\r\n                                  <div class=\"progress-section\">\r\n                                      <span class=\"progress-title\">Web Design</span>\r\n                                      <div class=\"progress\">\r\n                                          <div class=\"progress-bar brand-bg six-sec-ease-in-out\">\r\n                                              <span>90%</span>\r\n                                          </div>\r\n                                      </div><!-- /.progress -->\r\n                                  </div> <!-- progress-section end -->\r\n\r\n\r\n                                  <div class=\"progress-section\">\r\n                                      <span class=\"progress-title\">Mobile Interface</span>\r\n                                      <div class=\"progress\">\r\n                                          <div class=\"progress-bar brand-bg six-sec-ease-in-out\" >\r\n                                              <span>86%</span>\r\n                                          </div>\r\n                                      </div>\r\n                                  </div> <!-- progress-section end -->\r\n\r\n                                  <div class=\"progress-section\">\r\n                                      <span class=\"progress-title\">Mobile Interface</span>\r\n                                      <div class=\"progress\">\r\n                                          <div class=\"progress-bar brand-bg six-sec-ease-in-out\">\r\n                                              <span>86%</span>\r\n                                          </div>\r\n                                      </div>\r\n                                  </div> <!-- progress-section end -->\r\n                              </div><!-- /.col-md-6 -->\r\n\r\n                              <div class=\"col-sm-6\">\r\n                                  <div class=\"progress-section\">\r\n                                      <span class=\"progress-title\">Web Design</span>\r\n                                      <div class=\"progress\">\r\n                                          <div class=\"progress-bar brand-bg six-sec-ease-in-out\">\r\n                                              <span>90%</span>\r\n                                          </div>\r\n                                      </div>\r\n                                  </div> <!-- progress-section end -->\r\n\r\n                                  <div class=\"progress-section\">\r\n                                      <span class=\"progress-title\">Mobile Interface</span>\r\n                                      <div class=\"progress\">\r\n                                          <div class=\"progress-bar brand-bg six-sec-ease-in-out\">\r\n                                              <span>86%</span>\r\n                                          </div>\r\n                                      </div>\r\n                                  </div> <!-- progress-section end -->\r\n\r\n                                  <div class=\"progress-section\">\r\n                                      <span class=\"progress-title\">Mobile Interface</span>\r\n                                      <div class=\"progress\">\r\n                                          <div class=\"progress-bar brand-bg six-sec-ease-in-out\">\r\n                                              <span>86%</span>\r\n                                          </div>\r\n                                      </div>\r\n                                  </div> <!-- progress-section end -->\r\n                              </div><!-- /.col-md-6 -->\r\n\r\n                            </div><!-- /.row -->\r\n\r\n                        </div> <!--team-skill end -->\r\n                    </div> <!-- col-md-8 -->\r\n                </div> <!-- row -->\r\n              </div> <!--team-2 end -->\r\n\r\n              <div class=\"tab-pane fade\" id=\"team_3_bigperl\">\r\n                <div class=\"row\">\r\n                    <div class=\"col-md-4 col-sm-3\">\r\n                        <figure class=\"team-img text-center\">\r\n                            <img src=\"assets/img/team/about_us_team_large_3_bigperl.png\" class=\"img-responsive\" alt=\"about_us_team_large_3_bigperl\">\r\n\r\n                            <ul class=\"team-social-links list-inline\">\r\n                                <li><a href=\"#\"><i class=\"fa fa-facebook\"></i></a></li>\r\n                                <li><a href=\"#\"><i class=\"fa fa-twitter\"></i></a></li>\r\n                                <li><a href=\"#\"><i class=\"fa fa-google-plus\"></i></a></li>\r\n                                <li><a href=\"#\"><i class=\"fa fa-linkedin\"></i></a></li>\r\n                                <li><a href=\"#\"><i class=\"fa fa-dribbble\"></i></a></li>\r\n                                <li><a href=\"#\"><i class=\"fa fa-envelope-o\"></i></a></li>\r\n                            </ul>\r\n                        </figure>\r\n                    </div><!-- /.col-md-4 -->\r\n\r\n                    <div class=\"col-md-8 col-sm-9\">\r\n\r\n                        <div class=\"team-intro\">\r\n                            <h3>Tomas Udoya <small>Product Designer</small></h3>\r\n                            <p>A id a torquent tortor at lacus et donec platea eu scelerisque maecenas ac eros a adipiscing id lobortis cum lacus erat. Parturient eleifend adipiscing ultrices a cursus est feugiat porta a at condimentum fames adipiscing odio in nisi venenatis suspendisse suspendisse parturient. Leo congue sociosqu maecenas ligula eu penatibus at suscipit mus scelerisque.</p>\r\n                        </div>\r\n\r\n\r\n                        <div class=\"team-skill\">\r\n                            <div class=\"row\">\r\n\r\n                              <div class=\"col-sm-6\">\r\n\r\n                                  <div class=\"progress-section\">\r\n                                      <span class=\"progress-title\">Web Design</span>\r\n                                      <div class=\"progress\">\r\n                                          <div class=\"progress-bar brand-bg six-sec-ease-in-out\" >\r\n                                              <span>90%</span>\r\n                                          </div>\r\n                                      </div><!-- /.progress -->\r\n                                  </div> <!-- progress-section end -->\r\n\r\n\r\n                                  <div class=\"progress-section\">\r\n                                      <span class=\"progress-title\">Mobile Interface</span>\r\n                                      <div class=\"progress\">\r\n                                          <div class=\"progress-bar brand-bg six-sec-ease-in-out\" >\r\n                                              <span>86%</span>\r\n                                          </div>\r\n                                      </div>\r\n                                  </div> <!-- progress-section end -->\r\n\r\n                                  <div class=\"progress-section\">\r\n                                      <span class=\"progress-title\">Mobile Interface</span>\r\n                                      <div class=\"progress\">\r\n                                          <div class=\"progress-bar brand-bg six-sec-ease-in-out\">\r\n                                              <span>86%</span>\r\n                                          </div>\r\n                                      </div>\r\n                                  </div> <!-- progress-section end -->\r\n                              </div><!-- /.col-md-6 -->\r\n\r\n                              <div class=\"col-sm-6\">\r\n                                  <div class=\"progress-section\">\r\n                                      <span class=\"progress-title\">Web Design</span>\r\n                                      <div class=\"progress\">\r\n                                          <div class=\"progress-bar brand-bg six-sec-ease-in-out\">\r\n                                              <span>90%</span>\r\n                                          </div>\r\n                                      </div>\r\n                                  </div> <!-- progress-section end -->\r\n\r\n                                  <div class=\"progress-section\">\r\n                                      <span class=\"progress-title\">Mobile Interface</span>\r\n                                      <div class=\"progress\">\r\n                                          <div class=\"progress-bar brand-bg six-sec-ease-in-out\" >\r\n                                              <span>86%</span>\r\n                                          </div>\r\n                                      </div>\r\n                                  </div> <!-- progress-section end -->\r\n\r\n                                  <div class=\"progress-section\">\r\n                                      <span class=\"progress-title\">Mobile Interface</span>\r\n                                      <div class=\"progress\">\r\n                                          <div class=\"progress-bar brand-bg six-sec-ease-in-out\">\r\n                                              <span>86%</span>\r\n                                          </div>\r\n                                      </div>\r\n                                  </div> <!-- progress-section end -->\r\n                              </div><!-- /.col-md-6 -->\r\n\r\n                            </div><!-- /.row -->\r\n\r\n                        </div> <!--team-skill end -->\r\n                    </div> <!-- col-md-8 -->\r\n                </div> <!-- row -->\r\n              </div> <!--team-3 end -->\r\n\r\n              <div class=\"tab-pane fade\" id=\"team_4_bigperl\">\r\n                <div class=\"row\">\r\n                    <div class=\"col-md-4 col-sm-3\">\r\n                        <figure class=\"team-img text-center\">\r\n                            <img src=\"assets/img/team/about_us_team_large_4_bigperl.png\" class=\"img-responsive\" alt=\"about_us_team_large_4_bigperl\">\r\n\r\n                            <ul class=\"team-social-links list-inline\">\r\n                                <li><a href=\"#\"><i class=\"fa fa-facebook\"></i></a></li>\r\n                                <li><a href=\"#\"><i class=\"fa fa-twitter\"></i></a></li>\r\n                                <li><a href=\"#\"><i class=\"fa fa-google-plus\"></i></a></li>\r\n                                <li><a href=\"#\"><i class=\"fa fa-linkedin\"></i></a></li>\r\n                                <li><a href=\"#\"><i class=\"fa fa-dribbble\"></i></a></li>\r\n                                <li><a href=\"#\"><i class=\"fa fa-envelope-o\"></i></a></li>\r\n                            </ul>\r\n                        </figure>\r\n                    </div><!-- /.col-md-4 -->\r\n\r\n                    <div class=\"col-md-8 col-sm-9\">\r\n\r\n                        <div class=\"team-intro\">\r\n                            <h3>Jonathon Bin <small>Product Developer</small></h3>\r\n                            <p>A id a torquent tortor at lacus et donec platea eu scelerisque maecenas ac eros a adipiscing id lobortis cum lacus erat. Parturient eleifend adipiscing ultrices a cursus est feugiat porta a at condimentum fames adipiscing odio in nisi venenatis suspendisse suspendisse parturient. Leo congue sociosqu maecenas ligula eu penatibus at suscipit mus scelerisque.</p>\r\n                        </div>\r\n\r\n\r\n                        <div class=\"team-skill\">\r\n                            <div class=\"row\">\r\n\r\n                              <div class=\"col-sm-6\">\r\n\r\n                                  <div class=\"progress-section\">\r\n                                      <span class=\"progress-title\">Web Design</span>\r\n                                      <div class=\"progress\">\r\n                                          <div class=\"progress-bar brand-bg six-sec-ease-in-out\" >\r\n                                              <span>90%</span>\r\n                                          </div>\r\n                                      </div><!-- /.progress -->\r\n                                  </div> <!-- progress-section end -->\r\n\r\n\r\n                                  <div class=\"progress-section\">\r\n                                      <span class=\"progress-title\">Mobile Interface</span>\r\n                                      <div class=\"progress\">\r\n                                          <div class=\"progress-bar brand-bg six-sec-ease-in-out\">\r\n                                              <span>86%</span>\r\n                                          </div>\r\n                                      </div>\r\n                                  </div> <!-- progress-section end -->\r\n\r\n                                  <div class=\"progress-section\">\r\n                                      <span class=\"progress-title\">Mobile Interface</span>\r\n                                      <div class=\"progress\">\r\n                                          <div class=\"progress-bar brand-bg six-sec-ease-in-out\" >\r\n                                              <span>86%</span>\r\n                                          </div>\r\n                                      </div>\r\n                                  </div> <!-- progress-section end -->\r\n                              </div><!-- /.col-md-6 -->\r\n\r\n                              <div class=\"col-sm-6\">\r\n                                  <div class=\"progress-section\">\r\n                                      <span class=\"progress-title\">Web Design</span>\r\n                                      <div class=\"progress\">\r\n                                          <div class=\"progress-bar brand-bg six-sec-ease-in-out\" >\r\n                                              <span>90%</span>\r\n                                          </div>\r\n                                      </div>\r\n                                  </div> <!-- progress-section end -->\r\n\r\n                                  <div class=\"progress-section\">\r\n                                      <span class=\"progress-title\">Mobile Interface</span>\r\n                                      <div class=\"progress\">\r\n                                          <div class=\"progress-bar brand-bg six-sec-ease-in-out\" >\r\n                                              <span>86%</span>\r\n                                          </div>\r\n                                      </div>\r\n                                  </div> <!-- progress-section end -->\r\n\r\n                                  <div class=\"progress-section\">\r\n                                      <span class=\"progress-title\">Mobile Interface</span>\r\n                                      <div class=\"progress\">\r\n                                          <div class=\"progress-bar brand-bg six-sec-ease-in-out\" >\r\n                                              <span>86%</span>\r\n                                          </div>\r\n                                      </div>\r\n                                  </div> <!-- progress-section end -->\r\n                              </div><!-- /.col-md-6 -->\r\n\r\n                            </div><!-- /.row -->\r\n\r\n                        </div> <!--team-skill end -->\r\n                    </div> <!-- col-md-8 -->\r\n                </div> <!-- row -->\r\n              </div> <!--team-4 end -->\r\n\r\n          </div> <!--tab-content end -->\r\n        </div>\r\n\r\n    </div> <!--tab-pan end -->\r\n\r\n  </div><!-- /.container -->\r\n</section>\r\n\r\n\r\n<hr>\r\n\r\n<div class=\"container padding-top-20 padding-bottom-20\">\r\n\r\n    <div class=\"row\">\r\n      <div class=\"col-md-8 col-md-offset-2\">\r\n\r\n        <div class=\"thumb-carousel circle-thumb text-center\">\r\n          <ul class=\"slides\">\r\n            <li data-thumb=\"assets/img/client-thumb/about_us_1_bigperl.png\">\r\n              <div class=\"icon\">\r\n                  <img src=\"assets/img/about_us_quote_dark_bigperl.png\" alt=\"about_us_quote_dark_bigperl\">\r\n              </div>\r\n              <div class=\"content\">\r\n                  <p>Quam adipiscing vestibulum feugiat lacus leo a eget leo convallis sagittis nisi varius eros a imperdiet.Dui elementum ut a vestibulum eu fames hendrerit class conubia consequat curae.</p>\r\n\r\n                  <div class=\"testimonial-meta brand-color\">\r\n                      Jhon Doe,\r\n                      <span>User Interface Designer</span>\r\n                  </div>\r\n              </div>\r\n            </li>\r\n            <li data-thumb=\"assets/img/client-thumb/about_us_2_bigperl.png\">\r\n              <div class=\"icon\">\r\n                  <img src=\"assets/img/about_us_quote_dark_bigperl.png\" alt=\"about_us_quote_dark_bigperl\">\r\n              </div>\r\n              <div class=\"content\">\r\n                  <p>Quam adipiscing vestibulum feugiat lacus leo a eget leo convallis sagittis nisi varius eros a imperdiet.Dui elementum ut a vestibulum eu fames hendrerit class conubia consequat curae.</p>\r\n\r\n                  <div class=\"testimonial-meta brand-color\">\r\n                      Tomas Hody,\r\n                      <span>User Interface Designer</span>\r\n                  </div>\r\n              </div>\r\n            </li>\r\n            <li data-thumb=\"assets/img/client-thumb/about_us_3_bigperl.png\">\r\n              <div class=\"icon\">\r\n                  <img src=\"assets/img/about_us_quote_dark_bigperl.png\" alt=\"about_us_quote_dark_bigperl\">\r\n              </div>\r\n              <div class=\"content\">\r\n                  <p>Quam adipiscing vestibulum feugiat lacus leo a eget leo convallis sagittis nisi varius eros a imperdiet.Dui elementum ut a vestibulum eu fames hendrerit class conubia consequat curae.</p>\r\n\r\n                  <div class=\"testimonial-meta brand-color\">\r\n                      Elita Chow,\r\n                      <span>User Interface Designer</span>\r\n                  </div>\r\n              </div>\r\n            </li>\r\n            <li data-thumb=\"assets/img/client-thumb/about_us_4_bigperl.png\">\r\n              <div class=\"icon\">\r\n                  <img src=\"assets/img/about_us_quote_dark_bigperl.png\" alt=\"about_us_quote_dark_bigperl\">\r\n              </div>\r\n              <div class=\"content\">\r\n                  <p>Quam adipiscing vestibulum feugiat lacus leo a eget leo convallis sagittis nisi varius eros a imperdiet.Dui elementum ut a vestibulum eu fames hendrerit class conubia consequat curae.</p>\r\n\r\n                  <div class=\"testimonial-meta brand-color\">\r\n                      Alex Kumar,\r\n                      <span>User Interface Designer</span>\r\n                  </div>\r\n              </div>\r\n            </li>\r\n          </ul>\r\n        </div>\r\n\r\n      </div>\r\n    </div><!-- /.row -->\r\n\r\n</div><!-- /.container -->\r\n"

/***/ }),

/***/ "./src/app/AboutUs/sucess-stories/sucess-stories.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/AboutUs/sucess-stories/sucess-stories.component.ts ***!
  \********************************************************************/
/*! exports provided: SucessStoriesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SucessStoriesComponent", function() { return SucessStoriesComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var $;
var jQuery;
var SucessStoriesComponent = /** @class */ (function () {
    function SucessStoriesComponent() {
    }
    SucessStoriesComponent.prototype.ngOnInit = function () {
        // "use strict";
        // $(document).ready(function() {
        //   "use strict";
        //         $(document).ready(function() {
        //       $(".featured-carousel").length > 0 && $(".featured-carousel").owlCarousel({
        //         loop: !0,
        //         margin: 12,
        //         responsive: {
        //             0: {
        //                 items: 1
        //             },
        //             600: {
        //                 items: 2
        //             },
        //             1e3: {
        //                 items: 3
        //             }
        //         }
        //     })
        //   })
        // })
        //     function() {
        //       "use strict";
        //       $(document).ready(function() {
        //     $(".featured-carousel").length > 0 && $(".featured-carousel").owlCarousel({
        //       loop: !0,
        //       margin: 12,
        //       responsive: {
        //           0: {
        //               items: 1
        //           },
        //           600: {
        //               items: 2
        //           },
        //           1e3: {
        //               items: 3
        //           }
        //       }
        //   })
        // })
        // }(jQuery);
    };
    SucessStoriesComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-sucess-stories',
            template: __webpack_require__(/*! ./sucess-stories.component.html */ "./src/app/AboutUs/sucess-stories/sucess-stories.component.html"),
            styles: [__webpack_require__(/*! ./sucess-stories.component.css */ "./src/app/AboutUs/sucess-stories/sucess-stories.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], SucessStoriesComponent);
    return SucessStoriesComponent;
}());



/***/ }),

/***/ "./src/app/Industry/automotive/automotive.component.css":
/*!**************************************************************!*\
  !*** ./src/app/Industry/automotive/automotive.component.css ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/Industry/automotive/automotive.component.html":
/*!***************************************************************!*\
  !*** ./src/app/Industry/automotive/automotive.component.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = " <!--page title start-->\r\n <section class=\"page-title ptb-30\">\r\n        <div class=\"container padding-top-40\">\r\n            <div class=\"row\">\r\n                <div class=\"col-md-12\">\r\n                    <h6 class=\"pagetitles\">Industry</h6>\r\n                    <ol class=\"breadcrumb\">\r\n                        <li><a href=\"#\">Home</a></li>\r\n                        <li><a href=\"#\">industry</a></li>\r\n                        <li class=\"active\">automative</li>\r\n                    </ol>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </section>\r\n    <!--page title end-->\r\n\r\n        <img src=\"assets/img/industry/banner/automative_banner_bigperl.png\" class=\"fullwidth\" alt=\"automative_banner_bigperl\">\r\n\r\n          <div class=\"container padding-top-20\">\r\n              <div class=\"col-md-7\">\r\n                  <h2 class=\"text-bold mb-10\">Architecture Solutions</h2>\r\n                  <p class=\"text-justified\">Bigperl solutions being one of the leading business application development company working towards bringing an enterprise application distribution over various other platforms like BlackBerry, Android, and Apple etc. By making it available to the customer with a click of a button. Our penetration into the smart devices towards customer markets has made relatively inexpensive and easier for integration.</p>\r\n                  <h5><b>What we offer?</b></h5>\r\n                  <p class=\"text-justified\">We not just work on bringing a platform to our consumers reaching them with a click of a button, but also our analysts and creative team work on building the most innovative and user friendly app, where we cater various enterprises the opportunity to distribute their various applications. We also upgrade our platform and applications with the new features and in addition, we audit and forecast the various feature expected by our customers in the near future. Thus we work purely on introducing and catering the best features to people making them readily available.</p>\r\n                  <h5><b>What makes BigPerl a unique platform?</b></h5>\r\n                  <p class=\"text-justified\">Significantly we come across the most unique challenge suffered by enterprise application is unlike customer applications, these applications are built, designed and dedicated to the internal business process or even for a specific group of people and for this requirement enterprise cannot really rely on IT platforms like apple app store, android market etc.. and here is where our one of the key focus is, that’s to create a platform to serve any requirement, any time providing a secure mechanism to distribute, update and manage custom applications.</p>\r\n                  <p class=\"text-justified\">Many enterprises has acquired the services from our experienced distribution team and obtained futuristic benefits. Now it’s your turn too, let us know your requirements to get a free quote.</p>\r\n                  <p class=\"text-justified\">Enterprise app distribution refers to the process of making your apps readily available to your enterprise people who need them. Apart from distribution, you can also upgrade and audit your apps in a powerful and safe manner. With the increasing demand for mobile devices and tablets, the popularity of enterprise apps is improving day by day. Such apps help employees work away from offices using their own preferred devices. However, these apps cannot be distributed normally through app stores. They are distributed privately to the people that are connected to your enterprise. You can email these apps to your team, upload them on to an internal server or distribute them through a third party service. The last option is the one that is referred to as enterprise app distribution.</p>\r\n              </div>\r\n              <div class=\"col-md-5\">\r\n                  <img src=\"assets/img/industry/automative_bigperl.png\" class=\"img-responsive \" alt=\"automative_bigperl\">\r\n              </div>\r\n          </div>\r\n\r\n\r\n          <app-footer5></app-footer5>\r\n           \r\n<app-footer2></app-footer2>\r\n    "

/***/ }),

/***/ "./src/app/Industry/automotive/automotive.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/Industry/automotive/automotive.component.ts ***!
  \*************************************************************/
/*! exports provided: AutomotiveComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AutomotiveComponent", function() { return AutomotiveComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AutomotiveComponent = /** @class */ (function () {
    function AutomotiveComponent() {
    }
    AutomotiveComponent.prototype.ngOnInit = function () {
    };
    AutomotiveComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-automotive',
            template: __webpack_require__(/*! ./automotive.component.html */ "./src/app/Industry/automotive/automotive.component.html"),
            styles: [__webpack_require__(/*! ./automotive.component.css */ "./src/app/Industry/automotive/automotive.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], AutomotiveComponent);
    return AutomotiveComponent;
}());



/***/ }),

/***/ "./src/app/Industry/banking-and-financial/banking-and-financial.component.css":
/*!************************************************************************************!*\
  !*** ./src/app/Industry/banking-and-financial/banking-and-financial.component.css ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/Industry/banking-and-financial/banking-and-financial.component.html":
/*!*************************************************************************************!*\
  !*** ./src/app/Industry/banking-and-financial/banking-and-financial.component.html ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<!--page title start-->\r\n<section class=\"page-title ptb-30\">\r\n    <div class=\"container padding-top-40\">\r\n        <div class=\"row\">\r\n            <div class=\"col-md-12\">\r\n                <h6 class=\"pagetitles\">Industry</h6>\r\n                <ol class=\"breadcrumb\">\r\n                    <li><a href=\"#\">Home</a></li>\r\n                    <li><a href=\"#\">industry</a></li>\r\n                    <li class=\"active\">banking and financial</li>\r\n                </ol>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</section>\r\n<!--page title end-->\r\n\r\n<img src=\"assets/img/industry/banner/banking_financial_bigperl.png\" class=\"fullwidth\" alt=\"banking_financial_bigperl\">\r\n\r\n      <div class=\"container padding-top-20\">\r\n          <div class=\"col-md-7\">\r\n              <h2 class=\"text-bold mb-10\">Architecture Solutions</h2>\r\n              <p class=\"text-justified\">Bigperl solutions being one of the leading business application development company working towards bringing an enterprise application distribution over various other platforms like BlackBerry, Android, and Apple etc. By making it available to the customer with a click of a button. Our penetration into the smart devices towards customer markets has made relatively inexpensive and easier for integration.</p>\r\n              <h5><b>What we offer?</b></h5>\r\n              <p class=\"text-justified\">We not just work on bringing a platform to our consumers reaching them with a click of a button, but also our analysts and creative team work on building the most innovative and user friendly app, where we cater various enterprises the opportunity to distribute their various applications. We also upgrade our platform and applications with the new features and in addition, we audit and forecast the various feature expected by our customers in the near future. Thus we work purely on introducing and catering the best features to people making them readily available.</p>\r\n              <h5><b>What makes BigPerl a unique platform?</b></h5>\r\n              <p class=\"text-justified\">Significantly we come across the most unique challenge suffered by enterprise application is unlike customer applications, these applications are built, designed and dedicated to the internal business process or even for a specific group of people and for this requirement enterprise cannot really rely on IT platforms like apple app store, android market etc.. and here is where our one of the key focus is, that’s to create a platform to serve any requirement, any time providing a secure mechanism to distribute, update and manage custom applications.</p>\r\n              <p class=\"text-justified\">Many enterprises has acquired the services from our experienced distribution team and obtained futuristic benefits. Now it’s your turn too, let us know your requirements to get a free quote.</p>\r\n              <p class=\"text-justified\">Enterprise app distribution refers to the process of making your apps readily available to your enterprise people who need them. Apart from distribution, you can also upgrade and audit your apps in a powerful and safe manner. With the increasing demand for mobile devices and tablets, the popularity of enterprise apps is improving day by day. Such apps help employees work away from offices using their own preferred devices. However, these apps cannot be distributed normally through app stores. They are distributed privately to the people that are connected to your enterprise. You can email these apps to your team, upload them on to an internal server or distribute them through a third party service. The last option is the one that is referred to as enterprise app distribution.</p>\r\n          </div>\r\n          <div class=\"col-md-5\">\r\n              <img src=\"assets/img/industry/banking_bigperl.png\" class=\"img-responsive \" alt=\"banking_bigperl\">\r\n          </div>\r\n      </div>\r\n\r\n\r\n\r\n\r\n\r\n          <app-footer5></app-footer5>\r\n           \r\n          <app-footer2></app-footer2>\r\n              "

/***/ }),

/***/ "./src/app/Industry/banking-and-financial/banking-and-financial.component.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/Industry/banking-and-financial/banking-and-financial.component.ts ***!
  \***********************************************************************************/
/*! exports provided: BankingAndFinancialComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BankingAndFinancialComponent", function() { return BankingAndFinancialComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var BankingAndFinancialComponent = /** @class */ (function () {
    function BankingAndFinancialComponent() {
    }
    BankingAndFinancialComponent.prototype.ngOnInit = function () {
    };
    BankingAndFinancialComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-banking-and-financial',
            template: __webpack_require__(/*! ./banking-and-financial.component.html */ "./src/app/Industry/banking-and-financial/banking-and-financial.component.html"),
            styles: [__webpack_require__(/*! ./banking-and-financial.component.css */ "./src/app/Industry/banking-and-financial/banking-and-financial.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], BankingAndFinancialComponent);
    return BankingAndFinancialComponent;
}());



/***/ }),

/***/ "./src/app/Industry/consumer-electronics/consumer-electronics.component.css":
/*!**********************************************************************************!*\
  !*** ./src/app/Industry/consumer-electronics/consumer-electronics.component.css ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/Industry/consumer-electronics/consumer-electronics.component.html":
/*!***********************************************************************************!*\
  !*** ./src/app/Industry/consumer-electronics/consumer-electronics.component.html ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--page title start-->\r\n<section class=\"page-title ptb-30\">\r\n    <div class=\"container padding-top-40\">\r\n        <div class=\"row\">\r\n            <div class=\"col-md-12\">\r\n                <h6 class=\"pagetitles\">Industry</h6>\r\n                <ol class=\"breadcrumb\">\r\n                    <li><a href=\"#\">Home</a></li>\r\n                    <li><a href=\"#\">industry</a></li>\r\n                    <li class=\"active\">consumer electronics</li>\r\n                </ol>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</section>\r\n<!--page title end-->\r\n\r\n<img src=\"assets/img/industry/banner/consumer_banner_bigperl.png\" class=\"fullwidth\" alt=\"consumer_banner_bigperl\">\r\n\r\n      <div class=\"container padding-top-20\">\r\n          <div class=\"col-md-7\">\r\n              <h2 class=\"text-bold mb-10\">Architecture Solutions</h2>\r\n              <p class=\"text-justified\">Bigperl solutions being one of the leading business application development company working towards bringing an enterprise application distribution over various other platforms like BlackBerry, Android, and Apple etc. By making it available to the customer with a click of a button. Our penetration into the smart devices towards customer markets has made relatively inexpensive and easier for integration.</p>\r\n              <h5><b>What we offer?</b></h5>\r\n              <p class=\"text-justified\">We not just work on bringing a platform to our consumers reaching them with a click of a button, but also our analysts and creative team work on building the most innovative and user friendly app, where we cater various enterprises the opportunity to distribute their various applications. We also upgrade our platform and applications with the new features and in addition, we audit and forecast the various feature expected by our customers in the near future. Thus we work purely on introducing and catering the best features to people making them readily available.</p>\r\n              <h5><b>What makes BigPerl a unique platform?</b></h5>\r\n              <p class=\"text-justified\">Significantly we come across the most unique challenge suffered by enterprise application is unlike customer applications, these applications are built, designed and dedicated to the internal business process or even for a specific group of people and for this requirement enterprise cannot really rely on IT platforms like apple app store, android market etc.. and here is where our one of the key focus is, that’s to create a platform to serve any requirement, any time providing a secure mechanism to distribute, update and manage custom applications.</p>\r\n              <p class=\"text-justified\">Many enterprises has acquired the services from our experienced distribution team and obtained futuristic benefits. Now it’s your turn too, let us know your requirements to get a free quote.</p>\r\n              <p class=\"text-justified\">Enterprise app distribution refers to the process of making your apps readily available to your enterprise people who need them. Apart from distribution, you can also upgrade and audit your apps in a powerful and safe manner. With the increasing demand for mobile devices and tablets, the popularity of enterprise apps is improving day by day. Such apps help employees work away from offices using their own preferred devices. However, these apps cannot be distributed normally through app stores. They are distributed privately to the people that are connected to your enterprise. You can email these apps to your team, upload them on to an internal server or distribute them through a third party service. The last option is the one that is referred to as enterprise app distribution.</p>\r\n          </div>\r\n          <div class=\"col-md-5\">\r\n              <img src=\"assets/img/industry/consumer_bigperl.png\" class=\"img-responsive \" alt=\"consumer_bigperl\">\r\n          </div>\r\n      </div>\r\n\r\n\r\n\r\n\r\n          <app-footer5></app-footer5>\r\n           \r\n          <app-footer2></app-footer2>\r\n              "

/***/ }),

/***/ "./src/app/Industry/consumer-electronics/consumer-electronics.component.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/Industry/consumer-electronics/consumer-electronics.component.ts ***!
  \*********************************************************************************/
/*! exports provided: ConsumerElectronicsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConsumerElectronicsComponent", function() { return ConsumerElectronicsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ConsumerElectronicsComponent = /** @class */ (function () {
    function ConsumerElectronicsComponent() {
    }
    ConsumerElectronicsComponent.prototype.ngOnInit = function () {
    };
    ConsumerElectronicsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-consumer-electronics',
            template: __webpack_require__(/*! ./consumer-electronics.component.html */ "./src/app/Industry/consumer-electronics/consumer-electronics.component.html"),
            styles: [__webpack_require__(/*! ./consumer-electronics.component.css */ "./src/app/Industry/consumer-electronics/consumer-electronics.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], ConsumerElectronicsComponent);
    return ConsumerElectronicsComponent;
}());



/***/ }),

/***/ "./src/app/Industry/digital-marketing/digital-marketing.component.css":
/*!****************************************************************************!*\
  !*** ./src/app/Industry/digital-marketing/digital-marketing.component.css ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/Industry/digital-marketing/digital-marketing.component.html":
/*!*****************************************************************************!*\
  !*** ./src/app/Industry/digital-marketing/digital-marketing.component.html ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n\r\n        <!--page title start-->\r\n        <section class=\"page-title ptb-30\">\r\n                <div class=\"container padding-top-40\">\r\n                    <div class=\"row\">\r\n                        <div class=\"col-md-12\">\r\n                            <h6 class=\"pagetitles\">Industry</h6>\r\n                            <ol class=\"breadcrumb\">\r\n                                <li><a href=\"#\">Home</a></li>\r\n                                <li><a href=\"#\">industry</a></li>\r\n                                <li class=\"active\">digital marketing</li>\r\n                            </ol>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </section>\r\n            <!--page title end-->\r\n    \r\n                <img src=\"assets/img/industry/banner/digital_marketing_banner_bigperl.png\" class=\"fullwidth\" alt=\"digital_marketing_banner_bigperl\">\r\n    \r\n                  <div class=\"container padding-top-20\">\r\n                      <div class=\"col-md-7\">\r\n                          <h2 class=\"text-bold mb-10\">Architecture Solutions</h2>\r\n                          <p class=\"text-justified\">Bigperl solutions being one of the leading business application development company working towards bringing an enterprise application distribution over various other platforms like BlackBerry, Android, and Apple etc. By making it available to the customer with a click of a button. Our penetration into the smart devices towards customer markets has made relatively inexpensive and easier for integration.</p>\r\n                          <h5><b>What we offer?</b></h5>\r\n                          <p class=\"text-justified\">We not just work on bringing a platform to our consumers reaching them with a click of a button, but also our analysts and creative team work on building the most innovative and user friendly app, where we cater various enterprises the opportunity to distribute their various applications. We also upgrade our platform and applications with the new features and in addition, we audit and forecast the various feature expected by our customers in the near future. Thus we work purely on introducing and catering the best features to people making them readily available.</p>\r\n                          <h5><b>What makes BigPerl a unique platform?</b></h5>\r\n                          <p class=\"text-justified\">Significantly we come across the most unique challenge suffered by enterprise application is unlike customer applications, these applications are built, designed and dedicated to the internal business process or even for a specific group of people and for this requirement enterprise cannot really rely on IT platforms like apple app store, android market etc.. and here is where our one of the key focus is, that’s to create a platform to serve any requirement, any time providing a secure mechanism to distribute, update and manage custom applications.</p>\r\n                          <p class=\"text-justified\">Many enterprises has acquired the services from our experienced distribution team and obtained futuristic benefits. Now it’s your turn too, let us know your requirements to get a free quote.</p>\r\n                          <p class=\"text-justified\">Enterprise app distribution refers to the process of making your apps readily available to your enterprise people who need them. Apart from distribution, you can also upgrade and audit your apps in a powerful and safe manner. With the increasing demand for mobile devices and tablets, the popularity of enterprise apps is improving day by day. Such apps help employees work away from offices using their own preferred devices. However, these apps cannot be distributed normally through app stores. They are distributed privately to the people that are connected to your enterprise. You can email these apps to your team, upload them on to an internal server or distribute them through a third party service. The last option is the one that is referred to as enterprise app distribution.</p>\r\n                      </div>\r\n                      <div class=\"col-md-5\">\r\n                          <img src=\"assets/img/industry/digital_marketing_bigperl.png\" class=\"img-responsive \" alt=\"digital_marketing_bigperl\">\r\n                      </div>\r\n                  </div>\r\n    \r\n            \r\n\r\n          <app-footer5></app-footer5>\r\n           \r\n          <app-footer2></app-footer2>\r\n              "

/***/ }),

/***/ "./src/app/Industry/digital-marketing/digital-marketing.component.ts":
/*!***************************************************************************!*\
  !*** ./src/app/Industry/digital-marketing/digital-marketing.component.ts ***!
  \***************************************************************************/
/*! exports provided: DigitalMarketingComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DigitalMarketingComponent", function() { return DigitalMarketingComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var DigitalMarketingComponent = /** @class */ (function () {
    function DigitalMarketingComponent() {
    }
    DigitalMarketingComponent.prototype.ngOnInit = function () {
    };
    DigitalMarketingComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-digital-marketing',
            template: __webpack_require__(/*! ./digital-marketing.component.html */ "./src/app/Industry/digital-marketing/digital-marketing.component.html"),
            styles: [__webpack_require__(/*! ./digital-marketing.component.css */ "./src/app/Industry/digital-marketing/digital-marketing.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], DigitalMarketingComponent);
    return DigitalMarketingComponent;
}());



/***/ }),

/***/ "./src/app/Industry/education/education.component.css":
/*!************************************************************!*\
  !*** ./src/app/Industry/education/education.component.css ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/Industry/education/education.component.html":
/*!*************************************************************!*\
  !*** ./src/app/Industry/education/education.component.html ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n        <!--page title start-->\r\n        <section class=\"page-title ptb-30\">\r\n                <div class=\"container padding-top-40\">\r\n                    <div class=\"row\">\r\n                        <div class=\"col-md-12\">\r\n                            <h6 class=\"pagetitles\">Industry</h6>\r\n                            <ol class=\"breadcrumb\">\r\n                                <li><a href=\"#\">Home</a></li>\r\n                                <li><a href=\"#\">industry</a></li>\r\n                                <li class=\"active\">education</li>\r\n                            </ol>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </section>\r\n            <!--page title end-->\r\n    \r\n                <img src=\"assets/img/industry/banner/education_banners_bigperl.png\" class=\"fullwidth\" alt=\"education_banners_bigperl\">\r\n    \r\n                  <div class=\"container padding-top-20\">\r\n                      <div class=\"col-md-7\">\r\n                          <h2 class=\"text-bold mb-10\">Architecture Solutions</h2>\r\n                          <p class=\"text-justified\">Bigperl solutions being one of the leading business application development company working towards bringing an enterprise application distribution over various other platforms like BlackBerry, Android, and Apple etc. By making it available to the customer with a click of a button. Our penetration into the smart devices towards customer markets has made relatively inexpensive and easier for integration.</p>\r\n                          <h5><b>What we offer?</b></h5>\r\n                          <p class=\"text-justified\">We not just work on bringing a platform to our consumers reaching them with a click of a button, but also our analysts and creative team work on building the most innovative and user friendly app, where we cater various enterprises the opportunity to distribute their various applications. We also upgrade our platform and applications with the new features and in addition, we audit and forecast the various feature expected by our customers in the near future. Thus we work purely on introducing and catering the best features to people making them readily available.</p>\r\n                          <h5><b>What makes BigPerl a unique platform?</b></h5>\r\n                          <p class=\"text-justified\">Significantly we come across the most unique challenge suffered by enterprise application is unlike customer applications, these applications are built, designed and dedicated to the internal business process or even for a specific group of people and for this requirement enterprise cannot really rely on IT platforms like apple app store, android market etc.. and here is where our one of the key focus is, that’s to create a platform to serve any requirement, any time providing a secure mechanism to distribute, update and manage custom applications.</p>\r\n                          <p class=\"text-justified\">Many enterprises has acquired the services from our experienced distribution team and obtained futuristic benefits. Now it’s your turn too, let us know your requirements to get a free quote.</p>\r\n                          <p class=\"text-justified\">Enterprise app distribution refers to the process of making your apps readily available to your enterprise people who need them. Apart from distribution, you can also upgrade and audit your apps in a powerful and safe manner. With the increasing demand for mobile devices and tablets, the popularity of enterprise apps is improving day by day. Such apps help employees work away from offices using their own preferred devices. However, these apps cannot be distributed normally through app stores. They are distributed privately to the people that are connected to your enterprise. You can email these apps to your team, upload them on to an internal server or distribute them through a third party service. The last option is the one that is referred to as enterprise app distribution.</p>\r\n                      </div>\r\n                      <div class=\"col-md-5\">\r\n                          <img src=\"assets/img/industry/education_bigperl.png\" class=\"img-responsive \" alt=\"education_bigperl\">\r\n                      </div>\r\n                  </div>\r\n    \r\n            \r\n    \r\n\r\n\r\n          <app-footer5></app-footer5>\r\n           \r\n          <app-footer2></app-footer2>\r\n              "

/***/ }),

/***/ "./src/app/Industry/education/education.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/Industry/education/education.component.ts ***!
  \***********************************************************/
/*! exports provided: EducationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EducationComponent", function() { return EducationComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var EducationComponent = /** @class */ (function () {
    function EducationComponent() {
    }
    EducationComponent.prototype.ngOnInit = function () {
    };
    EducationComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-education',
            template: __webpack_require__(/*! ./education.component.html */ "./src/app/Industry/education/education.component.html"),
            styles: [__webpack_require__(/*! ./education.component.css */ "./src/app/Industry/education/education.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], EducationComponent);
    return EducationComponent;
}());



/***/ }),

/***/ "./src/app/Industry/government/government.component.css":
/*!**************************************************************!*\
  !*** ./src/app/Industry/government/government.component.css ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/Industry/government/government.component.html":
/*!***************************************************************!*\
  !*** ./src/app/Industry/government/government.component.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n\r\n        <!--page title start-->\r\n        <section class=\"page-title ptb-30\">\r\n                <div class=\"container padding-top-40\">\r\n                    <div class=\"row\">\r\n                        <div class=\"col-md-12\">\r\n                            <h6 class=\"pagetitles\">Industry</h6>\r\n                            <ol class=\"breadcrumb\">\r\n                                <li><a href=\"#\">Home</a></li>\r\n                                <li><a href=\"#\">industry</a></li>\r\n                                <li class=\"active\">government it</li>\r\n                            </ol>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </section>\r\n            <!--page title end-->\r\n    \r\n                <img src=\"assets/img/industry/banner/government_banner_bigperl.png\" class=\"fullwidth\" alt=\"government_banner_bigperl\">\r\n    \r\n                  <div class=\"container padding-top-20\">\r\n                      <div class=\"col-md-7\">\r\n                          <h2 class=\"text-bold mb-10\">Government</h2>\r\n                          <p class=\"text-justified\">Register an organization if you represent a business (including employers, sole traders and farming organisations), a charity, or other commercial or non-commercial organisation. Services include Electronic VAT Returns and PAYE Online for Employers.</p>\r\n                      </div>\r\n                      <div class=\"col-md-5\">\r\n                          <img src=\"assets/img/industry/government_bigperl.png\" class=\"img-responsive \" alt=\"government_bigperl\">\r\n                      </div>\r\n                  </div>\r\n    \r\n            \r\n\r\n\r\n          <app-footer5></app-footer5>\r\n           \r\n          <app-footer2></app-footer2>\r\n              "

/***/ }),

/***/ "./src/app/Industry/government/government.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/Industry/government/government.component.ts ***!
  \*************************************************************/
/*! exports provided: GovernmentComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GovernmentComponent", function() { return GovernmentComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var GovernmentComponent = /** @class */ (function () {
    function GovernmentComponent() {
    }
    GovernmentComponent.prototype.ngOnInit = function () {
    };
    GovernmentComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-government',
            template: __webpack_require__(/*! ./government.component.html */ "./src/app/Industry/government/government.component.html"),
            styles: [__webpack_require__(/*! ./government.component.css */ "./src/app/Industry/government/government.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], GovernmentComponent);
    return GovernmentComponent;
}());



/***/ }),

/***/ "./src/app/Industry/hospitality/hospitality.component.css":
/*!****************************************************************!*\
  !*** ./src/app/Industry/hospitality/hospitality.component.css ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/Industry/hospitality/hospitality.component.html":
/*!*****************************************************************!*\
  !*** ./src/app/Industry/hospitality/hospitality.component.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<!--page title start-->\r\n<section class=\"page-title ptb-30\">\r\n        <div class=\"container padding-top-40\">\r\n            <div class=\"row\">\r\n                <div class=\"col-md-12\">\r\n                    <h6 class=\"pagetitles\">Industry</h6>\r\n                    <ol class=\"breadcrumb\">\r\n                        <li><a href=\"#\">Home</a></li>\r\n                        <li><a href=\"#\">industry</a></li>\r\n                        <li class=\"active\">hospitality</li>\r\n                    </ol>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </section>\r\n    <!--page title end-->\r\n\r\n        <img src=\"assets/img/industry/banner/hospitality_banner_bigperl.jpg\" class=\"fullwidth\" alt=\"hospitality_banner_bigperl\">\r\n\r\n          <div class=\"container padding-top-20\">\r\n              <div class=\"col-md-7\">\r\n                  <h2 class=\"text-bold mb-10\">Architecture Solutions</h2>\r\n                  <p class=\"text-justified\">Bigperl solutions being one of the leading business application development company working towards bringing an enterprise application distribution over various other platforms like BlackBerry, Android, and Apple etc. By making it available to the customer with a click of a button. Our penetration into the smart devices towards customer markets has made relatively inexpensive and easier for integration.</p>\r\n                  <h5><b>What we offer?</b></h5>\r\n                  <p class=\"text-justified\">We not just work on bringing a platform to our consumers reaching them with a click of a button, but also our analysts and creative team work on building the most innovative and user friendly app, where we cater various enterprises the opportunity to distribute their various applications. We also upgrade our platform and applications with the new features and in addition, we audit and forecast the various feature expected by our customers in the near future. Thus we work purely on introducing and catering the best features to people making them readily available.</p>\r\n                  <h5><b>What makes BigPerl a unique platform?</b></h5>\r\n                  <p class=\"text-justified\">Significantly we come across the most unique challenge suffered by enterprise application is unlike customer applications, these applications are built, designed and dedicated to the internal business process or even for a specific group of people and for this requirement enterprise cannot really rely on IT platforms like apple app store, android market etc.. and here is where our one of the key focus is, that’s to create a platform to serve any requirement, any time providing a secure mechanism to distribute, update and manage custom applications.</p>\r\n                  <p class=\"text-justified\">Many enterprises has acquired the services from our experienced distribution team and obtained futuristic benefits. Now it’s your turn too, let us know your requirements to get a free quote.</p>\r\n                  <p class=\"text-justified\">Enterprise app distribution refers to the process of making your apps readily available to your enterprise people who need them. Apart from distribution, you can also upgrade and audit your apps in a powerful and safe manner. With the increasing demand for mobile devices and tablets, the popularity of enterprise apps is improving day by day. Such apps help employees work away from offices using their own preferred devices. However, these apps cannot be distributed normally through app stores. They are distributed privately to the people that are connected to your enterprise. You can email these apps to your team, upload them on to an internal server or distribute them through a third party service. The last option is the one that is referred to as enterprise app distribution.</p>\r\n              </div>\r\n              <div class=\"col-md-5\">\r\n                  <img src=\"assets/img/industry/hospitality_bigperl.png\" class=\"img-responsive \" alt=\"hospitality_bigperl\">\r\n              </div>\r\n          </div>\r\n\r\n\r\n\r\n\r\n          <app-footer5></app-footer5>\r\n           \r\n          <app-footer2></app-footer2>\r\n              "

/***/ }),

/***/ "./src/app/Industry/hospitality/hospitality.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/Industry/hospitality/hospitality.component.ts ***!
  \***************************************************************/
/*! exports provided: HospitalityComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HospitalityComponent", function() { return HospitalityComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var HospitalityComponent = /** @class */ (function () {
    function HospitalityComponent() {
    }
    HospitalityComponent.prototype.ngOnInit = function () {
    };
    HospitalityComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-hospitality',
            template: __webpack_require__(/*! ./hospitality.component.html */ "./src/app/Industry/hospitality/hospitality.component.html"),
            styles: [__webpack_require__(/*! ./hospitality.component.css */ "./src/app/Industry/hospitality/hospitality.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], HospitalityComponent);
    return HospitalityComponent;
}());



/***/ }),

/***/ "./src/app/Industry/it/it.component.css":
/*!**********************************************!*\
  !*** ./src/app/Industry/it/it.component.css ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/Industry/it/it.component.html":
/*!***********************************************!*\
  !*** ./src/app/Industry/it/it.component.html ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n        <!--page title start-->\r\n        <section class=\"page-title ptb-30\">\r\n                <div class=\"container padding-top-40\">\r\n                    <div class=\"row\">\r\n                        <div class=\"col-md-12\">\r\n                            <h6 class=\"pagetitles\">Industry</h6>\r\n                            <ol class=\"breadcrumb\">\r\n                                <li><a href=\"#\">Home</a></li>\r\n                                <li><a href=\"#\">industry</a></li>\r\n                                <li class=\"active\">it</li>\r\n                            </ol>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </section>\r\n            <!--page title end-->\r\n    \r\n                <img src=\"assets/img/industry/banner/it_banner_bigperl.png\" class=\"fullwidth\" alt=\"it_banner_bigperl\">\r\n    \r\n                  <div class=\"container padding-top-20\">\r\n                      <div class=\"col-md-7\">\r\n                          <h2 class=\"text-bold mb-10\">Architecture Solutions</h2>\r\n                          <p class=\"text-justified\">Bigperl solutions being one of the leading business application development company working towards bringing an enterprise application distribution over various other platforms like BlackBerry, Android, and Apple etc. By making it available to the customer with a click of a button. Our penetration into the smart devices towards customer markets has made relatively inexpensive and easier for integration.</p>\r\n                          <h5><b>What we offer?</b></h5>\r\n                          <p class=\"text-justified\">We not just work on bringing a platform to our consumers reaching them with a click of a button, but also our analysts and creative team work on building the most innovative and user friendly app, where we cater various enterprises the opportunity to distribute their various applications. We also upgrade our platform and applications with the new features and in addition, we audit and forecast the various feature expected by our customers in the near future. Thus we work purely on introducing and catering the best features to people making them readily available.</p>\r\n                          <h5><b>What makes BigPerl a unique platform?</b></h5>\r\n                          <p class=\"text-justified\">Significantly we come across the most unique challenge suffered by enterprise application is unlike customer applications, these applications are built, designed and dedicated to the internal business process or even for a specific group of people and for this requirement enterprise cannot really rely on IT platforms like apple app store, android market etc.. and here is where our one of the key focus is, that’s to create a platform to serve any requirement, any time providing a secure mechanism to distribute, update and manage custom applications.</p>\r\n                          <p class=\"text-justified\">Many enterprises has acquired the services from our experienced distribution team and obtained futuristic benefits. Now it’s your turn too, let us know your requirements to get a free quote.</p>\r\n                          <p class=\"text-justified\">Enterprise app distribution refers to the process of making your apps readily available to your enterprise people who need them. Apart from distribution, you can also upgrade and audit your apps in a powerful and safe manner. With the increasing demand for mobile devices and tablets, the popularity of enterprise apps is improving day by day. Such apps help employees work away from offices using their own preferred devices. However, these apps cannot be distributed normally through app stores. They are distributed privately to the people that are connected to your enterprise. You can email these apps to your team, upload them on to an internal server or distribute them through a third party service. The last option is the one that is referred to as enterprise app distribution.</p>\r\n                      </div>\r\n                      <div class=\"col-md-5\">\r\n                          <img src=\"assets/img/industry/it_bigperl.png\" class=\"img-responsive \" alt=\"it_bigperl\">\r\n                      </div>\r\n                  </div>\r\n    \r\n            \r\n\r\n\r\n\r\n          <app-footer5></app-footer5>\r\n           \r\n          <app-footer2></app-footer2>\r\n              "

/***/ }),

/***/ "./src/app/Industry/it/it.component.ts":
/*!*********************************************!*\
  !*** ./src/app/Industry/it/it.component.ts ***!
  \*********************************************/
/*! exports provided: ItComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ItComponent", function() { return ItComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ItComponent = /** @class */ (function () {
    function ItComponent() {
    }
    ItComponent.prototype.ngOnInit = function () {
    };
    ItComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-it',
            template: __webpack_require__(/*! ./it.component.html */ "./src/app/Industry/it/it.component.html"),
            styles: [__webpack_require__(/*! ./it.component.css */ "./src/app/Industry/it/it.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], ItComponent);
    return ItComponent;
}());



/***/ }),

/***/ "./src/app/Industry/life-science-and-health-care/life-science-and-health-care.component.css":
/*!**************************************************************************************************!*\
  !*** ./src/app/Industry/life-science-and-health-care/life-science-and-health-care.component.css ***!
  \**************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/Industry/life-science-and-health-care/life-science-and-health-care.component.html":
/*!***************************************************************************************************!*\
  !*** ./src/app/Industry/life-science-and-health-care/life-science-and-health-care.component.html ***!
  \***************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--page title start-->\r\n<section class=\"page-title ptb-30\">\r\n    <div class=\"container padding-top-40\">\r\n        <div class=\"row\">\r\n            <div class=\"col-md-12\">\r\n                <h6 class=\"pagetitles\">Industry</h6>\r\n                <ol class=\"breadcrumb\">\r\n                    <li><a href=\"#\">Home</a></li>\r\n                    <li><a href=\"#\">industry</a></li>\r\n                    <li class=\"active\">hospitality</li>\r\n                </ol>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</section>\r\n<!--page title end-->\r\n\r\n<img src=\"assets/img/industry/banner/life_science_banner_bigperl.png\" class=\"fullwidth\" alt=\"life_science_banner_bigperl\">\r\n\r\n      <div class=\"container padding-top-20\">\r\n          <div class=\"col-md-7\">\r\n              <h2 class=\"text-bold mb-10\">Architecture Solutions</h2>\r\n              <p class=\"text-justified\">Bigperl solutions being one of the leading business application development company working towards bringing an enterprise application distribution over various other platforms like BlackBerry, Android, and Apple etc. By making it available to the customer with a click of a button. Our penetration into the smart devices towards customer markets has made relatively inexpensive and easier for integration.</p>\r\n              <h5><b>What we offer?</b></h5>\r\n              <p class=\"text-justified\">We not just work on bringing a platform to our consumers reaching them with a click of a button, but also our analysts and creative team work on building the most innovative and user friendly app, where we cater various enterprises the opportunity to distribute their various applications. We also upgrade our platform and applications with the new features and in addition, we audit and forecast the various feature expected by our customers in the near future. Thus we work purely on introducing and catering the best features to people making them readily available.</p>\r\n              <h5><b>What makes BigPerl a unique platform?</b></h5>\r\n              <p class=\"text-justified\">Significantly we come across the most unique challenge suffered by enterprise application is unlike customer applications, these applications are built, designed and dedicated to the internal business process or even for a specific group of people and for this requirement enterprise cannot really rely on IT platforms like apple app store, android market etc.. and here is where our one of the key focus is, that’s to create a platform to serve any requirement, any time providing a secure mechanism to distribute, update and manage custom applications.</p>\r\n              <p class=\"text-justified\">Many enterprises has acquired the services from our experienced distribution team and obtained futuristic benefits. Now it’s your turn too, let us know your requirements to get a free quote.</p>\r\n              <p class=\"text-justified\">Enterprise app distribution refers to the process of making your apps readily available to your enterprise people who need them. Apart from distribution, you can also upgrade and audit your apps in a powerful and safe manner. With the increasing demand for mobile devices and tablets, the popularity of enterprise apps is improving day by day. Such apps help employees work away from offices using their own preferred devices. However, these apps cannot be distributed normally through app stores. They are distributed privately to the people that are connected to your enterprise. You can email these apps to your team, upload them on to an internal server or distribute them through a third party service. The last option is the one that is referred to as enterprise app distribution.</p>\r\n          </div>\r\n          <div class=\"col-md-5\">\r\n              <img src=\"assets/img/industry/life_science_bigperl.png\" class=\"img-responsive \" alt=\"life_science_bigperl\">\r\n          </div>\r\n      </div>\r\n\r\n\r\n<app-footer5></app-footer5>\r\n           \r\n<app-footer2></app-footer2>\r\n    "

/***/ }),

/***/ "./src/app/Industry/life-science-and-health-care/life-science-and-health-care.component.ts":
/*!*************************************************************************************************!*\
  !*** ./src/app/Industry/life-science-and-health-care/life-science-and-health-care.component.ts ***!
  \*************************************************************************************************/
/*! exports provided: LifeScienceAndHealthCareComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LifeScienceAndHealthCareComponent", function() { return LifeScienceAndHealthCareComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var LifeScienceAndHealthCareComponent = /** @class */ (function () {
    function LifeScienceAndHealthCareComponent() {
    }
    LifeScienceAndHealthCareComponent.prototype.ngOnInit = function () {
    };
    LifeScienceAndHealthCareComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-life-science-and-health-care',
            template: __webpack_require__(/*! ./life-science-and-health-care.component.html */ "./src/app/Industry/life-science-and-health-care/life-science-and-health-care.component.html"),
            styles: [__webpack_require__(/*! ./life-science-and-health-care.component.css */ "./src/app/Industry/life-science-and-health-care/life-science-and-health-care.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], LifeScienceAndHealthCareComponent);
    return LifeScienceAndHealthCareComponent;
}());



/***/ }),

/***/ "./src/app/Industry/manufacturing/manufacturing.component.css":
/*!********************************************************************!*\
  !*** ./src/app/Industry/manufacturing/manufacturing.component.css ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/Industry/manufacturing/manufacturing.component.html":
/*!*********************************************************************!*\
  !*** ./src/app/Industry/manufacturing/manufacturing.component.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n        <!--page title start-->\r\n        <section class=\"page-title ptb-30\">\r\n                <div class=\"container padding-top-40\">\r\n                    <div class=\"row\">\r\n                        <div class=\"col-md-12\">\r\n                            <h6 class=\"pagetitles\">Industry</h6>\r\n                            <ol class=\"breadcrumb\">\r\n                                <li><a href=\"#\">Home</a></li>\r\n                                <li><a href=\"#\">industry</a></li>\r\n                                <li class=\"active\">manufacturing</li>\r\n                            </ol>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </section>\r\n            <!--page title end-->\r\n    \r\n                <img src=\"assets/img/industry/banner/manufacturing_banner_bigperl.png\" class=\"fullwidth\" alt=\"manufacturing_banner_bigperl\">\r\n    \r\n                  <div class=\"container padding-top-20\">\r\n                      <div class=\"col-md-7\">\r\n                          <h2 class=\"text-bold mb-10\">Architecture Solutions</h2>\r\n                          <p class=\"text-justified\">Bigperl solutions being one of the leading business application development company working towards bringing an enterprise application distribution over various other platforms like BlackBerry, Android, and Apple etc. By making it available to the customer with a click of a button. Our penetration into the smart devices towards customer markets has made relatively inexpensive and easier for integration.</p>\r\n                          <h5><b>What we offer?</b></h5>\r\n                          <p class=\"text-justified\">We not just work on bringing a platform to our consumers reaching them with a click of a button, but also our analysts and creative team work on building the most innovative and user friendly app, where we cater various enterprises the opportunity to distribute their various applications. We also upgrade our platform and applications with the new features and in addition, we audit and forecast the various feature expected by our customers in the near future. Thus we work purely on introducing and catering the best features to people making them readily available.</p>\r\n                          <h5><b>What makes BigPerl a unique platform?</b></h5>\r\n                          <p class=\"text-justified\">Significantly we come across the most unique challenge suffered by enterprise application is unlike customer applications, these applications are built, designed and dedicated to the internal business process or even for a specific group of people and for this requirement enterprise cannot really rely on IT platforms like apple app store, android market etc.. and here is where our one of the key focus is, that’s to create a platform to serve any requirement, any time providing a secure mechanism to distribute, update and manage custom applications.</p>\r\n                          <p class=\"text-justified\">Many enterprises has acquired the services from our experienced distribution team and obtained futuristic benefits. Now it’s your turn too, let us know your requirements to get a free quote.</p>\r\n                          <p class=\"text-justified\">Enterprise app distribution refers to the process of making your apps readily available to your enterprise people who need them. Apart from distribution, you can also upgrade and audit your apps in a powerful and safe manner. With the increasing demand for mobile devices and tablets, the popularity of enterprise apps is improving day by day. Such apps help employees work away from offices using their own preferred devices. However, these apps cannot be distributed normally through app stores. They are distributed privately to the people that are connected to your enterprise. You can email these apps to your team, upload them on to an internal server or distribute them through a third party service. The last option is the one that is referred to as enterprise app distribution.</p>\r\n                      </div>\r\n                      <div class=\"col-md-5\">\r\n                          <img src=\"assets/img/industry/manufacturing_bigperl.png\" class=\"img-responsive \" alt=\"manufacturing_bigperl\">\r\n                      </div>\r\n                  </div>\r\n    \r\n\r\n\r\n\r\n\r\n          <app-footer5></app-footer5>\r\n           \r\n          <app-footer2></app-footer2>\r\n              "

/***/ }),

/***/ "./src/app/Industry/manufacturing/manufacturing.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/Industry/manufacturing/manufacturing.component.ts ***!
  \*******************************************************************/
/*! exports provided: ManufacturingComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ManufacturingComponent", function() { return ManufacturingComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ManufacturingComponent = /** @class */ (function () {
    function ManufacturingComponent() {
    }
    ManufacturingComponent.prototype.ngOnInit = function () {
    };
    ManufacturingComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-manufacturing',
            template: __webpack_require__(/*! ./manufacturing.component.html */ "./src/app/Industry/manufacturing/manufacturing.component.html"),
            styles: [__webpack_require__(/*! ./manufacturing.component.css */ "./src/app/Industry/manufacturing/manufacturing.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], ManufacturingComponent);
    return ManufacturingComponent;
}());



/***/ }),

/***/ "./src/app/Industry/media-entertainment/media-entertainment.component.css":
/*!********************************************************************************!*\
  !*** ./src/app/Industry/media-entertainment/media-entertainment.component.css ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/Industry/media-entertainment/media-entertainment.component.html":
/*!*********************************************************************************!*\
  !*** ./src/app/Industry/media-entertainment/media-entertainment.component.html ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n        <!--page title start-->\r\n        <section class=\"page-title ptb-30\">\r\n                <div class=\"container padding-top-40\">\r\n                    <div class=\"row\">\r\n                        <div class=\"col-md-12\">\r\n                            <h6 class=\"pagetitles\">Industry</h6>\r\n                            <ol class=\"breadcrumb\">\r\n                                <li><a href=\"#\">Home</a></li>\r\n                                <li><a href=\"#\">industry</a></li>\r\n                                <li class=\"active\">media entertainment</li>\r\n                            </ol>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </section>\r\n            <!--page title end-->\r\n    \r\n                <img src=\"assets/img/industry/banner/media_industry_bigperl.png\" class=\"fullwidth\" alt=\"media_industry_bigperl\">\r\n    \r\n                  <div class=\"container padding-top-20\">\r\n                      <div class=\"col-md-7\">\r\n                          <h2 class=\"text-bold mb-10\">Architecture Solutions</h2>\r\n                          <p class=\"text-justified\">Bigperl solutions being one of the leading business application development company working towards bringing an enterprise application distribution over various other platforms like BlackBerry, Android, and Apple etc. By making it available to the customer with a click of a button. Our penetration into the smart devices towards customer markets has made relatively inexpensive and easier for integration.</p>\r\n                          <h5><b>What we offer?</b></h5>\r\n                          <p class=\"text-justified\">We not just work on bringing a platform to our consumers reaching them with a click of a button, but also our analysts and creative team work on building the most innovative and user friendly app, where we cater various enterprises the opportunity to distribute their various applications. We also upgrade our platform and applications with the new features and in addition, we audit and forecast the various feature expected by our customers in the near future. Thus we work purely on introducing and catering the best features to people making them readily available.</p>\r\n                          <h5><b>What makes BigPerl a unique platform?</b></h5>\r\n                          <p class=\"text-justified\">Significantly we come across the most unique challenge suffered by enterprise application is unlike customer applications, these applications are built, designed and dedicated to the internal business process or even for a specific group of people and for this requirement enterprise cannot really rely on IT platforms like apple app store, android market etc.. and here is where our one of the key focus is, that’s to create a platform to serve any requirement, any time providing a secure mechanism to distribute, update and manage custom applications.</p>\r\n                          <p class=\"text-justified\">Many enterprises has acquired the services from our experienced distribution team and obtained futuristic benefits. Now it’s your turn too, let us know your requirements to get a free quote.</p>\r\n                          <p class=\"text-justified\">Enterprise app distribution refers to the process of making your apps readily available to your enterprise people who need them. Apart from distribution, you can also upgrade and audit your apps in a powerful and safe manner. With the increasing demand for mobile devices and tablets, the popularity of enterprise apps is improving day by day. Such apps help employees work away from offices using their own preferred devices. However, these apps cannot be distributed normally through app stores. They are distributed privately to the people that are connected to your enterprise. You can email these apps to your team, upload them on to an internal server or distribute them through a third party service. The last option is the one that is referred to as enterprise app distribution.</p>\r\n                      </div>\r\n                      <div class=\"col-md-5\">\r\n                          <img src=\"assets/img/industry/entertainment_bigperl.png\" class=\"img-responsive \" alt=\"entertainment_bigperl\">\r\n                      </div>\r\n                  </div>\r\n    \r\n\r\n\r\n\r\n          <app-footer5></app-footer5>\r\n           \r\n          <app-footer2></app-footer2>\r\n              "

/***/ }),

/***/ "./src/app/Industry/media-entertainment/media-entertainment.component.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/Industry/media-entertainment/media-entertainment.component.ts ***!
  \*******************************************************************************/
/*! exports provided: MediaEntertainmentComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MediaEntertainmentComponent", function() { return MediaEntertainmentComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var MediaEntertainmentComponent = /** @class */ (function () {
    function MediaEntertainmentComponent() {
    }
    MediaEntertainmentComponent.prototype.ngOnInit = function () {
    };
    MediaEntertainmentComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-media-entertainment',
            template: __webpack_require__(/*! ./media-entertainment.component.html */ "./src/app/Industry/media-entertainment/media-entertainment.component.html"),
            styles: [__webpack_require__(/*! ./media-entertainment.component.css */ "./src/app/Industry/media-entertainment/media-entertainment.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], MediaEntertainmentComponent);
    return MediaEntertainmentComponent;
}());



/***/ }),

/***/ "./src/app/Industry/real-estate/real-estate.component.css":
/*!****************************************************************!*\
  !*** ./src/app/Industry/real-estate/real-estate.component.css ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/Industry/real-estate/real-estate.component.html":
/*!*****************************************************************!*\
  !*** ./src/app/Industry/real-estate/real-estate.component.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n\r\n        <!--page title start-->\r\n        <section class=\"page-title ptb-30\">\r\n                <div class=\"container padding-top-40\">\r\n                    <div class=\"row\">\r\n                        <div class=\"col-md-12\">\r\n                            <h6 class=\"pagetitles\">Services</h6>\r\n                            <ol class=\"breadcrumb\">\r\n                                <li><a href=\"#\">Home</a></li>\r\n                                <li><a href=\"#\">industry</a></li>\r\n                                <li class=\"active\">real estate</li>\r\n                            </ol>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </section>\r\n            <!--page title end-->\r\n    \r\n                <img src=\"assets/img/industry/banner/real_estate_bigperl.png\" class=\"fullwidth\" alt=\"real_estate_bigperl\">\r\n    \r\n                  <div class=\"container padding-top-20\">\r\n                      <div class=\"col-md-7\">\r\n                          <h2 class=\"text-bold mb-10\">Architecture Solutions</h2>\r\n                          <p class=\"text-justified\">Bigperl solutions being one of the leading business application development company working towards bringing an enterprise application distribution over various other platforms like BlackBerry, Android, and Apple etc. By making it available to the customer with a click of a button. Our penetration into the smart devices towards customer markets has made relatively inexpensive and easier for integration.</p>\r\n                          <h5><b>What we offer?</b></h5>\r\n                          <p class=\"text-justified\">We not just work on bringing a platform to our consumers reaching them with a click of a button, but also our analysts and creative team work on building the most innovative and user friendly app, where we cater various enterprises the opportunity to distribute their various applications. We also upgrade our platform and applications with the new features and in addition, we audit and forecast the various feature expected by our customers in the near future. Thus we work purely on introducing and catering the best features to people making them readily available.</p>\r\n                          <h5><b>What makes BigPerl a unique platform?</b></h5>\r\n                          <p class=\"text-justified\">Significantly we come across the most unique challenge suffered by enterprise application is unlike customer applications, these applications are built, designed and dedicated to the internal business process or even for a specific group of people and for this requirement enterprise cannot really rely on IT platforms like apple app store, android market etc.. and here is where our one of the key focus is, that’s to create a platform to serve any requirement, any time providing a secure mechanism to distribute, update and manage custom applications.</p>\r\n                          <p class=\"text-justified\">Many enterprises has acquired the services from our experienced distribution team and obtained futuristic benefits. Now it’s your turn too, let us know your requirements to get a free quote.</p>\r\n                          <p class=\"text-justified\">Enterprise app distribution refers to the process of making your apps readily available to your enterprise people who need them. Apart from distribution, you can also upgrade and audit your apps in a powerful and safe manner. With the increasing demand for mobile devices and tablets, the popularity of enterprise apps is improving day by day. Such apps help employees work away from offices using their own preferred devices. However, these apps cannot be distributed normally through app stores. They are distributed privately to the people that are connected to your enterprise. You can email these apps to your team, upload them on to an internal server or distribute them through a third party service. The last option is the one that is referred to as enterprise app distribution.</p>\r\n                      </div>\r\n                      <div class=\"col-md-5\">\r\n                          <img src=\"assets/img/industry/real_eastate_bigperl.png\" class=\"img-responsive \" alt=\"real_eastate_bigperl\">\r\n                      </div>\r\n                  </div>\r\n    \r\n\r\n\r\n          <app-footer5></app-footer5>\r\n           \r\n          <app-footer2></app-footer2>\r\n              "

/***/ }),

/***/ "./src/app/Industry/real-estate/real-estate.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/Industry/real-estate/real-estate.component.ts ***!
  \***************************************************************/
/*! exports provided: RealEstateComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RealEstateComponent", function() { return RealEstateComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var RealEstateComponent = /** @class */ (function () {
    function RealEstateComponent() {
    }
    RealEstateComponent.prototype.ngOnInit = function () {
    };
    RealEstateComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-real-estate',
            template: __webpack_require__(/*! ./real-estate.component.html */ "./src/app/Industry/real-estate/real-estate.component.html"),
            styles: [__webpack_require__(/*! ./real-estate.component.css */ "./src/app/Industry/real-estate/real-estate.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], RealEstateComponent);
    return RealEstateComponent;
}());



/***/ }),

/***/ "./src/app/Industry/retail-and-consumer/retail-and-consumer.component.css":
/*!********************************************************************************!*\
  !*** ./src/app/Industry/retail-and-consumer/retail-and-consumer.component.css ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/Industry/retail-and-consumer/retail-and-consumer.component.html":
/*!*********************************************************************************!*\
  !*** ./src/app/Industry/retail-and-consumer/retail-and-consumer.component.html ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n        <!--page title start-->\r\n        <section class=\"page-title ptb-30\">\r\n                <div class=\"container padding-top-40\">\r\n                    <div class=\"row\">\r\n                        <div class=\"col-md-12\">\r\n                            <h6 class=\"pagetitles\">Industry</h6>\r\n                            <ol class=\"breadcrumb\">\r\n                                <li><a href=\"#\">Home</a></li>\r\n                                <li><a href=\"#\">industry</a></li>\r\n                                <li class=\"active\">retail and consumer</li>\r\n                            </ol>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </section>\r\n            <!--page title end-->\r\n    \r\n                <img src=\"assets/img/industry/banner/retail_banner_bigperl.png\" class=\"fullwidth\" alt=\"retail_banner_bigperl\">\r\n    \r\n                  <div class=\"container padding-top-20\">\r\n                      <div class=\"col-md-7\">\r\n                          <h2 class=\"text-bold mb-10\">Architecture Solutions</h2>\r\n                          <p class=\"text-justified\">Bigperl solutions being one of the leading business application development company working towards bringing an enterprise application distribution over various other platforms like BlackBerry, Android, and Apple etc. By making it available to the customer with a click of a button. Our penetration into the smart devices towards customer markets has made relatively inexpensive and easier for integration.</p>\r\n                          <h5><b>What we offer?</b></h5>\r\n                          <p class=\"text-justified\">We not just work on bringing a platform to our consumers reaching them with a click of a button, but also our analysts and creative team work on building the most innovative and user friendly app, where we cater various enterprises the opportunity to distribute their various applications. We also upgrade our platform and applications with the new features and in addition, we audit and forecast the various feature expected by our customers in the near future. Thus we work purely on introducing and catering the best features to people making them readily available.</p>\r\n                          <h5><b>What makes BigPerl a unique platform?</b></h5>\r\n                          <p class=\"text-justified\">Significantly we come across the most unique challenge suffered by enterprise application is unlike customer applications, these applications are built, designed and dedicated to the internal business process or even for a specific group of people and for this requirement enterprise cannot really rely on IT platforms like apple app store, android market etc.. and here is where our one of the key focus is, that’s to create a platform to serve any requirement, any time providing a secure mechanism to distribute, update and manage custom applications.</p>\r\n                          <p class=\"text-justified\">Many enterprises has acquired the services from our experienced distribution team and obtained futuristic benefits. Now it’s your turn too, let us know your requirements to get a free quote.</p>\r\n                          <p class=\"text-justified\">Enterprise app distribution refers to the process of making your apps readily available to your enterprise people who need them. Apart from distribution, you can also upgrade and audit your apps in a powerful and safe manner. With the increasing demand for mobile devices and tablets, the popularity of enterprise apps is improving day by day. Such apps help employees work away from offices using their own preferred devices. However, these apps cannot be distributed normally through app stores. They are distributed privately to the people that are connected to your enterprise. You can email these apps to your team, upload them on to an internal server or distribute them through a third party service. The last option is the one that is referred to as enterprise app distribution.</p>\r\n                      </div>\r\n                      <div class=\"col-md-5\">\r\n                          <img src=\"assets/img/industry/real_eastate_bigperl.png\" class=\"img-responsive \" alt=\"real_eastate_bigperl\">\r\n                      </div>\r\n                  </div>\r\n    \r\n            \r\n\r\n\r\n\r\n          <app-footer5></app-footer5>\r\n           \r\n          <app-footer2></app-footer2>\r\n              "

/***/ }),

/***/ "./src/app/Industry/retail-and-consumer/retail-and-consumer.component.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/Industry/retail-and-consumer/retail-and-consumer.component.ts ***!
  \*******************************************************************************/
/*! exports provided: RetailAndConsumerComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RetailAndConsumerComponent", function() { return RetailAndConsumerComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var RetailAndConsumerComponent = /** @class */ (function () {
    function RetailAndConsumerComponent() {
    }
    RetailAndConsumerComponent.prototype.ngOnInit = function () {
    };
    RetailAndConsumerComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-retail-and-consumer',
            template: __webpack_require__(/*! ./retail-and-consumer.component.html */ "./src/app/Industry/retail-and-consumer/retail-and-consumer.component.html"),
            styles: [__webpack_require__(/*! ./retail-and-consumer.component.css */ "./src/app/Industry/retail-and-consumer/retail-and-consumer.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], RetailAndConsumerComponent);
    return RetailAndConsumerComponent;
}());



/***/ }),

/***/ "./src/app/Industry/social-computing/social-computing.component.css":
/*!**************************************************************************!*\
  !*** ./src/app/Industry/social-computing/social-computing.component.css ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/Industry/social-computing/social-computing.component.html":
/*!***************************************************************************!*\
  !*** ./src/app/Industry/social-computing/social-computing.component.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n\r\n        <!--page title start-->\r\n        <section class=\"page-title ptb-30\">\r\n                <div class=\"container padding-top-40\">\r\n                    <div class=\"row\">\r\n                        <div class=\"col-md-12\">\r\n                            <h6 class=\"pagetitles\">Industry</h6>\r\n                            <ol class=\"breadcrumb\">\r\n                                <li><a href=\"#\">Home</a></li>\r\n                                <li><a href=\"#\">industry</a></li>\r\n                                <li class=\"active\">social computing</li>\r\n                            </ol>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </section>\r\n            <!--page title end-->\r\n    \r\n                <img src=\"assets/img/industry/banner/digital_marketing_banner_bigperl.png\" class=\"fullwidth\" alt=\"digital_marketing_banner_bigperl\">\r\n    \r\n                  <div class=\"container padding-top-20\">\r\n                      <div class=\"col-md-7\">\r\n                          <h2 class=\"text-bold mb-10\">Architecture Solutions</h2>\r\n                          <p class=\"text-justified\">Bigperl solutions being one of the leading business application development company working towards bringing an enterprise application distribution over various other platforms like BlackBerry, Android, and Apple etc. By making it available to the customer with a click of a button. Our penetration into the smart devices towards customer markets has made relatively inexpensive and easier for integration.</p>\r\n                          <h5><b>What we offer?</b></h5>\r\n                          <p class=\"text-justified\">We not just work on bringing a platform to our consumers reaching them with a click of a button, but also our analysts and creative team work on building the most innovative and user friendly app, where we cater various enterprises the opportunity to distribute their various applications. We also upgrade our platform and applications with the new features and in addition, we audit and forecast the various feature expected by our customers in the near future. Thus we work purely on introducing and catering the best features to people making them readily available.</p>\r\n                          <h5><b>What makes BigPerl a unique platform?</b></h5>\r\n                          <p class=\"text-justified\">Significantly we come across the most unique challenge suffered by enterprise application is unlike customer applications, these applications are built, designed and dedicated to the internal business process or even for a specific group of people and for this requirement enterprise cannot really rely on IT platforms like apple app store, android market etc.. and here is where our one of the key focus is, that’s to create a platform to serve any requirement, any time providing a secure mechanism to distribute, update and manage custom applications.</p>\r\n                          <p class=\"text-justified\">Many enterprises has acquired the services from our experienced distribution team and obtained futuristic benefits. Now it’s your turn too, let us know your requirements to get a free quote.</p>\r\n                          <p class=\"text-justified\">Enterprise app distribution refers to the process of making your apps readily available to your enterprise people who need them. Apart from distribution, you can also upgrade and audit your apps in a powerful and safe manner. With the increasing demand for mobile devices and tablets, the popularity of enterprise apps is improving day by day. Such apps help employees work away from offices using their own preferred devices. However, these apps cannot be distributed normally through app stores. They are distributed privately to the people that are connected to your enterprise. You can email these apps to your team, upload them on to an internal server or distribute them through a third party service. The last option is the one that is referred to as enterprise app distribution.</p>\r\n                      </div>\r\n                      <div class=\"col-md-5\">\r\n                          <img src=\"assets/img/industry/social_computing_bigperl.png\" class=\"img-responsive \" alt=\"social_computing_bigperl\">\r\n                      </div>\r\n                  </div>\r\n    \r\n            \r\n\r\n\r\n          <app-footer5></app-footer5>\r\n           \r\n          <app-footer2></app-footer2>\r\n              "

/***/ }),

/***/ "./src/app/Industry/social-computing/social-computing.component.ts":
/*!*************************************************************************!*\
  !*** ./src/app/Industry/social-computing/social-computing.component.ts ***!
  \*************************************************************************/
/*! exports provided: SocialComputingComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SocialComputingComponent", function() { return SocialComputingComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var SocialComputingComponent = /** @class */ (function () {
    function SocialComputingComponent() {
    }
    SocialComputingComponent.prototype.ngOnInit = function () {
    };
    SocialComputingComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-social-computing',
            template: __webpack_require__(/*! ./social-computing.component.html */ "./src/app/Industry/social-computing/social-computing.component.html"),
            styles: [__webpack_require__(/*! ./social-computing.component.css */ "./src/app/Industry/social-computing/social-computing.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], SocialComputingComponent);
    return SocialComputingComponent;
}());



/***/ }),

/***/ "./src/app/Industry/telecom/telecom.component.css":
/*!********************************************************!*\
  !*** ./src/app/Industry/telecom/telecom.component.css ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/Industry/telecom/telecom.component.html":
/*!*********************************************************!*\
  !*** ./src/app/Industry/telecom/telecom.component.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n\r\n\r\n        <!--page title start-->\r\n        <section class=\"page-title ptb-30\">\r\n                <div class=\"container padding-top-40\">\r\n                    <div class=\"row\">\r\n                        <div class=\"col-md-12\">\r\n                            <h6 class=\"pagetitles\">Industry</h6>\r\n                            <ol class=\"breadcrumb\">\r\n                                <li><a href=\"#\">Home</a></li>\r\n                                <li><a href=\"#\">industry</a></li>\r\n                                <li class=\"active\">telecom</li>\r\n                            </ol>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </section>\r\n            <!--page title end-->\r\n    \r\n                <img src=\"assets/img/industry/banner/telecom_banner_bigperl.png\" class=\"fullwidth\" alt=\"telecom_banner_bigperl\">\r\n    \r\n                  <div class=\"container padding-top-20\">\r\n                      <div class=\"col-md-7\">\r\n                          <h2 class=\"text-bold mb-10\">Architecture Solutions</h2>\r\n                          <p class=\"text-justified\">Bigperl solutions being one of the leading business application development company working towards bringing an enterprise application distribution over various other platforms like BlackBerry, Android, and Apple etc. By making it available to the customer with a click of a button. Our penetration into the smart devices towards customer markets has made relatively inexpensive and easier for integration.</p>\r\n                          <h5><b>What we offer?</b></h5>\r\n                          <p class=\"text-justified\">We not just work on bringing a platform to our consumers reaching them with a click of a button, but also our analysts and creative team work on building the most innovative and user friendly app, where we cater various enterprises the opportunity to distribute their various applications. We also upgrade our platform and applications with the new features and in addition, we audit and forecast the various feature expected by our customers in the near future. Thus we work purely on introducing and catering the best features to people making them readily available.</p>\r\n                          <h5><b>What makes BigPerl a unique platform?</b></h5>\r\n                          <p class=\"text-justified\">Significantly we come across the most unique challenge suffered by enterprise application is unlike customer applications, these applications are built, designed and dedicated to the internal business process or even for a specific group of people and for this requirement enterprise cannot really rely on IT platforms like apple app store, android market etc.. and here is where our one of the key focus is, that’s to create a platform to serve any requirement, any time providing a secure mechanism to distribute, update and manage custom applications.</p>\r\n                          <p class=\"text-justified\">Many enterprises has acquired the services from our experienced distribution team and obtained futuristic benefits. Now it’s your turn too, let us know your requirements to get a free quote.</p>\r\n                          <p class=\"text-justified\">Enterprise app distribution refers to the process of making your apps readily available to your enterprise people who need them. Apart from distribution, you can also upgrade and audit your apps in a powerful and safe manner. With the increasing demand for mobile devices and tablets, the popularity of enterprise apps is improving day by day. Such apps help employees work away from offices using their own preferred devices. However, these apps cannot be distributed normally through app stores. They are distributed privately to the people that are connected to your enterprise. You can email these apps to your team, upload them on to an internal server or distribute them through a third party service. The last option is the one that is referred to as enterprise app distribution.</p>\r\n                      </div>\r\n                      <div class=\"col-md-5\">\r\n                          <img src=\"assets/img/industry/telecom_bigperl.png\" class=\"img-responsive \" alt=\"telecom_bigperl\">\r\n                      </div>\r\n                  </div>\r\n    \r\n\r\n          <app-footer5></app-footer5>\r\n           \r\n          <app-footer2></app-footer2>\r\n              \r\n"

/***/ }),

/***/ "./src/app/Industry/telecom/telecom.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/Industry/telecom/telecom.component.ts ***!
  \*******************************************************/
/*! exports provided: TelecomComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TelecomComponent", function() { return TelecomComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var TelecomComponent = /** @class */ (function () {
    function TelecomComponent() {
    }
    TelecomComponent.prototype.ngOnInit = function () {
    };
    TelecomComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-telecom',
            template: __webpack_require__(/*! ./telecom.component.html */ "./src/app/Industry/telecom/telecom.component.html"),
            styles: [__webpack_require__(/*! ./telecom.component.css */ "./src/app/Industry/telecom/telecom.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], TelecomComponent);
    return TelecomComponent;
}());



/***/ }),

/***/ "./src/app/Industry/travel-transport-log/travel-transport-log.component.css":
/*!**********************************************************************************!*\
  !*** ./src/app/Industry/travel-transport-log/travel-transport-log.component.css ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/Industry/travel-transport-log/travel-transport-log.component.html":
/*!***********************************************************************************!*\
  !*** ./src/app/Industry/travel-transport-log/travel-transport-log.component.html ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n        <!--page title start-->\r\n        <section class=\"page-title ptb-30\">\r\n                <div class=\"container padding-top-40\">\r\n                    <div class=\"row\">\r\n                        <div class=\"col-md-12\">\r\n                            <h6 class=\"pagetitles\">Industry</h6>\r\n                            <ol class=\"breadcrumb\">\r\n                                <li><a href=\"#\">Home</a></li>\r\n                                <li><a href=\"#\">industry</a></li>\r\n                                <li class=\"active\">travel transport log</li>\r\n                            </ol>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </section>\r\n            <!--page title end-->\r\n    \r\n                <img src=\"assets/img/industry/banner/travel_banner_bigperl.png\" class=\"fullwidth\" alt=\"travel_banner_bigperl\">\r\n    \r\n                  <div class=\"container padding-top-20\">\r\n                      <div class=\"col-md-7\">\r\n                          <h2 class=\"text-bold mb-10\">Architecture Solutions</h2>\r\n                          <p class=\"text-justified\">Bigperl solutions being one of the leading business application development company working towards bringing an enterprise application distribution over various other platforms like BlackBerry, Android, and Apple etc. By making it available to the customer with a click of a button. Our penetration into the smart devices towards customer markets has made relatively inexpensive and easier for integration.</p>\r\n                          <h5><b>What we offer?</b></h5>\r\n                          <p class=\"text-justified\">We not just work on bringing a platform to our consumers reaching them with a click of a button, but also our analysts and creative team work on building the most innovative and user friendly app, where we cater various enterprises the opportunity to distribute their various applications. We also upgrade our platform and applications with the new features and in addition, we audit and forecast the various feature expected by our customers in the near future. Thus we work purely on introducing and catering the best features to people making them readily available.</p>\r\n                          <h5><b>What makes BigPerl a unique platform?</b></h5>\r\n                          <p class=\"text-justified\">Significantly we come across the most unique challenge suffered by enterprise application is unlike customer applications, these applications are built, designed and dedicated to the internal business process or even for a specific group of people and for this requirement enterprise cannot really rely on IT platforms like apple app store, android market etc.. and here is where our one of the key focus is, that’s to create a platform to serve any requirement, any time providing a secure mechanism to distribute, update and manage custom applications.</p>\r\n                          <p class=\"text-justified\">Many enterprises has acquired the services from our experienced distribution team and obtained futuristic benefits. Now it’s your turn too, let us know your requirements to get a free quote.</p>\r\n                          <p class=\"text-justified\">Enterprise app distribution refers to the process of making your apps readily available to your enterprise people who need them. Apart from distribution, you can also upgrade and audit your apps in a powerful and safe manner. With the increasing demand for mobile devices and tablets, the popularity of enterprise apps is improving day by day. Such apps help employees work away from offices using their own preferred devices. However, these apps cannot be distributed normally through app stores. They are distributed privately to the people that are connected to your enterprise. You can email these apps to your team, upload them on to an internal server or distribute them through a third party service. The last option is the one that is referred to as enterprise app distribution.</p>\r\n                      </div>\r\n                      <div class=\"col-md-5\">\r\n                          <img src=\"assets/img/industry/transport_bigperl.png\" class=\"img-responsive \" alt=\"transport_bigperl\">\r\n                      </div>\r\n                  </div>\r\n\r\n\r\n\r\n          <app-footer5></app-footer5>\r\n           \r\n          <app-footer2></app-footer2>\r\n              "

/***/ }),

/***/ "./src/app/Industry/travel-transport-log/travel-transport-log.component.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/Industry/travel-transport-log/travel-transport-log.component.ts ***!
  \*********************************************************************************/
/*! exports provided: TravelTransportLogComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TravelTransportLogComponent", function() { return TravelTransportLogComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var TravelTransportLogComponent = /** @class */ (function () {
    function TravelTransportLogComponent() {
    }
    TravelTransportLogComponent.prototype.ngOnInit = function () {
    };
    TravelTransportLogComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-travel-transport-log',
            template: __webpack_require__(/*! ./travel-transport-log.component.html */ "./src/app/Industry/travel-transport-log/travel-transport-log.component.html"),
            styles: [__webpack_require__(/*! ./travel-transport-log.component.css */ "./src/app/Industry/travel-transport-log/travel-transport-log.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], TravelTransportLogComponent);
    return TravelTransportLogComponent;
}());



/***/ }),

/***/ "./src/app/Products/architechtural-network/architechtural-network.component.css":
/*!**************************************************************************************!*\
  !*** ./src/app/Products/architechtural-network/architechtural-network.component.css ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/Products/architechtural-network/architechtural-network.component.html":
/*!***************************************************************************************!*\
  !*** ./src/app/Products/architechtural-network/architechtural-network.component.html ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n\r\n        <!--page title start-->\r\n        <section class=\"page-title ptb-30\">\r\n                <div class=\"container padding-top-40\">\r\n                    <div class=\"row\">\r\n                        <div class=\"col-md-12\">\r\n                            <h6 class=\"pagetitles\">Services</h6>\r\n                            <ol class=\"breadcrumb\">\r\n                                <li><a href=\"#\">Home</a></li>\r\n                                <li><a href=\"#\">products</a></li>\r\n                                <li class=\"active\">architectural network</li>\r\n                            </ol>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </section>\r\n            <!--page title end-->\r\n    \r\n                <img src=\"assets/img/products/banner/architectural_network_bigperl.png\" class=\"fullwidth\" alt=\"architectural_network_bigperl\">\r\n    \r\n                  <div class=\"container padding-top-20\">\r\n                      <div class=\"col-md-7\">\r\n                          <h2 class=\"text-bold mb-10\">Architectural network</h2>\r\n                          \r\n                      </div>\r\n                      <div class=\"col-md-5\">\r\n                          <img src=\"assets/img/products/architectural_network_bigperl.png\" class=\"img-responsive \" alt=\"architectural_network_bigperl\">\r\n                      </div>\r\n                  </div>\r\n    \r\n            \r\n    \r\n<app-footer5></app-footer5>\r\n           \r\n<app-footer2></app-footer2>"

/***/ }),

/***/ "./src/app/Products/architechtural-network/architechtural-network.component.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/Products/architechtural-network/architechtural-network.component.ts ***!
  \*************************************************************************************/
/*! exports provided: ArchitechturalNetworkComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ArchitechturalNetworkComponent", function() { return ArchitechturalNetworkComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ArchitechturalNetworkComponent = /** @class */ (function () {
    function ArchitechturalNetworkComponent() {
    }
    ArchitechturalNetworkComponent.prototype.ngOnInit = function () {
    };
    ArchitechturalNetworkComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-architechtural-network',
            template: __webpack_require__(/*! ./architechtural-network.component.html */ "./src/app/Products/architechtural-network/architechtural-network.component.html"),
            styles: [__webpack_require__(/*! ./architechtural-network.component.css */ "./src/app/Products/architechtural-network/architechtural-network.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], ArchitechturalNetworkComponent);
    return ArchitechturalNetworkComponent;
}());



/***/ }),

/***/ "./src/app/Products/construction-suppliers/construction-suppliers.component.css":
/*!**************************************************************************************!*\
  !*** ./src/app/Products/construction-suppliers/construction-suppliers.component.css ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/Products/construction-suppliers/construction-suppliers.component.html":
/*!***************************************************************************************!*\
  !*** ./src/app/Products/construction-suppliers/construction-suppliers.component.html ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n        <!--page title start-->\r\n        <section class=\"page-title ptb-30\">\r\n                <div class=\"container padding-top-40\">\r\n                    <div class=\"row\">\r\n                        <div class=\"col-md-12\">\r\n                            <h6 class=\"pagetitles\">Services</h6>\r\n                            <ol class=\"breadcrumb\">\r\n                                <li><a href=\"#\">Home</a></li>\r\n                                <li><a href=\"#\">products</a></li>\r\n                                <li class=\"active\">construction suppliers network</li>\r\n                            </ol>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </section>\r\n            <!--page title end-->\r\n    \r\n                <img src=\"assets/img/products/banner/constructor_supplier_bigperl.png\" class=\"fullwidth\" alt=\"constructor_supplier_bigperl\">\r\n    \r\n                  <div class=\"container padding-top-20\">\r\n                      <div class=\"col-md-7\">\r\n                          <h2 class=\"text-bold mb-10\">Construction suppliers network</h2>\r\n                          \r\n                      </div>\r\n                      <div class=\"col-md-5\">\r\n                          <img src=\"assets/img/products/construction_supplier_network_bigperl.png\" class=\"img-responsive \" alt=\"construction_supplier_network_bigperl\">\r\n                      </div>\r\n                  </div>\r\n           \r\n    \r\n    \r\n            \r\n    \r\n\r\n<app-footer5></app-footer5>\r\n           \r\n<app-footer2></app-footer2>"

/***/ }),

/***/ "./src/app/Products/construction-suppliers/construction-suppliers.component.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/Products/construction-suppliers/construction-suppliers.component.ts ***!
  \*************************************************************************************/
/*! exports provided: ConstructionSuppliersComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConstructionSuppliersComponent", function() { return ConstructionSuppliersComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ConstructionSuppliersComponent = /** @class */ (function () {
    function ConstructionSuppliersComponent() {
    }
    ConstructionSuppliersComponent.prototype.ngOnInit = function () {
    };
    ConstructionSuppliersComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-construction-suppliers',
            template: __webpack_require__(/*! ./construction-suppliers.component.html */ "./src/app/Products/construction-suppliers/construction-suppliers.component.html"),
            styles: [__webpack_require__(/*! ./construction-suppliers.component.css */ "./src/app/Products/construction-suppliers/construction-suppliers.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], ConstructionSuppliersComponent);
    return ConstructionSuppliersComponent;
}());



/***/ }),

/***/ "./src/app/Products/enterprice-crm-sales/enterprice-crm-sales.component.css":
/*!**********************************************************************************!*\
  !*** ./src/app/Products/enterprice-crm-sales/enterprice-crm-sales.component.css ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/Products/enterprice-crm-sales/enterprice-crm-sales.component.html":
/*!***********************************************************************************!*\
  !*** ./src/app/Products/enterprice-crm-sales/enterprice-crm-sales.component.html ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n        <!--page title start-->\r\n        <section class=\"page-title ptb-30\">\r\n                <div class=\"container padding-top-40\">\r\n                    <div class=\"row\">\r\n                        <div class=\"col-md-12\">\r\n                            <h6 class=\"pagetitles\">Services</h6>\r\n                            <ol class=\"breadcrumb\">\r\n                                <li><a href=\"#\">Home</a></li>\r\n                                <li><a href=\"#\">products</a></li>\r\n                                <li class=\"active\">enterprise crm sales</li>\r\n                            </ol>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </section>\r\n            <!--page title end-->\r\n    \r\n                <img src=\"assets/img/products/banner/crm_sales_bigperl.png\" class=\"fullwidth\" alt=\"crm_sales_bigperl\">\r\n    \r\n                  <div class=\"container padding-top-20\">\r\n                      <div class=\"col-md-7\">\r\n                          <h2 class=\"text-bold mb-10\">Enterprise CRM Sales</h2>\r\n                          \r\n                      </div>\r\n                      <div class=\"col-md-5\">\r\n                          <img src=\"assets/img/products/crm_sales_bigperl.png\" class=\"img-responsive \" alt=\"crm_sales_bigperl\">\r\n                      </div>\r\n                  </div>\r\n    \r\n            \r\n\r\n<app-footer5></app-footer5>\r\n           \r\n<app-footer2></app-footer2>"

/***/ }),

/***/ "./src/app/Products/enterprice-crm-sales/enterprice-crm-sales.component.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/Products/enterprice-crm-sales/enterprice-crm-sales.component.ts ***!
  \*********************************************************************************/
/*! exports provided: EnterpriceCrmSalesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EnterpriceCrmSalesComponent", function() { return EnterpriceCrmSalesComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var EnterpriceCrmSalesComponent = /** @class */ (function () {
    function EnterpriceCrmSalesComponent() {
    }
    EnterpriceCrmSalesComponent.prototype.ngOnInit = function () {
    };
    EnterpriceCrmSalesComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-enterprice-crm-sales',
            template: __webpack_require__(/*! ./enterprice-crm-sales.component.html */ "./src/app/Products/enterprice-crm-sales/enterprice-crm-sales.component.html"),
            styles: [__webpack_require__(/*! ./enterprice-crm-sales.component.css */ "./src/app/Products/enterprice-crm-sales/enterprice-crm-sales.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], EnterpriceCrmSalesComponent);
    return EnterpriceCrmSalesComponent;
}());



/***/ }),

/***/ "./src/app/Products/enterprice-crm-support/enterprice-crm-support.component.css":
/*!**************************************************************************************!*\
  !*** ./src/app/Products/enterprice-crm-support/enterprice-crm-support.component.css ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/Products/enterprice-crm-support/enterprice-crm-support.component.html":
/*!***************************************************************************************!*\
  !*** ./src/app/Products/enterprice-crm-support/enterprice-crm-support.component.html ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n        <!--page title start-->\r\n        <section class=\"page-title ptb-30\">\r\n                <div class=\"container padding-top-40\">\r\n                    <div class=\"row\">\r\n                        <div class=\"col-md-12\">\r\n                            <h6 class=\"pagetitles\">Services</h6>\r\n                            <ol class=\"breadcrumb\">\r\n                                <li><a href=\"#\">Home</a></li>\r\n                                <li><a href=\"#\">products</a></li>\r\n                                <li class=\"active\">enterprise crm support</li>\r\n                            </ol>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </section>\r\n            <!--page title end-->\r\n    \r\n                <img src=\"assets/img/products/banner/crm_product_bigperl.png\" class=\"fullwidth\" alt=\"crm_product_bigperl\">\r\n    \r\n                  <div class=\"container padding-top-20\">\r\n                      <div class=\"col-md-7\">\r\n                          <h2 class=\"text-bold mb-10\">Enterprise CRM Support</h2>\r\n                          \r\n                      </div>\r\n                      <div class=\"col-md-5\">\r\n                          <img src=\"assets/img/products/crm_support_bigperl.png\" class=\"img-responsive \" alt=\"crm_support_bigperl\">\r\n                      </div>\r\n                  </div>\r\n    \r\n            \r\n    \r\n\r\n<app-footer5></app-footer5>\r\n           \r\n<app-footer2></app-footer2>"

/***/ }),

/***/ "./src/app/Products/enterprice-crm-support/enterprice-crm-support.component.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/Products/enterprice-crm-support/enterprice-crm-support.component.ts ***!
  \*************************************************************************************/
/*! exports provided: EnterpriceCrmSupportComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EnterpriceCrmSupportComponent", function() { return EnterpriceCrmSupportComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var EnterpriceCrmSupportComponent = /** @class */ (function () {
    function EnterpriceCrmSupportComponent() {
    }
    EnterpriceCrmSupportComponent.prototype.ngOnInit = function () {
    };
    EnterpriceCrmSupportComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-enterprice-crm-support',
            template: __webpack_require__(/*! ./enterprice-crm-support.component.html */ "./src/app/Products/enterprice-crm-support/enterprice-crm-support.component.html"),
            styles: [__webpack_require__(/*! ./enterprice-crm-support.component.css */ "./src/app/Products/enterprice-crm-support/enterprice-crm-support.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], EnterpriceCrmSupportComponent);
    return EnterpriceCrmSupportComponent;
}());



/***/ }),

/***/ "./src/app/Products/field-force-automation/field-force-automation.component.css":
/*!**************************************************************************************!*\
  !*** ./src/app/Products/field-force-automation/field-force-automation.component.css ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/Products/field-force-automation/field-force-automation.component.html":
/*!***************************************************************************************!*\
  !*** ./src/app/Products/field-force-automation/field-force-automation.component.html ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n <!--page title start-->\r\n <section class=\"page-title ptb-30\">\r\n        <div class=\"container padding-top-40\">\r\n            <div class=\"row\">\r\n                <div class=\"col-md-12\">\r\n                    <h6 class=\"pagetitles\">Services</h6>\r\n                    <ol class=\"breadcrumb\">\r\n                        <li><a href=\"#\">Home</a></li>\r\n                        <li><a href=\"#\">products</a></li>\r\n                        <li class=\"active\">field force automation</li>\r\n                    </ol>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </section>\r\n    <!--page title end-->\r\n\r\n        <img src=\"assets/img/products/banner/field_force_automation_bigperl.png\" class=\"fullwidth\" alt=\"field_force_automation_bigperl\">\r\n\r\n\r\n          <div class=\"container padding-top-20\">\r\n              <div class=\"col-md-7\">\r\n                  <h2 class=\"text-bold mb-10\">feild force automation</h2>\r\n                  \r\n              </div>\r\n\r\n              <div class=\"col-md-5\">\r\n                  <img src=\"assets/img/products/ffa_bigperl.png\" class=\"img-responsive \" alt=\"ffa_bigperl\">\r\n              </div>\r\n          </div>\r\n\r\n    \r\n<app-footer5></app-footer5>\r\n           \r\n<app-footer2></app-footer2>"

/***/ }),

/***/ "./src/app/Products/field-force-automation/field-force-automation.component.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/Products/field-force-automation/field-force-automation.component.ts ***!
  \*************************************************************************************/
/*! exports provided: FieldForceAutomationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FieldForceAutomationComponent", function() { return FieldForceAutomationComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var FieldForceAutomationComponent = /** @class */ (function () {
    function FieldForceAutomationComponent() {
    }
    FieldForceAutomationComponent.prototype.ngOnInit = function () {
    };
    FieldForceAutomationComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-field-force-automation',
            template: __webpack_require__(/*! ./field-force-automation.component.html */ "./src/app/Products/field-force-automation/field-force-automation.component.html"),
            styles: [__webpack_require__(/*! ./field-force-automation.component.css */ "./src/app/Products/field-force-automation/field-force-automation.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], FieldForceAutomationComponent);
    return FieldForceAutomationComponent;
}());



/***/ }),

/***/ "./src/app/Products/food-on-wheel/food-on-wheel.component.css":
/*!********************************************************************!*\
  !*** ./src/app/Products/food-on-wheel/food-on-wheel.component.css ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/Products/food-on-wheel/food-on-wheel.component.html":
/*!*********************************************************************!*\
  !*** ./src/app/Products/food-on-wheel/food-on-wheel.component.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n        <!--page title start-->\r\n        <section class=\"page-title ptb-30\">\r\n                <div class=\"container padding-top-40\">\r\n                    <div class=\"row\">\r\n                        <div class=\"col-md-12\">\r\n                            <h6 class=\"pagetitles\">Services</h6>\r\n                            <ol class=\"breadcrumb\">\r\n                                <li><a href=\"#\">Home</a></li>\r\n                                <li><a href=\"#\">products</a></li>\r\n                                <li class=\"active\">food on wheel few ordering</li>\r\n                            </ol>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </section>\r\n            <!--page title end-->\r\n    \r\n                <img src=\"assets/img/products/banner/food_on_wheel_bigperl.png\" class=\"fullwidth\" alt=\"food_on_wheel_bigperl\">\r\n    \r\n                  <div class=\"container padding-top-20\">\r\n                      <div class=\"col-md-7\">\r\n                          <h2 class=\"text-bold mb-10\">Food on wheel few ordering</h2>\r\n                          \r\n                      </div>\r\n                      <div class=\"col-md-5\">\r\n                          <img src=\"assets/img/products/food_on_wheel_bigperl.png\" class=\"img-responsive \" alt=\"food_on_wheel_bigperl\">\r\n                      </div>\r\n                  </div>\r\n    \r\n            \r\n\r\n<app-footer5></app-footer5>\r\n           \r\n<app-footer2></app-footer2>"

/***/ }),

/***/ "./src/app/Products/food-on-wheel/food-on-wheel.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/Products/food-on-wheel/food-on-wheel.component.ts ***!
  \*******************************************************************/
/*! exports provided: FoodOnWheelComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FoodOnWheelComponent", function() { return FoodOnWheelComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var FoodOnWheelComponent = /** @class */ (function () {
    function FoodOnWheelComponent() {
    }
    FoodOnWheelComponent.prototype.ngOnInit = function () {
    };
    FoodOnWheelComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-food-on-wheel',
            template: __webpack_require__(/*! ./food-on-wheel.component.html */ "./src/app/Products/food-on-wheel/food-on-wheel.component.html"),
            styles: [__webpack_require__(/*! ./food-on-wheel.component.css */ "./src/app/Products/food-on-wheel/food-on-wheel.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], FoodOnWheelComponent);
    return FoodOnWheelComponent;
}());



/***/ }),

/***/ "./src/app/Products/gps-assets-traking/gps-assets-traking.component.css":
/*!******************************************************************************!*\
  !*** ./src/app/Products/gps-assets-traking/gps-assets-traking.component.css ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/Products/gps-assets-traking/gps-assets-traking.component.html":
/*!*******************************************************************************!*\
  !*** ./src/app/Products/gps-assets-traking/gps-assets-traking.component.html ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n\r\n        <!--page title start-->\r\n        <section class=\"page-title ptb-30\">\r\n                <div class=\"container padding-top-40\">\r\n                    <div class=\"row\">\r\n                        <div class=\"col-md-12\">\r\n                            <h6 class=\"pagetitles\">Services</h6>\r\n                            <ol class=\"breadcrumb\">\r\n                                <li><a href=\"#\">Home</a></li>\r\n                                <li><a href=\"#\">products</a></li>\r\n                                <li class=\"active\">gps assets tracking</li>\r\n                            </ol>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </section>\r\n            <!--page title end-->\r\n    \r\n                <img src=\"assets/img/products/banner/gps_assets_tracking_bigperl.png\" class=\"fullwidth\" alt=\"gps_assets_tracking_bigperl\">\r\n    \r\n                  <div class=\"container padding-top-20\">\r\n                      <div class=\"col-md-7\">\r\n                          <h2 class=\"text-bold mb-10\">gps assets tracking</h2>\r\n                          \r\n                      </div>\r\n    \r\n                      <div class=\"col-md-5\">\r\n                          <img src=\"assets/img/products/assets_tracking_bigperl.png\" class=\"img-responsive \" alt=\"assets_tracking_bigperl\">\r\n                      </div>\r\n                  </div>\r\n    \r\n            \r\n    \r\n<app-footer5></app-footer5>\r\n           \r\n<app-footer2></app-footer2>"

/***/ }),

/***/ "./src/app/Products/gps-assets-traking/gps-assets-traking.component.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/Products/gps-assets-traking/gps-assets-traking.component.ts ***!
  \*****************************************************************************/
/*! exports provided: GpsAssetsTrakingComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GpsAssetsTrakingComponent", function() { return GpsAssetsTrakingComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var GpsAssetsTrakingComponent = /** @class */ (function () {
    function GpsAssetsTrakingComponent() {
    }
    GpsAssetsTrakingComponent.prototype.ngOnInit = function () {
    };
    GpsAssetsTrakingComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-gps-assets-traking',
            template: __webpack_require__(/*! ./gps-assets-traking.component.html */ "./src/app/Products/gps-assets-traking/gps-assets-traking.component.html"),
            styles: [__webpack_require__(/*! ./gps-assets-traking.component.css */ "./src/app/Products/gps-assets-traking/gps-assets-traking.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], GpsAssetsTrakingComponent);
    return GpsAssetsTrakingComponent;
}());



/***/ }),

/***/ "./src/app/Products/gps-mobile-traking/gps-mobile-traking.component.css":
/*!******************************************************************************!*\
  !*** ./src/app/Products/gps-mobile-traking/gps-mobile-traking.component.css ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/Products/gps-mobile-traking/gps-mobile-traking.component.html":
/*!*******************************************************************************!*\
  !*** ./src/app/Products/gps-mobile-traking/gps-mobile-traking.component.html ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n        <!--page title start-->\r\n        <section class=\"page-title ptb-30\">\r\n                <div class=\"container padding-top-40\">\r\n                    <div class=\"row\">\r\n                        <div class=\"col-md-12\">\r\n                            <h6 class=\"pagetitles\">products</h6>\r\n                            <ol class=\"breadcrumb\">\r\n                                <li><a href=\"#\">Home</a></li>\r\n                                <li><a href=\"#\">products</a></li>\r\n                                <li class=\"active\">mobile tracking</li>\r\n                            </ol>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </section>\r\n            <!--page title end-->\r\n    \r\n                <img src=\"assets/img/products/banner/gps_mobile_tracking_bigperl.png\" class=\"fullwidth\" alt=\"gps_mobile_tracking_bigperl\">\r\n    \r\n                  <div class=\"container padding-top-20\">\r\n                      <div class=\"col-md-7\">\r\n                          <h2 class=\"text-bold mb-10\">Mobile Tracking</h2>\r\n                          \r\n                      </div>\r\n    \r\n                      <div class=\"col-md-5\">\r\n                          <img src=\"assets/img/products/gps_bigperl.png\" class=\"img-responsive \" alt=\"gps_bigperl\">\r\n                      </div>\r\n                  </div>\r\n    \r\n\r\n<app-footer5></app-footer5>\r\n           \r\n<app-footer2></app-footer2>"

/***/ }),

/***/ "./src/app/Products/gps-mobile-traking/gps-mobile-traking.component.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/Products/gps-mobile-traking/gps-mobile-traking.component.ts ***!
  \*****************************************************************************/
/*! exports provided: GpsMobileTrakingComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GpsMobileTrakingComponent", function() { return GpsMobileTrakingComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var GpsMobileTrakingComponent = /** @class */ (function () {
    function GpsMobileTrakingComponent() {
    }
    GpsMobileTrakingComponent.prototype.ngOnInit = function () {
    };
    GpsMobileTrakingComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-gps-mobile-traking',
            template: __webpack_require__(/*! ./gps-mobile-traking.component.html */ "./src/app/Products/gps-mobile-traking/gps-mobile-traking.component.html"),
            styles: [__webpack_require__(/*! ./gps-mobile-traking.component.css */ "./src/app/Products/gps-mobile-traking/gps-mobile-traking.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], GpsMobileTrakingComponent);
    return GpsMobileTrakingComponent;
}());



/***/ }),

/***/ "./src/app/Products/gps-vehicle-traking/gps-vehicle-traking.component.css":
/*!********************************************************************************!*\
  !*** ./src/app/Products/gps-vehicle-traking/gps-vehicle-traking.component.css ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/Products/gps-vehicle-traking/gps-vehicle-traking.component.html":
/*!*********************************************************************************!*\
  !*** ./src/app/Products/gps-vehicle-traking/gps-vehicle-traking.component.html ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n        <!--page title start-->\r\n        <section class=\"page-title ptb-30\">\r\n                <div class=\"container padding-top-40\">\r\n                    <div class=\"row\">\r\n                        <div class=\"col-md-12\">\r\n                            <h6 class=\"pagetitles\">Services</h6>\r\n                            <ol class=\"breadcrumb\">\r\n                                <li><a href=\"#\">Home</a></li>\r\n                                <li><a href=\"#\">Services</a></li>\r\n                                <li class=\"active\">adf developer</li>\r\n                            </ol>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </section>\r\n            <!--page title end-->\r\n    \r\n                <img src=\"assets/img/products/banner/gps_vehicle_tracking_bipgerl.png\" class=\"fullwidth\" alt=\"gps_vehicle_tracking_bipgerl\">\r\n    \r\n                  <div class=\"container padding-top-20\">\r\n                      <div class=\"col-md-7\">\r\n                          <h2 class=\"text-bold mb-10\">ADF Developer</h2>\r\n                          \r\n                      </div>\r\n    \r\n                      <div class=\"col-md-5\">\r\n                          <img src=\"assets/img/products/gps_vehicle_bigperl.png\" class=\"img-responsive \" alt=\"gps_vehicle_bigperl\">\r\n                      </div>\r\n                  </div>\r\n    \r\n            \r\n\r\n\r\n<app-footer5></app-footer5>\r\n           \r\n<app-footer2></app-footer2>"

/***/ }),

/***/ "./src/app/Products/gps-vehicle-traking/gps-vehicle-traking.component.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/Products/gps-vehicle-traking/gps-vehicle-traking.component.ts ***!
  \*******************************************************************************/
/*! exports provided: GpsVehicleTrakingComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GpsVehicleTrakingComponent", function() { return GpsVehicleTrakingComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var GpsVehicleTrakingComponent = /** @class */ (function () {
    function GpsVehicleTrakingComponent() {
    }
    GpsVehicleTrakingComponent.prototype.ngOnInit = function () {
    };
    GpsVehicleTrakingComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-gps-vehicle-traking',
            template: __webpack_require__(/*! ./gps-vehicle-traking.component.html */ "./src/app/Products/gps-vehicle-traking/gps-vehicle-traking.component.html"),
            styles: [__webpack_require__(/*! ./gps-vehicle-traking.component.css */ "./src/app/Products/gps-vehicle-traking/gps-vehicle-traking.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], GpsVehicleTrakingComponent);
    return GpsVehicleTrakingComponent;
}());



/***/ }),

/***/ "./src/app/Products/gps-wearables/gps-wearables.component.css":
/*!********************************************************************!*\
  !*** ./src/app/Products/gps-wearables/gps-wearables.component.css ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/Products/gps-wearables/gps-wearables.component.html":
/*!*********************************************************************!*\
  !*** ./src/app/Products/gps-wearables/gps-wearables.component.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n <!--page title start-->\r\n <section class=\"page-title ptb-30\">\r\n        <div class=\"container padding-top-40\">\r\n            <div class=\"row\">\r\n                <div class=\"col-md-12\">\r\n                    <h6 class=\"pagetitles\">Services</h6>\r\n                    <ol class=\"breadcrumb\">\r\n                        <li><a href=\"#\">Home</a></li>\r\n                        <li><a href=\"#\">products</a></li>\r\n                        <li class=\"active\">gps wearables</li>\r\n                    </ol>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </section>\r\n    <!--page title end-->\r\n\r\n        <img src=\"assets/img/products/banner/gps_vehicle_tracking_bipgerl.png\" class=\"fullwidth\" alt=\"gps_vehicle_tracking_bipgerl\">\r\n\r\n          <div class=\"container padding-top-20\">\r\n              <div class=\"col-md-7\">\r\n                  <h2 class=\"text-bold mb-10\">gps wearables</h2>\r\n                  \r\n              </div>\r\n\r\n              <div class=\"col-md-5\">\r\n                  <img src=\"assets/img/products/gps_vehicle_bigperl.png\" class=\"img-responsive \" alt=\"gps_vehicle_bigperl\">\r\n              </div>\r\n          </div>\r\n\r\n\r\n<app-footer5></app-footer5>\r\n           \r\n<app-footer2></app-footer2>\r\n"

/***/ }),

/***/ "./src/app/Products/gps-wearables/gps-wearables.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/Products/gps-wearables/gps-wearables.component.ts ***!
  \*******************************************************************/
/*! exports provided: GpsWearablesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GpsWearablesComponent", function() { return GpsWearablesComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var GpsWearablesComponent = /** @class */ (function () {
    function GpsWearablesComponent() {
    }
    GpsWearablesComponent.prototype.ngOnInit = function () {
    };
    GpsWearablesComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-gps-wearables',
            template: __webpack_require__(/*! ./gps-wearables.component.html */ "./src/app/Products/gps-wearables/gps-wearables.component.html"),
            styles: [__webpack_require__(/*! ./gps-wearables.component.css */ "./src/app/Products/gps-wearables/gps-wearables.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], GpsWearablesComponent);
    return GpsWearablesComponent;
}());



/***/ }),

/***/ "./src/app/Products/hotel-booking-engine/hotel-booking-engine.component.css":
/*!**********************************************************************************!*\
  !*** ./src/app/Products/hotel-booking-engine/hotel-booking-engine.component.css ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/Products/hotel-booking-engine/hotel-booking-engine.component.html":
/*!***********************************************************************************!*\
  !*** ./src/app/Products/hotel-booking-engine/hotel-booking-engine.component.html ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n        <!--page title start-->\r\n        <section class=\"page-title ptb-30\">\r\n                <div class=\"container padding-top-40\">\r\n                    <div class=\"row\">\r\n                        <div class=\"col-md-12\">\r\n                            <h6 class=\"pagetitles\">Services</h6>\r\n                            <ol class=\"breadcrumb\">\r\n                                <li><a href=\"#\">Home</a></li>\r\n                                <li><a href=\"#\">products</a></li>\r\n                                <li class=\"active\">hotel booking engine</li>\r\n                            </ol>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </section>\r\n            <!--page title end-->\r\n    \r\n                <img src=\"assets/img/products/banner/hotel_booking_engine_bigperl.png\" class=\"fullwidth\" alt=\"hotel_booking_engine_bigperl\">\r\n    \r\n                  <div class=\"container padding-top-20\">\r\n                      <div class=\"col-md-7\">\r\n                          <h2 class=\"text-bold mb-10\">Hotel booking engine</h2>\r\n                          <p class=\"text-justified\">Hotel Booking Engine is an online hotel reservation system. The scripts provides a powerful room booking and reservation management functionality and allows you to install a clear call-to-action tool on your hotel website to impact conversions and increase bookings. Our room booking system is highly customizable and compatible with various websites.</p>\r\n                          <p class=\"text-justified\">Take your business to the next level. A user friendly web based booking engine can help you increase your hotel rooms bookings online to a great extent. The software can be easily plugged in your website.</p>\r\n                          <h5><b>Key Features:</b></h5>\r\n                          <ul id=\"liststyle\">\r\n                            <li class=\"text-justified\">Accept Bookings Online <br>A complete room booking system to provide hotel managers with an easy way to manage reservations and booking availability.</li>\r\n                            <li>Mobile-friendly Design <br>The hotel reservation system front-end adapts to any screen resolution and device (PC, Mac, tablets, smartphone etc.)</li>\r\n                            <li class=\"text-justified\">Editable Booking Form <br>Define which customer details the front-end system will require from customers using simple drop-downs for each booking form field.</li>\r\n                            <li class=\"text-justified\">Multiple Languages <br>Translate the hotel reservation system into any language. Add a language tab on the booking form so clients can make their choice.</li>\r\n                            <li class=\"text-justified\">Payments Processing <br>Select and enable the payment methods which match your business best – PayPal, Authorize.Net, CC payments, wire transfers etc.</li>\r\n                          </ul>\r\n                      </div>\r\n                      <div class=\"col-md-5\">\r\n                          <img src=\"assets/img/products/hotel_booking_engine_bigperl.png\" class=\"img-responsive \" alt=\"hotel_booking_engine_bigperl\">\r\n                      </div>\r\n                  </div>\r\n    \r\n            \r\n    \r\n\r\n\r\n<app-footer5></app-footer5>\r\n           \r\n<app-footer2></app-footer2>"

/***/ }),

/***/ "./src/app/Products/hotel-booking-engine/hotel-booking-engine.component.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/Products/hotel-booking-engine/hotel-booking-engine.component.ts ***!
  \*********************************************************************************/
/*! exports provided: HotelBookingEngineComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HotelBookingEngineComponent", function() { return HotelBookingEngineComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var HotelBookingEngineComponent = /** @class */ (function () {
    function HotelBookingEngineComponent() {
    }
    HotelBookingEngineComponent.prototype.ngOnInit = function () {
    };
    HotelBookingEngineComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-hotel-booking-engine',
            template: __webpack_require__(/*! ./hotel-booking-engine.component.html */ "./src/app/Products/hotel-booking-engine/hotel-booking-engine.component.html"),
            styles: [__webpack_require__(/*! ./hotel-booking-engine.component.css */ "./src/app/Products/hotel-booking-engine/hotel-booking-engine.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], HotelBookingEngineComponent);
    return HotelBookingEngineComponent;
}());



/***/ }),

/***/ "./src/app/Products/hotel-inventory-management/hotel-inventory-management.component.css":
/*!**********************************************************************************************!*\
  !*** ./src/app/Products/hotel-inventory-management/hotel-inventory-management.component.css ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/Products/hotel-inventory-management/hotel-inventory-management.component.html":
/*!***********************************************************************************************!*\
  !*** ./src/app/Products/hotel-inventory-management/hotel-inventory-management.component.html ***!
  \***********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n        <!--page title start-->\r\n        <section class=\"page-title ptb-30\">\r\n                <div class=\"container padding-top-40\">\r\n                    <div class=\"row\">\r\n                        <div class=\"col-md-12\">\r\n                            <h6 class=\"pagetitles\">Services</h6>\r\n                            <ol class=\"breadcrumb\">\r\n                                <li><a href=\"#\">Home</a></li>\r\n                                <li><a href=\"#\">products</a></li>\r\n                                <li class=\"active\">hotel inventory management</li>\r\n                            </ol>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </section>\r\n            <!--page title end-->\r\n    \r\n                <img src=\"assets/img/products/banner/hotel_system_bigperl.png\" class=\"fullwidth\" alt=\"hotel_system_bigperl\">\r\n    \r\n                  <div class=\"container padding-top-20\">\r\n                      <div class=\"col-md-7\">\r\n                          <h2 class=\"text-bold mb-10\">Hotel inventory management</h2>\r\n                          <p class=\"text-justified\">A Hotel Inventory Management improves stock administration to a substantial degree and makes following of procurement and deals precise. There are various operations in stock which happen at the same time.These incorporate deals through purpose of offer terminals, room administration, refreshments, other room related consumables and durables.</p>\r\n                          <p class=\"text-justified\">Maintaining all these exercises can be troublesom. This would result in income spillage, wastage, and burglary. A decent stock administration framework helps a lodging anticipate request and supply rate with extraordinary precision and diminishes the possibility of blunder.It would help restaurants to access business insight, plan consumption and keep a more tightly control on benefit.</p>\r\n                          <h5><b>Services Offered:</b></h5>\r\n                          <ul id=\"liststyle\">\r\n                            <li class=\"text-justified\">Merchant Performance – Allows inn administrators to pick better performing sellers by following data, for example, time of conveyance time, precision of conveyance, cost viability and so forth.</li>\r\n                            <li class=\"text-justified\">Seller Accountability – Ensuring a merchant conveys the right shipment and inns. A coordinated stock administration framework permits inn supervisors to pinpoint mistakes in conveyance with incredible precision and make merchants responsible for their own particular activity.</li>\r\n                            <li>Request Management – to forestall bothoverstocking and stock excursion circumstances</li>\r\n                          </ul>\r\n                      </div>\r\n                      <div class=\"col-md-5\">\r\n                          <img src=\"assets/img/products/hotel_inventory_management_bigperl.png\" class=\"img-responsive \" alt=\"hotel_inventory_management_bigperl\">\r\n                      </div>\r\n                  </div>\r\n    \r\n            \r\n    \r\n\r\n\r\n<app-footer5></app-footer5>\r\n           \r\n<app-footer2></app-footer2>"

/***/ }),

/***/ "./src/app/Products/hotel-inventory-management/hotel-inventory-management.component.ts":
/*!*********************************************************************************************!*\
  !*** ./src/app/Products/hotel-inventory-management/hotel-inventory-management.component.ts ***!
  \*********************************************************************************************/
/*! exports provided: HotelInventoryManagementComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HotelInventoryManagementComponent", function() { return HotelInventoryManagementComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var HotelInventoryManagementComponent = /** @class */ (function () {
    function HotelInventoryManagementComponent() {
    }
    HotelInventoryManagementComponent.prototype.ngOnInit = function () {
    };
    HotelInventoryManagementComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-hotel-inventory-management',
            template: __webpack_require__(/*! ./hotel-inventory-management.component.html */ "./src/app/Products/hotel-inventory-management/hotel-inventory-management.component.html"),
            styles: [__webpack_require__(/*! ./hotel-inventory-management.component.css */ "./src/app/Products/hotel-inventory-management/hotel-inventory-management.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], HotelInventoryManagementComponent);
    return HotelInventoryManagementComponent;
}());



/***/ }),

/***/ "./src/app/Products/hotel-management-system/hotel-management-system.component.css":
/*!****************************************************************************************!*\
  !*** ./src/app/Products/hotel-management-system/hotel-management-system.component.css ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/Products/hotel-management-system/hotel-management-system.component.html":
/*!*****************************************************************************************!*\
  !*** ./src/app/Products/hotel-management-system/hotel-management-system.component.html ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n  <!--page title start-->\r\n  <section class=\"page-title ptb-30\">\r\n        <div class=\"container padding-top-40\">\r\n            <div class=\"row\">\r\n                <div class=\"col-md-12\">\r\n                    <h6 class=\"pagetitles\">Services</h6>\r\n                    <ol class=\"breadcrumb\">\r\n                        <li><a href=\"#\">Home</a></li>\r\n                        <li><a href=\"#\">products</a></li>\r\n                        <li class=\"active\">hotel management system</li>\r\n                    </ol>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </section>\r\n    <!--page title end-->\r\n\r\n        <img src=\"assets/img/products/banner/hotel_management_bigperl.png\" class=\"fullwidth\" alt=\"hotel_management_bigperl\">\r\n\r\n          <div class=\"container padding-top-20\">\r\n              <div class=\"col-md-7\">\r\n                  <h2 class=\"text-bold mb-10\">Hotel Management System</h2>\r\n                  <p>To succeed in your business, independent hoteliers need a comprehensive solution that is flexible and agile.</p>\r\n                  <p>Hotel Management System is an ideal software solution for Hospitality Industry that can be used at hotels, motels, inns, resorts, lodges etc.</p>\r\n                  <p class=\"text-justified\">Bigperl’s product of Hotel Management System is a comprehensive software suite consisting of integrated modules for various aspects of hotel management.It includes all the features required in a Hotel Management Software, Hotel Reservation Software, Hotel Reception Software (Front Office), Call Accounting, Hotel Point of Sales (Restaurant, Bar, Room Service, House Keeping or any other outlet), Inventory Management System and Hotel accounting software.</p>\r\n              </div>\r\n              <div class=\"col-md-5\">\r\n                  <img src=\"assets/img/products/hotel_management_bigperl.png\" class=\"img-responsive \" alt=\"hotel_management_bigperl\">\r\n              </div>\r\n          </div>\r\n\r\n\r\n<app-footer5></app-footer5>\r\n           \r\n<app-footer2></app-footer2>"

/***/ }),

/***/ "./src/app/Products/hotel-management-system/hotel-management-system.component.ts":
/*!***************************************************************************************!*\
  !*** ./src/app/Products/hotel-management-system/hotel-management-system.component.ts ***!
  \***************************************************************************************/
/*! exports provided: HotelManagementSystemComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HotelManagementSystemComponent", function() { return HotelManagementSystemComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var HotelManagementSystemComponent = /** @class */ (function () {
    function HotelManagementSystemComponent() {
    }
    HotelManagementSystemComponent.prototype.ngOnInit = function () {
    };
    HotelManagementSystemComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-hotel-management-system',
            template: __webpack_require__(/*! ./hotel-management-system.component.html */ "./src/app/Products/hotel-management-system/hotel-management-system.component.html"),
            styles: [__webpack_require__(/*! ./hotel-management-system.component.css */ "./src/app/Products/hotel-management-system/hotel-management-system.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], HotelManagementSystemComponent);
    return HotelManagementSystemComponent;
}());



/***/ }),

/***/ "./src/app/Products/sass-based-erp/sass-based-erp.component.css":
/*!**********************************************************************!*\
  !*** ./src/app/Products/sass-based-erp/sass-based-erp.component.css ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/Products/sass-based-erp/sass-based-erp.component.html":
/*!***********************************************************************!*\
  !*** ./src/app/Products/sass-based-erp/sass-based-erp.component.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--page title start-->\r\n<section class=\"page-title ptb-30\">\r\n        <div class=\"container padding-top-40\">\r\n            <div class=\"row\">\r\n                <div class=\"col-md-12\">\r\n                    <h6 class=\"pagetitles\">Services</h6>\r\n                    <ol class=\"breadcrumb\">\r\n                        <li><a href=\"#\">Home</a></li>\r\n                        <li><a href=\"#\">products</a></li>\r\n                        <li class=\"active\">sass based erp</li>\r\n                    </ol>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </section>\r\n    <!--page title end-->\r\n\r\n        <img src=\"assets/img/products/banner/sass_based_erp_bigperl.png\" class=\"fullwidth\" alt=\"sass_based_erp_bigperl\">\r\n\r\n          <div class=\"container padding-top-20\">\r\n              <div class=\"col-md-7\">\r\n                  <h2 class=\"text-bold mb-10\">SASS based ERP</h2>\r\n                  \r\n              </div>\r\n              <div class=\"col-md-5\">\r\n                  <img src=\"assets/img/products/sass_erp_bigperl.png\" class=\"img-responsive \" alt=\"sass_erp_bigperl\">\r\n              </div>\r\n          </div>\r\n\r\n    \r\n\r\n\r\n<app-footer5></app-footer5>\r\n           \r\n<app-footer2></app-footer2>"

/***/ }),

/***/ "./src/app/Products/sass-based-erp/sass-based-erp.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/Products/sass-based-erp/sass-based-erp.component.ts ***!
  \*********************************************************************/
/*! exports provided: SassBasedErpComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SassBasedErpComponent", function() { return SassBasedErpComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var SassBasedErpComponent = /** @class */ (function () {
    function SassBasedErpComponent() {
    }
    SassBasedErpComponent.prototype.ngOnInit = function () {
    };
    SassBasedErpComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-sass-based-erp',
            template: __webpack_require__(/*! ./sass-based-erp.component.html */ "./src/app/Products/sass-based-erp/sass-based-erp.component.html"),
            styles: [__webpack_require__(/*! ./sass-based-erp.component.css */ "./src/app/Products/sass-based-erp/sass-based-erp.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], SassBasedErpComponent);
    return SassBasedErpComponent;
}());



/***/ }),

/***/ "./src/app/Products/sass-based-payroll/sass-based-payroll.component.css":
/*!******************************************************************************!*\
  !*** ./src/app/Products/sass-based-payroll/sass-based-payroll.component.css ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/Products/sass-based-payroll/sass-based-payroll.component.html":
/*!*******************************************************************************!*\
  !*** ./src/app/Products/sass-based-payroll/sass-based-payroll.component.html ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n\r\n        <!--page title start-->\r\n        <section class=\"page-title ptb-30\">\r\n                <div class=\"container padding-top-40\">\r\n                    <div class=\"row\">\r\n                        <div class=\"col-md-12\">\r\n                            <h6 class=\"pagetitles\">Services</h6>\r\n                            <ol class=\"breadcrumb\">\r\n                                <li><a href=\"#\">Home</a></li>\r\n                                <li><a href=\"#\">products</a></li>\r\n                                <li class=\"active\">sass based payroll</li>\r\n                            </ol>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </section>\r\n            <!--page title end-->\r\n    \r\n                <img src=\"assets/img/products/banner/sass_based_erp_bigperl.png\" class=\"fullwidth\" alt=\"sass_based_erp_bigperl\">\r\n    \r\n                  <div class=\"container padding-top-20\">\r\n                      <div class=\"col-md-7\">\r\n                          <h2 class=\"text-bold mb-10\">SASS based payroll</h2>\r\n                          \r\n                      </div>\r\n                      <div class=\"col-md-5\">\r\n                          <img src=\"assets/img/products/sass_erp_bigperl.png\" class=\"img-responsive \" alt=\"sass_erp_bigperl\">\r\n                      </div>\r\n                  </div>\r\n    \r\n            \r\n    \r\n\r\n<app-footer5></app-footer5>\r\n           \r\n<app-footer2></app-footer2>"

/***/ }),

/***/ "./src/app/Products/sass-based-payroll/sass-based-payroll.component.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/Products/sass-based-payroll/sass-based-payroll.component.ts ***!
  \*****************************************************************************/
/*! exports provided: SassBasedPayrollComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SassBasedPayrollComponent", function() { return SassBasedPayrollComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var SassBasedPayrollComponent = /** @class */ (function () {
    function SassBasedPayrollComponent() {
    }
    SassBasedPayrollComponent.prototype.ngOnInit = function () {
    };
    SassBasedPayrollComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-sass-based-payroll',
            template: __webpack_require__(/*! ./sass-based-payroll.component.html */ "./src/app/Products/sass-based-payroll/sass-based-payroll.component.html"),
            styles: [__webpack_require__(/*! ./sass-based-payroll.component.css */ "./src/app/Products/sass-based-payroll/sass-based-payroll.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], SassBasedPayrollComponent);
    return SassBasedPayrollComponent;
}());



/***/ }),

/***/ "./src/app/Products/travel-booking-ecommerce/travel-booking-ecommerce.component.css":
/*!******************************************************************************************!*\
  !*** ./src/app/Products/travel-booking-ecommerce/travel-booking-ecommerce.component.css ***!
  \******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/Products/travel-booking-ecommerce/travel-booking-ecommerce.component.html":
/*!*******************************************************************************************!*\
  !*** ./src/app/Products/travel-booking-ecommerce/travel-booking-ecommerce.component.html ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n        <!--page title start-->\r\n        <section class=\"page-title ptb-30\">\r\n                <div class=\"container padding-top-40\">\r\n                    <div class=\"row\">\r\n                        <div class=\"col-md-12\">\r\n                            <h6 class=\"pagetitles\">Services</h6>\r\n                            <ol class=\"breadcrumb\">\r\n                                <li><a href=\"#\">Home</a></li>\r\n                                <li><a href=\"#\">products</a></li>\r\n                                <li class=\"active\">travel booking e-commerce</li>\r\n                            </ol>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </section>\r\n            <!--page title end-->\r\n    \r\n                <img src=\"assets/img/products/banner/travel_booking_bigperl.png\" class=\"fullwidth\" alt=\"travel_booking_bigperl\">\r\n    \r\n                  <div class=\"container padding-top-20\">\r\n                      <div class=\"col-md-7\">\r\n                          <h2 class=\"text-bold mb-10\">Travel booking e-commerce</h2>\r\n                          \r\n                      </div>\r\n                      <div class=\"col-md-5\">\r\n                          <img src=\"assets/img/products/travel_booking_bigperl.png\" class=\"img-responsive \" alt=\"travel_booking_bigperl\">\r\n                      </div>\r\n                  </div>\r\n    \r\n            \r\n    \r\n\r\n\r\n<app-footer5></app-footer5>\r\n           \r\n<app-footer2></app-footer2>"

/***/ }),

/***/ "./src/app/Products/travel-booking-ecommerce/travel-booking-ecommerce.component.ts":
/*!*****************************************************************************************!*\
  !*** ./src/app/Products/travel-booking-ecommerce/travel-booking-ecommerce.component.ts ***!
  \*****************************************************************************************/
/*! exports provided: TravelBookingEcommerceComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TravelBookingEcommerceComponent", function() { return TravelBookingEcommerceComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var TravelBookingEcommerceComponent = /** @class */ (function () {
    function TravelBookingEcommerceComponent() {
    }
    TravelBookingEcommerceComponent.prototype.ngOnInit = function () {
    };
    TravelBookingEcommerceComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-travel-booking-ecommerce',
            template: __webpack_require__(/*! ./travel-booking-ecommerce.component.html */ "./src/app/Products/travel-booking-ecommerce/travel-booking-ecommerce.component.html"),
            styles: [__webpack_require__(/*! ./travel-booking-ecommerce.component.css */ "./src/app/Products/travel-booking-ecommerce/travel-booking-ecommerce.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], TravelBookingEcommerceComponent);
    return TravelBookingEcommerceComponent;
}());



/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _components_how_indexing_and_deep_linking_help_how_indexing_and_deep_linking_help_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./components/how-indexing-and-deep-linking-help/how-indexing-and-deep-linking-help.component */ "./src/app/components/how-indexing-and-deep-linking-help/how-indexing-and-deep-linking-help.component.ts");
/* harmony import */ var _components_location_based_technology_formobile_app_location_based_technology_formobile_app_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./components/location-based-technology-formobile-app/location-based-technology-formobile-app.component */ "./src/app/components/location-based-technology-formobile-app/location-based-technology-formobile-app.component.ts");
/* harmony import */ var _components_xamarin_development_xamarin_development_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./components/xamarin-development/xamarin-development.component */ "./src/app/components/xamarin-development/xamarin-development.component.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _portfolio_portfolio_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./portfolio/portfolio.component */ "./src/app/portfolio/portfolio.component.ts");
/* harmony import */ var _careers_careers_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./careers/careers.component */ "./src/app/careers/careers.component.ts");
/* harmony import */ var _servicessection_android_app_development_android_app_development_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./servicessection/android-app-development/android-app-development.component */ "./src/app/servicessection/android-app-development/android-app-development.component.ts");
/* harmony import */ var _servicessection_iphone_app_development_iphone_app_development_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./servicessection/iphone-app-development/iphone-app-development.component */ "./src/app/servicessection/iphone-app-development/iphone-app-development.component.ts");
/* harmony import */ var _servicessection_ipad_app_development_ipad_app_development_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./servicessection/ipad-app-development/ipad-app-development.component */ "./src/app/servicessection/ipad-app-development/ipad-app-development.component.ts");
/* harmony import */ var _servicessection_adf_development_adf_development_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./servicessection/adf-development/adf-development.component */ "./src/app/servicessection/adf-development/adf-development.component.ts");
/* harmony import */ var _servicessection_adf_webcenter_development_adf_webcenter_development_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./servicessection/adf-webcenter-development/adf-webcenter-development.component */ "./src/app/servicessection/adf-webcenter-development/adf-webcenter-development.component.ts");
/* harmony import */ var _servicessection_blackberry_app_development_blackberry_app_development_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./servicessection/blackberry-app-development/blackberry-app-development.component */ "./src/app/servicessection/blackberry-app-development/blackberry-app-development.component.ts");
/* harmony import */ var _servicessection_cake_php_development_cake_php_development_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./servicessection/cake-php-development/cake-php-development.component */ "./src/app/servicessection/cake-php-development/cake-php-development.component.ts");
/* harmony import */ var _servicessection_facebook_app_development_facebook_app_development_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./servicessection/facebook-app-development/facebook-app-development.component */ "./src/app/servicessection/facebook-app-development/facebook-app-development.component.ts");
/* harmony import */ var _servicessection_ecommerce_development_ecommerce_development_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./servicessection/ecommerce-development/ecommerce-development.component */ "./src/app/servicessection/ecommerce-development/ecommerce-development.component.ts");
/* harmony import */ var _servicessection_ecommerce_developer_ecommerce_developer_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./servicessection/ecommerce-developer/ecommerce-developer.component */ "./src/app/servicessection/ecommerce-developer/ecommerce-developer.component.ts");
/* harmony import */ var _servicessection_drupal_development_drupal_development_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./servicessection/drupal-development/drupal-development.component */ "./src/app/servicessection/drupal-development/drupal-development.component.ts");
/* harmony import */ var _servicessection_codeigniter_development_codeigniter_development_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./servicessection/codeigniter-development/codeigniter-development.component */ "./src/app/servicessection/codeigniter-development/codeigniter-development.component.ts");
/* harmony import */ var _servicessection_cmc_development_cmc_development_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./servicessection/cmc-development/cmc-development.component */ "./src/app/servicessection/cmc-development/cmc-development.component.ts");
/* harmony import */ var _servicessection_laravel_development_laravel_development_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./servicessection/laravel-development/laravel-development.component */ "./src/app/servicessection/laravel-development/laravel-development.component.ts");
/* harmony import */ var _servicessection_joomla_development_joomla_development_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./servicessection/joomla-development/joomla-development.component */ "./src/app/servicessection/joomla-development/joomla-development.component.ts");
/* harmony import */ var _servicessection_java_j2ee_development_java_j2ee_development_component__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./servicessection/java-j2ee-development/java-j2ee-development.component */ "./src/app/servicessection/java-j2ee-development/java-j2ee-development.component.ts");
/* harmony import */ var _servicessection_internet_of_things_internet_of_things_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./servicessection/internet-of-things/internet-of-things.component */ "./src/app/servicessection/internet-of-things/internet-of-things.component.ts");
/* harmony import */ var _servicessection_hybrid_app_development_hybrid_app_development_component__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./servicessection/hybrid-app-development/hybrid-app-development.component */ "./src/app/servicessection/hybrid-app-development/hybrid-app-development.component.ts");
/* harmony import */ var _servicessection_python_web_development_python_web_development_component__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./servicessection/python-web-development/python-web-development.component */ "./src/app/servicessection/python-web-development/python-web-development.component.ts");
/* harmony import */ var _servicessection_python_developer_python_developer_component__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ./servicessection/python-developer/python-developer.component */ "./src/app/servicessection/python-developer/python-developer.component.ts");
/* harmony import */ var _servicessection_php_development_php_development_component__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ./servicessection/php-development/php-development.component */ "./src/app/servicessection/php-development/php-development.component.ts");
/* harmony import */ var _servicessection_php_developer_php_developer_component__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! ./servicessection/php-developer/php-developer.component */ "./src/app/servicessection/php-developer/php-developer.component.ts");
/* harmony import */ var _servicessection_phone_gap_development_phone_gap_development_component__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! ./servicessection/phone-gap-development/phone-gap-development.component */ "./src/app/servicessection/phone-gap-development/phone-gap-development.component.ts");
/* harmony import */ var _servicessection_magento_development_magento_development_component__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! ./servicessection/magento-development/magento-development.component */ "./src/app/servicessection/magento-development/magento-development.component.ts");
/* harmony import */ var _servicessection_magento_developer_magento_developer_component__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! ./servicessection/magento-developer/magento-developer.component */ "./src/app/servicessection/magento-developer/magento-developer.component.ts");
/* harmony import */ var _contact_us_contact_us_component__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(/*! ./contact-us/contact-us.component */ "./src/app/contact-us/contact-us.component.ts");
/* harmony import */ var _AboutUs_blogs_blogs_component__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(/*! ./AboutUs/blogs/blogs.component */ "./src/app/AboutUs/blogs/blogs.component.ts");
/* harmony import */ var _AboutUs_our_clients_our_clients_component__WEBPACK_IMPORTED_MODULE_34__ = __webpack_require__(/*! ./AboutUs/our-clients/our-clients.component */ "./src/app/AboutUs/our-clients/our-clients.component.ts");
/* harmony import */ var _AboutUs_sucess_stories_sucess_stories_component__WEBPACK_IMPORTED_MODULE_35__ = __webpack_require__(/*! ./AboutUs/sucess-stories/sucess-stories.component */ "./src/app/AboutUs/sucess-stories/sucess-stories.component.ts");
/* harmony import */ var _servicessection_ios_developer_ios_developer_component__WEBPACK_IMPORTED_MODULE_36__ = __webpack_require__(/*! ./servicessection/ios-developer/ios-developer.component */ "./src/app/servicessection/ios-developer/ios-developer.component.ts");
/* harmony import */ var _servicessection_android_developer_android_developer_component__WEBPACK_IMPORTED_MODULE_37__ = __webpack_require__(/*! ./servicessection/android-developer/android-developer.component */ "./src/app/servicessection/android-developer/android-developer.component.ts");
/* harmony import */ var _servicessection_xcart_development_xcart_development_component__WEBPACK_IMPORTED_MODULE_38__ = __webpack_require__(/*! ./servicessection/xcart-development/xcart-development.component */ "./src/app/servicessection/xcart-development/xcart-development.component.ts");
/* harmony import */ var _servicessection_word_press_development_word_press_development_component__WEBPACK_IMPORTED_MODULE_39__ = __webpack_require__(/*! ./servicessection/word-press-development/word-press-development.component */ "./src/app/servicessection/word-press-development/word-press-development.component.ts");
/* harmony import */ var _servicessection_windows_app_development_windows_app_development_component__WEBPACK_IMPORTED_MODULE_40__ = __webpack_require__(/*! ./servicessection/windows-app-development/windows-app-development.component */ "./src/app/servicessection/windows-app-development/windows-app-development.component.ts");
/* harmony import */ var _servicessection_web_application_development_web_application_development_component__WEBPACK_IMPORTED_MODULE_41__ = __webpack_require__(/*! ./servicessection/web-application-development/web-application-development.component */ "./src/app/servicessection/web-application-development/web-application-development.component.ts");
/* harmony import */ var _servicessection_wearable_devices_wearable_devices_component__WEBPACK_IMPORTED_MODULE_42__ = __webpack_require__(/*! ./servicessection/wearable-devices/wearable-devices.component */ "./src/app/servicessection/wearable-devices/wearable-devices.component.ts");
/* harmony import */ var _servicessection_ruby_on_rails_development_ruby_on_rails_development_component__WEBPACK_IMPORTED_MODULE_43__ = __webpack_require__(/*! ./servicessection/ruby-on-rails-development/ruby-on-rails-development.component */ "./src/app/servicessection/ruby-on-rails-development/ruby-on-rails-development.component.ts");
/* harmony import */ var _servicessection_ruby_on_rails_developer_ruby_on_rails_developer_component__WEBPACK_IMPORTED_MODULE_44__ = __webpack_require__(/*! ./servicessection/ruby-on-rails-developer/ruby-on-rails-developer.component */ "./src/app/servicessection/ruby-on-rails-developer/ruby-on-rails-developer.component.ts");
/* harmony import */ var _servicessection_responsive_web_development_responsive_web_development_component__WEBPACK_IMPORTED_MODULE_45__ = __webpack_require__(/*! ./servicessection/responsive-web-development/responsive-web-development.component */ "./src/app/servicessection/responsive-web-development/responsive-web-development.component.ts");
/* harmony import */ var _Products_travel_booking_ecommerce_travel_booking_ecommerce_component__WEBPACK_IMPORTED_MODULE_46__ = __webpack_require__(/*! ./Products/travel-booking-ecommerce/travel-booking-ecommerce.component */ "./src/app/Products/travel-booking-ecommerce/travel-booking-ecommerce.component.ts");
/* harmony import */ var _Products_sass_based_payroll_sass_based_payroll_component__WEBPACK_IMPORTED_MODULE_47__ = __webpack_require__(/*! ./Products/sass-based-payroll/sass-based-payroll.component */ "./src/app/Products/sass-based-payroll/sass-based-payroll.component.ts");
/* harmony import */ var _Products_sass_based_erp_sass_based_erp_component__WEBPACK_IMPORTED_MODULE_48__ = __webpack_require__(/*! ./Products/sass-based-erp/sass-based-erp.component */ "./src/app/Products/sass-based-erp/sass-based-erp.component.ts");
/* harmony import */ var _Products_hotel_management_system_hotel_management_system_component__WEBPACK_IMPORTED_MODULE_49__ = __webpack_require__(/*! ./Products/hotel-management-system/hotel-management-system.component */ "./src/app/Products/hotel-management-system/hotel-management-system.component.ts");
/* harmony import */ var _Products_hotel_inventory_management_hotel_inventory_management_component__WEBPACK_IMPORTED_MODULE_50__ = __webpack_require__(/*! ./Products/hotel-inventory-management/hotel-inventory-management.component */ "./src/app/Products/hotel-inventory-management/hotel-inventory-management.component.ts");
/* harmony import */ var _Products_hotel_booking_engine_hotel_booking_engine_component__WEBPACK_IMPORTED_MODULE_51__ = __webpack_require__(/*! ./Products/hotel-booking-engine/hotel-booking-engine.component */ "./src/app/Products/hotel-booking-engine/hotel-booking-engine.component.ts");
/* harmony import */ var _Products_gps_wearables_gps_wearables_component__WEBPACK_IMPORTED_MODULE_52__ = __webpack_require__(/*! ./Products/gps-wearables/gps-wearables.component */ "./src/app/Products/gps-wearables/gps-wearables.component.ts");
/* harmony import */ var _Products_gps_vehicle_traking_gps_vehicle_traking_component__WEBPACK_IMPORTED_MODULE_53__ = __webpack_require__(/*! ./Products/gps-vehicle-traking/gps-vehicle-traking.component */ "./src/app/Products/gps-vehicle-traking/gps-vehicle-traking.component.ts");
/* harmony import */ var _Products_gps_mobile_traking_gps_mobile_traking_component__WEBPACK_IMPORTED_MODULE_54__ = __webpack_require__(/*! ./Products/gps-mobile-traking/gps-mobile-traking.component */ "./src/app/Products/gps-mobile-traking/gps-mobile-traking.component.ts");
/* harmony import */ var _Products_gps_assets_traking_gps_assets_traking_component__WEBPACK_IMPORTED_MODULE_55__ = __webpack_require__(/*! ./Products/gps-assets-traking/gps-assets-traking.component */ "./src/app/Products/gps-assets-traking/gps-assets-traking.component.ts");
/* harmony import */ var _Products_food_on_wheel_food_on_wheel_component__WEBPACK_IMPORTED_MODULE_56__ = __webpack_require__(/*! ./Products/food-on-wheel/food-on-wheel.component */ "./src/app/Products/food-on-wheel/food-on-wheel.component.ts");
/* harmony import */ var _Products_field_force_automation_field_force_automation_component__WEBPACK_IMPORTED_MODULE_57__ = __webpack_require__(/*! ./Products/field-force-automation/field-force-automation.component */ "./src/app/Products/field-force-automation/field-force-automation.component.ts");
/* harmony import */ var _Products_enterprice_crm_support_enterprice_crm_support_component__WEBPACK_IMPORTED_MODULE_58__ = __webpack_require__(/*! ./Products/enterprice-crm-support/enterprice-crm-support.component */ "./src/app/Products/enterprice-crm-support/enterprice-crm-support.component.ts");
/* harmony import */ var _Products_enterprice_crm_sales_enterprice_crm_sales_component__WEBPACK_IMPORTED_MODULE_59__ = __webpack_require__(/*! ./Products/enterprice-crm-sales/enterprice-crm-sales.component */ "./src/app/Products/enterprice-crm-sales/enterprice-crm-sales.component.ts");
/* harmony import */ var _Products_construction_suppliers_construction_suppliers_component__WEBPACK_IMPORTED_MODULE_60__ = __webpack_require__(/*! ./Products/construction-suppliers/construction-suppliers.component */ "./src/app/Products/construction-suppliers/construction-suppliers.component.ts");
/* harmony import */ var _Products_architechtural_network_architechtural_network_component__WEBPACK_IMPORTED_MODULE_61__ = __webpack_require__(/*! ./Products/architechtural-network/architechtural-network.component */ "./src/app/Products/architechtural-network/architechtural-network.component.ts");
/* harmony import */ var _Industry_life_science_and_health_care_life_science_and_health_care_component__WEBPACK_IMPORTED_MODULE_62__ = __webpack_require__(/*! ./Industry/life-science-and-health-care/life-science-and-health-care.component */ "./src/app/Industry/life-science-and-health-care/life-science-and-health-care.component.ts");
/* harmony import */ var _Industry_travel_transport_log_travel_transport_log_component__WEBPACK_IMPORTED_MODULE_63__ = __webpack_require__(/*! ./Industry/travel-transport-log/travel-transport-log.component */ "./src/app/Industry/travel-transport-log/travel-transport-log.component.ts");
/* harmony import */ var _Industry_telecom_telecom_component__WEBPACK_IMPORTED_MODULE_64__ = __webpack_require__(/*! ./Industry/telecom/telecom.component */ "./src/app/Industry/telecom/telecom.component.ts");
/* harmony import */ var _Industry_social_computing_social_computing_component__WEBPACK_IMPORTED_MODULE_65__ = __webpack_require__(/*! ./Industry/social-computing/social-computing.component */ "./src/app/Industry/social-computing/social-computing.component.ts");
/* harmony import */ var _Industry_retail_and_consumer_retail_and_consumer_component__WEBPACK_IMPORTED_MODULE_66__ = __webpack_require__(/*! ./Industry/retail-and-consumer/retail-and-consumer.component */ "./src/app/Industry/retail-and-consumer/retail-and-consumer.component.ts");
/* harmony import */ var _Industry_real_estate_real_estate_component__WEBPACK_IMPORTED_MODULE_67__ = __webpack_require__(/*! ./Industry/real-estate/real-estate.component */ "./src/app/Industry/real-estate/real-estate.component.ts");
/* harmony import */ var _Industry_media_entertainment_media_entertainment_component__WEBPACK_IMPORTED_MODULE_68__ = __webpack_require__(/*! ./Industry/media-entertainment/media-entertainment.component */ "./src/app/Industry/media-entertainment/media-entertainment.component.ts");
/* harmony import */ var _Industry_manufacturing_manufacturing_component__WEBPACK_IMPORTED_MODULE_69__ = __webpack_require__(/*! ./Industry/manufacturing/manufacturing.component */ "./src/app/Industry/manufacturing/manufacturing.component.ts");
/* harmony import */ var _Industry_it_it_component__WEBPACK_IMPORTED_MODULE_70__ = __webpack_require__(/*! ./Industry/it/it.component */ "./src/app/Industry/it/it.component.ts");
/* harmony import */ var _Industry_hospitality_hospitality_component__WEBPACK_IMPORTED_MODULE_71__ = __webpack_require__(/*! ./Industry/hospitality/hospitality.component */ "./src/app/Industry/hospitality/hospitality.component.ts");
/* harmony import */ var _Industry_government_government_component__WEBPACK_IMPORTED_MODULE_72__ = __webpack_require__(/*! ./Industry/government/government.component */ "./src/app/Industry/government/government.component.ts");
/* harmony import */ var _Industry_education_education_component__WEBPACK_IMPORTED_MODULE_73__ = __webpack_require__(/*! ./Industry/education/education.component */ "./src/app/Industry/education/education.component.ts");
/* harmony import */ var _Industry_digital_marketing_digital_marketing_component__WEBPACK_IMPORTED_MODULE_74__ = __webpack_require__(/*! ./Industry/digital-marketing/digital-marketing.component */ "./src/app/Industry/digital-marketing/digital-marketing.component.ts");
/* harmony import */ var _Industry_consumer_electronics_consumer_electronics_component__WEBPACK_IMPORTED_MODULE_75__ = __webpack_require__(/*! ./Industry/consumer-electronics/consumer-electronics.component */ "./src/app/Industry/consumer-electronics/consumer-electronics.component.ts");
/* harmony import */ var _Industry_banking_and_financial_banking_and_financial_component__WEBPACK_IMPORTED_MODULE_76__ = __webpack_require__(/*! ./Industry/banking-and-financial/banking-and-financial.component */ "./src/app/Industry/banking-and-financial/banking-and-financial.component.ts");
/* harmony import */ var _Industry_automotive_automotive_component__WEBPACK_IMPORTED_MODULE_77__ = __webpack_require__(/*! ./Industry/automotive/automotive.component */ "./src/app/Industry/automotive/automotive.component.ts");
/* harmony import */ var _home1_home1_component__WEBPACK_IMPORTED_MODULE_78__ = __webpack_require__(/*! ./home1/home1.component */ "./src/app/home1/home1.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};















































































var appRoutes = [
    {
        path: '',
        component: _home1_home1_component__WEBPACK_IMPORTED_MODULE_78__["Home1Component"],
    },
    {
        path: 'home',
        component: _home1_home1_component__WEBPACK_IMPORTED_MODULE_78__["Home1Component"],
    },
    { path: 'portfolio', component: _portfolio_portfolio_component__WEBPACK_IMPORTED_MODULE_5__["PortfolioComponent"] },
    { path: 'careers', component: _careers_careers_component__WEBPACK_IMPORTED_MODULE_6__["CareersComponent"] },
    { path: 'android-app-development', component: _servicessection_android_app_development_android_app_development_component__WEBPACK_IMPORTED_MODULE_7__["AndroidAppDevelopmentComponent"] },
    { path: 'android-developer', component: _servicessection_android_developer_android_developer_component__WEBPACK_IMPORTED_MODULE_37__["AndroidDeveloperComponent"] },
    { path: 'iphone-app-development', component: _servicessection_iphone_app_development_iphone_app_development_component__WEBPACK_IMPORTED_MODULE_8__["IphoneAppDevelopmentComponent"] },
    { path: 'ipad-app-development', component: _servicessection_ipad_app_development_ipad_app_development_component__WEBPACK_IMPORTED_MODULE_9__["IpadAppDevelopmentComponent"] },
    { path: 'ios-developer', component: _servicessection_ios_developer_ios_developer_component__WEBPACK_IMPORTED_MODULE_36__["IosDeveloperComponent"] },
    { path: 'adf-developer', component: _servicessection_adf_development_adf_development_component__WEBPACK_IMPORTED_MODULE_10__["AdfDevelopmentComponent"] },
    { path: 'adf-and-web-development', component: _servicessection_adf_webcenter_development_adf_webcenter_development_component__WEBPACK_IMPORTED_MODULE_11__["AdfWebcenterDevelopmentComponent"] },
    { path: 'blackberry-app-development', component: _servicessection_blackberry_app_development_blackberry_app_development_component__WEBPACK_IMPORTED_MODULE_12__["BlackberryAppDevelopmentComponent"] },
    { path: 'cake-php-development', component: _servicessection_cake_php_development_cake_php_development_component__WEBPACK_IMPORTED_MODULE_13__["CakePhpDevelopmentComponent"] },
    { path: 'cmc-development', component: _servicessection_cmc_development_cmc_development_component__WEBPACK_IMPORTED_MODULE_19__["CmcDevelopmentComponent"] },
    { path: 'codeigniter-development', component: _servicessection_codeigniter_development_codeigniter_development_component__WEBPACK_IMPORTED_MODULE_18__["CodeigniterDevelopmentComponent"] },
    { path: 'drupal-development', component: _servicessection_drupal_development_drupal_development_component__WEBPACK_IMPORTED_MODULE_17__["DrupalDevelopmentComponent"] },
    { path: 'ecommerce-developer', component: _servicessection_ecommerce_developer_ecommerce_developer_component__WEBPACK_IMPORTED_MODULE_16__["EcommerceDeveloperComponent"] },
    { path: 'e-commerce-development', component: _servicessection_ecommerce_development_ecommerce_development_component__WEBPACK_IMPORTED_MODULE_15__["EcommerceDevelopmentComponent"] },
    { path: 'facebook-app-development', component: _servicessection_facebook_app_development_facebook_app_development_component__WEBPACK_IMPORTED_MODULE_14__["FacebookAppDevelopmentComponent"] },
    { path: 'hybrid-app-development', component: _servicessection_hybrid_app_development_hybrid_app_development_component__WEBPACK_IMPORTED_MODULE_24__["HybridAppDevelopmentComponent"] },
    { path: 'internet-of-things', component: _servicessection_internet_of_things_internet_of_things_component__WEBPACK_IMPORTED_MODULE_23__["InternetOfThingsComponent"] },
    { path: 'java-&-j2ee-development', component: _servicessection_java_j2ee_development_java_j2ee_development_component__WEBPACK_IMPORTED_MODULE_22__["JavaJ2eeDevelopmentComponent"] },
    { path: 'joomla-development', component: _servicessection_joomla_development_joomla_development_component__WEBPACK_IMPORTED_MODULE_21__["JoomlaDevelopmentComponent"] },
    { path: 'laravel-development', component: _servicessection_laravel_development_laravel_development_component__WEBPACK_IMPORTED_MODULE_20__["LaravelDevelopmentComponent"] },
    { path: 'magento-developer', component: _servicessection_magento_developer_magento_developer_component__WEBPACK_IMPORTED_MODULE_31__["MagentoDeveloperComponent"] },
    { path: 'magento-development', component: _servicessection_magento_development_magento_development_component__WEBPACK_IMPORTED_MODULE_30__["MagentoDevelopmentComponent"] },
    { path: 'phone-gap-development', component: _servicessection_phone_gap_development_phone_gap_development_component__WEBPACK_IMPORTED_MODULE_29__["PhoneGapDevelopmentComponent"] },
    { path: 'php-developer', component: _servicessection_php_developer_php_developer_component__WEBPACK_IMPORTED_MODULE_28__["PhpDeveloperComponent"] },
    { path: 'php-development', component: _servicessection_php_development_php_development_component__WEBPACK_IMPORTED_MODULE_27__["PhpDevelopmentComponent"] },
    { path: 'python-developer', component: _servicessection_python_developer_python_developer_component__WEBPACK_IMPORTED_MODULE_26__["PythonDeveloperComponent"] },
    { path: 'python-web-development', component: _servicessection_python_web_development_python_web_development_component__WEBPACK_IMPORTED_MODULE_25__["PythonWebDevelopmentComponent"] },
    { path: 'responsive-web-development', component: _servicessection_responsive_web_development_responsive_web_development_component__WEBPACK_IMPORTED_MODULE_45__["ResponsiveWebDevelopmentComponent"] },
    { path: 'ruby-on-rails-developer', component: _servicessection_ruby_on_rails_developer_ruby_on_rails_developer_component__WEBPACK_IMPORTED_MODULE_44__["RubyOnRailsDeveloperComponent"] },
    { path: 'ruby-on-rails-development', component: _servicessection_ruby_on_rails_development_ruby_on_rails_development_component__WEBPACK_IMPORTED_MODULE_43__["RubyOnRailsDevelopmentComponent"] },
    { path: 'wearable-devices', component: _servicessection_wearable_devices_wearable_devices_component__WEBPACK_IMPORTED_MODULE_42__["WearableDevicesComponent"] },
    { path: 'web-application-development', component: _servicessection_web_application_development_web_application_development_component__WEBPACK_IMPORTED_MODULE_41__["WebApplicationDevelopmentComponent"] },
    { path: 'windows-app-development', component: _servicessection_windows_app_development_windows_app_development_component__WEBPACK_IMPORTED_MODULE_40__["WindowsAppDevelopmentComponent"] },
    { path: 'word-press-development', component: _servicessection_word_press_development_word_press_development_component__WEBPACK_IMPORTED_MODULE_39__["WordPressDevelopmentComponent"] },
    { path: 'xcart-development', component: _servicessection_xcart_development_xcart_development_component__WEBPACK_IMPORTED_MODULE_38__["XCartDevelopmentComponent"] },
    { path: 'success-stories', component: _AboutUs_sucess_stories_sucess_stories_component__WEBPACK_IMPORTED_MODULE_35__["SucessStoriesComponent"] },
    { path: 'our-clients', component: _AboutUs_our_clients_our_clients_component__WEBPACK_IMPORTED_MODULE_34__["OurClientsComponent"] },
    { path: 'blogs', component: _AboutUs_blogs_blogs_component__WEBPACK_IMPORTED_MODULE_33__["BlogsComponent"] },
    { path: 'contact-us', component: _contact_us_contact_us_component__WEBPACK_IMPORTED_MODULE_32__["ContactUsComponent"] },
    { path: 'automative', component: _Industry_automotive_automotive_component__WEBPACK_IMPORTED_MODULE_77__["AutomotiveComponent"] },
    { path: 'banking-&-financial', component: _Industry_banking_and_financial_banking_and_financial_component__WEBPACK_IMPORTED_MODULE_76__["BankingAndFinancialComponent"] },
    { path: 'consumer-electronics', component: _Industry_consumer_electronics_consumer_electronics_component__WEBPACK_IMPORTED_MODULE_75__["ConsumerElectronicsComponent"] },
    { path: 'digital-marketing', component: _Industry_digital_marketing_digital_marketing_component__WEBPACK_IMPORTED_MODULE_74__["DigitalMarketingComponent"] },
    { path: 'education', component: _Industry_education_education_component__WEBPACK_IMPORTED_MODULE_73__["EducationComponent"] },
    { path: 'government', component: _Industry_government_government_component__WEBPACK_IMPORTED_MODULE_72__["GovernmentComponent"] },
    { path: 'hospitality', component: _Industry_hospitality_hospitality_component__WEBPACK_IMPORTED_MODULE_71__["HospitalityComponent"] },
    { path: 'it', component: _Industry_it_it_component__WEBPACK_IMPORTED_MODULE_70__["ItComponent"] },
    { path: 'manufacturing', component: _Industry_manufacturing_manufacturing_component__WEBPACK_IMPORTED_MODULE_69__["ManufacturingComponent"] },
    { path: 'media-entertainment', component: _Industry_media_entertainment_media_entertainment_component__WEBPACK_IMPORTED_MODULE_68__["MediaEntertainmentComponent"] },
    { path: 'real-estate', component: _Industry_real_estate_real_estate_component__WEBPACK_IMPORTED_MODULE_67__["RealEstateComponent"] },
    { path: 'retail-and-consumer', component: _Industry_retail_and_consumer_retail_and_consumer_component__WEBPACK_IMPORTED_MODULE_66__["RetailAndConsumerComponent"] },
    { path: 'social-computing', component: _Industry_social_computing_social_computing_component__WEBPACK_IMPORTED_MODULE_65__["SocialComputingComponent"] },
    { path: 'telecom', component: _Industry_telecom_telecom_component__WEBPACK_IMPORTED_MODULE_64__["TelecomComponent"] },
    { path: 'travel-transport-&-log', component: _Industry_travel_transport_log_travel_transport_log_component__WEBPACK_IMPORTED_MODULE_63__["TravelTransportLogComponent"] },
    { path: 'life-science-&-healthcare', component: _Industry_life_science_and_health_care_life_science_and_health_care_component__WEBPACK_IMPORTED_MODULE_62__["LifeScienceAndHealthCareComponent"] },
    { path: 'architechtural-network', component: _Products_architechtural_network_architechtural_network_component__WEBPACK_IMPORTED_MODULE_61__["ArchitechturalNetworkComponent"] },
    { path: 'construction-suppliers', component: _Products_construction_suppliers_construction_suppliers_component__WEBPACK_IMPORTED_MODULE_60__["ConstructionSuppliersComponent"] },
    { path: 'enterprice-crm-sales', component: _Products_enterprice_crm_sales_enterprice_crm_sales_component__WEBPACK_IMPORTED_MODULE_59__["EnterpriceCrmSalesComponent"] },
    { path: 'enterprice-crm-support', component: _Products_enterprice_crm_support_enterprice_crm_support_component__WEBPACK_IMPORTED_MODULE_58__["EnterpriceCrmSupportComponent"] },
    { path: 'field-force-automation', component: _Products_field_force_automation_field_force_automation_component__WEBPACK_IMPORTED_MODULE_57__["FieldForceAutomationComponent"] },
    { path: 'food-on-wheel', component: _Products_food_on_wheel_food_on_wheel_component__WEBPACK_IMPORTED_MODULE_56__["FoodOnWheelComponent"] },
    { path: 'gps-assets-traking', component: _Products_gps_assets_traking_gps_assets_traking_component__WEBPACK_IMPORTED_MODULE_55__["GpsAssetsTrakingComponent"] },
    { path: 'gps-mobile-traking', component: _Products_gps_mobile_traking_gps_mobile_traking_component__WEBPACK_IMPORTED_MODULE_54__["GpsMobileTrakingComponent"] },
    { path: 'gps-vehicle-traking', component: _Products_gps_vehicle_traking_gps_vehicle_traking_component__WEBPACK_IMPORTED_MODULE_53__["GpsVehicleTrakingComponent"] },
    { path: 'gps-wearables', component: _Products_gps_wearables_gps_wearables_component__WEBPACK_IMPORTED_MODULE_52__["GpsWearablesComponent"] },
    { path: 'hotel-booking-engine', component: _Products_hotel_booking_engine_hotel_booking_engine_component__WEBPACK_IMPORTED_MODULE_51__["HotelBookingEngineComponent"] },
    { path: 'hotel-inventory-management', component: _Products_hotel_inventory_management_hotel_inventory_management_component__WEBPACK_IMPORTED_MODULE_50__["HotelInventoryManagementComponent"] },
    { path: 'hotel-management-system', component: _Products_hotel_management_system_hotel_management_system_component__WEBPACK_IMPORTED_MODULE_49__["HotelManagementSystemComponent"] },
    { path: 'sass-based-erp', component: _Products_sass_based_erp_sass_based_erp_component__WEBPACK_IMPORTED_MODULE_48__["SassBasedErpComponent"] },
    { path: 'sass-based-payroll', component: _Products_sass_based_payroll_sass_based_payroll_component__WEBPACK_IMPORTED_MODULE_47__["SassBasedPayrollComponent"] },
    { path: 'travel-booking-e-commerce', component: _Products_travel_booking_ecommerce_travel_booking_ecommerce_component__WEBPACK_IMPORTED_MODULE_46__["TravelBookingEcommerceComponent"] },
    { path: 'xamarin-development', component: _components_xamarin_development_xamarin_development_component__WEBPACK_IMPORTED_MODULE_2__["XamarinDevelopmentComponent"] },
    { path: 'location-based-technology-formobile-app', component: _components_location_based_technology_formobile_app_location_based_technology_formobile_app_component__WEBPACK_IMPORTED_MODULE_1__["LocationBasedTechnologyFormobileAppComponent"] },
    { path: 'how-indexing-and-deep-linking-help', component: _components_how_indexing_and_deep_linking_help_how_indexing_and_deep_linking_help_component__WEBPACK_IMPORTED_MODULE_0__["HowIndexingAndDeepLinkingHelpComponent"] },
    {
        path: '**',
        redirectTo: '',
    },
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forRoot(appRoutes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n        <app-header (featureSelected)=\"onNavigate($event)\"></app-header>\r\n\r\n\r\n        <router-outlet></router-outlet>\r\n      \r\n   <app-footer></app-footer>\r\n\r\n\r\n\r\n\r\n\r\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
    }
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _toaster_service_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./toaster-service.service */ "./src/app/toaster-service.service.ts");
/* harmony import */ var _subscribe_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./subscribe.service */ "./src/app/subscribe.service.ts");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _header_header_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./header/header.component */ "./src/app/header/header.component.ts");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./home/home.component */ "./src/app/home/home.component.ts");
/* harmony import */ var _portfolio_portfolio_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./portfolio/portfolio.component */ "./src/app/portfolio/portfolio.component.ts");
/* harmony import */ var _careers_careers_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./careers/careers.component */ "./src/app/careers/careers.component.ts");
/* harmony import */ var src_app_app_routing_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! src/app/app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var _servicessection_servicessection_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./servicessection/servicessection.component */ "./src/app/servicessection/servicessection.component.ts");
/* harmony import */ var _servicessection_android_app_development_android_app_development_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./servicessection/android-app-development/android-app-development.component */ "./src/app/servicessection/android-app-development/android-app-development.component.ts");
/* harmony import */ var _servicessection_iphone_app_development_iphone_app_development_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./servicessection/iphone-app-development/iphone-app-development.component */ "./src/app/servicessection/iphone-app-development/iphone-app-development.component.ts");
/* harmony import */ var _servicessection_ipad_app_development_ipad_app_development_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./servicessection/ipad-app-development/ipad-app-development.component */ "./src/app/servicessection/ipad-app-development/ipad-app-development.component.ts");
/* harmony import */ var _servicessection_windows_app_development_windows_app_development_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./servicessection/windows-app-development/windows-app-development.component */ "./src/app/servicessection/windows-app-development/windows-app-development.component.ts");
/* harmony import */ var _servicessection_blackberry_app_development_blackberry_app_development_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./servicessection/blackberry-app-development/blackberry-app-development.component */ "./src/app/servicessection/blackberry-app-development/blackberry-app-development.component.ts");
/* harmony import */ var _servicessection_hybrid_app_development_hybrid_app_development_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./servicessection/hybrid-app-development/hybrid-app-development.component */ "./src/app/servicessection/hybrid-app-development/hybrid-app-development.component.ts");
/* harmony import */ var _servicessection_wearable_devices_wearable_devices_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./servicessection/wearable-devices/wearable-devices.component */ "./src/app/servicessection/wearable-devices/wearable-devices.component.ts");
/* harmony import */ var _servicessection_internet_of_things_internet_of_things_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./servicessection/internet-of-things/internet-of-things.component */ "./src/app/servicessection/internet-of-things/internet-of-things.component.ts");
/* harmony import */ var _servicessection_php_development_php_development_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./servicessection/php-development/php-development.component */ "./src/app/servicessection/php-development/php-development.component.ts");
/* harmony import */ var _servicessection_rar_development_rar_development_component__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./servicessection/rar-development/rar-development.component */ "./src/app/servicessection/rar-development/rar-development.component.ts");
/* harmony import */ var _servicessection_java_j2ee_development_java_j2ee_development_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./servicessection/java-j2ee-development/java-j2ee-development.component */ "./src/app/servicessection/java-j2ee-development/java-j2ee-development.component.ts");
/* harmony import */ var _servicessection_responsive_web_development_responsive_web_development_component__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./servicessection/responsive-web-development/responsive-web-development.component */ "./src/app/servicessection/responsive-web-development/responsive-web-development.component.ts");
/* harmony import */ var _servicessection_python_web_development_python_web_development_component__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./servicessection/python-web-development/python-web-development.component */ "./src/app/servicessection/python-web-development/python-web-development.component.ts");
/* harmony import */ var _servicessection_adf_webcenter_development_adf_webcenter_development_component__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ./servicessection/adf-webcenter-development/adf-webcenter-development.component */ "./src/app/servicessection/adf-webcenter-development/adf-webcenter-development.component.ts");
/* harmony import */ var _servicessection_facebook_app_development_facebook_app_development_component__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ./servicessection/facebook-app-development/facebook-app-development.component */ "./src/app/servicessection/facebook-app-development/facebook-app-development.component.ts");
/* harmony import */ var _servicessection_codeigniter_development_codeigniter_development_component__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! ./servicessection/codeigniter-development/codeigniter-development.component */ "./src/app/servicessection/codeigniter-development/codeigniter-development.component.ts");
/* harmony import */ var _servicessection_cake_php_development_cake_php_development_component__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! ./servicessection/cake-php-development/cake-php-development.component */ "./src/app/servicessection/cake-php-development/cake-php-development.component.ts");
/* harmony import */ var _servicessection_laravel_development_laravel_development_component__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! ./servicessection/laravel-development/laravel-development.component */ "./src/app/servicessection/laravel-development/laravel-development.component.ts");
/* harmony import */ var _servicessection_cmc_development_cmc_development_component__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! ./servicessection/cmc-development/cmc-development.component */ "./src/app/servicessection/cmc-development/cmc-development.component.ts");
/* harmony import */ var _servicessection_word_press_development_word_press_development_component__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(/*! ./servicessection/word-press-development/word-press-development.component */ "./src/app/servicessection/word-press-development/word-press-development.component.ts");
/* harmony import */ var _servicessection_drupal_development_drupal_development_component__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(/*! ./servicessection/drupal-development/drupal-development.component */ "./src/app/servicessection/drupal-development/drupal-development.component.ts");
/* harmony import */ var _servicessection_joomla_development_joomla_development_component__WEBPACK_IMPORTED_MODULE_34__ = __webpack_require__(/*! ./servicessection/joomla-development/joomla-development.component */ "./src/app/servicessection/joomla-development/joomla-development.component.ts");
/* harmony import */ var _servicessection_ecommerce_development_ecommerce_development_component__WEBPACK_IMPORTED_MODULE_35__ = __webpack_require__(/*! ./servicessection/ecommerce-development/ecommerce-development.component */ "./src/app/servicessection/ecommerce-development/ecommerce-development.component.ts");
/* harmony import */ var _servicessection_magento_development_magento_development_component__WEBPACK_IMPORTED_MODULE_36__ = __webpack_require__(/*! ./servicessection/magento-development/magento-development.component */ "./src/app/servicessection/magento-development/magento-development.component.ts");
/* harmony import */ var _servicessection_xcart_development_xcart_development_component__WEBPACK_IMPORTED_MODULE_37__ = __webpack_require__(/*! ./servicessection/xcart-development/xcart-development.component */ "./src/app/servicessection/xcart-development/xcart-development.component.ts");
/* harmony import */ var _servicessection_web_application_development_web_application_development_component__WEBPACK_IMPORTED_MODULE_38__ = __webpack_require__(/*! ./servicessection/web-application-development/web-application-development.component */ "./src/app/servicessection/web-application-development/web-application-development.component.ts");
/* harmony import */ var _servicessection_phone_gap_development_phone_gap_development_component__WEBPACK_IMPORTED_MODULE_39__ = __webpack_require__(/*! ./servicessection/phone-gap-development/phone-gap-development.component */ "./src/app/servicessection/phone-gap-development/phone-gap-development.component.ts");
/* harmony import */ var _servicessection_adf_development_adf_development_component__WEBPACK_IMPORTED_MODULE_40__ = __webpack_require__(/*! ./servicessection/adf-development/adf-development.component */ "./src/app/servicessection/adf-development/adf-development.component.ts");
/* harmony import */ var _servicessection_ecommerce_developer_ecommerce_developer_component__WEBPACK_IMPORTED_MODULE_41__ = __webpack_require__(/*! ./servicessection/ecommerce-developer/ecommerce-developer.component */ "./src/app/servicessection/ecommerce-developer/ecommerce-developer.component.ts");
/* harmony import */ var _home1_home1_component__WEBPACK_IMPORTED_MODULE_42__ = __webpack_require__(/*! ./home1/home1.component */ "./src/app/home1/home1.component.ts");
/* harmony import */ var _footer3_footer3_component__WEBPACK_IMPORTED_MODULE_43__ = __webpack_require__(/*! ./footer3/footer3.component */ "./src/app/footer3/footer3.component.ts");
/* harmony import */ var _footer_footer_component__WEBPACK_IMPORTED_MODULE_44__ = __webpack_require__(/*! ./footer/footer.component */ "./src/app/footer/footer.component.ts");
/* harmony import */ var _footer4_footer4_component__WEBPACK_IMPORTED_MODULE_45__ = __webpack_require__(/*! ./footer4/footer4.component */ "./src/app/footer4/footer4.component.ts");
/* harmony import */ var _footer5_footer5_component__WEBPACK_IMPORTED_MODULE_46__ = __webpack_require__(/*! ./footer5/footer5.component */ "./src/app/footer5/footer5.component.ts");
/* harmony import */ var _footer6_footer6_component__WEBPACK_IMPORTED_MODULE_47__ = __webpack_require__(/*! ./footer6/footer6.component */ "./src/app/footer6/footer6.component.ts");
/* harmony import */ var _footer2_footer2_component__WEBPACK_IMPORTED_MODULE_48__ = __webpack_require__(/*! ./footer2/footer2.component */ "./src/app/footer2/footer2.component.ts");
/* harmony import */ var _servicessection_magento_developer_magento_developer_component__WEBPACK_IMPORTED_MODULE_49__ = __webpack_require__(/*! ./servicessection/magento-developer/magento-developer.component */ "./src/app/servicessection/magento-developer/magento-developer.component.ts");
/* harmony import */ var _servicessection_php_developer_php_developer_component__WEBPACK_IMPORTED_MODULE_50__ = __webpack_require__(/*! ./servicessection/php-developer/php-developer.component */ "./src/app/servicessection/php-developer/php-developer.component.ts");
/* harmony import */ var _servicessection_python_developer_python_developer_component__WEBPACK_IMPORTED_MODULE_51__ = __webpack_require__(/*! ./servicessection/python-developer/python-developer.component */ "./src/app/servicessection/python-developer/python-developer.component.ts");
/* harmony import */ var _footer1_footer1_component__WEBPACK_IMPORTED_MODULE_52__ = __webpack_require__(/*! ./footer1/footer1.component */ "./src/app/footer1/footer1.component.ts");
/* harmony import */ var _servicessection_ruby_on_rails_developer_ruby_on_rails_developer_component__WEBPACK_IMPORTED_MODULE_53__ = __webpack_require__(/*! ./servicessection/ruby-on-rails-developer/ruby-on-rails-developer.component */ "./src/app/servicessection/ruby-on-rails-developer/ruby-on-rails-developer.component.ts");
/* harmony import */ var _servicessection_ruby_on_rails_development_ruby_on_rails_development_component__WEBPACK_IMPORTED_MODULE_54__ = __webpack_require__(/*! ./servicessection/ruby-on-rails-development/ruby-on-rails-development.component */ "./src/app/servicessection/ruby-on-rails-development/ruby-on-rails-development.component.ts");
/* harmony import */ var _servicessection_android_developer_android_developer_component__WEBPACK_IMPORTED_MODULE_55__ = __webpack_require__(/*! ./servicessection/android-developer/android-developer.component */ "./src/app/servicessection/android-developer/android-developer.component.ts");
/* harmony import */ var _servicessection_ios_developer_ios_developer_component__WEBPACK_IMPORTED_MODULE_56__ = __webpack_require__(/*! ./servicessection/ios-developer/ios-developer.component */ "./src/app/servicessection/ios-developer/ios-developer.component.ts");
/* harmony import */ var _AboutUs_our_clients_our_clients_component__WEBPACK_IMPORTED_MODULE_57__ = __webpack_require__(/*! ./AboutUs/our-clients/our-clients.component */ "./src/app/AboutUs/our-clients/our-clients.component.ts");
/* harmony import */ var _AboutUs_sucess_stories_sucess_stories_component__WEBPACK_IMPORTED_MODULE_58__ = __webpack_require__(/*! ./AboutUs/sucess-stories/sucess-stories.component */ "./src/app/AboutUs/sucess-stories/sucess-stories.component.ts");
/* harmony import */ var _AboutUs_blogs_blogs_component__WEBPACK_IMPORTED_MODULE_59__ = __webpack_require__(/*! ./AboutUs/blogs/blogs.component */ "./src/app/AboutUs/blogs/blogs.component.ts");
/* harmony import */ var _Industry_automotive_automotive_component__WEBPACK_IMPORTED_MODULE_60__ = __webpack_require__(/*! ./Industry/automotive/automotive.component */ "./src/app/Industry/automotive/automotive.component.ts");
/* harmony import */ var _Industry_digital_marketing_digital_marketing_component__WEBPACK_IMPORTED_MODULE_61__ = __webpack_require__(/*! ./Industry/digital-marketing/digital-marketing.component */ "./src/app/Industry/digital-marketing/digital-marketing.component.ts");
/* harmony import */ var _Industry_hospitality_hospitality_component__WEBPACK_IMPORTED_MODULE_62__ = __webpack_require__(/*! ./Industry/hospitality/hospitality.component */ "./src/app/Industry/hospitality/hospitality.component.ts");
/* harmony import */ var _Industry_media_entertainment_media_entertainment_component__WEBPACK_IMPORTED_MODULE_63__ = __webpack_require__(/*! ./Industry/media-entertainment/media-entertainment.component */ "./src/app/Industry/media-entertainment/media-entertainment.component.ts");
/* harmony import */ var _Industry_real_estate_real_estate_component__WEBPACK_IMPORTED_MODULE_64__ = __webpack_require__(/*! ./Industry/real-estate/real-estate.component */ "./src/app/Industry/real-estate/real-estate.component.ts");
/* harmony import */ var _Industry_social_computing_social_computing_component__WEBPACK_IMPORTED_MODULE_65__ = __webpack_require__(/*! ./Industry/social-computing/social-computing.component */ "./src/app/Industry/social-computing/social-computing.component.ts");
/* harmony import */ var _Industry_banking_and_financial_banking_and_financial_component__WEBPACK_IMPORTED_MODULE_66__ = __webpack_require__(/*! ./Industry/banking-and-financial/banking-and-financial.component */ "./src/app/Industry/banking-and-financial/banking-and-financial.component.ts");
/* harmony import */ var _Industry_education_education_component__WEBPACK_IMPORTED_MODULE_67__ = __webpack_require__(/*! ./Industry/education/education.component */ "./src/app/Industry/education/education.component.ts");
/* harmony import */ var _Industry_it_it_component__WEBPACK_IMPORTED_MODULE_68__ = __webpack_require__(/*! ./Industry/it/it.component */ "./src/app/Industry/it/it.component.ts");
/* harmony import */ var _Industry_manufacturing_manufacturing_component__WEBPACK_IMPORTED_MODULE_69__ = __webpack_require__(/*! ./Industry/manufacturing/manufacturing.component */ "./src/app/Industry/manufacturing/manufacturing.component.ts");
/* harmony import */ var _Industry_telecom_telecom_component__WEBPACK_IMPORTED_MODULE_70__ = __webpack_require__(/*! ./Industry/telecom/telecom.component */ "./src/app/Industry/telecom/telecom.component.ts");
/* harmony import */ var _Industry_consumer_electronics_consumer_electronics_component__WEBPACK_IMPORTED_MODULE_71__ = __webpack_require__(/*! ./Industry/consumer-electronics/consumer-electronics.component */ "./src/app/Industry/consumer-electronics/consumer-electronics.component.ts");
/* harmony import */ var _Industry_government_government_component__WEBPACK_IMPORTED_MODULE_72__ = __webpack_require__(/*! ./Industry/government/government.component */ "./src/app/Industry/government/government.component.ts");
/* harmony import */ var _Industry_retail_and_consumer_retail_and_consumer_component__WEBPACK_IMPORTED_MODULE_73__ = __webpack_require__(/*! ./Industry/retail-and-consumer/retail-and-consumer.component */ "./src/app/Industry/retail-and-consumer/retail-and-consumer.component.ts");
/* harmony import */ var _Industry_travel_transport_log_travel_transport_log_component__WEBPACK_IMPORTED_MODULE_74__ = __webpack_require__(/*! ./Industry/travel-transport-log/travel-transport-log.component */ "./src/app/Industry/travel-transport-log/travel-transport-log.component.ts");
/* harmony import */ var _contact_us_contact_us_component__WEBPACK_IMPORTED_MODULE_75__ = __webpack_require__(/*! ./contact-us/contact-us.component */ "./src/app/contact-us/contact-us.component.ts");
/* harmony import */ var _Industry_life_science_and_health_care_life_science_and_health_care_component__WEBPACK_IMPORTED_MODULE_76__ = __webpack_require__(/*! ./Industry/life-science-and-health-care/life-science-and-health-care.component */ "./src/app/Industry/life-science-and-health-care/life-science-and-health-care.component.ts");
/* harmony import */ var _Products_gps_mobile_traking_gps_mobile_traking_component__WEBPACK_IMPORTED_MODULE_77__ = __webpack_require__(/*! ./Products/gps-mobile-traking/gps-mobile-traking.component */ "./src/app/Products/gps-mobile-traking/gps-mobile-traking.component.ts");
/* harmony import */ var _Products_gps_vehicle_traking_gps_vehicle_traking_component__WEBPACK_IMPORTED_MODULE_78__ = __webpack_require__(/*! ./Products/gps-vehicle-traking/gps-vehicle-traking.component */ "./src/app/Products/gps-vehicle-traking/gps-vehicle-traking.component.ts");
/* harmony import */ var _Products_gps_wearables_gps_wearables_component__WEBPACK_IMPORTED_MODULE_79__ = __webpack_require__(/*! ./Products/gps-wearables/gps-wearables.component */ "./src/app/Products/gps-wearables/gps-wearables.component.ts");
/* harmony import */ var _Products_field_force_automation_field_force_automation_component__WEBPACK_IMPORTED_MODULE_80__ = __webpack_require__(/*! ./Products/field-force-automation/field-force-automation.component */ "./src/app/Products/field-force-automation/field-force-automation.component.ts");
/* harmony import */ var _Products_gps_assets_traking_gps_assets_traking_component__WEBPACK_IMPORTED_MODULE_81__ = __webpack_require__(/*! ./Products/gps-assets-traking/gps-assets-traking.component */ "./src/app/Products/gps-assets-traking/gps-assets-traking.component.ts");
/* harmony import */ var _Products_hotel_booking_engine_hotel_booking_engine_component__WEBPACK_IMPORTED_MODULE_82__ = __webpack_require__(/*! ./Products/hotel-booking-engine/hotel-booking-engine.component */ "./src/app/Products/hotel-booking-engine/hotel-booking-engine.component.ts");
/* harmony import */ var _Products_hotel_management_system_hotel_management_system_component__WEBPACK_IMPORTED_MODULE_83__ = __webpack_require__(/*! ./Products/hotel-management-system/hotel-management-system.component */ "./src/app/Products/hotel-management-system/hotel-management-system.component.ts");
/* harmony import */ var _Products_hotel_inventory_management_hotel_inventory_management_component__WEBPACK_IMPORTED_MODULE_84__ = __webpack_require__(/*! ./Products/hotel-inventory-management/hotel-inventory-management.component */ "./src/app/Products/hotel-inventory-management/hotel-inventory-management.component.ts");
/* harmony import */ var _Products_travel_booking_ecommerce_travel_booking_ecommerce_component__WEBPACK_IMPORTED_MODULE_85__ = __webpack_require__(/*! ./Products/travel-booking-ecommerce/travel-booking-ecommerce.component */ "./src/app/Products/travel-booking-ecommerce/travel-booking-ecommerce.component.ts");
/* harmony import */ var _Products_food_on_wheel_food_on_wheel_component__WEBPACK_IMPORTED_MODULE_86__ = __webpack_require__(/*! ./Products/food-on-wheel/food-on-wheel.component */ "./src/app/Products/food-on-wheel/food-on-wheel.component.ts");
/* harmony import */ var _Products_architechtural_network_architechtural_network_component__WEBPACK_IMPORTED_MODULE_87__ = __webpack_require__(/*! ./Products/architechtural-network/architechtural-network.component */ "./src/app/Products/architechtural-network/architechtural-network.component.ts");
/* harmony import */ var _Products_construction_suppliers_construction_suppliers_component__WEBPACK_IMPORTED_MODULE_88__ = __webpack_require__(/*! ./Products/construction-suppliers/construction-suppliers.component */ "./src/app/Products/construction-suppliers/construction-suppliers.component.ts");
/* harmony import */ var _Products_enterprice_crm_sales_enterprice_crm_sales_component__WEBPACK_IMPORTED_MODULE_89__ = __webpack_require__(/*! ./Products/enterprice-crm-sales/enterprice-crm-sales.component */ "./src/app/Products/enterprice-crm-sales/enterprice-crm-sales.component.ts");
/* harmony import */ var _Products_enterprice_crm_support_enterprice_crm_support_component__WEBPACK_IMPORTED_MODULE_90__ = __webpack_require__(/*! ./Products/enterprice-crm-support/enterprice-crm-support.component */ "./src/app/Products/enterprice-crm-support/enterprice-crm-support.component.ts");
/* harmony import */ var _Products_sass_based_erp_sass_based_erp_component__WEBPACK_IMPORTED_MODULE_91__ = __webpack_require__(/*! ./Products/sass-based-erp/sass-based-erp.component */ "./src/app/Products/sass-based-erp/sass-based-erp.component.ts");
/* harmony import */ var _Products_sass_based_payroll_sass_based_payroll_component__WEBPACK_IMPORTED_MODULE_92__ = __webpack_require__(/*! ./Products/sass-based-payroll/sass-based-payroll.component */ "./src/app/Products/sass-based-payroll/sass-based-payroll.component.ts");
/* harmony import */ var _components_xamarin_development_xamarin_development_component__WEBPACK_IMPORTED_MODULE_93__ = __webpack_require__(/*! ./components/xamarin-development/xamarin-development.component */ "./src/app/components/xamarin-development/xamarin-development.component.ts");
/* harmony import */ var _components_location_based_technology_formobile_app_location_based_technology_formobile_app_component__WEBPACK_IMPORTED_MODULE_94__ = __webpack_require__(/*! ./components/location-based-technology-formobile-app/location-based-technology-formobile-app.component */ "./src/app/components/location-based-technology-formobile-app/location-based-technology-formobile-app.component.ts");
/* harmony import */ var _components_how_indexing_and_deep_linking_help_how_indexing_and_deep_linking_help_component__WEBPACK_IMPORTED_MODULE_95__ = __webpack_require__(/*! ./components/how-indexing-and-deep-linking-help/how-indexing-and-deep-linking-help.component */ "./src/app/components/how-indexing-and-deep-linking-help/how-indexing-and-deep-linking-help.component.ts");
/* harmony import */ var _login_service__WEBPACK_IMPORTED_MODULE_96__ = __webpack_require__(/*! ./login.service */ "./src/app/login.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

































































































var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"],
                _header_header_component__WEBPACK_IMPORTED_MODULE_5__["HeaderComponent"],
                _home_home_component__WEBPACK_IMPORTED_MODULE_6__["HomeComponent"],
                _portfolio_portfolio_component__WEBPACK_IMPORTED_MODULE_7__["PortfolioComponent"],
                _careers_careers_component__WEBPACK_IMPORTED_MODULE_8__["CareersComponent"],
                _servicessection_servicessection_component__WEBPACK_IMPORTED_MODULE_12__["ServicessectionComponent"],
                _servicessection_android_app_development_android_app_development_component__WEBPACK_IMPORTED_MODULE_13__["AndroidAppDevelopmentComponent"],
                _servicessection_iphone_app_development_iphone_app_development_component__WEBPACK_IMPORTED_MODULE_14__["IphoneAppDevelopmentComponent"],
                _servicessection_ipad_app_development_ipad_app_development_component__WEBPACK_IMPORTED_MODULE_15__["IpadAppDevelopmentComponent"],
                _servicessection_windows_app_development_windows_app_development_component__WEBPACK_IMPORTED_MODULE_16__["WindowsAppDevelopmentComponent"],
                _servicessection_blackberry_app_development_blackberry_app_development_component__WEBPACK_IMPORTED_MODULE_17__["BlackberryAppDevelopmentComponent"],
                _servicessection_hybrid_app_development_hybrid_app_development_component__WEBPACK_IMPORTED_MODULE_18__["HybridAppDevelopmentComponent"],
                _servicessection_wearable_devices_wearable_devices_component__WEBPACK_IMPORTED_MODULE_19__["WearableDevicesComponent"],
                _servicessection_internet_of_things_internet_of_things_component__WEBPACK_IMPORTED_MODULE_20__["InternetOfThingsComponent"],
                _servicessection_php_development_php_development_component__WEBPACK_IMPORTED_MODULE_21__["PhpDevelopmentComponent"],
                _servicessection_rar_development_rar_development_component__WEBPACK_IMPORTED_MODULE_22__["RarDevelopmentComponent"],
                _servicessection_java_j2ee_development_java_j2ee_development_component__WEBPACK_IMPORTED_MODULE_23__["JavaJ2eeDevelopmentComponent"],
                _servicessection_responsive_web_development_responsive_web_development_component__WEBPACK_IMPORTED_MODULE_24__["ResponsiveWebDevelopmentComponent"],
                _servicessection_python_web_development_python_web_development_component__WEBPACK_IMPORTED_MODULE_25__["PythonWebDevelopmentComponent"],
                _servicessection_adf_webcenter_development_adf_webcenter_development_component__WEBPACK_IMPORTED_MODULE_26__["AdfWebcenterDevelopmentComponent"],
                _servicessection_facebook_app_development_facebook_app_development_component__WEBPACK_IMPORTED_MODULE_27__["FacebookAppDevelopmentComponent"],
                _servicessection_codeigniter_development_codeigniter_development_component__WEBPACK_IMPORTED_MODULE_28__["CodeigniterDevelopmentComponent"],
                _servicessection_cake_php_development_cake_php_development_component__WEBPACK_IMPORTED_MODULE_29__["CakePhpDevelopmentComponent"],
                _servicessection_laravel_development_laravel_development_component__WEBPACK_IMPORTED_MODULE_30__["LaravelDevelopmentComponent"],
                _servicessection_cmc_development_cmc_development_component__WEBPACK_IMPORTED_MODULE_31__["CmcDevelopmentComponent"],
                _servicessection_word_press_development_word_press_development_component__WEBPACK_IMPORTED_MODULE_32__["WordPressDevelopmentComponent"],
                _servicessection_drupal_development_drupal_development_component__WEBPACK_IMPORTED_MODULE_33__["DrupalDevelopmentComponent"],
                _servicessection_joomla_development_joomla_development_component__WEBPACK_IMPORTED_MODULE_34__["JoomlaDevelopmentComponent"],
                _servicessection_ecommerce_development_ecommerce_development_component__WEBPACK_IMPORTED_MODULE_35__["EcommerceDevelopmentComponent"],
                _servicessection_magento_development_magento_development_component__WEBPACK_IMPORTED_MODULE_36__["MagentoDevelopmentComponent"],
                _servicessection_xcart_development_xcart_development_component__WEBPACK_IMPORTED_MODULE_37__["XCartDevelopmentComponent"],
                _servicessection_web_application_development_web_application_development_component__WEBPACK_IMPORTED_MODULE_38__["WebApplicationDevelopmentComponent"],
                _servicessection_phone_gap_development_phone_gap_development_component__WEBPACK_IMPORTED_MODULE_39__["PhoneGapDevelopmentComponent"],
                _servicessection_adf_development_adf_development_component__WEBPACK_IMPORTED_MODULE_40__["AdfDevelopmentComponent"],
                _servicessection_ecommerce_developer_ecommerce_developer_component__WEBPACK_IMPORTED_MODULE_41__["EcommerceDeveloperComponent"],
                _home1_home1_component__WEBPACK_IMPORTED_MODULE_42__["Home1Component"],
                _footer3_footer3_component__WEBPACK_IMPORTED_MODULE_43__["Footer3Component"],
                _footer_footer_component__WEBPACK_IMPORTED_MODULE_44__["FooterComponent"],
                _footer4_footer4_component__WEBPACK_IMPORTED_MODULE_45__["Footer4Component"],
                _footer5_footer5_component__WEBPACK_IMPORTED_MODULE_46__["Footer5Component"],
                _footer6_footer6_component__WEBPACK_IMPORTED_MODULE_47__["Footer6Component"],
                _footer2_footer2_component__WEBPACK_IMPORTED_MODULE_48__["Footer2Component"],
                _servicessection_magento_developer_magento_developer_component__WEBPACK_IMPORTED_MODULE_49__["MagentoDeveloperComponent"],
                _servicessection_php_developer_php_developer_component__WEBPACK_IMPORTED_MODULE_50__["PhpDeveloperComponent"],
                _servicessection_python_developer_python_developer_component__WEBPACK_IMPORTED_MODULE_51__["PythonDeveloperComponent"],
                _footer1_footer1_component__WEBPACK_IMPORTED_MODULE_52__["Footer1Component"],
                _servicessection_ruby_on_rails_developer_ruby_on_rails_developer_component__WEBPACK_IMPORTED_MODULE_53__["RubyOnRailsDeveloperComponent"],
                _servicessection_ruby_on_rails_development_ruby_on_rails_development_component__WEBPACK_IMPORTED_MODULE_54__["RubyOnRailsDevelopmentComponent"],
                _servicessection_android_developer_android_developer_component__WEBPACK_IMPORTED_MODULE_55__["AndroidDeveloperComponent"],
                _servicessection_ios_developer_ios_developer_component__WEBPACK_IMPORTED_MODULE_56__["IosDeveloperComponent"],
                _AboutUs_our_clients_our_clients_component__WEBPACK_IMPORTED_MODULE_57__["OurClientsComponent"],
                _AboutUs_sucess_stories_sucess_stories_component__WEBPACK_IMPORTED_MODULE_58__["SucessStoriesComponent"],
                _AboutUs_blogs_blogs_component__WEBPACK_IMPORTED_MODULE_59__["BlogsComponent"],
                _Industry_automotive_automotive_component__WEBPACK_IMPORTED_MODULE_60__["AutomotiveComponent"],
                _Industry_digital_marketing_digital_marketing_component__WEBPACK_IMPORTED_MODULE_61__["DigitalMarketingComponent"],
                _Industry_hospitality_hospitality_component__WEBPACK_IMPORTED_MODULE_62__["HospitalityComponent"],
                _Industry_media_entertainment_media_entertainment_component__WEBPACK_IMPORTED_MODULE_63__["MediaEntertainmentComponent"],
                _Industry_real_estate_real_estate_component__WEBPACK_IMPORTED_MODULE_64__["RealEstateComponent"],
                _Industry_social_computing_social_computing_component__WEBPACK_IMPORTED_MODULE_65__["SocialComputingComponent"],
                _Industry_banking_and_financial_banking_and_financial_component__WEBPACK_IMPORTED_MODULE_66__["BankingAndFinancialComponent"],
                _Industry_education_education_component__WEBPACK_IMPORTED_MODULE_67__["EducationComponent"],
                _Industry_it_it_component__WEBPACK_IMPORTED_MODULE_68__["ItComponent"],
                _Industry_manufacturing_manufacturing_component__WEBPACK_IMPORTED_MODULE_69__["ManufacturingComponent"],
                _Industry_telecom_telecom_component__WEBPACK_IMPORTED_MODULE_70__["TelecomComponent"],
                _Industry_consumer_electronics_consumer_electronics_component__WEBPACK_IMPORTED_MODULE_71__["ConsumerElectronicsComponent"],
                _Industry_government_government_component__WEBPACK_IMPORTED_MODULE_72__["GovernmentComponent"],
                _Industry_retail_and_consumer_retail_and_consumer_component__WEBPACK_IMPORTED_MODULE_73__["RetailAndConsumerComponent"],
                _Industry_travel_transport_log_travel_transport_log_component__WEBPACK_IMPORTED_MODULE_74__["TravelTransportLogComponent"],
                _contact_us_contact_us_component__WEBPACK_IMPORTED_MODULE_75__["ContactUsComponent"],
                _Industry_life_science_and_health_care_life_science_and_health_care_component__WEBPACK_IMPORTED_MODULE_76__["LifeScienceAndHealthCareComponent"],
                _Products_gps_mobile_traking_gps_mobile_traking_component__WEBPACK_IMPORTED_MODULE_77__["GpsMobileTrakingComponent"],
                _Products_gps_vehicle_traking_gps_vehicle_traking_component__WEBPACK_IMPORTED_MODULE_78__["GpsVehicleTrakingComponent"],
                _Products_gps_wearables_gps_wearables_component__WEBPACK_IMPORTED_MODULE_79__["GpsWearablesComponent"],
                _Products_field_force_automation_field_force_automation_component__WEBPACK_IMPORTED_MODULE_80__["FieldForceAutomationComponent"],
                _Products_gps_assets_traking_gps_assets_traking_component__WEBPACK_IMPORTED_MODULE_81__["GpsAssetsTrakingComponent"],
                _Products_hotel_booking_engine_hotel_booking_engine_component__WEBPACK_IMPORTED_MODULE_82__["HotelBookingEngineComponent"],
                _Products_hotel_management_system_hotel_management_system_component__WEBPACK_IMPORTED_MODULE_83__["HotelManagementSystemComponent"],
                _Products_hotel_inventory_management_hotel_inventory_management_component__WEBPACK_IMPORTED_MODULE_84__["HotelInventoryManagementComponent"],
                _Products_travel_booking_ecommerce_travel_booking_ecommerce_component__WEBPACK_IMPORTED_MODULE_85__["TravelBookingEcommerceComponent"],
                _Products_food_on_wheel_food_on_wheel_component__WEBPACK_IMPORTED_MODULE_86__["FoodOnWheelComponent"],
                _Products_architechtural_network_architechtural_network_component__WEBPACK_IMPORTED_MODULE_87__["ArchitechturalNetworkComponent"],
                _Products_construction_suppliers_construction_suppliers_component__WEBPACK_IMPORTED_MODULE_88__["ConstructionSuppliersComponent"],
                _Products_enterprice_crm_sales_enterprice_crm_sales_component__WEBPACK_IMPORTED_MODULE_89__["EnterpriceCrmSalesComponent"],
                _Products_enterprice_crm_support_enterprice_crm_support_component__WEBPACK_IMPORTED_MODULE_90__["EnterpriceCrmSupportComponent"],
                _Products_sass_based_erp_sass_based_erp_component__WEBPACK_IMPORTED_MODULE_91__["SassBasedErpComponent"],
                _Products_sass_based_payroll_sass_based_payroll_component__WEBPACK_IMPORTED_MODULE_92__["SassBasedPayrollComponent"],
                _components_xamarin_development_xamarin_development_component__WEBPACK_IMPORTED_MODULE_93__["XamarinDevelopmentComponent"],
                _components_location_based_technology_formobile_app_location_based_technology_formobile_app_component__WEBPACK_IMPORTED_MODULE_94__["LocationBasedTechnologyFormobileAppComponent"],
                _components_how_indexing_and_deep_linking_help_how_indexing_and_deep_linking_help_component__WEBPACK_IMPORTED_MODULE_95__["HowIndexingAndDeepLinkingHelpComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["BrowserModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_10__["FormsModule"],
                _angular_http__WEBPACK_IMPORTED_MODULE_11__["HttpModule"],
                src_app_app_routing_module__WEBPACK_IMPORTED_MODULE_9__["AppRoutingModule"]
            ],
            providers: [_login_service__WEBPACK_IMPORTED_MODULE_96__["LoginService"], _subscribe_service__WEBPACK_IMPORTED_MODULE_1__["SubscribeService"], _toaster_service_service__WEBPACK_IMPORTED_MODULE_0__["ToasterService"]],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/careers/careers.component.css":
/*!***********************************************!*\
  !*** ./src/app/careers/careers.component.css ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/careers/careers.component.html":
/*!************************************************!*\
  !*** ./src/app/careers/careers.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section class=\"page-title ptb-30\">\r\n  <div class=\"container padding-top-40\">\r\n      <div class=\"row\">\r\n          <div class=\"col-md-12\">\r\n              <h6 class=\"pagetitles\">Careers</h6>\r\n              <ol class=\"breadcrumb\">\r\n                  <li><a href=\"#\">Home</a></li>\r\n                  <li><a href=\"#\">careers</a></li>\r\n              </ol>\r\n          </div>\r\n      </div>\r\n  </div>\r\n</section>\r\n<!--page title end-->\r\n\r\n\r\n\r\n<!-- Hero Section -->\r\n<section class=\"corporate-banner-1 bg-fixed parallax-bg height-350 valign-wrapper\" data-stellar-background-ratio=\"0.5\">\r\n  <div class=\"valign-cell\">\r\n    <div class=\"container\">\r\n      <div class=\"row\">\r\n          <div class=\"col-md-6 col-md-offset-6\">\r\n              <h1 class=\"intro-title text-capitalize\">Careers</h1>\r\n              <p class=\"lead\">Choose a job you love, and you will never have to work a day in your life</p>\r\n              <button type=\"button\" class=\"waves-effect waves-light btn\" data-toggle=\"modal\" data-target=\"#myModal\" >\r\n                        Apply for a Job\r\n                      </button>\r\n                      <div class=\"modal fade\" id=\"myModal\" tabindex=\"-1\"  style=\"width: 100%;min-height: 100%;background-color: transparent;\">\r\n                        <div class=\"modal-dialog\" >\r\n                          <div class=\"modal-content\">\r\n                            <div class=\"modal-header\">\r\n                              <button type=\"button\" class=\"close\" data-dismiss=\"modal\" ><span >&times;</span></button>\r\n                              <h4 class=\"modal-title\">Contact Us</h4>\r\n                            </div>\r\n                            <div class=\"modal-body\">\r\n                                  <div class=\"row\">\r\n                                  <div class=\"col-md-12\">\r\n                                    <form method=\"post\" id=\"contactForm\" (ngSubmit)=\"login();contactform.reset(); Success()\" #contactform=\"ngForm\" >\r\n                                      <div class=\"row\">\r\n                                        <div class=\"col-md-6\">\r\n                                          <div class=\"input-field\">\r\n                                            <i class=\"fa fa-user prefix fontcolors\"></i>\r\n                                            <input type=\"text\" name=\"name\" class=\"validate\" id=\"name\" [(ngModel)]='name' >\r\n                                            <label for=\"name\">Name</label>\r\n                                          </div>\r\n                                        </div><!-- /.col-md-6 -->\r\n                                        <div class=\"col-md-6\">\r\n                                          <div class=\"input-field\">\r\n                                            <i class=\"fa fa-envelope prefix fontcolors\"></i>\r\n                                            <input id=\"email\" type=\"email\" name=\"email\" class=\"validate\" [(ngModel)]='email' >\r\n                                            <label for=\"email\" data-error=\"wrong\" data-success=\"right\">Email</label>\r\n                                          </div>\r\n                                        </div><!-- /.col-md-6 -->\r\n                                      </div><!-- /.row -->\r\n\r\n                                      <div class=\"row\">\r\n                                        <div class=\"col-md-12\">\r\n                                          <div class=\"input-field\">\r\n                                            <i class=\"fa fa-book prefix fontcolors\"></i>\r\n                                            <input id=\"subject\" type=\"tel\" name=\"subject\" class=\"validate\" [(ngModel)]='subject' >\r\n                                            <label for=\"subject\">Subject</label>\r\n                                          </div>\r\n                                        </div><!-- /.col-md-6 -->\r\n                                        </div>\r\n                                      <div class=\"input-field\">\r\n                                        <i class=\"fa fa-commenting-o prefix fontcolors\"></i>\r\n                                        <textarea name=\"message\" id=\"message\"  class=\"form-control\" [(ngModel)]='message'  ></textarea>\r\n                                        <label for=\"message\">Message</label>\r\n                                      </div>\r\n                                      <button type=\"submit\" name=\"submit\" class=\"btn-sm btnclass\" [disabled]=\"!contactform.form.valid\" >Send Message</button>\r\n                                      </form>\r\n                                  </div>\r\n                            </div>\r\n                          </div>\r\n                        </div>\r\n                      </div>\r\n                      </div>\r\n          </div><!-- row -->\r\n      </div><!-- row -->\r\n    </div><!-- /.container -->\r\n  </div><!-- /.valign-cell -->\r\n</section>\r\n<!-- Hero Section End -->\r\n\r\n<div class=\"container ptb-30\">\r\n\r\n    <div class=\"row\">\r\n        <div class=\"col-md-4\">\r\n            <div class=\"featured-item hover-outline brand-hover radius-4\">\r\n                <div class=\"icon\">\r\n                    <i class=\"material-icons colored brand-icon\">chat</i>\r\n                </div>\r\n                <div class=\"desc\">\r\n                    <h2>Company Culture</h2>\r\n                    <p>some companies have a team-based culture with employee participation on all levels, while other have a more traditional and formal management style.We are example of an organization with a clear company culture</p>\r\n                </div>\r\n            </div><!-- /.featured-item -->\r\n        </div><!-- /.col-md-4 -->\r\n\r\n        <div class=\"col-md-4\">\r\n            <div class=\"featured-item hover-outline brand-hover radius-4\">\r\n                <div class=\"icon\">\r\n                    <i class=\"material-icons colored brand-icon\">important_devices</i>\r\n                </div>\r\n                <div class=\"desc\">\r\n                    <h2>Student and university programs</h2>\r\n                    <p>Bigperl Private solutions limited takes employees beyond the company to bring education to life with our unforgettable educational programs</p>\r\n                </div>\r\n            </div><!-- /.featured-item -->\r\n        </div><!-- /.col-md-4 -->\r\n\r\n        <div class=\"col-md-4\">\r\n            <div class=\"featured-item hover-outline brand-hover radius-4\">\r\n                <div class=\"icon\">\r\n                    <i class=\"material-icons colored brand-icon\">settings</i>\r\n                </div>\r\n                <div class=\"desc\">\r\n                    <h2>Bigperl IT Challenge</h2>\r\n                    <p>Technology advances rapidly and shows up in media on all sides. This means users, managers at all levels and even competitors pressure IT staff to implement this new technology just because it is new.</p>\r\n                </div>\r\n            </div><!-- /.featured-item -->\r\n        </div><!-- /.col-md-4 -->\r\n\r\n        <div class=\"col-md-4\">\r\n            <div class=\"featured-item hover-outline brand-hover radius-4\">\r\n                <div class=\"icon\">\r\n                    <i class=\"material-icons colored brand-icon\">mouse</i>\r\n                </div>\r\n                <div class=\"desc\">\r\n                    <h2>Mobile Applicaion Design</h2>\r\n                    <p>Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus.</p>\r\n                </div>\r\n            </div><!-- /.featured-item -->\r\n        </div><!-- /.col-md-4 -->\r\n\r\n        <div class=\"col-md-4\">\r\n            <div class=\"featured-item hover-outline brand-hover radius-4\">\r\n                <div class=\"icon\">\r\n                    <i class=\"material-icons colored brand-icon\">photo_camera</i>\r\n                </div>\r\n                <div class=\"desc\">\r\n                    <h2>Professional Photography</h2>\r\n                    <p>Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus.</p>\r\n                </div>\r\n            </div><!-- /.featured-item -->\r\n        </div><!-- /.col-md-4 -->\r\n\r\n        <div class=\"col-md-4\">\r\n            <div class=\"featured-item hover-outline brand-hover radius-4\">\r\n                <div class=\"icon\">\r\n                    <i class=\"material-icons colored brand-icon\">movie</i>\r\n                </div>\r\n                <div class=\"desc\">\r\n                    <h2>Moting Graphics Design</h2>\r\n                    <p>Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus.</p>\r\n                </div>\r\n            </div><!-- /.featured-item -->\r\n        </div><!-- /.col-md-4 -->\r\n    </div><!-- /.row -->\r\n</div>\r\n<section class=\"padding-top-20 padding-bottom-10 brand-bg\">\r\n    <div class=\"text-center\">\r\n      <div class=\"input-field\">\r\n      \r\n      <input type=\"text\" id=\"myInput\" onkeyup=\"myFunction()\" placeholder=\"Enter search terms\">\r\n      <i class=\"fa fa-search prefix fontcolors1\"></i>\r\n    </div>\r\n    </div>\r\n</section>\r\n<div class=\"container\" id=\"test-list\">\r\n  <ul id=\"myUL\" class=\"list\">\r\n    <li class=\"name\">\r\n      <a href=\"#\" data-toggle=\"modal\" data-target=\"#myModal\"><i class=\"material-icons\">add</i>Android App development</a>\r\n    </li>\r\n    <li class=\"name\">\r\n      <a href=\"#\" data-toggle=\"modal\" data-target=\"#myModal\"><i class=\"material-icons\">add</i>ADF & Web development</a>\r\n    </li>\r\n    <li class=\"name\">\r\n      <a href=\"#\" data-toggle=\"modal\" data-target=\"#myModal\"><i class=\"material-icons\">add</i>Android Developer</a>\r\n    </li>\r\n    <li class=\"name\">\r\n      <a href=\"#\" data-toggle=\"modal\" data-target=\"#myModal\"><i class=\"material-icons\">add</i>ADF Developer</a>\r\n    </li>\r\n    <li class=\"name\">\r\n      <a href=\"#\" data-toggle=\"modal\" data-target=\"#myModal\"><i class=\"material-icons\">add</i>Blackberry App Development</a>\r\n    </li>\r\n    <li class=\"name\">\r\n      <a href=\"#\" data-toggle=\"modal\" data-target=\"#myModal\"><i class=\"material-icons\">add</i>Codeigniter Development</a>\r\n    </li>\r\n    <li class=\"name\">\r\n      <a href=\"#\" data-toggle=\"modal\" data-target=\"#myModal\"><i class=\"material-icons\">add</i>CakePHP Development</a>\r\n    </li>\r\n    <li class=\"name\">\r\n      <a href=\"#\" data-toggle=\"modal\" data-target=\"#myModal\"><i class=\"material-icons\">add</i>CMS Development</a>\r\n    </li>\r\n    <li class=\"name\">\r\n      <a href=\"#\" data-toggle=\"modal\" data-target=\"#myModal\"><i class=\"material-icons\">add</i>Drupal Development</a>\r\n    </li>\r\n    <li class=\"name\">\r\n      <a href=\"#\" data-toggle=\"modal\" data-target=\"#myModal\"><i class=\"material-icons\">add</i>E-Commerce Development</a>\r\n    </li>\r\n    <li class=\"name\">\r\n      <a href=\"#\" data-toggle=\"modal\" data-target=\"#myModal\"><i class=\"material-icons\">add</i>E-Commerce Developer</a>\r\n    </li>\r\n    <li class=\"name\">\r\n      <a href=\"#\" data-toggle=\"modal\" data-target=\"#myModal\"><i class=\"material-icons\">add</i>Facebook App Development</a>\r\n    </li>\r\n    <li class=\"name\">\r\n      <a href=\"#\" data-toggle=\"modal\" data-target=\"#myModal\"><i class=\"material-icons\">add</i>Iphone App Development</a>\r\n    </li>\r\n    <li class=\"name\">\r\n      <a href=\"#\" data-toggle=\"modal\" data-target=\"#myModal\"><i class=\"material-icons\">add</i>Ipad App Development</a>\r\n    </li>\r\n    <li class=\"name\">\r\n      <a href=\"#\" data-toggle=\"modal\" data-target=\"#myModal\"><i class=\"material-icons\">add</i>Internet Of Things (IOT)</a>\r\n    </li>\r\n    <li class=\"name\">\r\n      <a href=\"#\" data-toggle=\"modal\" data-target=\"#myModal\"><i class=\"material-icons\">add</i>IOS Developer</a>\r\n    </li>\r\n    <li class=\"name\">\r\n      <a href=\"#\" data-toggle=\"modal\" data-target=\"#myModal\"><i class=\"material-icons\">add</i>Joomla Development</a>\r\n    </li>\r\n    <li class=\"name\">\r\n      <a href=\"#\" data-toggle=\"modal\" data-target=\"#myModal\"><i class=\"material-icons\">add</i>Java & J2EE Development</a>\r\n    </li>\r\n    <li class=\"name\">\r\n      <a href=\"#\" data-toggle=\"modal\" data-target=\"#myModal\"><i class=\"material-icons\">add</i>Laravel Development</a>\r\n    </li>\r\n    <li class=\"name\">\r\n      <a href=\"#\" data-toggle=\"modal\" data-target=\"#myModal\"><i class=\"material-icons\">add</i>Magento Development</a>\r\n    </li>\r\n    <li class=\"name\">\r\n      <a href=\"#\" data-toggle=\"modal\" data-target=\"#myModal\"><i class=\"material-icons\">add</i>Magento Developer</a>\r\n    </li>\r\n    <li class=\"name\">\r\n      <a href=\"#\" data-toggle=\"modal\" data-target=\"#myModal\"><i class=\"material-icons\">add</i>PHP Development</a>\r\n    </li>\r\n    <li class=\"name\">\r\n      <a href=\"#\" data-toggle=\"modal\" data-target=\"#myModal\"><i class=\"material-icons\">add</i>Python Development</a>\r\n    </li>\r\n    <li class=\"name\">\r\n      <a href=\"#\" data-toggle=\"modal\" data-target=\"#myModal\"><i class=\"material-icons\">add</i>PHP Developer</a>\r\n    </li>\r\n    <li class=\"name\">\r\n      <a href=\"#\" data-toggle=\"modal\" data-target=\"#myModal\"><i class=\"material-icons\">add</i>Python Developer</a>\r\n    </li>\r\n    <li class=\"name\">\r\n      <a href=\"#\" data-toggle=\"modal\" data-target=\"#myModal\"><i class=\"material-icons\">add</i>PhoneGap Developer</a>\r\n    </li>\r\n    <li class=\"name\">\r\n      <a href=\"#\" data-toggle=\"modal\" data-target=\"#myModal\"><i class=\"material-icons\">add</i>Responsive Web Development</a>\r\n    </li>\r\n    <li class=\"name\">\r\n      <a href=\"#\" data-toggle=\"modal\" data-target=\"#myModal\"><i class=\"material-icons\">add</i>ROR Development</a>\r\n    </li>\r\n    <li class=\"name\">\r\n      <a href=\"#\" data-toggle=\"modal\" data-target=\"#myModal\"><i class=\"material-icons\">add</i>ROR Developer</a>\r\n    </li>\r\n    <li class=\"name\">\r\n      <a href=\"#\" data-toggle=\"modal\" data-target=\"#myModal\"><i class=\"material-icons\">add</i>Windows App Development</a>\r\n    </li>\r\n    <li class=\"name\">\r\n      <a href=\"#\" data-toggle=\"modal\" data-target=\"#myModal\"><i class=\"material-icons\">add</i>Wearable Devices</a>\r\n    </li>\r\n    <li class=\"name\">\r\n      <a href=\"#\" data-toggle=\"modal\" data-target=\"#myModal\"><i class=\"material-icons\">add</i>Wordpress Development</a>\r\n    </li>\r\n    <li class=\"name\">\r\n      <a href=\"#\" data-toggle=\"modal\" data-target=\"#myModal\"><i class=\"material-icons\">add</i>Web App Developer</a>\r\n    </li>\r\n    <li class=\"name\">\r\n      <a href=\"#\" data-toggle=\"modal\" data-target=\"#myModal\"><i class=\"material-icons\">add</i>X-Cart Development</a>\r\n    </li>\r\n\r\n  </ul>\r\n  <ul class=\"pagination right\"></ul>\r\n</div>\r\n\r\n<app-footer2></app-footer2>\r\n\r\n"

/***/ }),

/***/ "./src/app/careers/careers.component.ts":
/*!**********************************************!*\
  !*** ./src/app/careers/careers.component.ts ***!
  \**********************************************/
/*! exports provided: CareersComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CareersComponent", function() { return CareersComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _user_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../user.model */ "./src/app/user.model.ts");
/* harmony import */ var _login_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../login.service */ "./src/app/login.service.ts");
/* harmony import */ var _toaster_service_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./../toaster-service.service */ "./src/app/toaster-service.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var CareersComponent = /** @class */ (function () {
    function CareersComponent(router, _loginService, toasterService) {
        this.router = router;
        this._loginService = _loginService;
        this.toasterService = toasterService;
        this.name = "";
        this.email = "";
        this.subject = "";
        this.message = "";
    }
    CareersComponent.prototype.Success = function () {
        this.toasterService.Success("Your message has been sent successfully.");
    };
    CareersComponent.prototype.Info = function () {
        this.toasterService.Info("Subscribed");
    };
    CareersComponent.prototype.ngOnInit = function () {
    };
    CareersComponent.prototype.login = function () {
        var _this = this;
        var user = new _user_model__WEBPACK_IMPORTED_MODULE_2__["User"]('', '', '', '');
        user.name = this.name;
        user.email = this.email;
        user.subject = this.subject;
        user.message = this.message;
        var data = [
            { 'name': user.name, 'email': user.email, 'subject': user.subject, 'message': user.message }
        ];
        this._loginService.sendLogin({ data: data })
            .subscribe(function (response) { return _this.handleResponse(response); }, function (error) { return _this.handleResponse(error); });
    };
    CareersComponent.prototype.handleResponse = function (response) {
        if (response.success) {
            console.log("success");
        }
        else if (response.error) {
            console.log("errror");
        }
        else {
        }
    };
    CareersComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-careers',
            template: __webpack_require__(/*! ./careers.component.html */ "./src/app/careers/careers.component.html"),
            styles: [__webpack_require__(/*! ./careers.component.css */ "./src/app/careers/careers.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _login_service__WEBPACK_IMPORTED_MODULE_3__["LoginService"], _toaster_service_service__WEBPACK_IMPORTED_MODULE_4__["ToasterService"]])
    ], CareersComponent);
    return CareersComponent;
}());



/***/ }),

/***/ "./src/app/components/how-indexing-and-deep-linking-help/how-indexing-and-deep-linking-help.component.css":
/*!****************************************************************************************************************!*\
  !*** ./src/app/components/how-indexing-and-deep-linking-help/how-indexing-and-deep-linking-help.component.css ***!
  \****************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/how-indexing-and-deep-linking-help/how-indexing-and-deep-linking-help.component.html":
/*!*****************************************************************************************************************!*\
  !*** ./src/app/components/how-indexing-and-deep-linking-help/how-indexing-and-deep-linking-help.component.html ***!
  \*****************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = " <!--page title start-->\r\n <section class=\"page-title ptb-30\">\r\n  <div class=\"container padding-top-40\">\r\n      <div class=\"row\">\r\n          <div class=\"col-md-12\">\r\n              <h6 class=\"pagetitles\">Services</h6>\r\n              <ol class=\"breadcrumb\">\r\n                  <li><a href=\"#\">Home</a></li>\r\n                  <li><a href=\"#\">Services</a></li>\r\n                  <li class=\"active\">How App Indexing and Deep linking help in the development of a successful App?</li>\r\n              </ol>\r\n          </div>\r\n      </div>\r\n  </div>\r\n</section>\r\n<!--page title end-->\r\n\r\n\r\n\r\n    <div class=\"container padding-top-20 bigperlArticle_3a\">\r\n        <div class=\"col-md-12\">\r\n            <h3 class=\"text-bold mb-11\" style=\"text-align: center;\">How App Indexing and Deep linking help in the development of a successful App?</h3>\r\n         \r\n            <p class=\"text-justified padding-top-40\" >\r\n            There are millions of apps present in Google’s Playstore and in Apple’s iTune. All these apps aim for maximum downloads by providing rich content. But the big question is that what efforts should be made to ensure maximum visibility and download of a mobile application? Well, we will answer this question in this article - we will also discuss the strategies for the development of a successful mobile app. But before we do that we want to highlight that now both Google and Apple are making efforts to surface the content of an app in the search results. These efforts are being made because of the shift of consumers from website to mobile apps. People are now not only consuming the content of apps but also buying through it. In fact, many companies are now following the app-only approach. This means that they are selling their products and services only through the app. </p>\r\n\r\n               <p class=\"text-justified padding-top-20\" >\r\n            Anyone can develop an app and make it live on the stores of different platform, but the big question again is that how do they make it outshine? This means that how should they encourage the number of downloads of the app by increasing its visibility? The answer to this question is – through app indexing and deep linking. </p>\r\n\r\n               <p class=\"text-justified padding-top-20\" >\r\n            Let us further understand app indexing and deep linking in detail. </p>\r\n             <h3 class=\"text-bold mb-11 padding-top-20\">App Indexing</h3>\r\n               <img src=\"assets/img/mobiles_bigperl.png\" class=\"img-responsive \" alt=\"Image\">\r\n             <p  class=\"text-bold \"style=\"text-align: center;font-size:8px\"> Pic credits: smallbiztrends.com  </p>\r\n              <p class=\"text-justified padding-top-20\" >\r\n            Let us understand Google’s app indexing by considering an example. Suppose you go to Google’s homepage to search for some content. Now Google will show an app in its search results, which is relevant to the content you’re looking for. When you click on the app’s link it will redirect you to the playstore and urge you to install it. When a user searches for keywords Google presents to him a list of apps, which he hasn’t installed on his device. This encourages the users to install the apps hence increasing the number of app downloads. However, if the user has already installed the app it will show results from inside the app. The result includes the icon of the app and other information from the app. Please note that the above discussed points are valid only in the case of an Android device. For iOS devices you have a slight variation. For iOS devices the app shows up in the search result of only those users who have already installed the app on their devices. </p>\r\n            <p class=\"text-justified padding-top-20\" >\r\n            In Apple devices the main aim of app indexing is to provide a better experience to the users rather than promoting the apps. When it comes to app indexing, Apple is resorting to different means. For instance, it introduced Spotlight search in iOS 9. With its help the users can search for web and app content, as the apps are integrated into the Spotlight search. </p>\r\n                <p class=\"text-justified padding-top-20\" >\r\n           Google’s app indexing also plays a major role in the ranking of your app in the search results, whether you have installed the app or not.</p>\r\n\r\n\r\n        </div>\r\n\r\n        <!-- <div class=\"col-md-5\">\r\n            <img src=\"assets/img/ipad-2.png\" class=\"img-responsive \" alt=\"Image\">\r\n        </div> -->\r\n    </div>\r\n     <div class=\"container padding-top-40 bigperlArticle_3a\">\r\n        <div class=\"col-md-12\">\r\n            <h3 class=\"text-bold mb-11\">Deep linking</h3>\r\n            <img src=\"assets/img/mobiles2_bigperl.png\" class=\"img-responsive \" alt=\"Image\">\r\n             <p  class=\"text-bold \"style=\"text-align: center;font-size:8px\"> Pic credits: blogs.position2.com  </p>\r\n            <p class=\"text-justified padding-top-20\">Deep linking can be defined as the process of linking a specific page or function inside the app with the search engine results. This means that clicking on the link of the app in the search results will take you to the relevant content inside the app. For instance, suppose you have installed the LinkedIn app. Now suppose you go to Google and search for “Mobile application development methodologies”. In the search results you’ll see a link to the LinkedIn app that will redirect you to an article on the topic you’ve been searching for. In other words, instead of being redirected to LinkedIn’s mobile website, you’ll be redirected to the app – if you’ve installed it. </p>\r\n\r\n            <p class=\"text-justified padding-top-20\">The entire process of app indexing and deep linking provide an enhanced experience to the users. It redirects the user to an environment, which they are familiar with. It bridges the gap between web and mobile app and provides a seamless experience to the user, by redirecting him from the website to the native app. </p>\r\n\r\n            <p class=\"text-justified padding-top-20\">Businesses are always looking for a way to increase the visibility of their app. Other than this, they are also looking for methods to encourage app downloads. App indexing and deep linking is the answer to both these concerns. Businesses should consider this solution all the more because it is backed by giant tech companies like Google and Apple. This power packed solution engages your existing users and at the same time encourages more downloads. </p>\r\n\r\n           \r\n        </div>\r\n\r\n        <!-- <div class=\"col-md-5\">\r\n            <img src=\"assets/img/ipad-2.png\" class=\"img-responsive \" alt=\"Image\">\r\n        </div> -->\r\n    </div>\r\n\r\n    <div class=\"container padding-top-40 bigperlArticle_3b\">\r\n        <div class=\"col-md-12\">\r\n            <h3 class=\"text-bold mb-11\">Conclusion</h3>\r\n            <img src=\"assets/img/mobile_bigperl.png\" class=\"img-responsive \" alt=\"Image\">\r\n             <p  class=\"text-bold \"style=\"text-align: center;font-size:8px\"> Pic credits: searchengineland.com</p>\r\n            <h3 class=\"text-bold mb-11 padding-top-20\">Efficient</h3>\r\n            <p class=\"text-justified padding-top-20\">The above discussion testifies that app indexing and deep linking is instrumental to the development of a successful app. The power packed solution of deep linking and app indexing is a must if you want your current users to stay engaged and new users to find you. Since people are shifting to apps from websites, both app indexing and deep linking and bound to innovate and grow exponentially in the near future. Therefore, you need to make sure that your app is properly linked for the successful growth of your business.</p>\r\n            \r\n            <p class=\"text-justified padding-top-20\">At <a href=\"https://bigperl.com/\">BigPerl</a> we take care of your complete app development strategy, which includes the points discussed above.  <a href=\"https://bigperl.com/contact-us\">contact</a> us with your requirements today! </p>\r\n            \r\n            \r\n        </div>\r\n\r\n        \r\n    </div>\r\n\r\n    <app-footer5></app-footer5>\r\n           \r\n    <app-footer2></app-footer2>\r\n\r\n"

/***/ }),

/***/ "./src/app/components/how-indexing-and-deep-linking-help/how-indexing-and-deep-linking-help.component.ts":
/*!***************************************************************************************************************!*\
  !*** ./src/app/components/how-indexing-and-deep-linking-help/how-indexing-and-deep-linking-help.component.ts ***!
  \***************************************************************************************************************/
/*! exports provided: HowIndexingAndDeepLinkingHelpComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HowIndexingAndDeepLinkingHelpComponent", function() { return HowIndexingAndDeepLinkingHelpComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var HowIndexingAndDeepLinkingHelpComponent = /** @class */ (function () {
    function HowIndexingAndDeepLinkingHelpComponent() {
    }
    HowIndexingAndDeepLinkingHelpComponent.prototype.ngOnInit = function () {
    };
    HowIndexingAndDeepLinkingHelpComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-how-indexing-and-deep-linking-help',
            template: __webpack_require__(/*! ./how-indexing-and-deep-linking-help.component.html */ "./src/app/components/how-indexing-and-deep-linking-help/how-indexing-and-deep-linking-help.component.html"),
            styles: [__webpack_require__(/*! ./how-indexing-and-deep-linking-help.component.css */ "./src/app/components/how-indexing-and-deep-linking-help/how-indexing-and-deep-linking-help.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], HowIndexingAndDeepLinkingHelpComponent);
    return HowIndexingAndDeepLinkingHelpComponent;
}());



/***/ }),

/***/ "./src/app/components/location-based-technology-formobile-app/location-based-technology-formobile-app.component.css":
/*!**************************************************************************************************************************!*\
  !*** ./src/app/components/location-based-technology-formobile-app/location-based-technology-formobile-app.component.css ***!
  \**************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/location-based-technology-formobile-app/location-based-technology-formobile-app.component.html":
/*!***************************************************************************************************************************!*\
  !*** ./src/app/components/location-based-technology-formobile-app/location-based-technology-formobile-app.component.html ***!
  \***************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--page title start-->\r\n<section class=\"page-title ptb-30\">\r\n  <div class=\"container padding-top-40\">\r\n      <div class=\"row\">\r\n          <div class=\"col-md-12\">\r\n              <h6 class=\"pagetitles\">Services</h6>\r\n              <ol class=\"breadcrumb\">\r\n                  <li><a href=\"#\">Home</a></li>\r\n                  <li><a href=\"#\">Services</a></li>\r\n                  <li class=\"active\">Location-based technologies for mobile apps</li>\r\n              </ol>\r\n          </div>\r\n      </div>\r\n  </div>\r\n</section>\r\n<!--page title end-->\r\n\r\n\r\n\r\n    <div class=\"container padding-top-20 location-based-technology1\">\r\n        <div class=\"col-md-12\">\r\n            <h3 class=\"text-bold mb-11\" style=\"text-align: center;\">Location-based technologies for mobile apps</h3>\r\n         \r\n            \r\n            \r\n               <img src=\"assets/img/location-based-tech_bigperl.png\" class=\"img-responsive \" alt=\"Image\">\r\n             <p  class=\"text-bold \"style=\"text-align: center;font-size:8px\"> Pic credits: mindinventory.com  </p>\r\n              <p class=\"text-justified padding-top-20\" >\r\n           Mobile apps have revolutionized the way businesses function. On the consumer level, they have allowed us to break down the barriers of boundaries and stay connected from anywhere in the world. Now you might have noticed that many apps ask you for your location. This is because having access to the location allows the app to provide you with a personalized experience. The personalized experience makes the user experience more relevant.  </p>\r\n            <p class=\"text-justified padding-top-20\" >\r\n            As many things are happening in the field of IoT, location is becoming even more important. Location is a very important aspect of the concept of IoT, it encompasses the ability of things to sense and communicate their geographic position. Therefore, location acts as the organizing principle for anything that is connected to the internet. </p>\r\n                <p class=\"text-justified padding-top-20\" >\r\n           Businesses are also now realizing the importance of mobile apps with location-based features. In fact, most of the popular apps these days are built on the location-based technology. There are many options available to integrate location based technology for your mobile app. The most important options include the following – BLE/Beacons, Wi-Fi, and GPS. Let us discuss these technologies in detail, one-by-one.</p>\r\n\r\n\r\n        </div>\r\n\r\n        <!-- <div class=\"col-md-5\">\r\n            <img src=\"assets/img/ipad-2.png\" class=\"img-responsive \" alt=\"Image\">\r\n        </div> -->\r\n    </div>\r\n     <div class=\"container padding-top-40 location-based-technology2\">\r\n        <div class=\"col-md-12\">\r\n            <h3 class=\"text-bold mb-11\">BLE/Beacons</h3>\r\n            <img src=\"assets/img/bluetooth_bigperl.png\" class=\"img-responsive \" alt=\"Image\">\r\n             <p  class=\"text-bold \"style=\"text-align: center;font-size:8px\"> Pic credits: rfidjournal.com </p>\r\n            <p class=\"text-justified padding-top-20\">BLE or Bluetooth Low Energy is the power and application friendly version of Bluetooth, which allows connectivity between devices and beacons. BLE is already in wide use. It is the basis for Google’s Eddystone and Apple’s iBeacon. Beacons work in tandem with mobile apps and trigger particular messages or actions based on rules. For instance, they trigger a push notification when a user is at a certain distance from a beacon.</p>\r\n\r\n            <h3 class=\"text-bold mb-11 padding-top-20\">Advantages of Bluetooth Low Energy</h3>\r\n            <ul class=\"dashed\">\r\n               <li>It has the ability to work across various platforms and devices</li>\r\n              <li>It has a well-documented and standardized app development architecture</li>\r\n              <li>It uses very less power and preserves the battery life of the device</li>\r\n              <li>It offers high accuracy via proximity detection </li>\r\n              <li>It is the best available option for context-aware messaging</li>\r\n              <li>It has a widely accessible technology</li>\r\n               <li>Opt-in (Requires user’s consent)</li>\r\n            </ul>\r\n\r\n            <h3 class=\"text-bold mb-11 padding-top-20\">Disadvantages of Bluetooth Low Energy</h3>\r\n            <ul class=\"dashed\">\r\n               <li>It has a lower range than GPS</li>\r\n              <li>It is not ideal for outdoor environments</li>\r\n              <li>Requires an app</li>\r\n            </ul>\r\n\r\n            <p class=\"text-justified padding-top-20\">BLE and Beacons can be used for indoor environments, micro-location activities and for tracking specific customer data. </p>\r\n             </div>\r\n           </div>\r\n\r\n    <div class=\"container padding-top-20 location-based-technology\">\r\n\r\n             <div class=\"col-md-12\">\r\n            <h3 class=\"text-bold mb-11  padding-top-20\">Wi-Fi</h3>\r\n            <img src=\"assets/img/wifi_bigperl.png\" class=\"img-responsive \" alt=\"Image\">\r\n             <p  class=\"text-bold \"style=\"text-align: center;font-size:8px\"> Pic credits: walkbase.com  </p>\r\n            <p class=\"text-justified padding-top-20\">Wi-Fi leverages the existing Wi-Fi infrastructure in order to detect devices that have the Wi-Fi turned on. This method can be used for sending push notifications and other relevant information to the customers, based on their location. This technology has wide applications in all industries ranging from retail to hospitality. </p>\r\n\r\n            <h3 class=\"text-bold mb-11 padding-top-20\">Advantages of Wi-Fi</h3>\r\n            <ul class=\"dashed\">\r\n               <li>It is widely accessible as all devices are able to connect to networks</li>\r\n              <li>It can be used without an app</li>\r\n             \r\n            </ul>\r\n\r\n            <h3 class=\"text-bold mb-11 padding-top-20\">Disadvantages of Wi-Fi</h3>\r\n            <ul class=\"dashed\">\r\n               <li>It poses concerns with both privacy and relevancy as consumers are not explicitly asked permission (no opt-in)</li>\r\n              <li>Not a good option for context-aware messaging</li>\r\n              \r\n            </ul>\r\n\r\n            <p class=\"text-justified padding-top-20\">It can be used in indoor environments for delivering general mobile content.</p>\r\n             </div>\r\n\r\n             <div class=\"col-md-12\">\r\n            <h3 class=\"text-bold mb-11  padding-top-20\">GPS (Geofencing)</h3>\r\n            <img src=\"assets/img/gps_bigperl.png\" class=\"img-responsive \" alt=\"Image\">\r\n             <p  class=\"text-bold \"style=\"text-align: center;font-size:8px\"> Pic credits: clearbridgemobile.com </p>\r\n            <p class=\"text-justified padding-top-20\">Geofencing makes use of GPS technology to pinpoint a user’s location in proximity to a given area. It makes use of the co-ordinates from a user’s device to push messages and actions to his device, when he enters a pre-determined area. </p>\r\n\r\n            <h3 class=\"text-bold mb-11 padding-top-20\">Advantages of GPS</h3>\r\n            <ul class=\"dashed\">\r\n               <li> It is a widespread technology, which means that it is highly accessible</li>\r\n              <li>Its range is un-limited</li>\r\n              <li>Opt-in (Requires user’s consent)</li>\r\n             \r\n            </ul>\r\n\r\n            <h3 class=\"text-bold mb-11 padding-top-20\">Disadvantages of GPS</h3>\r\n            <ul class=\"dashed\">\r\n               <li> It requires a lot of power. Therefore, it drains the mobile device batteries quickly.</li>\r\n              <li>It is less accurate for micro-location proximity based activities</li>\r\n\r\n              \r\n            </ul>\r\n\r\n            <p class=\"text-justified padding-top-20\">It can be used in the outdoor environment for macro-location activities.</p>\r\n             </div>\r\n\r\n     \r\n    </div>\r\n\r\n    <div class=\"container padding-top-40 bigperlArticle_3b\">\r\n        <div class=\"col-md-12\">\r\n            <h3 class=\"text-bold mb-11\">Conclusion</h3>\r\n            \r\n           \r\n            <p class=\"text-justified padding-top-20\">Now the big question is that what should one choose between beacon, Wi-Fi and GPS? To answer this question, let us understand the concept of IoT. For IoT to function efficiently, a device needs to know what exactly it is communicating with. This means that there needs to be a way to differentiate between things within the network. It has been found that beacons by far are the most effective means to accomplish this as compared to Wi-Fi or GPS. Besides this, beacons are also the best available technology to facilitate indoor location positioning on a more granular level. It also provides hyper-relevant personalized messaging to the users and gathers more detailed user intelligence.</p>\r\n             <p class=\"text-justified padding-top-20\">Though Beacon stands apart from the rest, choosing the right location-based technology for the mobile app of your company depends on what you are trying to accomplish. Many cases also see a combination of these technologies. </p>\r\n            <p class=\"text-justified padding-top-20\">If you also want your mobile app to possess the capabilities of hyper-localization, context-awareness, and segmentation based on user behaviour, <a href=\"https://bigperl.com/contact-us\">contact</a> <a href=\"https://bigperl.com/\">BigPerl</a> with your project requirements. </p>\r\n            \r\n            \r\n            \r\n        </div>\r\n\r\n        \r\n    </div>\r\n\r\n    <app-footer5></app-footer5>\r\n           \r\n    <app-footer2></app-footer2>\r\n        \r\n\r\n"

/***/ }),

/***/ "./src/app/components/location-based-technology-formobile-app/location-based-technology-formobile-app.component.ts":
/*!*************************************************************************************************************************!*\
  !*** ./src/app/components/location-based-technology-formobile-app/location-based-technology-formobile-app.component.ts ***!
  \*************************************************************************************************************************/
/*! exports provided: LocationBasedTechnologyFormobileAppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LocationBasedTechnologyFormobileAppComponent", function() { return LocationBasedTechnologyFormobileAppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var LocationBasedTechnologyFormobileAppComponent = /** @class */ (function () {
    function LocationBasedTechnologyFormobileAppComponent() {
    }
    LocationBasedTechnologyFormobileAppComponent.prototype.ngOnInit = function () {
    };
    LocationBasedTechnologyFormobileAppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-location-based-technology-formobile-app',
            template: __webpack_require__(/*! ./location-based-technology-formobile-app.component.html */ "./src/app/components/location-based-technology-formobile-app/location-based-technology-formobile-app.component.html"),
            styles: [__webpack_require__(/*! ./location-based-technology-formobile-app.component.css */ "./src/app/components/location-based-technology-formobile-app/location-based-technology-formobile-app.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], LocationBasedTechnologyFormobileAppComponent);
    return LocationBasedTechnologyFormobileAppComponent;
}());



/***/ }),

/***/ "./src/app/components/xamarin-development/xamarin-development.component.css":
/*!**********************************************************************************!*\
  !*** ./src/app/components/xamarin-development/xamarin-development.component.css ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/xamarin-development/xamarin-development.component.html":
/*!***********************************************************************************!*\
  !*** ./src/app/components/xamarin-development/xamarin-development.component.html ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n        <!--page title start-->\r\n        <section class=\"page-title ptb-30\">\r\n          <div class=\"container padding-top-40\">\r\n              <div class=\"row\">\r\n                  <div class=\"col-md-12\">\r\n                      <h6 class=\"pagetitles\">Services</h6>\r\n                      <ol class=\"breadcrumb\">\r\n                          <li><a href=\"#\">Home</a></li>\r\n                          <li><a href=\"#\">Services</a></li>\r\n                          <li class=\"active\">Cross Platform Mobile Application Development with Xamarin</li>\r\n                      </ol>\r\n                  </div>\r\n              </div>\r\n          </div>\r\n      </section>\r\n      <!--page title end-->\r\n\r\n\r\n            <div class=\"container padding-top-20 xamarianSec1\">\r\n                <div class=\"col-md-12\">\r\n                    <h2 class=\"text-bold mb-11\">What is Xamarin?</h2>\r\n                    <img src=\"assets/img/services/abc_bigperl.png\" class=\"img-responsive  \" alt=\"Image\" >\r\n                    \r\n                     <p  class=\"text-bold padding-top-40\"style=\"text-align: center;font-size:8px\"> Pic credits: Xamarin.com </p>\r\n                    <p class=\"text-justified padding-top-40\" >Xamarin is a cross-platform tool used for the purpose of mobile application development. This tool has been developed by the company Xamarin. Xamarin was acquired by Microsoft Technologies last year. Since the company has been acquired by Microsoft, it is investing time and money in the development of this cross platform tool. The joining of Xamarin and Microsoft has led to fresh hopes for the latter in the field of mobile applications. Xamarin is a fresh hope to develop native performing apps on Microsoft technologies. Besides this, the acquisition of Xamarin by Microsoft may also lead to a future where you have Microsoft services on each and every platform. For instance, this can lead to a scenario where you can have Windows App on Android and iOS phones. </p>\r\n                </div>\r\n\r\n                <!-- <div class=\"col-md-5\">\r\n                    <img src=\"assets/img/ipad-2.png\" class=\"img-responsive \" alt=\"Image\">\r\n                </div> -->\r\n            </div>\r\n             <div class=\"container padding-top-40 xamarianSec\">\r\n                <div class=\"col-md-12\">\r\n                    <h2 class=\"text-bold mb-11\">Why you need Xamarin?</h2>\r\n                    <img src=\"assets/img/services/app_logic_bigperl.png\" class=\"img-responsive \" alt=\"Image\">\r\n                     <p  class=\"text-bold \"style=\"text-align: center;font-size:8px\"> Pic credits: hanselman.com </p>\r\n                    <p class=\"text-justified padding-top-20\">The world is shifting from websites to mobile apps. This fact is supported by the data that the mobile app download numbers were around 65 million in the year 2012, but have crossed 225 million in the year 2016. Mobile apps have become substantial for the survival of any business. In fact many e-commerce businesses are adopting an app-only model. This means that they are selling their products or services only through the mobile app. Consumers also prefer to use a mobile app rather than websites. This major shift of the consumers towards mobile apps has led the mobile application development industry to deliver apps, which offer a seamless experience to the users. </p>\r\n\r\n                    <p class=\"text-justified padding-top-20\">The urgency to deliver efficient mobile applications has led to the birth of cross-platform app development frameworks. These frameworks make use of technology to produce a single app for multiple platforms. With the help of cross-platform frameworks like Xamarin you write minimal source-code to produce a universal app, which works perfectly on all platforms. The process of cross-platform app development can save your organization a lot of time and money. So if you want to get your app out in the market as quickly as possible, and cater the needs of consumers using either of the dominant mobile platforms – Android or iOS, you need Xamarin!</p>\r\n\r\n                    <p class=\"text-justified padding-top-20\">The journey of cross-platform app development is not a sudden one, it went through many phases. In the past you had frameworks such as Titanium, after which many web technology based frameworks such as PhoneGap came into limelight. However, these web technology based frameworks had quite a few limitations against Objective C. Objective C is the primary language that is used to write software codes for iOS. Besides this the web technology based frameworks also had a few limitations against Java. Java is the primary language that is used for Android app development. To overcome these limitations the Xamarin cross-platform framework was developed. Xamarin uses C# to develop apps that run perfectly on android, iOS, Mac, Windows 8 and Windows 8 phone. </p>\r\n\r\n                    <p class=\"text-justified padding-top-20\">If you compare Xamarin with other cross-platform app development frameworks, you will witness that it provides the users with a far better user experience. Other than this, it also provides more security and better performance. </p>\r\n\r\n                    <p class=\"text-justified padding-top-20\">To develop an app for iOS devices Objective C is used, whereas Java and .NET is used for Android and Windows based devices. But with the help of Xamarin platform one language, which is C# can be used to produce a universal app that runs on android, iOS and Windows.</p>\r\n                </div>\r\n\r\n                <!-- <div class=\"col-md-5\">\r\n                    <img src=\"assets/img/ipad-2.png\" class=\"img-responsive \" alt=\"Image\">\r\n                </div> -->\r\n            </div>\r\n\r\n            <div class=\"container padding-top-40 xamarianSec\">\r\n                <div class=\"col-md-12\">\r\n                    <h2 class=\"text-bold mb-11\">Advantages of using Xamarin platform</h2>\r\n                    <img src=\"assets/img/services/xamarinforms_bigperl.png\" class=\"img-responsive \" alt=\"Image\">\r\n                     <p  class=\"text-bold \"style=\"text-align: center;font-size:8px\"> Pic credits: alsocreative.com</p>\r\n                    <h3 class=\"text-bold mb-11 padding-top-20\">Efficient</h3>\r\n                    <p class=\"text-justified padding-top-20\">To develop an app on android platform one needs to have excellent knowledge of Java. Similarly, to develop an app for iOS platform the developer needs to know Objective C. This means that if you’re not using cross-platform app development frameworks, you’ll have to learn both of these languages. The process of learning Java and Objective C becomes more complex as there is an issue of underlying core classes such as collections. This requires you to understand two different ways of doing the same thing. This somehow decreases the efficiency. However, with Xamarin you are required to learn just one programming language that is C#, and also one core set of classes that are effective on all the platforms. If you already have knowledge of .NET/C#, it will be very easy for you to work with Xamarin. If you’re not proficient in .NET/C#, the time that you’ll spend in learning Xamarin shall be considerably reduced. This will increase the overall efficiency of the process.  </p>\r\n                    <h3 class=\"text-bold mb-11 padding-top-20\">Captures the uniqueness of each platform</h3>\r\n                    <p class=\"text-justified padding-top-20\">Both Android and iOS have features that are unique to each platform. Both the platforms have UI and SDK features, which are unique to each of the platform’s behaviour and appearance. Most of the cross-platform app development tools hide this uniqueness. This makes the apps feel foreign to the platforms they run on. Xamarin on the other hand does not hide the unique features associated with each of the platform. In addition to .NET classes it has iOS specific .NET classes and also Android specific .NET classes. This means that the unique features of each of the platform in highlighted with the help of Xamarin</p>\r\n                    <h3 class=\"text-bold mb-11 padding-top-20\">Saves time</h3>\r\n                    <p class=\"text-justified padding-top-20\">Since with Xamarin you need to write one source code and share it across Android and iOS, it saves a lot of time. Consider a case where you do not use cross-platform tools to develop the app. This means that you’ll have to write separate codes for android and iOS. In other words, Xamarin gives us more time to focus on app features by preventing us from writing more code.</p>\r\n                    <h3 class=\"text-bold mb-11 padding-top-20\">Fewer bugs</h3>\r\n                    <p class=\"text-justified\">Xamarin requires writing less codes, this means that one will make few errors. This naturally results in fewer bugs. Besides this, due to the efficiency of the Xamarin platform one gets more time for testing the app and eliminating the bugs. </p>\r\n                    <br>\r\n                    <h3 class=\"text-bold mb-11 padding-top-20\">Conclusion</h3>\r\n                    <img src=\"assets/img/services/xamarin-joins-microsoft-e1462214082451_bigperl.png\" class=\"img-responsive \" alt=\"Image\">\r\n                     <p  class=\"text-bold \"style=\"text-align: center;font-size:8px\"> Pic credits: Xamarin.com </p>\r\n                    <p class=\"text-justified padding-top-20\">With each passing day, Xamarin is becoming a must-have tool for mobile application development. It supports multiple platforms and requires minimal amount of duplication work. Besides this, the future prospects of the Xamarin platform seem to be very bright. As already stated, in the future we can have Windows app on Android and iOS platforms. </p>\r\n                     <p class=\"text-justified padding-top-20\">If you also want to save both time and money by getting your app developed with the help of Xamarin platform, then <a href=\"https://bigperl.com/contact-us\">contact</a> <a href=\"https://bigperl.com/\">BigPerl</a> with your requirements. We will make sure that we deliver you a universal app that runs effectively on all platforms. </p>\r\n                </div>\r\n\r\n                \r\n            </div>\r\n\r\n            <app-footer5></app-footer5>\r\n           \r\n            <app-footer2></app-footer2>\r\n                "

/***/ }),

/***/ "./src/app/components/xamarin-development/xamarin-development.component.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/components/xamarin-development/xamarin-development.component.ts ***!
  \*********************************************************************************/
/*! exports provided: XamarinDevelopmentComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "XamarinDevelopmentComponent", function() { return XamarinDevelopmentComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var XamarinDevelopmentComponent = /** @class */ (function () {
    function XamarinDevelopmentComponent() {
    }
    XamarinDevelopmentComponent.prototype.ngOnInit = function () {
    };
    XamarinDevelopmentComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-xamarin-development',
            template: __webpack_require__(/*! ./xamarin-development.component.html */ "./src/app/components/xamarin-development/xamarin-development.component.html"),
            styles: [__webpack_require__(/*! ./xamarin-development.component.css */ "./src/app/components/xamarin-development/xamarin-development.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], XamarinDevelopmentComponent);
    return XamarinDevelopmentComponent;
}());



/***/ }),

/***/ "./src/app/contact-us/contact-us.component.css":
/*!*****************************************************!*\
  !*** ./src/app/contact-us/contact-us.component.css ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/contact-us/contact-us.component.html":
/*!******************************************************!*\
  !*** ./src/app/contact-us/contact-us.component.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--page title start-->\r\n<section class=\"page-title ptb-30\">\r\n  <div class=\"container padding-top-40\">\r\n      <div class=\"row\">\r\n          <div class=\"col-md-12\">\r\n              <h6 class=\"pagetitles\">Contact us</h6>\r\n              <ol class=\"breadcrumb\">\r\n                  <li><a href=\"#\">Home</a></li>\r\n                  <li class=\"active\">contact us</li>\r\n              </ol>\r\n          </div>\r\n      </div>\r\n  </div>\r\n</section>\r\n<!--page title end-->\r\n\r\n\r\n<!-- contact-form-section -->\r\n<section class=\"section-padding\">\r\n\r\n<div class=\"container\">\r\n\r\n  <div class=\"text-center mb-15\">\r\n      <h2 class=\"section-title text-uppercase\">Get in Touch</h2>\r\n      <p class=\"section-sub\">BigPerl Solutions is one of the fastest growing Mobile Application Development company. we are specialised in providing best in class technology and support solution to cater growing business needs.</p>\r\n  </div>\r\n\r\n  <div class=\"row\">\r\n      <div class=\"col-md-6 padding-right-0\">\r\n        <div class=\"panel panel-success\">\r\n          <div class=\"panel-body\">\r\n          <h3><b>Contact Us</b></h3>\r\n              <div class=\"row\">\r\n                <div class=\"col-md-12\">\r\n                  <form method=\"post\" id=\"contactForm\" (ngSubmit)=\"login();contactform.reset(); Success()\" #contactform=\"ngForm\" >\r\n                    <div class=\"row\">\r\n                      <div class=\"col-md-6\">\r\n                        <div class=\"input-field\">\r\n                          <i class=\"fa fa-user prefix fontcolors\"></i>\r\n                          <input type=\"text\" name=\"name\" class=\"validate\" id=\"name\" style=\"height:20px\" [(ngModel)]='name' >\r\n                          <label for=\"name\">Name</label>\r\n                        </div>\r\n                      </div><!-- /.col-md-6 -->\r\n                      <div class=\"col-md-6\">\r\n                        <div class=\"input-field\">\r\n                          <i class=\"fa fa-envelope prefix fontcolors\"></i>\r\n                          <input id=\"email\" type=\"email\" name=\"email\" class=\"validate\" style=\"height:20px\"  [(ngModel)]='email' >\r\n                          <label for=\"email\" data-error=\"wrong\" data-success=\"right\">Email</label>\r\n                        </div>\r\n                      </div><!-- /.col-md-6 -->\r\n                    </div><!-- /.row -->\r\n\r\n                    <div class=\"row\">\r\n                      <div class=\"col-md-12\">\r\n                        <div class=\"input-field\">\r\n                          <i class=\"fa fa-book prefix fontcolors\"></i>\r\n                          <input id=\"subject\" type=\"tel\" name=\"subject\" class=\"validate\" style=\"height:20px\" [(ngModel)]='subject' >\r\n                          <label for=\"subject\">Subject</label>\r\n                        </div>\r\n                      </div><!-- /.col-md-6 -->\r\n                      </div>\r\n                    <div class=\"input-field\">\r\n                      <i class=\"fa fa-commenting-o prefix fontcolors\"></i>\r\n                      <textarea name=\"message\" id=\"message\" class=\"form-control\"  [(ngModel)]='message' style=\"border:none\" ></textarea>\r\n                      <label for=\"message\">Message</label>\r\n                    </div>\r\n                    <button type=\"submit\" name=\"submit\" class=\"btn-sm btnclass\"  [disabled]=\"!contactform.form.valid\" >Send Message</button>\r\n                    </form>\r\n                </div>\r\n              </div>\r\n          </div>\r\n        </div>\r\n      </div><!-- /.col-md-8 -->\r\n\r\n      <div class=\"col-md-6 contact-info padding-right-0\">\r\n          <iframe src=\"https://www.google.com/maps/d/embed?mid=1zaoCDS4JispLhVabcJnbm9A3Q_4\" width=\"554\" height=\"330\"  frameborder=\"0\" style=\"border:1px solid #D4D4D4;border-radius: 5px;\" allowfullscreen></iframe>\r\n      </div><!-- /.col-md-4 -->\r\n  </div><!-- /.row --><br>\r\n    <div class=\"panel panel-success col-md-12 mb-40\">\r\n      <div class=\"panel-body\">\r\n        <div class=\"row\">\r\n          <div class=\"col-md-3 borderright\">\r\n            <h3><b><a>*</a>India :</b></h3>\r\n            <p><b>#1444,Kengeri Upanagara <br>Old Outer Ring Rd, Kengeri Satellite, <br>Town Karnataka 560060.</b></p>\r\n          </div>\r\n          <div class=\"col-md-3 borderright\">\r\n              <h3><b><a>*</a>Jaipur :</b></h3>\r\n              <p><b>4th Floor , P.No 5 Niram Nagar AB, Ajmer Road , Jaipur ,<br> Rajasthan , Pin : 302019</b><br></p>\r\n          </div>\r\n          <div class=\"col-md-3 borderright\">\r\n              <h3><b><a>*</a>Canada</b></h3>\r\n              <p><b>25th bay street toronto , <br> Ontario , canada M5J 2N8</b></p><br>\r\n          </div>\r\n          <div class=\"col-md-3\">\r\n              <h3><b><a>*</a>United States :</b></h3>\r\n            <p><b>9350 Wilshire Blvd, Suite 203, <br>Beverly Hills, CA 90212</b></p>\r\n          </div>\r\n        </div>\r\n    </div>\r\n    </div>\r\n</div>\r\n</section>\r\n<!-- contact-form-section End -->"

/***/ }),

/***/ "./src/app/contact-us/contact-us.component.ts":
/*!****************************************************!*\
  !*** ./src/app/contact-us/contact-us.component.ts ***!
  \****************************************************/
/*! exports provided: ContactUsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactUsComponent", function() { return ContactUsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _user_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../user.model */ "./src/app/user.model.ts");
/* harmony import */ var _login_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../login.service */ "./src/app/login.service.ts");
/* harmony import */ var _toaster_service_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./../toaster-service.service */ "./src/app/toaster-service.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ContactUsComponent = /** @class */ (function () {
    function ContactUsComponent(router, _loginService, toasterService) {
        this.router = router;
        this._loginService = _loginService;
        this.toasterService = toasterService;
        this.name = "";
        this.email = "";
        this.subject = "";
        this.message = "";
    }
    ContactUsComponent.prototype.Success = function () {
        this.toasterService.Success("Your message has been sent successfully.");
    };
    ContactUsComponent.prototype.Info = function () {
        this.toasterService.Info("Subscribed");
    };
    ContactUsComponent.prototype.ngOnInit = function () {
    };
    ContactUsComponent.prototype.login = function () {
        var _this = this;
        var user = new _user_model__WEBPACK_IMPORTED_MODULE_2__["User"]('', '', '', '');
        user.name = this.name;
        user.email = this.email;
        user.subject = this.subject;
        user.message = this.message;
        var data = [
            { 'name': user.name, 'email': user.email, 'subject': user.subject, 'message': user.message }
        ];
        this._loginService.sendLogin({ data: data })
            .subscribe(function (response) { return _this.handleResponse(response); }, function (error) { return _this.handleResponse(error); });
    };
    ContactUsComponent.prototype.handleResponse = function (response) {
        if (response.success) {
            console.log("success");
        }
        else if (response.error) {
            console.log("errror");
        }
        else {
        }
    };
    ContactUsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-contact-us',
            template: __webpack_require__(/*! ./contact-us.component.html */ "./src/app/contact-us/contact-us.component.html"),
            styles: [__webpack_require__(/*! ./contact-us.component.css */ "./src/app/contact-us/contact-us.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _login_service__WEBPACK_IMPORTED_MODULE_3__["LoginService"], _toaster_service_service__WEBPACK_IMPORTED_MODULE_4__["ToasterService"]])
    ], ContactUsComponent);
    return ContactUsComponent;
}());



/***/ }),

/***/ "./src/app/footer/footer.component.css":
/*!*********************************************!*\
  !*** ./src/app/footer/footer.component.css ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "textarea{\r\n    height: 50px;\r\n}\r\n\r\n.row{\r\n    margin-bottom: 6px;\r\n}\r\n\r\n.whatsappme__button1 {\r\n   \r\n    position: fixed;\r\n      z-index: 1004;\r\n     bottom: 28px;\r\n     right:30px;\r\n      height: 63px;\r\n      min-width: 63px;\r\n      max-width: 95vw;\r\n      background-color:#c58652;\r\n      color: #fff;\r\n      border-radius: 30px;\r\n      box-shadow: 1px 6px 24px 0 rgba(7, 94, 84, .24);\r\n      cursor: pointer;\r\n      transition: background-color 500ms ease-in-out;\r\n  }\r\n\r\n.pull-right img{\r\n      padding-left: 5px;\r\n  }\r\n\r\n.whatsappme__box1 {\r\n       display: none;\r\n    position: fixed;\r\n           bottom: 107px;\r\n      right:10px;\r\n      \r\n   z-index: 1000;\r\n      \r\n      \r\n     \r\n  /*\r\n   \r\n     \r\n      z-index: 1;\r\n  */\r\n      width: calc(100vw - 40px);\r\n      max-width: 400px;\r\n      min-height: 280px;\r\n      padding-bottom: 10px;\r\n      border-radius: 5px;\r\n      \r\n       background: #fafafa ;\r\n  /*\r\n      background: #ede4dd url(whatsapp_background.png) center repeat-y;\r\n      background-size: 100% auto;\r\n  */\r\n      box-shadow: 0 2px 6px 0 rgba(0, 0, 0, .5);\r\n  /*\r\n      overflow: hidden;\r\n      transform: scale3d(0, 0, 0);\r\n      opacity: 0;\r\n      transition: opacity 300ms ease-out, transform 0ms linear 300ms;\r\n  */\r\n  }\r\n\r\n.whatsappme__box1 img{\r\n     height: 40px;\r\n        margin: 10px;\r\n  }\r\n\r\n.whatsapp-text1 {\r\n          \r\n      \r\n      position: absolute;\r\n      top: 18px;\r\n      right: 284px;\r\n      width: 34px;\r\n      height: 34px;\r\n      /* border-radius: 50%; */\r\n      /* background: #000; */\r\n      color: #fff;\r\n      line-height: 25px;\r\n      font-size: 23px;\r\n      text-align: center;\r\n      opacity: 0.4;\r\n  \r\n          \r\n      }\r\n\r\n.whatsappme__header1 {\r\n      height: 70px;\r\n      padding: 0 26px;\r\n      background-color: #2e8c7d;\r\n      color: rgba(255, 255, 255, .5);\r\n      border-radius: 33px 33px 0 0;\r\n  }\r\n\r\n.whatsappme__close1 {\r\n      position: absolute;\r\n      top: 18px;\r\n      right: 24px;\r\n      width: 34px;\r\n      height: 34px;\r\n      border-radius: 50%;\r\n      background: #000;\r\n      color: #fff;\r\n      line-height: 34px;\r\n      font-size: 25px;\r\n      text-align: center;\r\n      opacity: .4;\r\n      cursor: pointer;\r\n      transition: opacity 300ms ease-out;\r\n  }\r\n\r\n.whatsappme__message1 {\r\n      position: relative;\r\n      min-height: 80px;\r\n      padding: 20px 22px;\r\n      margin: 34px 26px;\r\n      border-radius: 32px;\r\n      background-color: #fff;\r\n      color: #4A4A4A;\r\n      box-shadow: 0 1px 2px 0 rgba(0, 0, 0, 0.3);\r\n  }\r\n\r\n.whatsappme__message1::before {\r\n  /*\r\n      content: '';\r\n      display: block;\r\n      position: absolute;\r\n      bottom: 30px;\r\n      left: -18px;\r\n      width: 18px;\r\n      height: 18px;\r\n    background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADcAAAA1CAYAAADlE3NNAAAEr0lEQ…00059JUY3ZHFm8k1lT0cGfnJw8c0ojepWFzd6CMpnM3y8AJPEkZ9khO4IAAAAASUVORK5CYII=);\r\n    background-size: 100%;\r\n*/\r\n    \r\n    content:\"\";\r\n    position: absolute;\r\n       right: 100%;\r\n     top: 70px;\r\n     width: 0;\r\n     height: 0;\r\n     border-top: 7px solid transparent;\r\n     border-right: 22px solid #fff;\r\n     border-bottom: 4px solid transparent;\r\n     \r\n }\r\n\r\nform{\r\n     width:350px;\r\n     padding: 15px 10px 10px 40px;\r\n }\r\n\r\n.triangle-isosceles {\r\n    position: fixed;\r\n      z-index: 1004;\r\n     bottom: 38px;\r\n     right:112px;\r\n      height: 34px;\r\n      width: 156px;\r\n      background-color: rgb(255, 255, 255);\r\n      color: #fff;\r\n      border-radius: 5px;\r\n      box-shadow: 1px 6px 24px 0 rgba(7, 94, 84, .24);\r\n      cursor: pointer;\r\n      transition: background-color 500ms ease-in-out;\r\n  }\r\n\r\n/* creates triangle */\r\n\r\n.triangle-isosceles:after {\r\n    content: \"\";\r\n    display: block; /* reduce the damage in FF3.0 */\r\n   \r\n    position: absolute;\r\n    bottom: 13px;\r\n    left: 156px;\r\n    width: 0;\r\n    border-width: 14px 17px 0px 0px;\r\n    border-style: solid;\r\n    border-color: #fff transparent;\r\n  }\r\n\r\n.pull-right .dot {\r\n    position: fixed;\r\n    z-index: 1005;\r\n    bottom: 44px;\r\n    right: 31px;\r\n    height: 14px;\r\n    min-width: 14px;\r\n    max-width: 95vw;\r\n    background-color: #6ab937;\r\n    color: #fff;\r\n    border-radius: 30px;\r\n    box-shadow: 1px 6px 24px 0 rgba(7, 94, 84, .24);\r\n    cursor: pointer;\r\n    transition: background-color 500ms ease-in-out;\r\n}\r\n    \r\n"

/***/ }),

/***/ "./src/app/footer/footer.component.html":
/*!**********************************************!*\
  !*** ./src/app/footer/footer.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n\r\n <div class=\"triangle-isosceles\"><p style=\"color:rgb(53, 48, 48);padding:2px\">Welcome To Bigperl</p></div>\r\n<div class=\"pull-right\">\r\n\r\n    <span class=\"dot\"></span>  \r\n <div class=\"  whatsappme__button1\">\r\n    \r\n   <img src=\"assets/img/icons8-administrator-male-50_bigperl.png\" alt=\"Kiwi standing on oval\"> \r\n   \r\n   \r\n\r\n</div>\r\n</div>\r\n\r\n<div class=\"pull-right\">\r\n<div class=\"whatsappme__box1\" >\r\n        <div class=\"whatsappme__close1\">×</div>\r\n    <form method=\"post\" id=\"contactFormTest\" (ngSubmit)=\"login(); contactform.reset(); Success() \" #contactform=\"ngForm\" >\r\n            \r\n        <div class=\"row\">\r\n            <div class=\"col-md-12\">\r\n                <div class=\"input-field\">\r\n                    <i class=\"fa fa-user prefix fontcolors\"></i>\r\n                    <input type=\"text\" name=\"name\" class=\"validate\" id=\"name\" style=\"height:20px\" [(ngModel)]='name' >\r\n                    <label for=\"name\">Name</label>\r\n                </div>\r\n            </div>\r\n        </div>\r\n            <!-- /.col-md-6 -->\r\n            <div class=\"row\">\r\n            <div class=\"col-md-12\">\r\n                <div class=\"input-field\">\r\n                    <i class=\"fa fa-envelope prefix fontcolors\"></i>\r\n                    <input id=\"email\" type=\"email\" name=\"email\" class=\"validate\" style=\"height:20px\"  [(ngModel)]='email'>\r\n                    <label for=\"email\" data-error=\"wrong\" data-success=\"right\">Email</label>\r\n                </div>\r\n            </div>\r\n            <!-- /.col-md-6 -->\r\n        </div>\r\n        <!-- /.row -->\r\n\r\n        <div class=\"row\">\r\n            <div class=\"col-md-12\">\r\n                <div class=\"input-field\">\r\n                    <i class=\"fa fa-book prefix fontcolors\"></i>\r\n                    <input id=\"subject\" type=\"tel\" name=\"subject\" class=\"validate\" style=\"height:20px\" [(ngModel)]='subject'>\r\n                    <label for=\"subject\">Subject</label>\r\n                </div>\r\n            </div>\r\n            <!-- /.col-md-6 -->\r\n        </div>\r\n\r\n        <div class=\"row\">\r\n            <div class=\"col-md-12\">\r\n        <div class=\"input-field\">\r\n            <i class=\"fa fa-commenting-o prefix fontcolors\"></i>\r\n            <textarea name=\"message\" id=\"message\" class=\"form-control\" [(ngModel)]='message' style=\"border:none;height:127px\"></textarea>\r\n            <label for=\"message\">Message</label>\r\n        </div>\r\n    </div>\r\n</div>\r\n        <button type=\"submit\" name=\"submit\" class=\"btn-sm btnclass\"  [disabled]=\"!contactform.form.valid\" >Send Message</button>\r\n    </form>\r\n</div>\r\n</div>\r\n  <app-footer4></app-footer4>"

/***/ }),

/***/ "./src/app/footer/footer.component.ts":
/*!********************************************!*\
  !*** ./src/app/footer/footer.component.ts ***!
  \********************************************/
/*! exports provided: FooterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FooterComponent", function() { return FooterComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _user_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../user.model */ "./src/app/user.model.ts");
/* harmony import */ var _login_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../login.service */ "./src/app/login.service.ts");
/* harmony import */ var _toaster_service_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./../toaster-service.service */ "./src/app/toaster-service.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var FooterComponent = /** @class */ (function () {
    function FooterComponent(router, _loginService, toasterService) {
        this.router = router;
        this._loginService = _loginService;
        this.toasterService = toasterService;
        this.name = "";
        this.email = "";
        this.subject = "";
        this.message = "";
    }
    FooterComponent.prototype.Success = function () {
        this.toasterService.Success("Your message has been sent successfully.");
    };
    FooterComponent.prototype.Info = function () {
        this.toasterService.Info("Subscribed");
    };
    FooterComponent.prototype.ngOnInit = function () {
        $(document).ready(function () {
            $(".position5").hide();
            $(".position2").show();
            $('.position2').click(function () {
                $(".position5").slideToggle();
            });
        });
        $(".waves-circle i").click(function () { $(this).text(function (i, text) { return text === "close" ? "comment" : "close"; }); });
        (function ($) { $('div.whatsappme__button1').click(function () { $('div.whatsappme__box1').show(); }); $('div.whatsappme__close1').click(function () { $('div.whatsappme__box1').hide(); }); }(jQuery));
    };
    FooterComponent.prototype.login = function () {
        var _this = this;
        var user = new _user_model__WEBPACK_IMPORTED_MODULE_2__["User"]('', '', '', '');
        user.name = this.name;
        user.email = this.email;
        user.subject = this.subject;
        user.message = this.message;
        var data = [
            { 'name': user.name, 'email': user.email, 'subject': user.subject, 'message': user.message }
        ];
        this._loginService.sendLogin({ data: data })
            .subscribe(function (response) { return _this.handleResponse(response); }, function (error) { return _this.handleResponse(error); });
    };
    FooterComponent.prototype.handleResponse = function (response) {
        if (response.success) {
            console.log("success");
        }
        else if (response.error) {
            console.log("errror");
        }
        else {
        }
    };
    FooterComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-footer',
            template: __webpack_require__(/*! ./footer.component.html */ "./src/app/footer/footer.component.html"),
            styles: [__webpack_require__(/*! ./footer.component.css */ "./src/app/footer/footer.component.css")],
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _login_service__WEBPACK_IMPORTED_MODULE_3__["LoginService"], _toaster_service_service__WEBPACK_IMPORTED_MODULE_4__["ToasterService"]])
    ], FooterComponent);
    return FooterComponent;
}());



/***/ }),

/***/ "./src/app/footer1/footer1.component.css":
/*!***********************************************!*\
  !*** ./src/app/footer1/footer1.component.css ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/footer1/footer1.component.html":
/*!************************************************!*\
  !*** ./src/app/footer1/footer1.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section class=\"padding-top-20 padding-bottom-10 brand-bg\">\r\n  <div class=\"container\">\r\n    \r\n    <div class=\"text-center\">\r\n        <button class=\"waves-effect waves-light btn-large blue\" id=\"btnwidth\" data-toggle=\"modal\" data-target=\"#myModals\"><img src=\"assets/img/ico/trending_up_bigperl.png\" alt=\"trending_up_bigperl\" style=\"margin-right: 13px;\">Get free consultation from our Experts <img src=\"assets/img/ico/trending_up_bigperl.png\" alt=\"trending_up_bigperl\" style=\"margin-left: 10px;\"></button>\r\n        <div class=\"modal fade\" id=\"myModals\" tabindex=\"-1\"  style=\"width: 100%;min-height: 100%;background-color: transparent;\">\r\n                          <div class=\"modal-dialog\" >\r\n                            <div class=\"modal-content\">\r\n                              <div class=\"modal-header\">\r\n                                <button type=\"button\" class=\"close\" data-dismiss=\"modal\"   ><span   >&times;</span></button>\r\n                                <h4 class=\"modal-title\">Contact Us</h4>\r\n                              </div>\r\n                              <div class=\"modal-body\">\r\n                                    <div class=\"row\">\r\n                                    <div class=\"col-md-12\">\r\n                                      <form method=\"post\"  id=\"contactForm\" (ngSubmit)=\"login()\" #contactform=\"ngForm\">\r\n                                        <div class=\"row\">\r\n                                          <div class=\"col-md-6\">\r\n                                            <div class=\"input-field\">\r\n                                              <i class=\"fa fa-user prefix fontcolors\"></i>\r\n                                              <input type=\"text\" name=\"name\" class=\"validate\" id=\"name\" [(ngModel)]='name' >\r\n                                              <label for=\"name\">Name</label>\r\n                                            </div>\r\n                                          </div><!-- /.col-md-6 -->\r\n                                          <div class=\"col-md-6\">\r\n                                            <div class=\"input-field\">\r\n                                              <i class=\"fa fa-envelope prefix fontcolors\"></i>\r\n                                              <input id=\"email\" type=\"email\" name=\"email\" class=\"validate\" [(ngModel)]='email' >\r\n                                              <label for=\"email\" data-error=\"wrong\" data-success=\"right\">Email</label>\r\n                                            </div>\r\n                                          </div><!-- /.col-md-6 -->\r\n                                        </div><!-- /.row -->\r\n\r\n                                        <div class=\"row\">\r\n                                          <div class=\"col-md-12\">\r\n                                            <div class=\"input-field\">\r\n                                              <i class=\"fa fa-book prefix fontcolors\"></i>\r\n                                              <input id=\"subject\" type=\"tel\" name=\"subject\" class=\"validate\" [(ngModel)]='subject' >\r\n                                              <label for=\"subject\">Subject</label>\r\n                                            </div>\r\n                                          </div><!-- /.col-md-6 -->\r\n                                          </div>\r\n                                        <div class=\"input-field\">\r\n                                          <i class=\"fa fa-commenting-o prefix fontcolors\"></i>\r\n                                          <textarea name=\"message\" id=\"message\" class=\"materialize-textarea\" [(ngModel)]='message' ></textarea>\r\n                                          <label for=\"message\">Message</label>\r\n                                        </div>\r\n                                        <button type=\"submit\" name=\"submit\" class=\"btn-sm btnclass\"  [disabled]=\"!contactform.form.valid\" >Send Message</button>\r\n                                        </form>\r\n                                    </div>\r\n                              </div>\r\n                            </div>\r\n                          </div>\r\n                        </div>\r\n                        </div>\r\n    </div>\r\n  </div>\r\n</section>\r\n"

/***/ }),

/***/ "./src/app/footer1/footer1.component.ts":
/*!**********************************************!*\
  !*** ./src/app/footer1/footer1.component.ts ***!
  \**********************************************/
/*! exports provided: Footer1Component */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Footer1Component", function() { return Footer1Component; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _user_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../user.model */ "./src/app/user.model.ts");
/* harmony import */ var _login_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../login.service */ "./src/app/login.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var Footer1Component = /** @class */ (function () {
    function Footer1Component(router, _loginService) {
        this.router = router;
        this._loginService = _loginService;
        this.name = "";
        this.email = "";
        this.subject = "";
        this.message = "";
    }
    Footer1Component.prototype.ngOnInit = function () {
    };
    Footer1Component.prototype.login = function () {
        var _this = this;
        var user = new _user_model__WEBPACK_IMPORTED_MODULE_2__["User"]('', '', '', '');
        user.name = this.name;
        user.email = this.email;
        user.subject = this.subject;
        user.message = this.message;
        var data = [
            { 'name': user.name, 'email': user.email, 'subject': user.subject, 'message': user.message }
        ];
        this._loginService.sendLogin({ data: data })
            .subscribe(function (response) { return _this.handleResponse(response); }, function (error) { return _this.handleResponse(error); });
    };
    Footer1Component.prototype.handleResponse = function (response) {
        if (response.success) {
            console.log("success");
        }
        else if (response.error) {
            console.log("errror");
        }
        else {
        }
    };
    Footer1Component = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-footer1',
            template: __webpack_require__(/*! ./footer1.component.html */ "./src/app/footer1/footer1.component.html"),
            styles: [__webpack_require__(/*! ./footer1.component.css */ "./src/app/footer1/footer1.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _login_service__WEBPACK_IMPORTED_MODULE_3__["LoginService"]])
    ], Footer1Component);
    return Footer1Component;
}());



/***/ }),

/***/ "./src/app/footer2/footer2.component.css":
/*!***********************************************!*\
  !*** ./src/app/footer2/footer2.component.css ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/footer2/footer2.component.html":
/*!************************************************!*\
  !*** ./src/app/footer2/footer2.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n\r\n<div class=\"padding-top-20 padding-bottom-10 brand-bg\">\r\n  <div class=\"text-center\">\r\n    <form method=\"post\" id=\"contactForm2\" (ngSubmit)=\"subscribe();subscribeform.reset(); Info()\" #subscribeform=\"ngForm\">\r\n        <input type=\"email\" name=\"email\" placeholder=\"Subscribe to Newsletter\" id=\"inputs\" style=\"height:10px\" [(ngModel)]='email' >\r\n        <button class=\"btn btn-sm indigo\" name=\"submit\" id=\"inputbtnstyle\"  [disabled]=\"!subscribeform.form.valid\" ><i class=\"material-icons\">send</i></button>\r\n    </form>\r\n  </div>\r\n</div>\r\n<!-- /.container -->\r\n\r\n<app-footer3></app-footer3>\r\n\r\n\r\n"

/***/ }),

/***/ "./src/app/footer2/footer2.component.ts":
/*!**********************************************!*\
  !*** ./src/app/footer2/footer2.component.ts ***!
  \**********************************************/
/*! exports provided: Footer2Component */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Footer2Component", function() { return Footer2Component; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _user_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../user.model */ "./src/app/user.model.ts");
/* harmony import */ var _login_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../login.service */ "./src/app/login.service.ts");
/* harmony import */ var _subscribe_model__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../subscribe.model */ "./src/app/subscribe.model.ts");
/* harmony import */ var _subscribe_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./../subscribe.service */ "./src/app/subscribe.service.ts");
/* harmony import */ var _toaster_service_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./../toaster-service.service */ "./src/app/toaster-service.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var Footer2Component = /** @class */ (function () {
    function Footer2Component(router, _loginService, _subscribeService, toasterService) {
        this.router = router;
        this._loginService = _loginService;
        this._subscribeService = _subscribeService;
        this.toasterService = toasterService;
        this.name = "";
        this.email = "";
        this.subject = "";
        this.message = "";
    }
    Footer2Component.prototype.Success = function () {
        this.toasterService.Success("Thank you! Your message has been sent successfully.");
    };
    Footer2Component.prototype.Info = function () {
        this.toasterService.Info("Subscribed");
    };
    Footer2Component.prototype.ngOnInit = function () {
        function myFunction() { var input, filter, ul, li, a, i; input = document.getElementById("myInput"); filter = input.value.toUpperCase(); ul = document.getElementById("myUL"); li = ul.getElementsByTagName("li"); for (i = 0; i < li.length; i++) {
            a = li[i].getElementsByTagName("a")[0];
            if (a.innerHTML.toUpperCase().indexOf(filter) > -1) {
                li[i].style.display = "";
            }
            else {
                li[i].style.display = "none";
            }
        } }
        $('#customers-carousel1').owlCarousel({ responsive: { 0: { items: 1 }, 600: { items: 3 }, 1000: { items: 4 } }, loop: !0, margin: 30, nav: !0, smartSpeed: 900, navText: ["<i class='fa fa-chevron-left position'></i>", "<i class='fa fa-chevron-right position1'></i>"], autoplay: !0, autoplayTimeout: 3000 });
        $(document).ready(function () { $(".position5").hide(); $(".position2").show(); $('.position2').click(function () { $(".position5").slideToggle(); }); });
        $(".waves-circle i").click(function () { $(this).text(function (i, text) { return text === "close" ? "comment" : "close"; }); });
        var monkeyList = new List('test-list', { valueNames: ['name'], page: 6, pagination: !0 });
    };
    Footer2Component.prototype.login = function () {
        var _this = this;
        var user = new _user_model__WEBPACK_IMPORTED_MODULE_2__["User"]('', '', '', '');
        user.name = this.name;
        user.email = this.email;
        user.subject = this.subject;
        user.message = this.message;
        var data = [
            { 'name': user.name, 'email': user.email, 'subject': user.subject, 'message': user.message }
        ];
        this._loginService.sendLogin({ data: data })
            .subscribe(function (response) { return _this.handleResponse(response); }, function (error) { return _this.handleResponse(error); });
    };
    // handleResponse(response) {
    //   if (response.success) {
    //     console.log("success");
    //   } else if (response.error) {
    //     console.log("errror");
    //   } else {
    //   }
    // }
    Footer2Component.prototype.subscribe = function () {
        var _this = this;
        var subscribe = new _subscribe_model__WEBPACK_IMPORTED_MODULE_4__["Subscribe"]('');
        subscribe.email = this.email;
        var data = [
            { 'email': subscribe.email }
        ];
        this._subscribeService.sendSubscribe({ data: data })
            .subscribe(function (response) { return _this.handleResponse(response); }, function (error) { return _this.handleResponse(error); });
    };
    Footer2Component.prototype.handleResponse = function (response) {
        if (response.success) {
            console.log("success");
        }
        else if (response.error) {
            console.log("errror");
        }
        else {
        }
    };
    Footer2Component = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-footer2',
            template: __webpack_require__(/*! ./footer2.component.html */ "./src/app/footer2/footer2.component.html"),
            styles: [__webpack_require__(/*! ./footer2.component.css */ "./src/app/footer2/footer2.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _login_service__WEBPACK_IMPORTED_MODULE_3__["LoginService"], _subscribe_service__WEBPACK_IMPORTED_MODULE_5__["SubscribeService"],
            _toaster_service_service__WEBPACK_IMPORTED_MODULE_6__["ToasterService"]])
    ], Footer2Component);
    return Footer2Component;
}());



/***/ }),

/***/ "./src/app/footer3/footer3.component.css":
/*!***********************************************!*\
  !*** ./src/app/footer3/footer3.component.css ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/footer3/footer3.component.html":
/*!************************************************!*\
  !*** ./src/app/footer3/footer3.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section class=\"section-padding\">\r\n  <div id=\"customer-banner\">\r\n  <div class=\"section container\">\r\n      <div class=\"text-center mb-80\">\r\n         <br> \r\n         <h2 class=\"section-title text-uppercase\">Mostly Used Technologies</h2>\r\n      </div>\r\n      <div class=\"row\">\r\n          <div class=\"owl-carousel owl-theme\" id=\"customers-carousel1\">\r\n              <div class=\"item active\">\r\n                      <div class=\"featured-item seo-service\">\r\n                        <div class=\"icon\">\r\n                            <img class=\"img-responsive\" src=\"assets/img/seo/codeigniter_developement_bigperl.jpg\" alt=\"codeigniter_developement_bigperl\">\r\n                        </div>\r\n                        <div class=\"desc\">\r\n                            <h2>Codeigniter Development</h2>\r\n                            <p>Codeigniter web framework is Model - View - Controller framework built in PHP language</p>\r\n                            <div class=\"bg-overlay\"></div>\r\n                            <p><a class=\"learn-more\" routerLink=\"/codeigniter-development.php\">Learn More <i class=\"fa fa-long-arrow-right\"></i></a></p>\r\n                        </div>\r\n                    </div>\r\n              </div>\r\n              <div class=\"item\">\r\n                          <div class=\"featured-item seo-service\">\r\n                            <div class=\"icon\">\r\n                                <img class=\"img-responsive\" src=\"assets/img/seo/wordpress_developement_bigperl.jpg\" alt=\"wordpress_developement_bigperl\">\r\n                            </div>\r\n                            <div class=\"desc\">\r\n                                <h2>Wordpress Development</h2>\r\n                                <p>Want to build your website fast with less not no knowledge of programming language ?</p>\r\n                                <div class=\"bg-overlay\"></div>\r\n                                <p><a class=\"learn-more\" routerLink=\"/word-press-development.php\">Learn More <i class=\"fa fa-long-arrow-right\"></i></a></p>\r\n                            </div>\r\n                        </div>\r\n              </div>\r\n\r\n              <div class=\"item\">\r\n                      <div class=\"featured-item seo-service\">\r\n                            <div class=\"icon\">\r\n                                <img class=\"img-responsive\" src=\"assets/img/seo/e_commerce_development_bigperl.jpg\" alt=\"e_commerce_development_bigperl\">\r\n                            </div>\r\n                            <div class=\"desc\">\r\n                                <h2>E-Commerce Development</h2>\r\n                                <p>E-Commerce is bringing in lot of flaxibility for end user on the other hand development ...</p>\r\n                                <div class=\"bg-overlay\"></div>\r\n                                <p><a class=\"learn-more\" routerLink=\"/ecommerce-development.php\">Learn More <i class=\"fa fa-long-arrow-right\"></i></a></p>\r\n                            </div>\r\n                        </div>\r\n              </div>\r\n              <div class=\"item\">\r\n                      <div class=\"featured-item seo-service\">\r\n                        <div class=\"icon\">\r\n                            <img class=\"img-responsive\" src=\"assets/img/seo/windows_app_development_bigperl.jpg\" alt=\"windows_app_development_bigperl\">\r\n                        </div>\r\n                        <div class=\"desc\">\r\n                            <h2>Windows App Development</h2>\r\n                            <p>With introducing Windows 10 on desktop, mobile and table Microsoft has already proved that...</p>\r\n                            <div class=\"bg-overlay\"></div>\r\n                            <p><a class=\"learn-more\" routerLink=\"/windows-app-development.php\">Learn More <i class=\"fa fa-long-arrow-right\"></i></a></p>\r\n                        </div>\r\n                    </div>\r\n              </div>\r\n              <div class=\"item\">\r\n                      <div class=\"featured-item seo-service\">\r\n                        <div class=\"icon\">\r\n                            <img class=\"img-responsive\" src=\"assets/img/seo/magento_app_development_bigperl.jpg\" alt=\"magento_app_development_bigperl\">\r\n                        </div>\r\n                        <div class=\"desc\">\r\n                            <h2>Magento Development</h2>\r\n                            <p>Good E-Commerse doesnot aloways need high upfront technology and time investment. Magento framework already ...</p>\r\n                            <div class=\"bg-overlay\"></div>\r\n                            <p><a class=\"learn-more\" routerLink=\"/magento-development.php\">Learn More <i class=\"fa fa-long-arrow-right\"></i></a></p>\r\n                        </div>\r\n                    </div>\r\n              </div>\r\n              <div class=\"item\">\r\n                      <div class=\"featured-item seo-service\">\r\n                        <div class=\"icon\">\r\n                            <img class=\"img-responsive\" src=\"assets/img/seo/laravel_development_bigperl.jpg\" alt=\"laravel_development_bigperl\">\r\n                        </div>\r\n                        <div class=\"desc\">\r\n                            <h2>Laravel Development</h2>\r\n                            <p> With multiple new features and updates Laravel framework is getting high populatrity among Laravel Development community...</p>\r\n                            <div class=\"bg-overlay\"></div>\r\n                            <p><a class=\"learn-more\" routerLink=\"/laravel-development.php\">Learn More <i class=\"fa fa-long-arrow-right\"></i></a></p>\r\n                        </div>\r\n                    </div>\r\n              </div>\r\n              <div class=\"item\">\r\n                      <div class=\"featured-item seo-service\">\r\n                        <div class=\"icon\">\r\n                            <img class=\"img-responsive\" src=\"assets/img/seo/cake_php_develpoment_bigperl.jpg\" alt=\"cake_php_develpoment_bigperl\">\r\n                        </div>\r\n                        <div class=\"desc\">\r\n                            <h2>CakePHP Development</h2>\r\n                            <p>Looking for high performing, secure and robust  php framework for your cloud based application. CakePHP is one of the strongly..</p>\r\n                            <div class=\"bg-overlay\"></div>\r\n                            <p><a class=\"learn-more\" routerLink=\"/cake-php-development.php\">Learn More <i class=\"fa fa-long-arrow-right\"></i></a></p>\r\n                        </div>\r\n                    </div>\r\n              </div>\r\n              <div class=\"item\">\r\n                      <div class=\"featured-item seo-service\">\r\n                        <div class=\"icon\">\r\n                            <img class=\"img-responsive\" src=\"assets/img/seo/adf_app_development_bigperl.jpg\" alt=\"adf_app_development_bigperl\">\r\n                        </div>\r\n                        <div class=\"desc\">\r\n                            <h2>ADF & Webenter Development</h2>\r\n                            <p>Oracle Corp need not to have introduction in producing worlds top performing enterprise..</p>\r\n                            <div class=\"bg-overlay\"></div>\r\n                            <p><a class=\"learn-more\" routerLink=\"/adf-webcenter-development.php\">Learn More <i class=\"fa fa-long-arrow-right\"></i></a></p>\r\n                        </div>\r\n                    </div>\r\n              </div>\r\n              <div class=\"item\">\r\n                      <div class=\"featured-item seo-service\">\r\n                        <div class=\"icon\">\r\n                            <img class=\"img-responsive\" src=\"assets/img/seo/php_development_bigperl.jpg\" alt=\"php_development_bigperl\">\r\n                        </div>\r\n                        <div class=\"desc\">\r\n                            <h2>PHP Development</h2>\r\n                            <p>With its market leadership for more than two dacades and having majority market share, php has already proven its deep... </p>\r\n                            <div class=\"bg-overlay\"></div>\r\n                            <p><a class=\"learn-more\" routerLink=\"/php-development.php\">Learn More <i class=\"fa fa-long-arrow-right\"></i></a></p>\r\n                        </div>\r\n                    </div>\r\n              </div>\r\n              <div class=\"item\">\r\n                      <div class=\"featured-item seo-service\">\r\n                        <div class=\"icon\">\r\n                            <img class=\"img-responsive\" src=\"assets/img/seo/ruby_on_rails_development_bigperl.jpg\" alt=\"ruby_on_rails_development_bigperl\">\r\n                        </div>\r\n                        <div class=\"desc\">\r\n                            <h2>Ruby On Rails (ROR) Development</h2>\r\n                            <p>Type save and high performance gain Ruby On Rails (ROR) is Model - View - Controller (MVC) ...</p>\r\n                            <div class=\"bg-overlay\"></div>\r\n                            <p><a class=\"learn-more\" routerLink=\"/ruby-on-rails-development.php\">Learn More <i class=\"fa fa-long-arrow-right\"></i></a></p>\r\n                        </div>\r\n                    </div>\r\n              </div>\r\n              <div class=\"item\">\r\n                      <div class=\"featured-item seo-service\">\r\n                        <div class=\"icon\">\r\n                            <img class=\"img-responsive\" src=\"assets/img/seo/java_development_bigperl.jpg\" alt=\"java_development_bigperl\">\r\n                        </div>\r\n                        <div class=\"desc\">\r\n                            <h2>Java & Jee Development</h2>\r\n                            <p>Having deep penetration in enterprise application development and over 80% ...</p>\r\n                            <div class=\"bg-overlay\"></div>\r\n                            <p><a class=\"learn-more\" routerLink=\"/java-j2ee-development.php\">Learn More <i class=\"fa fa-long-arrow-right\"></i></a></p>\r\n                        </div>\r\n                    </div>\r\n              </div>\r\n              <div class=\"item\">\r\n                      <div class=\"featured-item seo-service\">\r\n                        <div class=\"icon\">\r\n                            <img class=\"img-responsive\" src=\"assets/img/seo/cms_development_bigperl.jpg\" alt=\"cms_development_bigperl\">\r\n                        </div>\r\n                        <div class=\"desc\">\r\n                            <h2>CMS Development</h2>\r\n                            <p>Content Management System, one of the biggest market segment to build websites without learning complex underlying technologies... </p>\r\n                            <div class=\"bg-overlay\"></div>\r\n                            <p><a class=\"learn-more\" routerLink=\"/cmc-development.php\">Learn More <i class=\"fa fa-long-arrow-right\"></i></a></p>\r\n                        </div>\r\n                    </div>\r\n              </div>\r\n          </div>\r\n      </div>\r\n  </div>\r\n  </div>\r\n</section>\r\n"

/***/ }),

/***/ "./src/app/footer3/footer3.component.ts":
/*!**********************************************!*\
  !*** ./src/app/footer3/footer3.component.ts ***!
  \**********************************************/
/*! exports provided: Footer3Component */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Footer3Component", function() { return Footer3Component; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var Footer3Component = /** @class */ (function () {
    function Footer3Component() {
    }
    Footer3Component.prototype.ngOnInit = function () {
        //  $('button').click(function(){
        //    alert("hello");
        //  });
        $('#customers-carousel').owlCarousel({ responsive: { 0: { items: 1 }, 600: { items: 3 }, 1000: { items: 4 } }, loop: !0, margin: 30, nav: !0, smartSpeed: 900, navText: ["<i class='fa fa-chevron-left position'></i>", "<i class='fa fa-chevron-right position1'></i>"], autoplay: !0, autoplayTimeout: 3000 });
        $('#customers-carousel1').owlCarousel({ responsive: { 0: { items: 1 }, 600: { items: 3 }, 1000: { items: 4 } }, loop: !0, margin: 30, nav: !0, smartSpeed: 900, navText: ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"], autoplay: !0, autoplayTimeout: 3000 });
        $('#customers-carousel2').owlCarousel({ responsive: { 0: { items: 1 }, 600: { items: 3 }, 1000: { items: 4 } }, loop: !0, margin: 30, nav: !0, navText: ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"], smartSpeed: 500, autoplay: !0, autoplayTimeout: 2000 });
        $('#customers-carousel3').owlCarousel({ responsive: { 0: { items: 1 }, 600: { items: 3 }, 1000: { items: 4 } }, loop: !0, margin: 30, nav: !0, smartSpeed: 2000, navText: ["<i class='fa fa-chevron-left position'></i>", "<i class='fa fa-chevron-right position1'></i>"], autoplay: !0, autoplayTimeout: 3000 });
    };
    Footer3Component = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-footer3',
            template: __webpack_require__(/*! ./footer3.component.html */ "./src/app/footer3/footer3.component.html"),
            styles: [__webpack_require__(/*! ./footer3.component.css */ "./src/app/footer3/footer3.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], Footer3Component);
    return Footer3Component;
}());



/***/ }),

/***/ "./src/app/footer4/footer4.component.css":
/*!***********************************************!*\
  !*** ./src/app/footer4/footer4.component.css ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#return-to-top {\r\n   \r\n    top:0px;\r\n    right: 20px;\r\n    background: #e91e63!important;\r\n    background: rgba(201, 24, 24, 0.7);\r\n    width: 50px;\r\n    height: 50px;\r\n    display: block;\r\n    text-decoration: none;\r\n    border-radius: 5px;\r\n    transition: all 0.3s ease;\r\n}\r\n#return-to-top i {\r\n   \r\n    color: #fff;\r\n    margin: 0;\r\n    position: relative;\r\n    left: 16px;\r\n    top: 13px;\r\n    font-size: 19px;\r\n    transition: all 0.3s ease;\r\n}\r\n#return-to-top:hover {\r\n    background: rgba(0, 0, 0, 0.9);\r\n}\r\n#return-to-top:hover i {\r\n    color: #fff;\r\n    top: 5px;\r\n}\r\n"

/***/ }),

/***/ "./src/app/footer4/footer4.component.html":
/*!************************************************!*\
  !*** ./src/app/footer4/footer4.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--footer 1 start -->\r\n<footer class=\"footer footer-one\">\r\n  <div class=\"primary-footer brand-bg\">\r\n      <div class=\"container\">\r\n<!-- \r\n          <a href=\"#top\" id=\"back2Top\" class=\"page-scroll btn-floating btn-large pink back-top waves-effect waves-light tt-animate btt\" data-section=\"#top\" style=\"margin-top: 80px;\">\r\n                    <i class=\"material-icons\">keyboard_arrow_up</i>   \r\n                  </a> -->\r\n               \r\n                 \r\n          <div class=\"row\">\r\n              <div class=\"col-md-3 widget clearfix\">\r\n                  <h2 class=\"white-text\">About BigPerl Solutions</h2>\r\n                  <p>BigPerl Solutions is Mobile Application Development Company specialized in Mobility, Internet Of Things, Software Development and Cloud Based Technologies. Founded in 2012, BigPerl is now providing solutions across the globe and having deep presence in India ,United States ,Canada,United Kingdom and Australia.</p>\r\n\r\n                  <ul class=\"social-link tt-animate ltr\">\r\n                      <li><a href=\"https://www.facebook.com/BigperlSolutions/\"><i class=\"fa fa-facebook\"></i></a></li>\r\n                      <li><a href=\"https://twitter.com/BigPerl\"><i class=\"fa fa-twitter\"></i></a></li>\r\n                      <li><a href=\"#\"><i class=\"fa fa-tumblr\"></i></a></li>\r\n                      <li><a href=\"https://www.linkedin.com/company/bigperl-solutions-pvt-ltd-?trk=top_nav_home\"><i class=\"fa fa-linkedin\"></i></a></li>\r\n                      <li><a href=\"#\"><i class=\"fa fa-dribbble\"></i></a></li>\r\n                      <li><a href=\"#\"><i class=\"fa fa-instagram\"></i></a></li>\r\n                      <li><a href=\"#\"><i class=\"fa fa-rss\"></i></a></li>\r\n                  </ul>\r\n              </div>\r\n              <!-- /.col-md-3 -->\r\n\r\n              <div class=\"col-md-3 widget\">\r\n                  <h2 class=\"white-text\">Imporant links</h2>\r\n\r\n                  <ul class=\"footer-list\">\r\n                      <li><a href=\"#\">About us</a></li>\r\n                      <li><a href=\"#\">Services</a></li>\r\n                      <li><a href=\"#\">Terms &amp; Condition</a></li>\r\n                      <li><a href=\"#\">Privacy Policy</a></li>\r\n                      <li><a href=\"contact-us\">Contact Us</a></li>\r\n                  </ul>\r\n              </div>\r\n              <!-- /.col-md-3 -->\r\n\r\n              <div class=\"col-md-3 widget\">\r\n                  <h2 class=\"white-text\">Our Specilizations</h2>\r\n\r\n                  <ul class=\"footer-list\">\r\n                      <li><a routerLink=\"/android-app-development\">Android Application Development</a></li>\r\n                      <li><a routerLink=\"/iphone-app-development\">iOS Application Development</a></li>\r\n                      <li><a routerLink=\"/windows-app-development\">Windows Application Development</a></li>\r\n                      <li><a href=\"#\">Mobile App Remediation (Android / iOS)</a></li>\r\n                      <li><a routerLink=\"/web-application-development\">Website Development</a></li>\r\n                      <li><a routerLink=\"/internet-of-things\">Internet Of Things</a></li>\r\n                      <li><a href=\"#\">Search Engine Optimization</a></li>\r\n                  </ul>\r\n              </div>\r\n              <!-- /.col-md-3 -->\r\n\r\n              <div class=\"col-md-3 widget\">\r\n                  <!--      <h2 class=\"white-text\">Subcribe Widget</h2>-->\r\n                  <a href=\"javascript:\" id=\"return-to-top\" class=\"pull-right\"><i class=\"icon-chevron-up\" ></i></a>\r\n                  <div class=\"widget-tags \" style=\"padding:10px\">\r\n                      <h2 class=\"white-text\">Tag Cloud</h2>\r\n\r\n\r\n                      <form method=\"post\" id=\"contactForm6\" (ngSubmit)=\"subscribe(); subscribeform.reset(); Success()\" #subscribeform=\"ngForm\" >\r\n                        <div class=\"form-group clearfix\">\r\n                            <label class=\"sr-only\" for=\"subscribe\">Email address</label>\r\n                            <input type=\"email\" class=\"form-control\" name=\"email\" id=\"subscribe\" placeholder=\"Email address\"   [(ngModel)]='email' >\r\n                            <button type=\"submit\" name=\"submit\" class=\"tt-animate ltr\"  [disabled]=\"!subscribeform.form.valid\" ><i class=\"fa fa-long-arrow-right\"></i></button>\r\n                        </div>\r\n                    </form>\r\n                      <a routerLink=\"/android-app-development\">Android App Development</a>\r\n                      <a routerLink=\"/iphone-app-development\">iOS App Development</a>\r\n                      <a href=\"#\">Mobile App Remediation</a>\r\n                      <a routerLink=\"/web-application-development\">Web Development</a>\r\n                      <a href=\"#\">Search Engine Optimization</a>\r\n                  </div>\r\n                  <!-- /.widget-tags -->\r\n              </div>\r\n              <!-- /.col-md-3 -->\r\n          </div>\r\n          <!-- /.row -->\r\n      </div>\r\n      <!-- /.container -->\r\n  </div>\r\n  <!-- /.primary-footer -->\r\n\r\n  <div class=\"secondary-footer brand-bg darken-2\">\r\n      <div class=\"container\">\r\n          <span class=\"copy-text\">Copyright &copy; 2017 <a href=\"http://bigperl.com\" target=\"_blank\">Bigperl Solutions Pvt. Ltd</a> &nbsp;  | &nbsp;  All Rights Reserved </span>\r\n      </div>\r\n      <!-- /.container -->\r\n  </div>\r\n  <!-- /.secondary-footer -->\r\n\r\n \r\n</footer>\r\n<!--footer 1 end-->"

/***/ }),

/***/ "./src/app/footer4/footer4.component.ts":
/*!**********************************************!*\
  !*** ./src/app/footer4/footer4.component.ts ***!
  \**********************************************/
/*! exports provided: Footer4Component */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Footer4Component", function() { return Footer4Component; });
/* harmony import */ var _subscribe_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./../subscribe.service */ "./src/app/subscribe.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _subscribe_model__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../subscribe.model */ "./src/app/subscribe.model.ts");
/* harmony import */ var _toaster_service_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./../toaster-service.service */ "./src/app/toaster-service.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var Footer4Component = /** @class */ (function () {
    function Footer4Component(router, _subscribeService, toasterService) {
        this.router = router;
        this._subscribeService = _subscribeService;
        this.toasterService = toasterService;
        this.email = "";
    }
    Footer4Component.prototype.Success = function () {
        this.toasterService.Success("Your message has been sent successfully.");
    };
    Footer4Component.prototype.Info = function () {
        this.toasterService.Info("Subscribed");
    };
    Footer4Component.prototype.ngOnInit = function () {
        // ===== Scroll to Top ==== 
        $(window).scroll(function () { if ($(this).scrollTop() >= 50) {
            $('#return-to-top').fadeIn(200);
        }
        else {
            $('#return-to-top').fadeOut(200);
        } });
        $('#return-to-top').click(function () { $('body,html').animate({ scrollTop: 0 }, 500); });
    };
    Footer4Component.prototype.subscribe = function () {
        var _this = this;
        var subscribe = new _subscribe_model__WEBPACK_IMPORTED_MODULE_3__["Subscribe"]('');
        subscribe.email = this.email;
        var data = [
            { 'email': subscribe.email }
        ];
        this._subscribeService.sendSubscribe({ data: data })
            .subscribe(function (response) { return _this.handleResponse(response); }, function (error) { return _this.handleResponse(error); });
        console.log(data);
    };
    Footer4Component.prototype.handleResponse = function (response) {
        if (response.success) {
            console.log("success");
        }
        else if (response.error) {
            console.log("errror");
        }
        else {
        }
    };
    Footer4Component = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-footer4',
            template: __webpack_require__(/*! ./footer4.component.html */ "./src/app/footer4/footer4.component.html"),
            styles: [__webpack_require__(/*! ./footer4.component.css */ "./src/app/footer4/footer4.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _subscribe_service__WEBPACK_IMPORTED_MODULE_0__["SubscribeService"], _toaster_service_service__WEBPACK_IMPORTED_MODULE_4__["ToasterService"]])
    ], Footer4Component);
    return Footer4Component;
}());



/***/ }),

/***/ "./src/app/footer5/footer5.component.css":
/*!***********************************************!*\
  !*** ./src/app/footer5/footer5.component.css ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/footer5/footer5.component.html":
/*!************************************************!*\
  !*** ./src/app/footer5/footer5.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section class=\"padding-top-20 padding-bottom-10 brand-bg\">\r\n  <div class=\"container\">\r\n    \r\n    <div class=\"text-center\">\r\n        <button class=\"waves-effect waves-light btn-large blue\" id=\"btnwidth\" data-toggle=\"modal\" data-target=\"#myModals\"><img src=\"assets/img/ico/trending_up_bigperl.png\" alt=\"trending_up_bigperl\" style=\"margin-right: 13px;\">Get free consultation from our Experts <img src=\"assets/img/ico/trending_up_bigperl.png\" alt=\"trending_up_bigperl\" style=\"margin-left: 10px;\"></button>\r\n        <div class=\"modal fade\" id=\"myModals\" tabindex=\"-1\"  style=\"width: 100%;min-height: 100%;background-color: transparent;\">\r\n                          <div class=\"modal-dialog\" >\r\n                            <div class=\"modal-content\">\r\n                              <div class=\"modal-header\">\r\n                                <button type=\"button\" class=\"close\" data-dismiss=\"modal\"   ><span   >&times;</span></button>\r\n                                <h4 class=\"modal-title\">Contact Us</h4>\r\n                              </div>\r\n                              <div class=\"modal-body\">\r\n                                    <div class=\"row\">\r\n                                    <div class=\"col-md-12\">\r\n                                        <form method=\"post\" id=\"contactForm\" (ngSubmit)=\"login();contactform.reset(); Success()\" #contactform=\"ngForm\" >\r\n                                          <div class=\"row\">\r\n                                            <div class=\"col-md-6\">\r\n                                              <div class=\"input-field\">\r\n                                                <i class=\"fa fa-user prefix fontcolors\"></i>\r\n                                                <input type=\"text\" name=\"name\" class=\"validate\" id=\"name\" [(ngModel)]='name' >\r\n                                                <label for=\"name\">Name</label>\r\n                                              </div>\r\n                                            </div><!-- /.col-md-6 -->\r\n                                            <div class=\"col-md-6\">\r\n                                              <div class=\"input-field\">\r\n                                                <i class=\"fa fa-envelope prefix fontcolors\"></i>\r\n                                                <input id=\"email\" type=\"email\" name=\"email\" class=\"validate\" [(ngModel)]='email' >\r\n                                                <label for=\"email\" data-error=\"wrong\" data-success=\"right\">Email</label>\r\n                                              </div>\r\n                                            </div><!-- /.col-md-6 -->\r\n                                          </div><!-- /.row -->\r\n\r\n                                          <div class=\"row\">\r\n                                            <div class=\"col-md-12\">\r\n                                              <div class=\"input-field\">\r\n                                                <i class=\"fa fa-book prefix fontcolors\"></i>\r\n                                                <input id=\"subject\" type=\"tel\" name=\"subject\" class=\"validate\" [(ngModel)]='subject' >\r\n                                                <label for=\"subject\">Subject</label>\r\n                                              </div>\r\n                                            </div><!-- /.col-md-6 -->\r\n                                            </div>\r\n                                          <div class=\"input-field\">\r\n                                            <i class=\"fa fa-commenting-o prefix fontcolors\"></i>\r\n                                            <textarea name=\"message\" id=\"message\" class=\"materialize-textarea\" [(ngModel)]='message' ></textarea>\r\n                                            <label for=\"message\">Message</label>\r\n                                          </div>\r\n                                          <button type=\"submit\" name=\"submit\" class=\"btn-sm btnclass\"  [disabled]=\"!contactform.form.valid\" >Send Message</button>\r\n                                          </form>\r\n                                    </div>\r\n                              </div>\r\n                            </div>\r\n                          </div>\r\n                        </div>\r\n                        </div>\r\n    </div>\r\n  </div>\r\n</section>\r\n<section class=\"section-padding banner-10 bg-fixed parallax-bg overlay light-9\" data-stellar-background-ratio=\"0.5\">\r\n  <div class=\"container\">\r\n    <div class=\"text-center ptb-30\">\r\n        <h2 class=\"section-title text-uppercase\">Technologies we use</h2>\r\n        <p class=\"section-sub\">We believe in Scalability, Performance, Usability and No Customer Locking. Our team always use best-proven technologies best as per global standards.</p>\r\n    </div>\r\n\r\n\r\n    <div class=\"featured-carousel brand-dot\">\r\n        <div class=\"featured-item border-box radius-4 hover brand-hover\">\r\n            <div class=\"icon mb-30\">\r\n                <img src=\"assets/img/angular_logo_bigperl.png\" alt=\"angular_logo_bigperl\">\r\n            </div>\r\n            <div class=\"desc\">\r\n                <h2>Angular JS</h2>\r\n                <p>Supported by Google Angular JS is not fasted growing user interface framework built in javascript. It increases performance, usability, and interactivity ...</p>\r\n            </div>\r\n\r\n\r\n\r\n        </div><!-- /.featured-item -->\r\n\r\n        <div class=\"featured-item border-box radius-4 hover brand-hover\">\r\n            <div class=\"icon mb-30\">\r\n                <img src=\"assets/img/node_logo_bigperl.png\" alt=\"node_logo_bigperl\">\r\n            </div>\r\n            <div class=\"desc\">\r\n                <h2>Node JS</h2>\r\n                <p>First time in the history of technology javascript become one of the powerful one of the powerfull back-end technology. Widely accepted ..</p>\r\n            </div>\r\n        </div><!-- /.featured-item -->\r\n\r\n        <div class=\"featured-item border-box radius-4 hover brand-hover\">\r\n            <div class=\"icon mb-30\">\r\n                <img src=\"assets/img/material_logo_bigperl.png\" alt=\"material_logo_bigperl\">\r\n            </div>\r\n            <div class=\"desc\">\r\n                <h2>materialize CSS</h2>\r\n                <p>The innovation of Google which is changing the way websites are getting designed. Material design is bringing in highest level of interactivity ...</p>\r\n            </div>\r\n        </div><!-- /.featured-item -->\r\n    </div>\r\n\r\n  </div><!-- /.container -->\r\n  <div class=\"mocup-wrapper text-center\">\r\n      <img src=\"assets/img/landscape_mockup_bigperl.jpg\" alt=\"image\">\r\n  </div>\r\n</section><br>"

/***/ }),

/***/ "./src/app/footer5/footer5.component.ts":
/*!**********************************************!*\
  !*** ./src/app/footer5/footer5.component.ts ***!
  \**********************************************/
/*! exports provided: Footer5Component */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Footer5Component", function() { return Footer5Component; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _user_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../user.model */ "./src/app/user.model.ts");
/* harmony import */ var _login_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../login.service */ "./src/app/login.service.ts");
/* harmony import */ var _toaster_service_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./../toaster-service.service */ "./src/app/toaster-service.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var Footer5Component = /** @class */ (function () {
    function Footer5Component(router, _loginService, toasterService) {
        this.router = router;
        this._loginService = _loginService;
        this.toasterService = toasterService;
        this.name = "";
        this.email = "";
        this.subject = "";
        this.message = "";
    }
    Footer5Component.prototype.Success = function () {
        this.toasterService.Success("Your message has been sent successfully.");
    };
    Footer5Component.prototype.Info = function () {
        this.toasterService.Info("Subscribed");
    };
    Footer5Component.prototype.ngOnInit = function () {
        $(".featured-carousel").length > 0 && $(".featured-carousel").owlCarousel({
            loop: !0,
            margin: 12,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 2
                },
                1e3: {
                    items: 3
                }
            }
        });
    };
    Footer5Component.prototype.login = function () {
        var _this = this;
        var user = new _user_model__WEBPACK_IMPORTED_MODULE_2__["User"]('', '', '', '');
        user.name = this.name;
        user.email = this.email;
        user.subject = this.subject;
        user.message = this.message;
        var data = [
            { 'name': user.name, 'email': user.email, 'subject': user.subject, 'message': user.message }
        ];
        this._loginService.sendLogin({ data: data })
            .subscribe(function (response) { return _this.handleResponse(response); }, function (error) { return _this.handleResponse(error); });
    };
    Footer5Component.prototype.handleResponse = function (response) {
        if (response.success) {
            console.log("success");
        }
        else if (response.error) {
            console.log("errror");
        }
        else {
        }
    };
    Footer5Component = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-footer5',
            template: __webpack_require__(/*! ./footer5.component.html */ "./src/app/footer5/footer5.component.html"),
            styles: [__webpack_require__(/*! ./footer5.component.css */ "./src/app/footer5/footer5.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _login_service__WEBPACK_IMPORTED_MODULE_3__["LoginService"], _toaster_service_service__WEBPACK_IMPORTED_MODULE_4__["ToasterService"]])
    ], Footer5Component);
    return Footer5Component;
}());



/***/ }),

/***/ "./src/app/footer6/footer6.component.css":
/*!***********************************************!*\
  !*** ./src/app/footer6/footer6.component.css ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/footer6/footer6.component.html":
/*!************************************************!*\
  !*** ./src/app/footer6/footer6.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"padding-top-20 padding-bottom-10 brand-bg\">\r\n  <div class=\"text-center\">\r\n    <form method=\"post\" id=\"contactForm2\" (ngSubmit)=\"subscribe()\" #subscribeform=\"ngForm\">\r\n        <input type=\"email\" name=\"email\" placeholder=\"Subscribe to Newsletter\" id=\"inputs\" style=\"height:10px\" [(ngModel)]='email' >\r\n        <button class=\"btn btn-sm indigo\" name=\"submit\" id=\"inputbtnstyle\"  [disabled]=\"!subscribeform.form.valid\" ><i class=\"material-icons\">send</i></button>\r\n    </form>\r\n  </div>\r\n</div>\r\n<!-- /.container -->\r\n\r\n<section class=\"section-padding\">\r\n\r\n  <div id=\"customer-banner\">\r\n      <div class=\"section container\">\r\n          <div class=\"text-center mb-80\">\r\n            <h2 class=\"section-title text-uppercase\">Mostly Used Technologies</h2>\r\n        </div>\r\n        <div class=\"row\">\r\n            <div class=\"owl-carousel owl-theme\" id=\"customers-carousel1\">\r\n                <div class=\"item active\">\r\n                        <div class=\"featured-item seo-service\">\r\n                          <div class=\"icon\">\r\n                              <img class=\"img-responsive\" src=\"assets/img/seo/codeigniter_developement_bigperl.jpg\" alt=\"codeigniter_developement_bigperl\">\r\n                          </div>\r\n                          <div class=\"desc\">\r\n                              <h2>Codeigniter Development</h2>\r\n                              <p>Codeigniter web framework is Model - View - Controller framework built in PHP language</p>\r\n                              <div class=\"bg-overlay\"></div>\r\n                              <p><a class=\"learn-more\" routerLink=\"/codeigniter-development.php\">Learn More <i class=\"fa fa-long-arrow-right\"></i></a></p>\r\n                          </div>\r\n                      </div>\r\n                </div>\r\n                <div class=\"item\">\r\n                            <div class=\"featured-item seo-service\">\r\n                              <div class=\"icon\">\r\n                                  <img class=\"img-responsive\" src=\"assets/img/seo/wordpress_developement_bigperl.jpg\" alt=\"wordpress_developement_bigperl\">\r\n                              </div>\r\n                              <div class=\"desc\">\r\n                                  <h2>Wordpress Development</h2>\r\n                                  <p>Want to build your website fast with less not no knowledge of programming language ?</p>\r\n                                  <div class=\"bg-overlay\"></div>\r\n                                  <p><a class=\"learn-more\" routerLink=\"/word-press-development.php\">Learn More <i class=\"fa fa-long-arrow-right\"></i></a></p>\r\n                              </div>\r\n                          </div>\r\n                </div>\r\n  \r\n                <div class=\"item\">\r\n                        <div class=\"featured-item seo-service\">\r\n                              <div class=\"icon\">\r\n                                  <img class=\"img-responsive\" src=\"assets/img/seo/e_commerce_development_bigperl.jpg\" alt=\"e_commerce_development_bigperl\">\r\n                              </div>\r\n                              <div class=\"desc\">\r\n                                  <h2>E-Commerce Development</h2>\r\n                                  <p>E-Commerce is bringing in lot of flaxibility for end user on the other hand development ...</p>\r\n                                  <div class=\"bg-overlay\"></div>\r\n                                  <p><a class=\"learn-more\" routerLink=\"/ecommerce-development.php\">Learn More <i class=\"fa fa-long-arrow-right\"></i></a></p>\r\n                              </div>\r\n                          </div>\r\n                </div>\r\n                <div class=\"item\">\r\n                        <div class=\"featured-item seo-service\">\r\n                          <div class=\"icon\">\r\n                              <img class=\"img-responsive\" src=\"assets/img/seo/windows_app_development_bigperl.jpg\" alt=\"windows_app_development_bigperl\">\r\n                          </div>\r\n                          <div class=\"desc\">\r\n                              <h2>Windows App Development</h2>\r\n                              <p>With introducing Windows 10 on desktop, mobile and table Microsoft has already proved that...</p>\r\n                              <div class=\"bg-overlay\"></div>\r\n                              <p><a class=\"learn-more\" routerLink=\"/windows-app-development.php\">Learn More <i class=\"fa fa-long-arrow-right\"></i></a></p>\r\n                          </div>\r\n                      </div>\r\n                </div>\r\n                <div class=\"item\">\r\n                        <div class=\"featured-item seo-service\">\r\n                          <div class=\"icon\">\r\n                              <img class=\"img-responsive\" src=\"assets/img/seo/magento_app_development_bigperl.jpg\" alt=\"magento_app_development_bigperl\">\r\n                          </div>\r\n                          <div class=\"desc\">\r\n                              <h2>Magento Development</h2>\r\n                              <p>Good E-Commerse doesnot aloways need high upfront technology and time investment. Magento framework already ...</p>\r\n                              <div class=\"bg-overlay\"></div>\r\n                              <p><a class=\"learn-more\" routerLink=\"/magento-development.php\">Learn More <i class=\"fa fa-long-arrow-right\"></i></a></p>\r\n                          </div>\r\n                      </div>\r\n                </div>\r\n                <div class=\"item\">\r\n                        <div class=\"featured-item seo-service\">\r\n                          <div class=\"icon\">\r\n                              <img class=\"img-responsive\" src=\"assets/img/seo/laravel_development_bigperl.jpg\" alt=\"laravel_development_bigperl\">\r\n                          </div>\r\n                          <div class=\"desc\">\r\n                              <h2>Laravel Development</h2>\r\n                              <p> With multiple new features and updates Laravel framework is getting high populatrity among Laravel Development community...</p>\r\n                              <div class=\"bg-overlay\"></div>\r\n                              <p><a class=\"learn-more\" routerLink=\"/laravel-development.php\">Learn More <i class=\"fa fa-long-arrow-right\"></i></a></p>\r\n                          </div>\r\n                      </div>\r\n                </div>\r\n                <div class=\"item\">\r\n                        <div class=\"featured-item seo-service\">\r\n                          <div class=\"icon\">\r\n                              <img class=\"img-responsive\" src=\"assets/img/seo/cake_php_develpoment_bigperl.jpg\" alt=\"cake_php_develpoment_bigperl\">\r\n                          </div>\r\n                          <div class=\"desc\">\r\n                              <h2>CakePHP Development</h2>\r\n                              <p>Looking for high performing, secure and robust  php framework for your cloud based application. CakePHP is one of the strongly..</p>\r\n                              <div class=\"bg-overlay\"></div>\r\n                              <p><a class=\"learn-more\" routerLink=\"/cake-php-development.php\">Learn More <i class=\"fa fa-long-arrow-right\"></i></a></p>\r\n                          </div>\r\n                      </div>\r\n                </div>\r\n                <div class=\"item\">\r\n                        <div class=\"featured-item seo-service\">\r\n                          <div class=\"icon\">\r\n                              <img class=\"img-responsive\" src=\"assets/img/seo/adf_app_development_bigperl.jpg\" alt=\"adf_app_development_bigperl\">\r\n                          </div>\r\n                          <div class=\"desc\">\r\n                              <h2>ADF & Webenter Development</h2>\r\n                              <p>Oracle Corp need not to have introduction in producing worlds top performing enterprise..</p>\r\n                              <div class=\"bg-overlay\"></div>\r\n                              <p><a class=\"learn-more\" routerLink=\"/adf-webcenter-development.php\">Learn More <i class=\"fa fa-long-arrow-right\"></i></a></p>\r\n                          </div>\r\n                      </div>\r\n                </div>\r\n                <div class=\"item\">\r\n                        <div class=\"featured-item seo-service\">\r\n                          <div class=\"icon\">\r\n                              <img class=\"img-responsive\" src=\"assets/img/seo/php_development_bigperl.jpg\" alt=\"php_development_bigperl\">\r\n                          </div>\r\n                          <div class=\"desc\">\r\n                              <h2>PHP Development</h2>\r\n                              <p>With its market leadership for more than two dacades and having majority market share, php has already proven its deep... </p>\r\n                              <div class=\"bg-overlay\"></div>\r\n                              <p><a class=\"learn-more\" routerLink=\"/php-development.php\">Learn More <i class=\"fa fa-long-arrow-right\"></i></a></p>\r\n                          </div>\r\n                      </div>\r\n                </div>\r\n                <div class=\"item\">\r\n                        <div class=\"featured-item seo-service\">\r\n                          <div class=\"icon\">\r\n                              <img class=\"img-responsive\" src=\"assets/img/seo/ruby_on_rails_development_bigperl.jpg\" alt=\"ruby_on_rails_development_bigperl\">\r\n                          </div>\r\n                          <div class=\"desc\">\r\n                              <h2>Ruby On Rails (ROR) Development</h2>\r\n                              <p>Type save and high performance gain Ruby On Rails (ROR) is Model - View - Controller (MVC) ...</p>\r\n                              <div class=\"bg-overlay\"></div>\r\n                              <p><a class=\"learn-more\" routerLink=\"/ruby-on-rails-development.php\">Learn More <i class=\"fa fa-long-arrow-right\"></i></a></p>\r\n                          </div>\r\n                      </div>\r\n                </div>\r\n                <div class=\"item\">\r\n                        <div class=\"featured-item seo-service\">\r\n                          <div class=\"icon\">\r\n                              <img class=\"img-responsive\" src=\"assets/img/seo/java_development_bigperl.jpg\" alt=\"java_development_bigperl\">\r\n                          </div>\r\n                          <div class=\"desc\">\r\n                              <h2>Java & Jee Development</h2>\r\n                              <p>Having deep penetration in enterprise application development and over 80% ...</p>\r\n                              <div class=\"bg-overlay\"></div>\r\n                              <p><a class=\"learn-more\" routerLink=\"/java-j2ee-development.php\">Learn More <i class=\"fa fa-long-arrow-right\"></i></a></p>\r\n                          </div>\r\n                      </div>\r\n                </div>\r\n                <div class=\"item\">\r\n                        <div class=\"featured-item seo-service\">\r\n                          <div class=\"icon\">\r\n                              <img class=\"img-responsive\" src=\"assets/img/seo/cms_development_bigperl.jpg\" alt=\"cms_development_bigperl\">\r\n                          </div>\r\n                          <div class=\"desc\">\r\n                              <h2>CMS Development</h2>\r\n                              <p>Content Management System, one of the biggest market segment to build websites without learning complex underlying technologies... </p>\r\n                              <div class=\"bg-overlay\"></div>\r\n                              <p><a class=\"learn-more\" routerLink=\"/cmc-development.php\">Learn More <i class=\"fa fa-long-arrow-right\"></i></a></p>                          </div>\r\n                      </div>\r\n                  </div>\r\n              </div>\r\n          </div>\r\n      </div>\r\n  </div>\r\n</section>\r\n\r\n<app-footer3></app-footer3>\r\n<div class=\"position5\">\r\n  <div class=\"panel panel-success\">\r\n      <div class=\"panel-body\">\r\n          <div class=\"row\">\r\n              <div class=\"col-md-12\">\r\n                  <form method=\"post\" id=\"contactForm\" (ngSubmit)=\"login()\" #contactform=\"ngForm\" >\r\n                      <div class=\"row\">\r\n                          <div class=\"col-md-6\">\r\n                              <div class=\"input-field\">\r\n                                  <i class=\"fa fa-user prefix fontcolors\"></i>\r\n                                  <input type=\"text\" name=\"name\" class=\"validate\" id=\"name\"  [(ngModel)]='name' >\r\n                                  <label for=\"name\">Name</label>\r\n                              </div>\r\n                          </div>\r\n                          <!-- /.col-md-6 -->\r\n                          <div class=\"col-md-6\">\r\n                              <div class=\"input-field\">\r\n                                  <i class=\"fa fa-envelope prefix fontcolors\"></i>\r\n                                  <input id=\"email\" type=\"email\" name=\"email\" class=\"validate\"  [(ngModel)]='email' >\r\n                                  <label for=\"email\" data-error=\"wrong\" data-success=\"right\">Email</label>\r\n                              </div>\r\n                          </div>\r\n                          <!-- /.col-md-6 -->\r\n                      </div>\r\n                      <!-- /.row -->\r\n\r\n                      <div class=\"row\">\r\n                          <div class=\"col-md-12\">\r\n                              <div class=\"input-field\">\r\n                                  <i class=\"fa fa-book prefix fontcolors\"></i>\r\n                                  <input id=\"subject\" type=\"tel\" name=\"subject\" class=\"validate\" [(ngModel)]='subject' >\r\n                                  <label for=\"subject\">Subject</label>\r\n                              </div>\r\n                          </div>\r\n                          <!-- /.col-md-6 -->\r\n                      </div>\r\n                      <div class=\"input-field\">\r\n                          <i class=\"fa fa-commenting-o prefix fontcolors\"></i>\r\n                          <textarea name=\"message\" id=\"message\" class=\"materialize-textarea\" [(ngModel)]='message' ></textarea>\r\n                          <label for=\"message\">Message</label>\r\n                      </div>\r\n                      <button type=\"submit\" name=\"submit\" class=\"btn-sm btnclass\" [disabled]=\"!contactform.form.valid\" >Send Message</button>\r\n                  </form>\r\n              </div>\r\n          </div>\r\n      </div>\r\n  </div>\r\n</div>\r\n<button class=\"waves-effect waves-circle waves-light btn-floating btn-large blue position2\"><i class=\"material-icons\" id=\"btnpad\">comment</i></button>\r\n\r\n\r\n"

/***/ }),

/***/ "./src/app/footer6/footer6.component.ts":
/*!**********************************************!*\
  !*** ./src/app/footer6/footer6.component.ts ***!
  \**********************************************/
/*! exports provided: Footer6Component */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Footer6Component", function() { return Footer6Component; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _user_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../user.model */ "./src/app/user.model.ts");
/* harmony import */ var _login_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../login.service */ "./src/app/login.service.ts");
/* harmony import */ var _subscribe_model__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../subscribe.model */ "./src/app/subscribe.model.ts");
/* harmony import */ var _subscribe_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./../subscribe.service */ "./src/app/subscribe.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var Footer6Component = /** @class */ (function () {
    function Footer6Component(router, _loginService, _subscribeService) {
        this.router = router;
        this._loginService = _loginService;
        this._subscribeService = _subscribeService;
        this.name = "";
        this.email = "";
        this.subject = "";
        this.message = "";
    }
    Footer6Component.prototype.ngOnInit = function () {
    };
    Footer6Component.prototype.login = function () {
        var _this = this;
        var user = new _user_model__WEBPACK_IMPORTED_MODULE_2__["User"]('', '', '', '');
        user.name = this.name;
        user.email = this.email;
        user.subject = this.subject;
        user.message = this.message;
        var data = [
            { 'name': user.name, 'email': user.email, 'subject': user.subject, 'message': user.message }
        ];
        this._loginService.sendLogin({ data: data })
            .subscribe(function (response) { return _this.handleResponse(response); }, function (error) { return _this.handleResponse(error); });
    };
    // handleResponse(response) {
    //   if (response.success) {
    //     console.log("success");
    //   } else if (response.error) {
    //     console.log("errror");
    //   } else {
    //   }
    // }
    Footer6Component.prototype.subscribe = function () {
        var _this = this;
        var subscribe = new _subscribe_model__WEBPACK_IMPORTED_MODULE_4__["Subscribe"]('');
        subscribe.email = this.email;
        var data = [
            { 'email': subscribe.email }
        ];
        this._subscribeService.sendSubscribe({ data: data })
            .subscribe(function (response) { return _this.handleResponse(response); }, function (error) { return _this.handleResponse(error); });
    };
    Footer6Component.prototype.handleResponse = function (response) {
        if (response.success) {
            console.log("success");
        }
        else if (response.error) {
            console.log("errror");
        }
        else {
        }
    };
    Footer6Component = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-footer6',
            template: __webpack_require__(/*! ./footer6.component.html */ "./src/app/footer6/footer6.component.html"),
            styles: [__webpack_require__(/*! ./footer6.component.css */ "./src/app/footer6/footer6.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _login_service__WEBPACK_IMPORTED_MODULE_3__["LoginService"], _subscribe_service__WEBPACK_IMPORTED_MODULE_5__["SubscribeService"]])
    ], Footer6Component);
    return Footer6Component;
}());



/***/ }),

/***/ "./src/app/header/header.component.css":
/*!*********************************************!*\
  !*** ./src/app/header/header.component.css ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n\r\n\r\n/* Navbar container */\r\n.navbar {\r\n    overflow: hidden;\r\n    background-color: #333;\r\n    font-family: Arial;\r\n  }\r\n/* Links inside the navbar */\r\n.navbar a {\r\n    float: left;\r\n    font-size: 16px;\r\n    color: white;\r\n    text-align: center;\r\n    padding: 14px 16px;\r\n    text-decoration: none;\r\n  }\r\n/* The dropdown container */\r\n.dropdown {\r\n    float: left;\r\n    overflow: hidden;\r\n  }\r\n/* Dropdown button */\r\n.dropdown .dropbtn {\r\n    font-size: 16px; \r\n    border: none;\r\n    outline: none;\r\n    color: white;\r\n    padding: 14px 16px;\r\n    background-color: inherit;\r\n    font: inherit; /* Important for vertical align on mobile phones */\r\n    margin: 0; /* Important for vertical align on mobile phones */\r\n  }\r\n/* Add a red background color to navbar links on hover */\r\n.navbar a:hover, .dropdown:hover .dropbtn {\r\n    background-color: red;\r\n  }\r\n/* Dropdown content (hidden by default) */\r\n.dropdown-content {\r\n    display: none;\r\n    position: absolute;\r\n    background-color: #f9f9f9;\r\n    width: 100%;\r\n    left: 0;\r\n    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);\r\n    z-index: 1;\r\n  }\r\n/* Mega Menu header, if needed */\r\n.dropdown-content .header {\r\n    background: red;\r\n    padding: 16px;\r\n    color: white;\r\n  }\r\n/* Show the dropdown menu on hover */\r\n.dropdown:hover .dropdown-content {\r\n    display: block;\r\n  }\r\n/* Create three equal columns that floats next to each other */\r\n.column {\r\n    float: left;\r\n    width: 33.33%;\r\n    padding: 10px;\r\n    background-color: #ccc;\r\n    height: 250px;\r\n  }\r\n/* Style links inside the columns */\r\n.column a {\r\n    float: none;\r\n    color: black;\r\n    padding: 16px;\r\n    text-decoration: none;\r\n    display: block;\r\n    text-align: left;\r\n  }\r\n/* Add a background color on hover */\r\n.column a:hover {\r\n    background-color: #ddd;\r\n  }\r\n.kkll{\r\n    width:100%\r\n  }\r\n.wideHeader{\r\n   width: 100%;\r\n  padding: 10px;\r\n  \r\n  }\r\n.wideHeader h2{\r\n    font-size: 12px;\r\n    font-weight: 600;\r\n    padding-left: 20px\r\n  }\r\n.wideHeader li ul li{\r\n\r\n    line-height: 10px;\r\n\r\n  }\r\n.whatsappme__button {\r\n   \r\n  position: fixed;\r\n    z-index: 1004;\r\n   bottom: 28px;\r\n    left: 30px;\r\n    height: 60px;\r\n    min-width: 60px;\r\n    max-width: 95vw;\r\n    background-color: #25D366;\r\n    color: #fff;\r\n    border-radius: 30px;\r\n    box-shadow: 1px 6px 24px 0 rgba(7, 94, 84, .24);\r\n    cursor: pointer;\r\n    transition: background-color 500ms ease-in-out;\r\n}\r\n.whatsappme__box {\r\n     display: none;\r\n  position: fixed;\r\n         bottom: 23px;\r\n    left: 22px;\r\n    \r\n z-index: 1000;\r\n    \r\n    \r\n   \r\n/*\r\n \r\n   \r\n    z-index: 1;\r\n*/\r\n    width: calc(100vw - 40px);\r\n    max-width: 400px;\r\n    min-height: 280px;\r\n    padding-bottom: 60px;\r\n    border-radius: 32px;\r\n    \r\n     background: #ede4dd url('whatsapp_background_bigperl.png') center repeat-y;\r\n/*\r\n    background: #ede4dd url(whatsapp_background.png) center repeat-y;\r\n    background-size: 100% auto;\r\n*/\r\n    box-shadow: 0 2px 6px 0 rgba(0, 0, 0, .5);\r\n/*\r\n    overflow: hidden;\r\n    transform: scale3d(0, 0, 0);\r\n    opacity: 0;\r\n    transition: opacity 300ms ease-out, transform 0ms linear 300ms;\r\n*/\r\n}\r\n.whatsappme__box img{\r\n   height: 40px;\r\n      margin: 10px;\r\n}\r\n.whatsapp-text {\r\n        \r\n    \r\n    position: absolute;\r\n    top: 18px;\r\n    right: 284px;\r\n    width: 34px;\r\n    height: 34px;\r\n    /* border-radius: 50%; */\r\n    /* background: #000; */\r\n    color: #fff;\r\n    line-height: 25px;\r\n    font-size: 23px;\r\n    text-align: center;\r\n    opacity: 0.4;\r\n\r\n        \r\n    }\r\n.whatsappme__header {\r\n    height: 70px;\r\n    padding: 0 26px;\r\n    background-color: #2e8c7d;\r\n    color: rgba(255, 255, 255, .5);\r\n    border-radius: 33px 33px 0 0;\r\n}\r\n.whatsappme__close {\r\n    position: absolute;\r\n    top: 18px;\r\n    right: 24px;\r\n    width: 34px;\r\n    height: 34px;\r\n    border-radius: 50%;\r\n    background: #000;\r\n    color: #fff;\r\n    line-height: 34px;\r\n    font-size: 25px;\r\n    text-align: center;\r\n    opacity: .4;\r\n    cursor: pointer;\r\n    transition: opacity 300ms ease-out;\r\n}\r\n.whatsappme__message {\r\n    position: relative;\r\n    min-height: 80px;\r\n    padding: 20px 22px;\r\n    margin: 34px 26px;\r\n    border-radius: 32px;\r\n    background-color: #fff;\r\n    color: #4A4A4A;\r\n    box-shadow: 0 1px 2px 0 rgba(0, 0, 0, 0.3);\r\n}\r\n.whatsappme__message::before {\r\n/*\r\n    content: '';\r\n    display: block;\r\n    position: absolute;\r\n    bottom: 30px;\r\n    left: -18px;\r\n    width: 18px;\r\n    height: 18px;\r\n    background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADcAAAA1CAYAAADlE3NNAAAEr0lEQ…00059JUY3ZHFm8k1lT0cGfnJw8c0ojepWFzd6CMpnM3y8AJPEkZ9khO4IAAAAASUVORK5CYII=);\r\n    background-size: 100%;\r\n*/\r\n    \r\n    content:\"\";\r\n   position: absolute;\r\n      right: 100%;\r\n    top: 70px;\r\n    width: 0;\r\n    height: 0;\r\n    border-top: 7px solid transparent;\r\n    border-right: 22px solid #fff;\r\n    border-bottom: 4px solid transparent;\r\n    \r\n}\r\n"

/***/ }),

/***/ "./src/app/header/header.component.html":
/*!**********************************************!*\
  !*** ./src/app/header/header.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "        \r\n<header id=\"header \" class=\"tt-nav \" >\r\n\r\n        <div class=\"whatsappme__button\">\r\n                <a href=\"https://api.whatsapp.com/send?phone=15105853238\">\r\n               <img src=\"assets/img/whatsapp/if_whatsapp_287520_bigperl.svg\" alt=\"Kiwi standing on oval\">   \r\n        </a>\r\n            </div>\r\n        \r\n            <div class=\"whatsappme__box\">\r\n                <header class=\"whatsappme__header\">\r\n                    <img src=\"assets/img/whatsapp/whatsappicon_bigperl.png\">\r\n                    <div class=\"whatsapp-text\">whatsapp\r\n                    </div>\r\n        \r\n                    <div class=\"whatsappme__close\">×</div>\r\n                </header>\r\n                <div class=\"whatsappme__message\">Hello,<br>Welcome to Bigperl solution Pvt Ltd<br><br>How Can I Help You?</div>\r\n            </div>\r\n        <div class=\"top-bar dark-bg lighten-2 visible-md visible-lg\">\r\n            <div class=\"container-fluid\">\r\n                <div class=\"row\">\r\n                    <!-- Social Icon -->\r\n                    <div class=\"col-md-6\">\r\n                        <!-- Social Icon -->\r\n                        <ul class=\"list-inline social-top tt-animate btt\">\r\n                            <li>\r\n                                <a href=\"https://www.facebook.com/BigperlSolutions/\" target=\"_blank\">\r\n                                    <i class=\"fa fa-facebook\"></i>\r\n                                </a>\r\n                            </li>\r\n                            <li>\r\n                                <a href=\"https://twitter.com/BigPerl\" target=\"_blank\">\r\n                                    <i class=\"fa fa-twitter\"></i>\r\n                                </a>\r\n                            </li>\r\n                            <li>\r\n                                <a href=\"https://www.linkedin.com/company/bigperl-solutions-pvt-ltd-?trk=top_nav_home\" target=\"_blank\">\r\n                                    <i class=\"fa fa-linkedin\"></i>\r\n                                </a>\r\n                            </li>\r\n                        </ul>\r\n                    </div>\r\n                    <div class=\"col-md-6 text-right\">\r\n                        <ul class=\"topbar-cta no-margin\">\r\n                            <li class=\"mr-20\">\r\n                                <a href=\"mailto:info@bigperl.com\">\r\n                                    <i class=\"material-icons mr-10\">comment</i>info@bigperl.com\r\n                                </a>\r\n                            </li>\r\n                        </ul>\r\n                    </div>\r\n                </div>\r\n                <!-- /.row -->\r\n            </div>\r\n            <!-- /.container -->\r\n        </div>\r\n        <!-- /.top-bar -->\r\n        <div class=\"header-sticky light-header \">\r\n            <div class=\"container-fluid\">\r\n                <div id=\"materialize-menu\" class=\"menuzord menuzord-responsive\">\r\n                    <!--logo start-->\r\n                    <a href=\"index\" class=\"logo-brand\">\r\n                        <img src=\"assets/img/seo/logo_bigperl.png\" alt=\"logo_bigperl\" >\r\n                        </a>\r\n                        <!--logo end-->\r\n                        <!-- menu start-->\r\n                        <ul class=\"menuzord-menu pull-right  menuzord-indented\">\r\n                            <li>\r\n                                <a routerLink=\"/home\" class=\"cvcv\">  Home</a>\r\n                            </li>\r\n                            <li>\r\n                                <a href=\"javascript:void(0)\">About Us</a>\r\n                                <ul class=\"dropdown\">\r\n                                    <li>\r\n                                        <a routerLink=\"/our-clients\">Our Clients</a>\r\n                                    </li>\r\n                                    <li>\r\n                                        <a routerLink=\"/success-stories\">Success Stories</a>\r\n                                    </li>\r\n                                    <li>\r\n                                        <a routerLink=\"/blogs\">Blogs</a>\r\n                                    </li>\r\n                                    <li>\r\n                                        <a href=\"#\">Recongnition</a>\r\n                                    </li>\r\n                                    <li>\r\n                                        <a href=\"#\">News Room</a>\r\n                                    </li>\r\n                                </ul>\r\n                            </li>\r\n                            <li>\r\n                                <a >Industry</a>\r\n                                <ul class=\"dropdown kkll\" >\r\n                                    <div class=\"row container-fluid wideHeader\">\r\n                                        <div class=\"col-md-4\">\r\n                                            <li>\r\n                                                <ul class=\"dropdown2\" >\r\n                                                    <li>\r\n                                                        <a routerLink=\"/automative\">Automative</a>\r\n                                                    </li>\r\n                                                    <li>\r\n                                                        <a routerLink=\"/digital-marketing\">Digital Marketing</a>\r\n                                                    </li>\r\n                                                    <li>\r\n                                                        <a routerLink=\"/hospitality\">Hospitality</a>\r\n                                                    </li>\r\n                                                    <li>\r\n                                                        <a href=\"media-entertainment\">Media & Entertainment</a>\r\n                                                    </li>\r\n                                                    <li>\r\n                                                        <a routerLink=\"/real-estate\">Real Estate</a>\r\n                                                    </li>\r\n                                                    <li>\r\n                                                        <a routerLink=\"/social-computing\">Social Computing</a>\r\n                                                    </li>\r\n                                                </ul >\r\n                                            </li>\r\n                                        </div>\r\n                                        <div class=\"col-md-4\">\r\n                                            <li>\r\n                                                <ul class=\"dropdown2\" >\r\n                                                    <li>\r\n                                                        <a routerLink=\"/banking-&-financial\">Banking & Financial</a>\r\n                                                    </li>\r\n                                                    <li>\r\n                                                        <a routerLink=\"/education\">Education</a>\r\n                                                    </li>\r\n                                                    <li>\r\n                                                        <a routerLink=\"/it\">IT</a>\r\n                                                    </li>\r\n                                                    <li>\r\n                                                        <a routerLink=\"/manufacturing\">Manufacturing</a>\r\n                                                    </li>\r\n                                                    <li>\r\n                                                        <a routerLink=\"/telecom\">Telecom</a>\r\n                                                    </li>\r\n                                                </ul >\r\n                                            </li>\r\n                                        </div>\r\n                                        <div class=\"col-md-4\">\r\n                                            <li>\r\n                                                <ul class=\"dropdown2\" >\r\n                                                    <li>\r\n                                                        <a routerLink=\"/consumer-electronics\">Consumer Electronics</a>\r\n                                                    </li>\r\n                                                    <li>\r\n                                                        <a routerLink=\"/government\">Government</a>\r\n                                                    </li>\r\n                                                    <li>\r\n                                                        <a routerLink=\"/life-science-&-healthcare\">Life Science & Healthcare</a>\r\n                                                    </li>\r\n                                                    <li>\r\n                                                        <a routerLink=\"/retail-and-consumer\">Retail and consumer</a>\r\n                                                    </li>\r\n                                                    <li>\r\n                                                        <a routerLink=\"/travel-transport-&-log\">TravelTransport & Log</a>\r\n                                                    </li>\r\n                                                </ul >\r\n                                            </li>\r\n                                        </div>\r\n                                    </div>\r\n                                </ul>\r\n                            </li>\r\n                            <li>\r\n                                <a >Services</a>\r\n                                <ul class=\"dropdown kkll\" >\r\n                                    <div class=\"row container-fluid wideHeader\">\r\n                                        <div class=\"col-md-3\">\r\n                                            <li>\r\n                                                <h2>Mobile App Development</h2>\r\n                                                <ul class=\"dropdown2\" >\r\n                                                    <li>\r\n                                                        <a id=\"android\" routerLink=\"/android-app-development\">Android App Development</a>\r\n                                                    </li>\r\n                                                    <li>\r\n                                                        <a routerLink=\"/iphone-app-development\">IPhone App Development</a>\r\n                                                    </li>\r\n                                                    <li>\r\n                                                        <a  routerLink=\"/ipad-app-development\">Ipad App Development</a>\r\n                                                    </li>\r\n                                                    <li>\r\n                                                        <a routerLink=\"/windows-app-development\">Windows App Development</a>\r\n                                                    </li>\r\n                                                    <li>\r\n                                                        <a routerLink=\"/blackberry-app-development\">BlackBerry App Development</a>\r\n                                                    </li>\r\n                                                    <li>\r\n                                                        <a routerLink=\"/hybrid-app-development\">Hybrid App Development</a>\r\n                                                    </li>\r\n                                                    <li>\r\n                                                        <a routerLink=\"/wearable-devices\">Wearable Devices</a>\r\n                                                    </li>\r\n                                                    <li>\r\n                                                        <a routerLink=\"/internet-of-things\">Internet Of Things (IOT)</a>\r\n                                                    </li>\r\n                                                </ul >\r\n                                            </li>\r\n                                        </div>\r\n                                        <div class=\"col-md-3\">\r\n                                            <li>\r\n                                                <h2>Website Development</h2>\r\n                                                <ul class=\"dropdown2\" >\r\n                                                    <li>\r\n                                                        <a routerLink=\"/php-development\">PHP Development</a>\r\n                                                    </li>\r\n                                                    <li>\r\n                                                        <a routerLink=\"/ruby-on-rails-development\">ROR Development</a>\r\n                                                    </li>\r\n                                                    <li>\r\n                                                        <a routerLink=\"/java-&-j2ee-development\">Java & J2EE Development</a>\r\n                                                    </li>\r\n                                                    <li>\r\n                                                        <a routerLink=\"/responsive-web-development\">Responsive Web Development</a>\r\n                                                    </li>\r\n                                                    <li>\r\n                                                        <a routerLink=\"/python-web-development\">Python Development</a>\r\n                                                    </li>\r\n                                                    <li>\r\n                                                        <a routerLink=\"/adf-and-web-development\">ADF and Web Development</a>\r\n                                                    </li>\r\n                                                    <li>\r\n                                                        <a routerLink=\"/facebook-app-development\">Facebook App Development</a>\r\n                                                    </li>\r\n                                                </ul >\r\n                                            </li>\r\n                                        </div>\r\n                                        <div class=\"col-md-3\">\r\n                                            <li>\r\n                                                <h2>Open Source Customization</h2>\r\n                                                <ul class=\"dropdown2\" >\r\n                                                    <li>\r\n                                                        <a routerLink=\"/codeigniter-development\">Codeigniter Development</a>\r\n                                                    </li>\r\n                                                    <li>\r\n                                                        <a routerLink=\"/cake-php-development\">Cakephp Development</a>\r\n                                                    </li>\r\n                                                    <li>\r\n                                                        <a routerLink=\"/laravel-development\">Laravel Development</a>\r\n                                                    </li>\r\n                                                    <li>\r\n                                                        <a routerLink=\"/cmc-development\">CMS Development</a>\r\n                                                    </li>\r\n                                                    <li>\r\n                                                        <a routerLink=\"/word-press-development\">Wordpress Development</a>\r\n                                                    </li>\r\n                                                    <li>\r\n                                                        <a routerLink=\"/drupal-development\">Drupal Development</a>\r\n                                                    </li>\r\n                                                    <li>\r\n                                                        <a routerLink=\"/joomla-development\">Joomla Development</a>\r\n                                                    </li>\r\n                                                    <li>\r\n                                                        <a routerLink=\"/e-commerce-development\">E-Commerce Development</a>\r\n                                                    </li>\r\n                                                    <li>\r\n                                                        <a routerLink=\"/magento-development\">Magento Development</a>\r\n                                                    </li>\r\n                                                    <li>\r\n                                                        <a routerLink=\"/xcart-development\">X-Cart Development</a>\r\n                                                    </li>\r\n                                                </ul >\r\n                                            </li>\r\n                                        </div>\r\n                                        <div class=\"col-md-3\">\r\n                                            <li>\r\n                                                <h2>Specialized Home</h2>\r\n                                                <ul class=\"dropdown2\" >\r\n                                                    <li>\r\n                                                        <a routerLink=\"/web-application-development\">Web Application Developer</a>\r\n                                                    </li>\r\n                                                    <li>\r\n                                                        <a routerLink=\"/php-developer\">PHP Developer</a>\r\n                                                    </li>\r\n                                                    <li>\r\n                                                        <a routerLink=\"/ecommerce-developer\">eCommerce Developer</a>\r\n                                                    </li>\r\n                                                    <li>\r\n                                                        <a routerLink=\"/magento-developer\">Magento Developer</a>\r\n                                                    </li>\r\n                                                    <li>\r\n                                                        <a routerLink=\"/android-developer\">Android Developer</a>\r\n                                                    </li>\r\n                                                    <li>\r\n                                                        <a routerLink=\"/ios-developer\">IOS Developer</a>\r\n                                                    </li>\r\n                                                    <li>\r\n                                                        <a routerLink=\"/phone-gap-development\">PhoneGap Developer</a>\r\n                                                    </li>\r\n                                                    <li>\r\n                                                        <a routerLink=\"/python-developer\">Python Developer</a>\r\n                                                    </li>\r\n                                                    <li>\r\n                                                        <a routerLink=\"/ruby-on-rails-developer\">ROR Developer</a>\r\n                                                    </li>\r\n                                                    <li>\r\n                                                        <a routerLink=\"/adf-developer\">ADF Developer</a>\r\n                                                    </li>\r\n                                                </ul >\r\n                                            </li>\r\n                                        </div>\r\n                                    </div>\r\n                                </ul>\r\n                            </li>\r\n                            <li>\r\n                                <a >Products</a>\r\n                                <ul class=\"dropdown kkll\" >\r\n                                    <div class=\"row container-fluid wideHeader\">\r\n                                        <div class=\"col-md-3\">\r\n                                            <li>\r\n                                                <h2>GPS & Mobility</h2>\r\n                                                <ul class=\"dropdown2\" >\r\n                                                    <li>\r\n                                                        <a routerLink=\"/gps-mobile-traking\">GPS Mobile Tracking</a>\r\n                                                    </li>\r\n                                                    <li>\r\n                                                        <a routerLink=\"/gps-vehicle-traking\">GPS Vehicle Tracking</a>\r\n                                                    </li>\r\n                                                    <li>\r\n                                                        <a routerLink=\"/gps-wearables\">GPS Wearables</a>\r\n                                                    </li>\r\n                                                    <li>\r\n                                                        <a routerLink=\"/field-force-automation\">Field Force Automation (FFA)</a>\r\n                                                    </li>\r\n                                                    <li>\r\n                                                        <a routerLink=\"/gps-assets-traking\">GPS Assets Tracking</a>\r\n                                                    </li>\r\n                                                </ul >\r\n                                            </li>\r\n                                        </div>\r\n                                        <div class=\"col-md-3\">\r\n                                            <li>\r\n                                                <h2>Hospitality Automation</h2>\r\n                                                <ul class=\"dropdown2\" >\r\n                                                    <li>\r\n                                                        <a routerLink=\"/hotel-booking-engine\">Hotel Booking Engine</a>\r\n                                                    </li>\r\n                                                    <li>\r\n                                                        <a routerLink=\"/hotel-management-system\">Hotel Management System</a>\r\n                                                    </li>\r\n                                                    <li>\r\n                                                        <a  routerLink=\"/hotel-inventory-management\">Hotel Inventory Management</a>\r\n                                                    </li>\r\n                                                    <li>\r\n                                                        <a  routerLink=\"/travel-booking-e-commerce\">Travel Booking E-Commerce</a>\r\n                                                    </li>\r\n                                                    <li>\r\n                                                        <a routerLink=\"/food-on-wheel\">Food On Wheel (FOW) Ordering</a>\r\n                                                    </li>\r\n                                                </ul >\r\n                                            </li>\r\n                                        </div>\r\n                                        <div class=\"col-md-3\">\r\n                                            <li>\r\n                                                <h2>Architectural IT</h2>\r\n                                                <ul class=\"dropdown2\" >\r\n                                                    <li>\r\n                                                        <a routerLink=\"/architechtural-network\">Architectural Network</a>\r\n                                                    </li>\r\n                                                    <li>\r\n                                                        <a routerLink=\"/construction-suppliers\">Construction Supplier N/W</a>\r\n                                                    </li>\r\n                                                </ul >\r\n                                            </li>\r\n                                        </div>\r\n                                        <div class=\"col-md-3\">\r\n                                            <li>\r\n                                                <h2>ERP & CRM</h2>\r\n                                                <ul class=\"dropdown2\" >\r\n                                                    <li>\r\n                                                        <a routerLink=\"/enterprice-crm-sales\">Enterprise CRM - Sales</a>\r\n                                                    </li>\r\n                                                    <li>\r\n                                                        <a routerLink=\"/enterprice-crm-support\">Enterprise CRM - Support</a>\r\n                                                    </li>\r\n                                                    <li>\r\n                                                        <a routerLink=\"/sass-based-erp\">SaaS Based ERP</a>\r\n                                                    </li>\r\n                                                    <li>\r\n                                                        <a routerLink=\"/sass-based-payroll\">SaaS Based Payroll</a>\r\n                                                    </li>\r\n                                                </ul >\r\n                                            </li>\r\n                                        </div>\r\n                                    </div>\r\n                                </ul>\r\n                            </li>\r\n                            <li >\r\n                                <a routerLink=\"/portfolio\" class=\"bbb\">Portfolio</a>\r\n                            </li>\r\n                            <li>\r\n                                <a routerLink=\"/careers\">Careers</a>\r\n                            </li>\r\n                            <li>\r\n                                <a routerLink=\"/contact-us\">Contact</a>\r\n                            </li>\r\n                        </ul>\r\n                        <!-- menu end-->\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </header>\r\n        <!--header end-->\r\n    "

/***/ }),

/***/ "./src/app/header/header.component.ts":
/*!********************************************!*\
  !*** ./src/app/header/header.component.ts ***!
  \********************************************/
/*! exports provided: HeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderComponent", function() { return HeaderComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var HeaderComponent = /** @class */ (function () {
    function HeaderComponent() {
    }
    HeaderComponent.prototype.ngOnInit = function () {
        var main = function () { $('.dropdown').click(function () { $(this).fadeOut(); }); };
        $(document).ready(main);
        (function ($) { $('div.whatsappme__button').hover(function () { $('div.whatsappme__box').show(); }); $('div.whatsappme__close').click(function () { $('div.whatsappme__box').hide(); }); }(jQuery));
    };
    HeaderComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-header',
            template: __webpack_require__(/*! ./header.component.html */ "./src/app/header/header.component.html"),
            styles: [__webpack_require__(/*! ./header.component.css */ "./src/app/header/header.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], HeaderComponent);
    return HeaderComponent;
}());



/***/ }),

/***/ "./src/app/home/home.component.css":
/*!*****************************************!*\
  !*** ./src/app/home/home.component.css ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/home/home.component.html":
/*!******************************************!*\
  !*** ./src/app/home/home.component.html ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n        <!-- start revolution slider 5.0 -->\r\n        <section class=\"rev_slider_wrapper mt-xs-45\">\r\n          <div class=\"rev_slider materialize-slider\" >\r\n           <ul>\r\n \r\n             <!-- slide 1 start -->\r\n             <li data-transition=\"fade\" data-slotamount=\"default\" data-easein=\"Power4.easeInOut\" data-easeout=\"Power4.easeInOut\" data-masterspeed=\"2000\" data-thumb=\"assets/img/seo/slider/slider-bg-1.jpg\"  data-rotate=\"0\"  data-fstransition=\"fade\" data-fsmasterspeed=\"1500\" data-fsslotamount=\"7\" data-saveperformance=\"off\"  data-title=\"materialize Material\" data-description=\"\">\r\n \r\n                 <!-- MAIN IMAGE -->\r\n                 <img src=\"assets/img/seo/slider/mobile_application_development_slider_1_bigperl.jpg\"  alt=\"mobile_application_development_slider_1_bigperl\"  data-bgposition=\"center center\" data-bgfit=\"cover\" data-bgrepeat=\"no-repeat\" data-bgparallax=\"10\" class=\"rev-slidebg\" data-no-retina>\r\n \r\n \r\n                 <!-- LAYER NR. 1 -->\r\n                 <div class=\"tp-caption NotGeneric-Title tp-resizeme\"\r\n                     data-x=\"['left','left','center','center']\" data-hoffset=\"['20','20','0','0']\"\r\n                     data-y=\"['center','center','top','top']\" data-voffset=\"['-100','-100','50','50']\"\r\n                     data-fontsize=\"['70','60','50','45']\"\r\n                     data-lineheight=\"['70','60','40','40']\"\r\n                     data-width=\"none\"\r\n                     data-height=\"none\"\r\n                     data-whitespace=\"nowrap\"\r\n                     data-transform_idle=\"o:1;\"\r\n                     data-transform_in=\"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:600;e:Power4.easeInOut;\"\r\n                     data-transform_out=\"y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;\"\r\n                     data-mask_in=\"x:0px;y:[100%];s:inherit;e:inherit;\"\r\n                     data-mask_out=\"x:inherit;y:inherit;s:inherit;e:inherit;\"\r\n                     data-start=\"800\"\r\n                     data-splitin=\"none\"\r\n                     data-splitout=\"none\"\r\n                     data-responsive_offset=\"on\"\r\n                     style=\"z-index: 5; color: #fff;background-color: rgba(0,0,0,0.6);border-radius: 5px;padding:5px;margin-top: 120px;white-space: nowrap;\">Mobile Application<br> Development\r\n                 </div>\r\n \r\n                 <!-- LAYER NR. 2 -->\r\n                 <div class=\"tp-caption tp-resizeme rev-subheading\"\r\n                     data-x=\"['left','left','center','center']\" data-hoffset=\"['20','20','0','0']\"\r\n                     data-y=\"['center','center','top','top']\" data-voffset=\"['0','0','140','140']\"\r\n                     data-whitespace=\"nowrap\"\r\n                     data-transform_idle=\"o:1;\"\r\n \r\n                     data-transform_in=\"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:600;e:Power4.easeInOut;\"\r\n                     data-transform_out=\"y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;\"\r\n                     data-mask_in=\"x:0px;y:[100%];s:inherit;e:inherit;\"\r\n                     data-mask_out=\"x:inherit;y:inherit;s:inherit;e:inherit;\"\r\n                     data-start=\"1000\"\r\n                     data-splitin=\"none\"\r\n                     data-splitout=\"none\"\r\n                     data-responsive_offset=\"on\"\r\n                     style=\"z-index: 6; color: #fff; background-color: rgba(0,0,0,0.6);border-radius: 5px;padding:5px 90px 5px 5px;margin-top: 140px; white-space: nowrap;\"><b><strong>iOS Application Development | Android Application Development | Windows <br> Application Development | Wearable Devices | Internet Of Things</strong></b>\r\n                 </div>\r\n \r\n                 <!-- LAYER NR. 3 -->\r\n                 <div class=\"tp-caption tp-resizeme\"\r\n                     data-x=\"['left','left','center','center']\" data-hoffset=\"['20','20','0','0']\"\r\n                     data-y=\"['middle','middle','top','top']\" data-voffset=\"['100','100','220','220']\"\r\n                     data-width=\"none\"\r\n                     data-height=\"none\"\r\n                     data-whitespace=\"nowrap\"\r\n                     data-transform_idle=\"o:1;\"\r\n                     data-style_hover=\"cursor:default;\"\r\n                     data-transform_in=\"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:600;e:Power4.easeInOut;\"\r\n                     data-transform_out=\"y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;\"\r\n                     data-mask_in=\"x:0px;y:[100%];s:inherit;e:inherit;\"\r\n                     data-mask_out=\"x:inherit;y:inherit;s:inherit;e:inherit;\"\r\n                     data-start=\"1200\"\r\n                     data-splitin=\"none\"\r\n                     data-splitout=\"none\"\r\n                     data-responsive_offset=\"on\"\r\n                     style=\"z-index: 7;background-color: rgba(0,0,0,0.6); border-radius: 5px;padding:5px; white-space: nowrap;margin-top: 60px;\">\r\n                     <h3 style=\"color: #fff;\"><b>Software Development and onsite consulting</b></h3>\r\n                 </div>\r\n \r\n                 <!-- LAYER NR. 4 -->\r\n                 <div class=\"tp-caption tp-resizeme\"\r\n                     data-x=\"['right','right','center','center']\" data-hoffset=\"['30','30','0','0']\"\r\n                     data-y=\"['middle','middle','bottom','bottom']\" data-voffset=\"['30','30','50','50']\"\r\n                     data-transform_idle=\"o:1;\"\r\n                     data-transform_in=\"y:30px;opacity:0;s:600;e:Power2.easeOut;\"\r\n                     data-transform_out=\"opacity:0;s:1000;e:Power4.easeIn;s:1500;e:Power4.easeIn;\"\r\n                     data-start=\"1200\"\r\n                     style=\"z-index: 8;margin-right: 60px;\">\r\n                         <div><img src=\"assets/img/seo/slider/mobile_application_development_monitor_bigperl.png\" alt=\"mobile_application_development_monitor_bigperl\">\r\n                         </div>\r\n                 </div>\r\n \r\n                 <!-- LAYER NR. 5 -->\r\n                 <div class=\"tp-caption tp-resizeme rs-parallaxlevel-2\"\r\n                     data-x=\"['right','right','center','center']\" data-hoffset=\"['300','300','-180','-180']\"\r\n                     data-y=\"['middle','middle','middle','middle']\" data-voffset=\"['-160','-160','0','0']\"\r\n                     data-transform_idle=\"o:1;\"\r\n                     data-transform_in=\"y:30px;opacity:0;s:600;e:Power2.easeOut;\"\r\n                     data-transform_out=\"opacity:0;s:1000;e:Power4.easeIn;s:1500;e:Power4.easeIn;\"\r\n                     data-start=\"1500\"\r\n                     style=\"z-index: 9;margin-top: 180px;\">\r\n                         <div><img src=\"assets/img/seo/slider/mobile_application_development_envelope_bigperl.png\" alt=\"mobile_application_development_envelope_bigperl\">\r\n                         </div>\r\n                 </div>\r\n \r\n                 <!-- LAYER NR. 6 -->\r\n                 <div class=\"tp-caption tp-resizeme rs-parallaxlevel-2\"\r\n                     data-x=\"['right','right','center','center']\" data-hoffset=\"['160','160','0','0']\"\r\n                     data-y=\"['middle','middle','middle','middle']\" data-voffset=\"['-220','-220','-60','-60']\"\r\n                     data-transform_idle=\"o:1;\"\r\n                     data-transform_in=\"y:30px;opacity:0;s:600;e:Power2.easeOut;\"\r\n                     data-transform_out=\"opacity:0;s:1000;e:Power4.easeIn;s:1500;e:Power4.easeIn;\"\r\n                     data-start=\"1700\"\r\n                     style=\"z-index: 10;margin-top: 240px;\">\r\n                         <div><img src=\"assets/img/seo/slider/mobile_application_development_bar_bigperl.png\" alt=\"mobile_application_development_bar_bigperl\" >\r\n                         </div>\r\n                 </div>\r\n \r\n                 <!-- LAYER NR. 7 -->\r\n                 <div class=\"tp-caption tp-resizeme rs-parallaxlevel-2\"\r\n                     data-x=\"['right','right','center','center']\" data-hoffset=\"['20','20','180','180']\"\r\n                     data-y=\"['middle','middle','middle','middle']\" data-voffset=\"['-170','-170','0','0']\"\r\n                     data-transform_idle=\"o:1;\"\r\n                     data-transform_in=\"y:30px;opacity:0;s:600;e:Power2.easeOut;\"\r\n                     data-transform_out=\"opacity:0;s:1000;e:Power4.easeIn;s:1500;e:Power4.easeIn;\"\r\n                     data-start=\"1900\"\r\n                     style=\"z-index: 11;margin-top:180px;\">\r\n                         <div><img src=\"assets/img/seo/slider/mobile_application_development_chart_bigperl.png\" alt=\"mobile_application_development_chart_bigperl\" >\r\n                         </div>\r\n                 </div>\r\n \r\n                 <!-- LAYER NR. 8 -->\r\n                 <div class=\"tp-caption tp-resizeme rs-parallaxlevel-3\"\r\n                     data-x=\"['right','right','center','center']\" data-hoffset=\"['200','200','-100','-100']\"\r\n                     data-y=\"['middle','middle','bottom','bottom']\" data-voffset=\"['80','80','30','30']\"\r\n                     data-transform_idle=\"o:1;\"\r\n                     data-transform_in=\"y:30px;opacity:0;s:600;e:Power2.easeOut;\"\r\n                     data-transform_out=\"opacity:0;s:1000;e:Power4.easeIn;s:1500;e:Power4.easeIn;\"\r\n                     data-start=\"2100\"\r\n                     style=\"z-index: 12;margin-top: 80px;margin-right: -80px;\">\r\n                         <div class=\"rs-pendulum\" data-easing=\"Power4.easeInOut\" data-startdeg=\"0\" data-enddeg=\"15\"><img src=\"assets/img/seo/slider/mobile_application_development_seo_mag_bigperl.png\" alt=\"mobile_application_development_seo_mag_bigperl\" >\r\n                         </div>\r\n                 </div>\r\n \r\n             </li>\r\n             <!-- slide 1 end -->\r\n \r\n \r\n             <!-- slide 2 start -->\r\n             <li data-transition=\"fade\" data-slotamount=\"default\" data-easein=\"Power4.easeInOut\" data-easeout=\"Power4.easeInOut\" data-masterspeed=\"2000\" data-thumb=\"assets/img/seo/slider/slider-bg-2.png\"  data-rotate=\"0\"  data-fstransition=\"fade\" data-fsmasterspeed=\"1500\" data-fsslotamount=\"7\" data-saveperformance=\"off\"  data-title=\"materialize Material\" data-description=\"\">\r\n \r\n                 <!-- MAIN IMAGE -->\r\n                 <img src=\"assets/img/seo/slider/mobile_application_development_slider_2_bigperl.png\"  alt=\"mobile_application_development_slider_2_bigperl\"  data-bgposition=\"center center\" data-bgfit=\"cover\" data-bgrepeat=\"no-repeat\" data-bgparallax=\"10\" class=\"rev-slidebg\" data-no-retina>\r\n \r\n \r\n                 <!-- LAYER NR. 1 -->\r\n                 <div class=\"tp-caption NotGeneric-Title tp-resizeme\"\r\n                     data-x=\"['left','left','center','center']\" data-hoffset=\"['20','20','0','0']\"\r\n                     data-y=\"['center','center','top','top']\" data-voffset=\"['-100','-100','50','50']\"\r\n                     data-fontsize=\"['70','60','50','45']\"\r\n                     data-lineheight=\"['70','60','40','40']\"\r\n                     data-width=\"none\"\r\n                     data-height=\"none\"\r\n                     data-whitespace=\"nowrap\"\r\n                     data-transform_idle=\"o:1;\"\r\n                     data-transform_in=\"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:600;e:Power4.easeInOut;\"\r\n                     data-transform_out=\"y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;\"\r\n                     data-mask_in=\"x:0px;y:[100%];s:inherit;e:inherit;\"\r\n                     data-mask_out=\"x:inherit;y:inherit;s:inherit;e:inherit;\"\r\n                     data-start=\"800\"\r\n                     data-splitin=\"none\"\r\n                     data-splitout=\"none\"\r\n                     data-responsive_offset=\"on\"\r\n                     style=\"z-index: 5; color: #fff;background-color: rgba(0,0,0,0.6);border-radius: 5px;padding:5px;margin-top: 120px; white-space: nowrap;\">Responsive web <br> development\r\n                 </div>\r\n \r\n                 <!-- LAYER NR. 2 -->\r\n                 <div class=\"tp-caption tp-resizeme rev-subheading\"\r\n                     data-x=\"['left','left','center','center']\" data-hoffset=\"['20','20','0','0']\"\r\n                     data-y=\"['center','center','top','top']\" data-voffset=\"['0','0','140','140']\"\r\n                     data-whitespace=\"nowrap\"\r\n                     data-transform_idle=\"o:1;\"\r\n \r\n                     data-transform_in=\"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:600;e:Power4.easeInOut;\"\r\n                     data-transform_out=\"y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;\"\r\n                     data-mask_in=\"x:0px;y:[100%];s:inherit;e:inherit;\"\r\n                     data-mask_out=\"x:inherit;y:inherit;s:inherit;e:inherit;\"\r\n                     data-start=\"1000\"\r\n                     data-splitin=\"none\"\r\n                     data-splitout=\"none\"\r\n                     data-responsive_offset=\"on\"\r\n                     style=\"z-index: 6; color: #fff; white-space: nowrap;border-radius: 5px;padding:5px 10px 5px 5px;background-color: rgba(0,0,0,0.6);margin-top: 120px;\">\r\n                     <b>Parallax Website Templates | E-Commerce Websites | Web 2.0 Development</b>\r\n                 </div>\r\n \r\n                 <!-- LAYER NR. 3 -->\r\n                 <div class=\"tp-caption tp-resizeme\"\r\n                     data-x=\"['left','left','center','center']\" data-hoffset=\"['20','20','0','0']\"\r\n                     data-y=\"['middle','middle','top','top']\" data-voffset=\"['100','100','220','220']\"\r\n                     data-width=\"none\"\r\n                     data-height=\"none\"\r\n                     data-whitespace=\"nowrap\"\r\n                     data-transform_idle=\"o:1;\"\r\n                     data-style_hover=\"cursor:default;\"\r\n                     data-transform_in=\"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:600;e:Power4.easeInOut;\"\r\n                     data-transform_out=\"y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;\"\r\n                     data-mask_in=\"x:0px;y:[100%];s:inherit;e:inherit;\"\r\n                     data-mask_out=\"x:inherit;y:inherit;s:inherit;e:inherit;\"\r\n                     data-start=\"1200\"\r\n                     data-splitin=\"none\"\r\n                     data-splitout=\"none\"\r\n                     data-responsive_offset=\"on\"\r\n                     style=\"z-index: 7; white-space: nowrap;margin-top: 50px;\">\r\n                 </div>\r\n \r\n                 <!-- LAYER NR. 4 -->\r\n                 <div class=\"tp-caption tp-resizeme\"\r\n                     data-basealign=\"slide\"\r\n                     data-x=\"['right','right','center','center']\" data-hoffset=\"['0','0','0','0']\"\r\n                     data-y=\"['bottom','bottom','bottom','bottom']\" data-voffset=\"['0','0','0','0']\"\r\n                     data-transform_idle=\"o:1;\"\r\n                     data-transform_in=\"y:30px;opacity:0;s:600;e:Power2.easeOut;\"\r\n                     data-transform_out=\"opacity:0;s:1000;e:Power4.easeIn;s:1500;e:Power4.easeIn;\"\r\n                     data-start=\"1500\"\r\n                     style=\"z-index: 8;margin-bottom: 30px;margin-right: 80px;\">\r\n                     <div>\r\n                         <img src=\"assets/img/seo/slider/mobile_application_development_graph1_bigperl.png\" alt=\"mobile_application_development_graph1_bigperl\">\r\n                     </div>\r\n                 </div>\r\n             </li>\r\n             <!-- slide 2 end -->\r\n             <!-- slide 3 start -->\r\n             <li data-transition=\"fade\" data-slotamount=\"default\" data-easein=\"Power4.easeInOut\" data-easeout=\"Power4.easeInOut\" data-masterspeed=\"2000\" data-thumb=\"assets/img/seo/slider/slider-bg-2.png\"  data-rotate=\"0\"  data-fstransition=\"fade\" data-fsmasterspeed=\"1500\" data-fsslotamount=\"7\" data-saveperformance=\"off\"  data-title=\"materialize Material\" data-description=\"\">\r\n \r\n                 <!-- MAIN IMAGE -->\r\n                 <img src=\"assets/img/seo/slider/mobile_application_development_slider_3_bigperl.png\"  alt=\"mobile_application_development_slider_3_bigperl\"  data-bgposition=\"center center\" data-bgfit=\"cover\" data-bgrepeat=\"no-repeat\" data-bgparallax=\"10\" class=\"rev-slidebg\" data-no-retina>\r\n \r\n \r\n                 <!-- LAYER NR. 1 -->\r\n                 <div class=\"tp-caption NotGeneric-Title tp-resizeme\"\r\n                     data-x=\"['left','left','center','center']\" data-hoffset=\"['20','20','0','0']\"\r\n                     data-y=\"['center','center','top','top']\" data-voffset=\"['-100','-100','50','50']\"\r\n                     data-fontsize=\"['70','60','50','45']\"\r\n                     data-lineheight=\"['70','60','40','40']\"\r\n                     data-width=\"none\"\r\n                     data-height=\"none\"\r\n                     data-whitespace=\"nowrap\"\r\n                     data-transform_idle=\"o:1;\"\r\n                     data-transform_in=\"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:600;e:Power4.easeInOut;\"\r\n                     data-transform_out=\"y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;\"\r\n                     data-mask_in=\"x:0px;y:[100%];s:inherit;e:inherit;\"\r\n                     data-mask_out=\"x:inherit;y:inherit;s:inherit;e:inherit;\"\r\n                     data-start=\"800\"\r\n                     data-splitin=\"none\"\r\n                     data-splitout=\"none\"\r\n                     data-responsive_offset=\"on\"\r\n                     style=\"z-index: 5; color: #fff;background-color: rgba(0,0,0,0.6);border-radius: 5px;padding:5px; white-space: nowrap;margin-top: 120px;\">Responsive web <br> development\r\n                 </div>\r\n \r\n                 <!-- LAYER NR. 2 -->\r\n                 <div class=\"tp-caption tp-resizeme rev-subheading\"\r\n                     data-x=\"['left','left','center','center']\" data-hoffset=\"['20','20','0','0']\"\r\n                     data-y=\"['center','center','top','top']\" data-voffset=\"['0','0','140','140']\"\r\n                     data-whitespace=\"nowrap\"\r\n                     data-transform_idle=\"o:1;\"\r\n \r\n                     data-transform_in=\"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:600;e:Power4.easeInOut;\"\r\n                     data-transform_out=\"y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;\"\r\n                     data-mask_in=\"x:0px;y:[100%];s:inherit;e:inherit;\"\r\n                     data-mask_out=\"x:inherit;y:inherit;s:inherit;e:inherit;\"\r\n                     data-start=\"1000\"\r\n                     data-splitin=\"none\"\r\n                     data-splitout=\"none\"\r\n                     data-responsive_offset=\"on\"\r\n                     style=\"z-index: 6; color: #fff; white-space: nowrap;border-radius: 5px;padding:5px 10px 5px 5px;background-color: rgba(0,0,0,0.6);margin-top: 120px;\"><b>Parallax Website Templates | E-Commerce Websites | Responsive Web Development</b>\r\n                 </div>\r\n \r\n                 <!-- LAYER NR. 3 -->\r\n                 <div class=\"tp-caption tp-resizeme\"\r\n                     data-x=\"['left','left','center','center']\" data-hoffset=\"['20','20','0','0']\"\r\n                     data-y=\"['middle','middle','top','top']\" data-voffset=\"['100','100','220','220']\"\r\n                     data-width=\"none\"\r\n                     data-height=\"none\"\r\n                     data-whitespace=\"nowrap\"\r\n                     data-transform_idle=\"o:1;\"\r\n                     data-style_hover=\"cursor:default;\"\r\n                     data-transform_in=\"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:600;e:Power4.easeInOut;\"\r\n                     data-transform_out=\"y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;\"\r\n                     data-mask_in=\"x:0px;y:[100%];s:inherit;e:inherit;\"\r\n                     data-mask_out=\"x:inherit;y:inherit;s:inherit;e:inherit;\"\r\n                     data-start=\"1200\"\r\n                     data-splitin=\"none\"\r\n                     data-splitout=\"none\"\r\n                     data-responsive_offset=\"on\"\r\n                     style=\"z-index: 7; white-space: nowrap;margin-top: 50px;\">\r\n                 </div>\r\n \r\n                 <!-- LAYER NR. 4 -->\r\n                 <div class=\"tp-caption tp-resizeme\"\r\n                     data-basealign=\"slide\"\r\n                     data-x=\"['right','right','center','center']\" data-hoffset=\"['0','0','0','0']\"\r\n                     data-y=\"['bottom','bottom','bottom','bottom']\" data-voffset=\"['0','0','0','0']\"\r\n                     data-transform_idle=\"o:1;\"\r\n                     data-transform_in=\"y:30px;opacity:0;s:600;e:Power2.easeOut;\"\r\n                     data-transform_out=\"opacity:0;s:1000;e:Power4.easeIn;s:1500;e:Power4.easeIn;\"\r\n                     data-start=\"1500\"\r\n                     style=\"z-index: 8;margin-bottom: 30px;margin-right: 80px;\">\r\n                     <div>\r\n                         <img src=\"assets/img/seo/slider/mobile_application_development_graph_bigperl.png\" alt=\"graph_bigperl\">\r\n                     </div>\r\n                 </div>\r\n             </li>\r\n             <!-- slide 3 end -->\r\n \r\n \r\n           </ul>\r\n          </div><!-- end revolution slider -->\r\n         </section><!-- end of slider wrapper -->\r\n \r\n         <section class=\"section-padding\">\r\n             <div class=\"container\">\r\n                 <div class=\"text-center mb-80 padding-top-20\">\r\n                     <h2 class=\"section-title text-uppercase\">What We Do</h2>\r\n                 </div>\r\n                 <div class=\"portfolio portfolio-with-title col-4 gutter\">\r\n                     <div class=\"portfolio-item\">\r\n                         <div class=\"featured-item seo-service\">\r\n                             <div class=\"icon\">\r\n                                 <img class=\"img-responsive\" src=\"assets/img/seo/ios_app_development_bigperl.jpg\" alt=\"ios_app_development_bigperl\">\r\n                             </div>\r\n                             <div class=\"desc\">\r\n                                 <h2 class=\"text-center\">IOS App<br>Development</h2>\r\n                                 <ul class=\"list-icon1 text-center\">\r\n                                     <li>iPhone App Development</li>\r\n                                     <li>iPad Apps Development</li>\r\n                                     <li>Wearable Apps</li>\r\n                                     <li>tvOS Apps</li>\r\n                                 </ul>\r\n                                 <div class=\"bg-overlay\"></div>\r\n                                 <p><a class=\"learn-more\" href=\"ios-application-development\">Learn More <i class=\"fa fa-long-arrow-right\"></i></a></p>\r\n                             </div>\r\n                         </div>\r\n                     </div><!-- /.portfolio-item -->\r\n                 </div>\r\n                 <div class=\"portfolio portfolio-with-title col-4 gutter\">\r\n                     <div class=\"portfolio-item\">\r\n                         <div class=\"featured-item seo-service\">\r\n                             <div class=\"icon\">\r\n                                 <img class=\"img-responsive\" src=\"assets/img/seo/android_app_development_bigperl.jpg\" alt=\"android_app_development_bigperl\">\r\n                             </div>\r\n                             <div class=\"desc\">\r\n                                 <h2 class=\"text-center\">Android App Development</h2>\r\n                                 <ul class=\"list-icon1 text-center\">\r\n                                     <li>Android Game Development</li>\r\n                                     <li>Wearable App development</li>\r\n                                     <li>Android Mobile Apps</li>\r\n                                     <li>Android Tab Apps</li>\r\n                                 </ul>\r\n                                 <div class=\"bg-overlay\"></div>\r\n                                 <p><a class=\"learn-more\" href=\"android-application-development\">Learn More <i class=\"fa fa-long-arrow-right\"></i></a></p>\r\n                             </div>\r\n                         </div>\r\n                     </div><!-- /.portfolio-item -->\r\n                 </div>\r\n                 <div class=\"portfolio portfolio-with-title col-4 gutter\">\r\n                     <div class=\"portfolio-item\">\r\n                         <div class=\"featured-item seo-service\">\r\n                             <div class=\"icon\">\r\n                                 <img class=\"img-responsive\" src=\"assets/img/seo/hybrid_app_development_bigperl.jpg\" alt=\"hybrid_app_development_bigperl\">\r\n                             </div>\r\n                             <div class=\"desc\">\r\n                                 <h2 class=\"text-center\">Hybrid App Development</h2>\r\n                                 <ul class=\"list-icon1 text-center\">\r\n                                     <li>PhoneGap App Development</li>\r\n                                     <li>IONIC Apps Development</li>\r\n                                     <li>Cocoon Framework</li>\r\n                                     <li>Mobile Angular UI</li>\r\n                                 </ul>\r\n                                 <div class=\"bg-overlay\"></div>\r\n                                 <p><a class=\"learn-more\" href=\"hybrid-mobile-application-development\">Learn More <i class=\"fa fa-long-arrow-right\"></i></a></p>\r\n                             </div>\r\n                         </div>\r\n                     </div><!-- /.portfolio-item -->\r\n                 </div>\r\n                 <div class=\"portfolio portfolio-with-title col-4 gutter\">\r\n                     <div class=\"portfolio-item\">\r\n                         <div class=\"featured-item seo-service\">\r\n                             <div class=\"icon\">\r\n                                 <img class=\"img-responsive\" src=\"assets/img/seo/xamarin_app_development_bigperl.jpg\" alt=\"xamarin_app_development_bigperl\">\r\n                             </div>\r\n                             <div class=\"desc\">\r\n                                 <h2 class=\"text-center\">Xamarin App Development</h2>\r\n                                 <ul class=\"list-icon1 text-center\">\r\n                                     <li>Xamarin Integration Services</li>\r\n                                     <li>Xamarin App Development</li>\r\n                                     <li>Xamarin Maintenance</li>\r\n                                     <li>Support Services</li>\r\n                                 </ul>\r\n                                 <div class=\"bg-overlay\"></div>\r\n                                 <p><a class=\"learn-more\" href=\"xamarin-development.php\">Learn More <i class=\"fa fa-long-arrow-right\"></i></a></p>\r\n                             </div>\r\n                         </div>\r\n                     </div><!-- /.portfolio-item -->\r\n                 </div>\r\n             </div>\r\n         </section>\r\n \r\n         <section class=\"section-padding dark-bg lighten-4\">\r\n             <div class=\"container\">\r\n               <div class=\"row\">\r\n                 <div class=\"col-md-7 light-grey-text padding-top-30\">\r\n                   <h2 class=\"font-40 mb-30 white-text\">What is unique in BigPerl</h2>\r\n                   <p>Technology always changes very frequently, expectations evolved itself from traditional websites to multi-screen size mobile app to BigData and Machine Learning.Gaming changed from Computers to mobiles and now Virtual Reality ,Augmented Reality and Mixed Reality.All in just ten years.We are early adopters on new innovations in technology.</p>\r\n \r\n                   <ul class=\"list-icon mb-15 extendleft\">\r\n                     <li class=\"intended\"><i class=\"material-icons\">done</i>One stop solution for complete digital need.</li>\r\n                         <li class=\"intended\"><i class=\"material-icons\">done</i>Dedicated R&D team to explore on new technologies.</li>\r\n                     <li class=\"intended\"><i class=\"material-icons\">done</i>Unique payment structure , Pay As You Go, Subscription (SaaS) & T&M</li>\r\n                         <li class=\"intended\"><i class=\"material-icons\">done</i>Best in class personalised, post - production support for Entrepreneurs & Enterprise Customers.</li>\r\n                     <li class=\"intended\"><i class=\"material-icons\">done</i>30 + Unique technology supported.</li>\r\n                         <li>\r\n                                 <button type=\"button\" class=\"waves-effect waves-light btn blue\" data-toggle=\"modal\" data-target=\"#myModal\" >\r\n                                   Get A Quote\r\n                                 </button>\r\n                                 <div class=\"modal fade\" id=\"myModal\" tabindex=\"-1\"  style=\"width: 100%;min-height: 100%;background-color: transparent;\">\r\n                                   <div class=\"modal-dialog\" >\r\n                                     <div class=\"modal-content\">\r\n                                       <div class=\"modal-header\">\r\n                                         <button type=\"button\" class=\"close\" data-dismiss=\"modal\"   ><span   >&times;</span></button>\r\n                                         <h4 class=\"modal-title\">Contact Us</h4>\r\n                                       </div>\r\n                                       <div class=\"modal-body\">\r\n                                             <div class=\"row\">\r\n                                             <div class=\"col-md-12\">\r\n                                                 <form method=\"post\" id=\"contactForm\" name=\"contact-form\">\r\n                                                   <div class=\"row\">\r\n                                                     <div class=\"col-md-6\">\r\n                                                       <div class=\"input-field\">\r\n                                                         <i class=\"fa fa-user prefix fontcolors\"></i>\r\n                                                         <input type=\"text\" name=\"name\" class=\"validate\" id=\"name\" style=\"height:20px\">\r\n                                                         <label for=\"name\">Name</label>\r\n                                                       </div>\r\n                                                     </div><!-- /.col-md-6 -->\r\n                                                     <div class=\"col-md-6\">\r\n                                                       <div class=\"input-field\">\r\n                                                         <i class=\"fa fa-envelope prefix fontcolors\"></i>\r\n                                                         <input id=\"email\" type=\"email\" name=\"email\" class=\"validate\" style=\"height:20px\">\r\n                                                         <label for=\"email\" data-error=\"wrong\" data-success=\"right\">Email</label>\r\n                                                       </div>\r\n                                                     </div><!-- /.col-md-6 -->\r\n                                                   </div><!-- /.row -->\r\n \r\n                                                   <div class=\"row\">\r\n                                                     <div class=\"col-md-12\">\r\n                                                       <div class=\"input-field\">\r\n                                                         <i class=\"fa fa-book prefix fontcolors\"></i>\r\n                                                         <input id=\"subject\" type=\"tel\" name=\"subject\" class=\"validate\" style=\"height:20px\" >\r\n                                                         <label for=\"subject\">Subject</label>\r\n                                                       </div>\r\n                                                     </div><!-- /.col-md-6 -->\r\n                                                     </div>\r\n                                                   <div class=\"input-field\">\r\n                                                     <i class=\"fa fa-commenting-o prefix fontcolors\"></i>\r\n                                                     <textarea name=\"message\" id=\"message\" class=\"materialize-textarea\" ></textarea>\r\n                                                     <label for=\"message\">Message</label>\r\n                                                   </div>\r\n                                                   <button type=\"submit\" name=\"submit\" class=\"btn-sm btnclass\">Send Message</button>\r\n                                                   </form>\r\n                                                   </div>\r\n                                       </div>\r\n                                     </div>\r\n                                   </div>\r\n                                 </div>\r\n                                 </div>\r\n \r\n                         </li>\r\n                   </ul>\r\n \r\n                 </div><!-- /.col-md-7 -->\r\n \r\n                 <div class=\"col-md-5 mt-sm-30\">\r\n                    <img src=\"assets/img/seo/mobile_application_development_seo_bigperl_2.png\" alt=\"mobile_application_development_seo_bigperl\" class=\"img-responsive\">\r\n                 </div><!-- /.col-md-5 -->\r\n               </div><!-- /.row -->\r\n             </div><!-- /.container -->\r\n         </section>\r\n \r\n \r\n         <section class=\"section-padding\" style=\"padding-top: 10px;\">\r\n             <div class=\"container\">\r\n               <div class=\"text-center mb-80 padding-top-20 ptb-30\">\r\n                   <h2 class=\"section-title text-uppercase\">Why Choose Us</h2>\r\n                   <p class=\"section-sub\">BigPerl Solutions is one of the fastest growing Mobile Application Development company. we are specialised in providing best in class technology and support solution to cater growing business needs.</p>\r\n               </div>\r\n \r\n \r\n                 <div class=\"row\">\r\n                     <div class=\"col-md-4 mb-50\">\r\n                         <div class=\"featured-item feature-icon icon-hover icon-hover-brand icon-outline\">\r\n                             <div class=\"icon\">\r\n                                 <i class=\"material-icons colored brand-icon\">library_books</i>\r\n                             </div>\r\n                             <div class=\"desc\">\r\n                                 <h2>Mobile App Development</h2>\r\n                                 <p>Building right app stately is key. Our consultants work with you to create successful statigy.</p>\r\n                             </div>\r\n                         </div><!-- /.featured-item -->\r\n                     </div><!-- /.col-md-4 -->\r\n \r\n                     <div class=\"col-md-4 mb-50\">\r\n                         <div class=\"featured-item feature-icon icon-hover icon-hover-brand icon-outline\">\r\n                             <div class=\"icon\">\r\n                                 <i class=\"material-icons colored brand-icon\">equalizer</i>\r\n                             </div>\r\n                             <div class=\"desc\">\r\n                                 <h2>Android App Development</h2>\r\n                                 <p>We have team of 40+ Native Android developers along with R&D team which works on upcoming technologies like kotlin</p>\r\n                             </div>\r\n                         </div><!-- /.featured-item -->\r\n                     </div><!-- /.col-md-4 -->\r\n \r\n                     <div class=\"col-md-4 mb-50\">\r\n                         <div class=\"featured-item feature-icon icon-hover icon-hover-brand icon-outline\">\r\n                             <div class=\"icon\">\r\n                                 <i class=\"material-icons colored brand-icon\">share</i>\r\n                             </div>\r\n                             <div class=\"desc\">\r\n                                 <h2>iOS App Development</h2>\r\n                                 <p>We have 60+ iOS developers working on Xcode, Swift 3 & Objective-C. We have dedicated R&D team, which experiments new features and releases.</p>\r\n                             </div>\r\n                         </div><!-- /.featured-item -->\r\n                     </div><!-- /.col-md-4 -->\r\n \r\n                     <div class=\"col-md-4 mb-50\">\r\n                         <div class=\"featured-item feature-icon icon-hover icon-hover-brand icon-outline\">\r\n                             <div class=\"icon\">\r\n                                 <i class=\"material-icons colored brand-icon\">build</i>\r\n                             </div>\r\n                             <div class=\"desc\">\r\n                                 <h2>Windows App Development</h2>\r\n                                 <p>We have Dedicated team of xamarin app developers which works on windows native app development.\r\n </p>\r\n                             </div>\r\n                         </div><!-- /.featured-item -->\r\n                     </div><!-- /.col-md-4 -->\r\n \r\n                     <div class=\"col-md-4 mb-50\">\r\n                         <div class=\"featured-item feature-icon icon-hover icon-hover-brand icon-outline\">\r\n                             <div class=\"icon\">\r\n                                 <i class=\"material-icons colored brand-icon\">youtube_searched_for</i>\r\n                             </div>\r\n                             <div class=\"desc\">\r\n                                 <h2>Hybrid App Development</h2>\r\n                                 <p>Hybrid Mobile Apps are picking up well in app stores. We have dedicated team of 25+ developers work on PhoneGap, Ionic & AngularJs frameworks.\r\n </p>\r\n                             </div>\r\n                         </div><!-- /.featured-item -->\r\n                     </div><!-- /.col-md-4 -->\r\n \r\n                     <div class=\"col-md-4 mb-50\">\r\n                         <div class=\"featured-item feature-icon icon-hover icon-hover-brand icon-outline\">\r\n                             <div class=\"icon\">\r\n                                 <i class=\"material-icons colored brand-icon\">person</i>\r\n                             </div>\r\n                             <div class=\"desc\">\r\n                                 <h2>E-Commerse Web Development</h2>\r\n                                 <p>Building E-commerse web is always a key, Our team works on most of leading technologies e.g. : AngularJs, NodeJS, Php , Python, ROR development stack.</p>\r\n                             </div>\r\n                         </div><!-- /.featured-item -->\r\n                     </div><!-- /.col-md-4 -->\r\n               </div><!-- /.row -->\r\n               </div>\r\n         </section>\r\n \r\n         <section class=\"padding-top-20 padding-bottom-10 brand-bg\">\r\n           <div class=\"container\">\r\n             <div class=\"text-center\">\r\n                 <button class=\"waves-effect waves-light btn-large blue\" id=\"btnwidth\" data-toggle=\"modal\" data-target=\"#myModals\"><img src=\"assets/img/ico/trending_up_bigperl.png\" alt=\"trending_up_bigperl\" style=\"margin-right: 13px;\">Get free consultation from our Experts <img src=\"assets/img/ico/trending_up_bigperl.png\" alt=\"trending_up_bigperl\" style=\"margin-left: 10px;\"></button>\r\n                 <div class=\"modal fade\" id=\"myModals\" tabindex=\"-1\"  style=\"width: 100%;min-height: 100%;background-color: transparent;\">\r\n                                   <div class=\"modal-dialog\" >\r\n                                     <div class=\"modal-content\">\r\n                                       <div class=\"modal-header\">\r\n                                         <button type=\"button\" class=\"close\" data-dismiss=\"modal\"   ><span   >&times;</span></button>\r\n                                         <h4 class=\"modal-title\">Contact Us</h4>\r\n                                       </div>\r\n                                       <div class=\"modal-body\">\r\n                                             <div class=\"row\">\r\n                                             <div class=\"col-md-12\">\r\n                                                 <form method=\"post\" id=\"contactFormTest1\" name=\"contact-form\">\r\n                                                   <div class=\"row\">\r\n                                                     <div class=\"col-md-6\">\r\n                                                       <div class=\"input-field\">\r\n                                                         <i class=\"fa fa-user prefix fontcolors\"></i>\r\n                                                         <input type=\"text\" name=\"name\" class=\"validate\" id=\"name\" style=\"height:20px\">\r\n                                                         <label for=\"name\">Name</label>\r\n                                                       </div>\r\n                                                     </div><!-- /.col-md-6 -->\r\n                                                     <div class=\"col-md-6\">\r\n                                                       <div class=\"input-field\">\r\n                                                         <i class=\"fa fa-envelope prefix fontcolors\"></i>\r\n                                                         <input id=\"email\" type=\"email\" name=\"email\" class=\"validate\" style=\"height:20px\" >\r\n                                                         <label for=\"email\" data-error=\"wrong\" data-success=\"right\">Email</label>\r\n                                                       </div>\r\n                                                     </div><!-- /.col-md-6 -->\r\n                                                   </div><!-- /.row -->\r\n \r\n                                                   <div class=\"row\">\r\n                                                     <div class=\"col-md-12\">\r\n                                                       <div class=\"input-field\">\r\n                                                         <i class=\"fa fa-book prefix fontcolors\"></i>\r\n                                                         <input id=\"subject\" type=\"tel\" name=\"subject\" class=\"validate\" style=\"height:20px\">\r\n                                                         <label for=\"subject\">Subject</label>\r\n                                                       </div>\r\n                                                     </div><!-- /.col-md-6 -->\r\n                                                     </div>\r\n                                                   <div class=\"input-field\">\r\n                                                     <i class=\"fa fa-commenting-o prefix fontcolors\"></i>\r\n                                                     <textarea name=\"message\" id=\"message\" class=\"materialize-textarea\" ></textarea>\r\n                                                     <label for=\"message\">Message</label>\r\n                                                   </div>\r\n                                                   <button type=\"submit\" name=\"submit\" class=\"btn-sm btnclass\">Send Message</button>\r\n                                                   </form>\r\n                                             </div>\r\n                                       </div>\r\n                                     </div>\r\n                                   </div>\r\n                                 </div>\r\n                                 </div>\r\n             </div>\r\n           </div>\r\n         </section>\r\n \r\n         <?php include 'footer3.php'; ?>\r\n \r\n \r\n         <section class=\"section-padding\">\r\n             <div class=\"container\">\r\n \r\n               <div class=\"text-center mb-80\">\r\n                   <h1 class=\"text-uppercase\">Our Successful Clients</h1>\r\n                   <p class=\"section-sub\">Our client trust us to help them in building high performing software solutions. We have awesome list of global clients working with us for 5+ years.</p>\r\n               </div>\r\n               <div class=\"clients-grid gutter\">\r\n                 <div class=\"row\">\r\n                     <div class=\"owl-carousel owl-theme\" id=\"customers-carousel2\">\r\n                         <div class=\"item active\">\r\n                             <div class=\"border-box\">\r\n                                 <a href=\"#\">\r\n                                     <img src=\"assets/img/client-logo/our_clients_akg_bigperl_2.png\" alt=\"our_clients_akg_bigperl\">\r\n                                 </a>\r\n                             </div>\r\n                         </div>\r\n                         <div class=\"item\">\r\n                             <div class=\"border-box\">\r\n                                 <a href=\"#\">\r\n                                     <img src=\"assets/img/client-logo/our_clients_ang_bigperl_2.png\" alt=\"our_clients_ang_bigperl\">\r\n                                 </a>\r\n                             </div>\r\n                         </div>\r\n                         <div class=\"item\">\r\n                             <div class=\"border-box\">\r\n                                 <a href=\"#\">\r\n                                     <img src=\"assets/img/client-logo/our_clients_archi_bigperl_2.png\" alt=\"our_clients_archi_bigperl\">\r\n                                 </a>\r\n                             </div>\r\n                         </div>\r\n                         <div class=\"item\">\r\n                             <div class=\"border-box\">\r\n                                 <a href=\"#\">\r\n                                     <img src=\"assets/img/client-logo/our_clients_arfma_bigperl_2.png\" alt=\"our_clients_arfma_bigperl\">\r\n                                 </a>\r\n                             </div>\r\n                         </div>\r\n                         <div class=\"item\">\r\n                             <div class=\"border-box\">\r\n                                 <a href=\"#\">\r\n                                     <img src=\"assets/img/client-logo/our_clients_barma_bigperl_2.jpg\" alt=\"our_clients_barma_bigperl\">\r\n                                 </a>\r\n                             </div>\r\n                         </div>\r\n                         <div class=\"item\">\r\n                             <div class=\"border-box\">\r\n                                 <a href=\"#\">\r\n                                     <img src=\"assets/img/client-logo/our_clients_ccat_bigperl_2.png\" alt=\"our_clients_ccat_bigperl\">\r\n                                 </a>\r\n                             </div>\r\n                         </div>\r\n                         <div class=\"item\">\r\n                             <div class=\"border-box\">\r\n                                 <a href=\"#\">\r\n                                     <img src=\"assets/img/client-logo/our_clients_kvrc_bigperl_2.png\" alt=\"our_clients_kvrc_bigperl\">\r\n                                 </a>\r\n                             </div>\r\n                         </div>\r\n                         <div class=\"item\">\r\n                             <div class=\"border-box\">\r\n                                 <a href=\"#\">\r\n                                     <img src=\"assets/img/client-logo/our_clients_fashion_bigperl_2.png\" alt=\"our_clients_fashion_bigperl\">\r\n                                 </a>\r\n                             </div>\r\n                         </div>\r\n                         <div class=\"item\">\r\n                             <div class=\"border-box\">\r\n                                 <a href=\"#\">\r\n                                     <img src=\"assets/img/client-logo/our_clients_gp_bigperl_2.png\" alt=\"our_clients_gp_bigperl\">\r\n                                 </a>\r\n                             </div>\r\n                         </div>\r\n                         <div class=\"item\">\r\n                             <div class=\"border-box\">\r\n                                 <a href=\"#\">\r\n                                     <img src=\"assets/img/client-logo/our_clients_he_bigperl_2.png\" alt=\"he_bigperl\">\r\n                                 </a>\r\n                             </div>\r\n                         </div>\r\n                         <div class=\"item\">\r\n                             <div class=\"border-box\">\r\n                                 <a href=\"#\">\r\n                                     <img src=\"assets/img/client-logo/our_clients_jms_bigperl_2.png\" alt=\"our_clients_jms_bigperl\">\r\n                                 </a>\r\n                             </div>\r\n                         </div>\r\n                         <div class=\"item\">\r\n                             <div class=\"border-box\">\r\n                                 <a href=\"#\">\r\n                                     <img src=\"assets/img/client-logo/our_clients_kuch_bigperl_2.png\" alt=\"our_clients_kuch_bigperl\">\r\n                                 </a>\r\n                             </div>\r\n                         </div>\r\n                         <div class=\"item\">\r\n                             <div class=\"border-box\">\r\n                                 <a href=\"#\">\r\n                                     <img src=\"assets/img/client-logo/our_clients_kvrc_bigperl_2.png\" alt=\"our_clients_kvrc_bigperl\">\r\n                                 </a>\r\n                             </div>\r\n                         </div>\r\n                     </div>\r\n                 </div><!-- /.row -->\r\n               </div><!-- /.clients-grid -->\r\n             </div><!-- /.container -->\r\n         </section><br>\r\n \r\n           <div class=\"padding-top-20 padding-bottom-10 brand-bg\">\r\n             <div class=\"text-center\">\r\n                 <form method=\"post\" id=\"contactForm2\" name=\"contact-form\">\r\n                     <input type=\"email\" name=\"email\" placeholder=\"Subscribe to Newsletter\" id=\"inputs\" style=\"height:10px\">\r\n                     <button class=\"btn btn-sm indigo\" name=\"submit\" id=\"inputbtnstyle\"><i class=\"material-icons\">send</i></button>\r\n                 </form>\r\n             </div>\r\n           </div><!-- /.container -->\r\n \r\n         <section class=\"section-padding featured-list-news\">\r\n             <div class=\"container\">\r\n \r\n               <div class=\"text-center mb-80 padding-top-20\">\r\n                   <h2 class=\"section-title text-uppercase\">Latest Blog</h2>\r\n                   <p class=\"section-sub\">We are living in digital world. Most of business already understood and adopted the digital curve. This starts from paperless offices till live monintiering of crops using mobile app and iOT solutions. </p>\r\n               </div>\r\n \r\n               <div class=\"row\">\r\n                 <div class=\"col-md-6\">\r\n                   <div class=\"post-wrapper featured-news\">\r\n \r\n                     <div class=\"thumb-wrapper waves-effect waves-block waves-light\">\r\n                       <a href=\"#\"><img src=\"assets/img/blog/blog_12_bigperl.jpg\" class=\"img-responsive\" alt=\"blog_12_bigperl\" ></a>\r\n                     </div><!-- .post-thumb -->\r\n \r\n                     <div class=\"blog-content\">\r\n                       <header class=\"entry-header-wrapper\">\r\n                         <div class=\"entry-header\">\r\n                           <h2 class=\"entry-title\"><a href=\"#\">My Most Memorable Moment as a Product Designer</a></h2>\r\n \r\n                           <div class=\"entry-meta\">\r\n                               <ul class=\"list-inline\">\r\n                                   <li>\r\n                                       By <a href=\"#\">Bigperl</a>\r\n                                   </li>\r\n \r\n                                   <li>\r\n                                       In <a href=\"#\">Technology</a>\r\n                                   </li>\r\n \r\n                                   <li>\r\n                                       At <a href=\"#\">2018</a>\r\n                                   </li>\r\n                               </ul>\r\n                           </div><!-- .entry-meta -->\r\n                         </div>\r\n                       </header><!-- /.entry-header-wrapper -->\r\n \r\n                       <div class=\"entry-content\">\r\n                         <p>Maecenas varius finibus orci vel dignissim. Nam posuere, magna pellentesque accumsan tincidunt, libero lorem.</p>\r\n \r\n                         <a href=\"location-based-technology-for-mobile-app\" class=\"readmore\">Read Full Post <i class=\"fa fa-long-arrow-right\"></i></a>\r\n                       </div><!-- .entry-content -->\r\n                     </div>\r\n \r\n                    </div><!-- /.post-wrapper -->\r\n                 </div>\r\n                 <div class=\"col-md-6\">\r\n \r\n                     <div class=\"post-wrapper list-article\">\r\n \r\n                       <div class=\"hover-overlay brand-bg\"></div>\r\n \r\n                       <div class=\"thumb-wrapper waves-effect waves-block waves-light\">\r\n                         <a href=\"#\"><img src=\"assets/img/blog/blog_22_bigperl.jpg\" class=\"img-responsive\" alt=\"blog_22_bigperl\" ></a>\r\n                       </div><!-- .post-thumb -->\r\n \r\n                       <div class=\"blog-content\">\r\n                         <header class=\"entry-header-wrapper\">\r\n                           <div class=\"entry-header\">\r\n                             <h2 class=\"entry-title\"><a href=\"#\">Ideas That Moved Us in 2015</a></h2>\r\n \r\n                             <div class=\"entry-meta\">\r\n                                 <ul class=\"list-inline\">\r\n                                     <li>\r\n                                         By <a href=\"#\">BigPerl</a>\r\n                                     </li>\r\n \r\n                                     <li>\r\n                                         In <a href=\"#\">Technology</a>\r\n                                     </li>\r\n \r\n                                     <li>\r\n                                         At <a href=\"#\">Jan 15, 2016</a>\r\n                                     </li>\r\n                                 </ul>\r\n                             </div><!-- .entry-meta -->\r\n                           </div><!-- /.entry-header -->\r\n                         </header><!-- /.entry-header-wrapper -->\r\n \r\n                         <div class=\"entry-content\">\r\n                           <p>Maecenas varius finibus orci vel dignissim. Nam posuere, magna pellentesque accumsan.</p>\r\n                           <a href=\"bigperlArticle_3\" class=\"readmore\">Read Full Post <i class=\"fa fa-long-arrow-right\"></i></a>\r\n                         </div><!-- .entry-content -->\r\n                       </div>\r\n \r\n                    </div><!-- /.post-wrapper -->\r\n \r\n                     <div class=\"post-wrapper list-article\">\r\n \r\n                       <div class=\"hover-overlay brand-bg\"></div>\r\n                        \r\n \r\n                       <div class=\"thumb-wrapper waves-effect waves-block waves-light\">\r\n                         <a href=\"#\"><img src=\"assets/img/blog/blog_23_bigperl.jpg\" class=\"img-responsive\" alt=\"blog_23_bigperl\" ></a>\r\n                       </div><!-- .post-thumb -->\r\n \r\n                       <div class=\"blog-content\">\r\n                         <header class=\"entry-header-wrapper\">\r\n                           <div class=\"entry-header\">\r\n                             <h2 class=\"entry-title\"><a href=\"#\">Ideas That Moved Us in 2015</a></h2>\r\n \r\n                             <div class=\"entry-meta\">\r\n                                 <ul class=\"list-inline\">\r\n                                     <li>\r\n                                         By <a href=\"#\">BigPerl</a>\r\n                                     </li>\r\n \r\n                                     <li>\r\n                                         In <a href=\"#\">Technology</a>\r\n                                     </li>\r\n \r\n                                     <li>\r\n                                         At <a href=\"#\">Jan 15, 2016</a>\r\n                                     </li>\r\n                                 </ul>\r\n                             </div><!-- .entry-meta -->\r\n                           </div><!-- /.entry-header -->\r\n                         </header><!-- /.entry-header-wrapper -->\r\n \r\n                         <div class=\"entry-content\">\r\n                           <p>Maecenas varius finibus orci vel dignissim. Nam posuere, magna pellentesque accumsan</p>\r\n                           <a href=\"xamarin-development\" class=\"readmore\">Read Full Post <i class=\"fa fa-long-arrow-right\"></i></a>\r\n                         </div><!-- .entry-content -->\r\n                       </div>\r\n \r\n                    </div><!-- /.post-wrapper -->\r\n \r\n                 </div><!-- /.col-md-6 -->\r\n               </div><!-- /.row -->\r\n \r\n             </div><!-- /.container -->\r\n         </section>"

/***/ }),

/***/ "./src/app/home/home.component.ts":
/*!****************************************!*\
  !*** ./src/app/home/home.component.ts ***!
  \****************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var HomeComponent = /** @class */ (function () {
    function HomeComponent() {
    }
    HomeComponent.prototype.ngOnInit = function () { };
    HomeComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-home',
            template: __webpack_require__(/*! ./home.component.html */ "./src/app/home/home.component.html"),
            styles: [__webpack_require__(/*! ./home.component.css */ "./src/app/home/home.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], HomeComponent);
    return HomeComponent;
}());



/***/ }),

/***/ "./src/app/home1/home1.component.css":
/*!*******************************************!*\
  !*** ./src/app/home1/home1.component.css ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "input, select, textarea{\r\n    color: #050505;\r\n}"

/***/ }),

/***/ "./src/app/home1/home1.component.html":
/*!********************************************!*\
  !*** ./src/app/home1/home1.component.html ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n       \r\n     <!-- start revolution slider 5.0 -->\r\n     <section class=\"rev_slider_wrapper mt-xs-45\">\r\n           \r\n          <!-- <a href=\"#top\" class=\"page-scroll btn-floating btn-large pink back-top waves-effect waves-light tt-animate btt\" data-section=\"#top\" style=\"margin-top: 80px;\">\r\n            <i class=\"material-icons\">keyboard_arrow_up</i>   \r\n          </a> -->\r\n        <div class=\"rev_slider materialize-slider\" >\r\n         <ul>\r\n\r\n           <!-- slide 1 start -->\r\n           <li data-transition=\"fade\" data-slotamount=\"default\" data-easein=\"Power4.easeInOut\" data-easeout=\"Power4.easeInOut\" data-masterspeed=\"2000\" data-thumb=\"assets/img/seo/slider/slider-bg-1.jpg\"  data-rotate=\"0\"  data-fstransition=\"fade\" data-fsmasterspeed=\"1500\" data-fsslotamount=\"7\" data-saveperformance=\"off\"  data-title=\"materialize Material\" data-description=\"\">\r\n\r\n               <!-- MAIN IMAGE -->\r\n               <img src=\"assets/img/seo/slider/mobile_application_development_slider_1_bigperl.jpg\"  alt=\"mobile_application_development_slider_1_bigperl\"  data-bgposition=\"center center\" data-bgfit=\"cover\" data-bgrepeat=\"no-repeat\" data-bgparallax=\"10\" class=\"rev-slidebg\" data-no-retina>\r\n\r\n\r\n               <!-- LAYER NR. 1 -->\r\n               <div class=\"tp-caption NotGeneric-Title tp-resizeme\"\r\n                   data-x=\"['left','left','center','center']\" data-hoffset=\"['20','20','0','0']\"\r\n                   data-y=\"['center','center','top','top']\" data-voffset=\"['-100','-100','50','50']\"\r\n                   data-fontsize=\"['70','60','50','45']\"\r\n                   data-lineheight=\"['70','60','40','40']\"\r\n                   data-width=\"none\"\r\n                   data-height=\"none\"\r\n                   data-whitespace=\"nowrap\"\r\n                   data-transform_idle=\"o:1;\"\r\n                   data-transform_in=\"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:600;e:Power4.easeInOut;\"\r\n                   data-transform_out=\"y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;\"\r\n                   data-mask_in=\"x:0px;y:[100%];s:inherit;e:inherit;\"\r\n                   data-mask_out=\"x:inherit;y:inherit;s:inherit;e:inherit;\"\r\n                   data-start=\"800\"\r\n                   data-splitin=\"none\"\r\n                   data-splitout=\"none\"\r\n                   data-responsive_offset=\"on\"\r\n                   style=\"z-index: 5; color: #fff;background-color: rgba(0,0,0,0.6);border-radius: 5px;padding:5px;margin-top: 120px;white-space: nowrap;\">Mobile Application<br> Development\r\n               </div>\r\n\r\n               <!-- LAYER NR. 2 -->\r\n               <div class=\"tp-caption tp-resizeme rev-subheading\"\r\n                   data-x=\"['left','left','center','center']\" data-hoffset=\"['20','20','0','0']\"\r\n                   data-y=\"['center','center','top','top']\" data-voffset=\"['0','0','140','140']\"\r\n                   data-whitespace=\"nowrap\"\r\n                   data-transform_idle=\"o:1;\"\r\n\r\n                   data-transform_in=\"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:600;e:Power4.easeInOut;\"\r\n                   data-transform_out=\"y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;\"\r\n                   data-mask_in=\"x:0px;y:[100%];s:inherit;e:inherit;\"\r\n                   data-mask_out=\"x:inherit;y:inherit;s:inherit;e:inherit;\"\r\n                   data-start=\"1000\"\r\n                   data-splitin=\"none\"\r\n                   data-splitout=\"none\"\r\n                   data-responsive_offset=\"on\"\r\n                   style=\"z-index: 6; color: #fff; background-color: rgba(0,0,0,0.6);border-radius: 5px;padding:5px 90px 5px 5px;margin-top: 140px; white-space: nowrap;\"><b><strong>iOS Application Development | Android Application Development | Windows <br> Application Development | Wearable Devices | Internet Of Things</strong></b>\r\n               </div>\r\n\r\n               <!-- LAYER NR. 3 -->\r\n               <div class=\"tp-caption tp-resizeme\"\r\n                   data-x=\"['left','left','center','center']\" data-hoffset=\"['20','20','0','0']\"\r\n                   data-y=\"['middle','middle','top','top']\" data-voffset=\"['100','100','220','220']\"\r\n                   data-width=\"none\"\r\n                   data-height=\"none\"\r\n                   data-whitespace=\"nowrap\"\r\n                   data-transform_idle=\"o:1;\"\r\n                   data-style_hover=\"cursor:default;\"\r\n                   data-transform_in=\"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:600;e:Power4.easeInOut;\"\r\n                   data-transform_out=\"y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;\"\r\n                   data-mask_in=\"x:0px;y:[100%];s:inherit;e:inherit;\"\r\n                   data-mask_out=\"x:inherit;y:inherit;s:inherit;e:inherit;\"\r\n                   data-start=\"1200\"\r\n                   data-splitin=\"none\"\r\n                   data-splitout=\"none\"\r\n                   data-responsive_offset=\"on\"\r\n                   style=\"z-index: 7;background-color: rgba(0,0,0,0.6); border-radius: 5px;padding:5px; white-space: nowrap;margin-top: 60px;\">\r\n                   <h3 style=\"color: #fff;\"><b>Software Development and onsite consulting</b></h3>\r\n               </div>\r\n\r\n               <!-- LAYER NR. 4 -->\r\n               <div class=\"tp-caption tp-resizeme\"\r\n                   data-x=\"['right','right','center','center']\" data-hoffset=\"['30','30','0','0']\"\r\n                   data-y=\"['middle','middle','bottom','bottom']\" data-voffset=\"['30','30','50','50']\"\r\n                   data-transform_idle=\"o:1;\"\r\n                   data-transform_in=\"y:30px;opacity:0;s:600;e:Power2.easeOut;\"\r\n                   data-transform_out=\"opacity:0;s:1000;e:Power4.easeIn;s:1500;e:Power4.easeIn;\"\r\n                   data-start=\"1200\"\r\n                   style=\"z-index: 8;margin-right: 60px;\">\r\n                       <div><img src=\"assets/img/seo/slider/mobile_application_development_monitor_bigperl.png\" alt=\"mobile_application_development_monitor_bigperl\">\r\n                       </div>\r\n               </div>\r\n\r\n               <!-- LAYER NR. 5 -->\r\n               <div class=\"tp-caption tp-resizeme rs-parallaxlevel-2\"\r\n                   data-x=\"['right','right','center','center']\" data-hoffset=\"['300','300','-180','-180']\"\r\n                   data-y=\"['middle','middle','middle','middle']\" data-voffset=\"['-160','-160','0','0']\"\r\n                   data-transform_idle=\"o:1;\"\r\n                   data-transform_in=\"y:30px;opacity:0;s:600;e:Power2.easeOut;\"\r\n                   data-transform_out=\"opacity:0;s:1000;e:Power4.easeIn;s:1500;e:Power4.easeIn;\"\r\n                   data-start=\"1500\"\r\n                   style=\"z-index: 9;margin-top: 180px;\">\r\n                       <div><img src=\"assets/img/seo/slider/mobile_application_development_envelope_bigperl.png\" alt=\"mobile_application_development_envelope_bigperl\">\r\n                       </div>\r\n               </div>\r\n\r\n               <!-- LAYER NR. 6 -->\r\n               <div class=\"tp-caption tp-resizeme rs-parallaxlevel-2\"\r\n                   data-x=\"['right','right','center','center']\" data-hoffset=\"['160','160','0','0']\"\r\n                   data-y=\"['middle','middle','middle','middle']\" data-voffset=\"['-220','-220','-60','-60']\"\r\n                   data-transform_idle=\"o:1;\"\r\n                   data-transform_in=\"y:30px;opacity:0;s:600;e:Power2.easeOut;\"\r\n                   data-transform_out=\"opacity:0;s:1000;e:Power4.easeIn;s:1500;e:Power4.easeIn;\"\r\n                   data-start=\"1700\"\r\n                   style=\"z-index: 10;margin-top: 240px;\">\r\n                       <div><img src=\"assets/img/seo/slider/mobile_application_development_bar_bigperl.png\" alt=\"mobile_application_development_bar_bigperl\" >\r\n                       </div>\r\n               </div>\r\n\r\n               <!-- LAYER NR. 7 -->\r\n               <div class=\"tp-caption tp-resizeme rs-parallaxlevel-2\"\r\n                   data-x=\"['right','right','center','center']\" data-hoffset=\"['20','20','180','180']\"\r\n                   data-y=\"['middle','middle','middle','middle']\" data-voffset=\"['-170','-170','0','0']\"\r\n                   data-transform_idle=\"o:1;\"\r\n                   data-transform_in=\"y:30px;opacity:0;s:600;e:Power2.easeOut;\"\r\n                   data-transform_out=\"opacity:0;s:1000;e:Power4.easeIn;s:1500;e:Power4.easeIn;\"\r\n                   data-start=\"1900\"\r\n                   style=\"z-index: 11;margin-top:180px;\">\r\n                       <div><img src=\"assets/img/seo/slider/mobile_application_development_chart_bigperl.png\" alt=\"mobile_application_development_chart_bigperl\" >\r\n                       </div>\r\n               </div>\r\n\r\n               <!-- LAYER NR. 8 -->\r\n               <div class=\"tp-caption tp-resizeme rs-parallaxlevel-3\"\r\n                   data-x=\"['right','right','center','center']\" data-hoffset=\"['200','200','-100','-100']\"\r\n                   data-y=\"['middle','middle','bottom','bottom']\" data-voffset=\"['80','80','30','30']\"\r\n                   data-transform_idle=\"o:1;\"\r\n                   data-transform_in=\"y:30px;opacity:0;s:600;e:Power2.easeOut;\"\r\n                   data-transform_out=\"opacity:0;s:1000;e:Power4.easeIn;s:1500;e:Power4.easeIn;\"\r\n                   data-start=\"2100\"\r\n                   style=\"z-index: 12;margin-top: 80px;margin-right: -80px;\">\r\n                       <div class=\"rs-pendulum\" data-easing=\"Power4.easeInOut\" data-startdeg=\"0\" data-enddeg=\"15\"><img src=\"assets/img/seo/slider/mobile_application_development_seo_mag_bigperl.png\" alt=\"mobile_application_development_seo_mag_bigperl\" >\r\n                       </div>\r\n               </div>\r\n\r\n           </li>\r\n           <!-- slide 1 end -->\r\n\r\n\r\n           <!-- slide 2 start -->\r\n           <li data-transition=\"fade\" data-slotamount=\"default\" data-easein=\"Power4.easeInOut\" data-easeout=\"Power4.easeInOut\" data-masterspeed=\"2000\" data-thumb=\"assets/img/seo/slider/slider-bg-2.png\"  data-rotate=\"0\"  data-fstransition=\"fade\" data-fsmasterspeed=\"1500\" data-fsslotamount=\"7\" data-saveperformance=\"off\"  data-title=\"materialize Material\" data-description=\"\">\r\n\r\n               <!-- MAIN IMAGE -->\r\n               <img src=\"assets/img/seo/slider/mobile_application_development_slider_2_bigperl.png\"  alt=\"mobile_application_development_slider_2_bigperl\"  data-bgposition=\"center center\" data-bgfit=\"cover\" data-bgrepeat=\"no-repeat\" data-bgparallax=\"10\" class=\"rev-slidebg\" data-no-retina>\r\n\r\n\r\n               <!-- LAYER NR. 1 -->\r\n               <div class=\"tp-caption NotGeneric-Title tp-resizeme\"\r\n                   data-x=\"['left','left','center','center']\" data-hoffset=\"['20','20','0','0']\"\r\n                   data-y=\"['center','center','top','top']\" data-voffset=\"['-100','-100','50','50']\"\r\n                   data-fontsize=\"['70','60','50','45']\"\r\n                   data-lineheight=\"['70','60','40','40']\"\r\n                   data-width=\"none\"\r\n                   data-height=\"none\"\r\n                   data-whitespace=\"nowrap\"\r\n                   data-transform_idle=\"o:1;\"\r\n                   data-transform_in=\"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:600;e:Power4.easeInOut;\"\r\n                   data-transform_out=\"y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;\"\r\n                   data-mask_in=\"x:0px;y:[100%];s:inherit;e:inherit;\"\r\n                   data-mask_out=\"x:inherit;y:inherit;s:inherit;e:inherit;\"\r\n                   data-start=\"800\"\r\n                   data-splitin=\"none\"\r\n                   data-splitout=\"none\"\r\n                   data-responsive_offset=\"on\"\r\n                   style=\"z-index: 5; color: #fff;background-color: rgba(0,0,0,0.6);border-radius: 5px;padding:5px;margin-top: 120px; white-space: nowrap;\">Responsive web <br> development\r\n               </div>\r\n\r\n               <!-- LAYER NR. 2 -->\r\n               <div class=\"tp-caption tp-resizeme rev-subheading\"\r\n                   data-x=\"['left','left','center','center']\" data-hoffset=\"['20','20','0','0']\"\r\n                   data-y=\"['center','center','top','top']\" data-voffset=\"['0','0','140','140']\"\r\n                   data-whitespace=\"nowrap\"\r\n                   data-transform_idle=\"o:1;\"\r\n\r\n                   data-transform_in=\"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:600;e:Power4.easeInOut;\"\r\n                   data-transform_out=\"y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;\"\r\n                   data-mask_in=\"x:0px;y:[100%];s:inherit;e:inherit;\"\r\n                   data-mask_out=\"x:inherit;y:inherit;s:inherit;e:inherit;\"\r\n                   data-start=\"1000\"\r\n                   data-splitin=\"none\"\r\n                   data-splitout=\"none\"\r\n                   data-responsive_offset=\"on\"\r\n                   style=\"z-index: 6; color: #fff; white-space: nowrap;border-radius: 5px;padding:5px 10px 5px 5px;background-color: rgba(0,0,0,0.6);margin-top: 120px;\">\r\n                   <b>Parallax Website Templates | E-Commerce Websites | Web 2.0 Development</b>\r\n               </div>\r\n\r\n               <!-- LAYER NR. 3 -->\r\n               <div class=\"tp-caption tp-resizeme\"\r\n                   data-x=\"['left','left','center','center']\" data-hoffset=\"['20','20','0','0']\"\r\n                   data-y=\"['middle','middle','top','top']\" data-voffset=\"['100','100','220','220']\"\r\n                   data-width=\"none\"\r\n                   data-height=\"none\"\r\n                   data-whitespace=\"nowrap\"\r\n                   data-transform_idle=\"o:1;\"\r\n                   data-style_hover=\"cursor:default;\"\r\n                   data-transform_in=\"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:600;e:Power4.easeInOut;\"\r\n                   data-transform_out=\"y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;\"\r\n                   data-mask_in=\"x:0px;y:[100%];s:inherit;e:inherit;\"\r\n                   data-mask_out=\"x:inherit;y:inherit;s:inherit;e:inherit;\"\r\n                   data-start=\"1200\"\r\n                   data-splitin=\"none\"\r\n                   data-splitout=\"none\"\r\n                   data-responsive_offset=\"on\"\r\n                   style=\"z-index: 7; white-space: nowrap;margin-top: 50px;\">\r\n               </div>\r\n\r\n               <!-- LAYER NR. 4 -->\r\n               <div class=\"tp-caption tp-resizeme\"\r\n                   data-basealign=\"slide\"\r\n                   data-x=\"['right','right','center','center']\" data-hoffset=\"['0','0','0','0']\"\r\n                   data-y=\"['bottom','bottom','bottom','bottom']\" data-voffset=\"['0','0','0','0']\"\r\n                   data-transform_idle=\"o:1;\"\r\n                   data-transform_in=\"y:30px;opacity:0;s:600;e:Power2.easeOut;\"\r\n                   data-transform_out=\"opacity:0;s:1000;e:Power4.easeIn;s:1500;e:Power4.easeIn;\"\r\n                   data-start=\"1500\"\r\n                   style=\"z-index: 8;margin-bottom: 30px;margin-right: 80px;\">\r\n                   <div>\r\n                       <img src=\"assets/img/seo/slider/mobile_application_development_graph1_bigperl.png\" alt=\"mobile_application_development_graph1_bigperl\">\r\n                   </div>\r\n               </div>\r\n           </li>\r\n           <!-- slide 2 end -->\r\n           <!-- slide 3 start -->\r\n           <li data-transition=\"fade\" data-slotamount=\"default\" data-easein=\"Power4.easeInOut\" data-easeout=\"Power4.easeInOut\" data-masterspeed=\"2000\" data-thumb=\"assets/img/seo/slider/slider-bg-2.png\"  data-rotate=\"0\"  data-fstransition=\"fade\" data-fsmasterspeed=\"1500\" data-fsslotamount=\"7\" data-saveperformance=\"off\"  data-title=\"materialize Material\" data-description=\"\">\r\n\r\n               <!-- MAIN IMAGE -->\r\n               <img src=\"assets/img/seo/slider/mobile_application_development_slider_3_bigperl.png\"  alt=\"mobile_application_development_slider_3_bigperl\"  data-bgposition=\"center center\" data-bgfit=\"cover\" data-bgrepeat=\"no-repeat\" data-bgparallax=\"10\" class=\"rev-slidebg\" data-no-retina>\r\n\r\n\r\n               <!-- LAYER NR. 1 -->\r\n               <div class=\"tp-caption NotGeneric-Title tp-resizeme\"\r\n                   data-x=\"['left','left','center','center']\" data-hoffset=\"['20','20','0','0']\"\r\n                   data-y=\"['center','center','top','top']\" data-voffset=\"['-100','-100','50','50']\"\r\n                   data-fontsize=\"['70','60','50','45']\"\r\n                   data-lineheight=\"['70','60','40','40']\"\r\n                   data-width=\"none\"\r\n                   data-height=\"none\"\r\n                   data-whitespace=\"nowrap\"\r\n                   data-transform_idle=\"o:1;\"\r\n                   data-transform_in=\"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:600;e:Power4.easeInOut;\"\r\n                   data-transform_out=\"y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;\"\r\n                   data-mask_in=\"x:0px;y:[100%];s:inherit;e:inherit;\"\r\n                   data-mask_out=\"x:inherit;y:inherit;s:inherit;e:inherit;\"\r\n                   data-start=\"800\"\r\n                   data-splitin=\"none\"\r\n                   data-splitout=\"none\"\r\n                   data-responsive_offset=\"on\"\r\n                   style=\"z-index: 5; color: #fff;background-color: rgba(0,0,0,0.6);border-radius: 5px;padding:5px; white-space: nowrap;margin-top: 120px;\">Responsive web <br> development\r\n               </div>\r\n\r\n               <!-- LAYER NR. 2 -->\r\n               <div class=\"tp-caption tp-resizeme rev-subheading\"\r\n                   data-x=\"['left','left','center','center']\" data-hoffset=\"['20','20','0','0']\"\r\n                   data-y=\"['center','center','top','top']\" data-voffset=\"['0','0','140','140']\"\r\n                   data-whitespace=\"nowrap\"\r\n                   data-transform_idle=\"o:1;\"\r\n\r\n                   data-transform_in=\"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:600;e:Power4.easeInOut;\"\r\n                   data-transform_out=\"y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;\"\r\n                   data-mask_in=\"x:0px;y:[100%];s:inherit;e:inherit;\"\r\n                   data-mask_out=\"x:inherit;y:inherit;s:inherit;e:inherit;\"\r\n                   data-start=\"1000\"\r\n                   data-splitin=\"none\"\r\n                   data-splitout=\"none\"\r\n                   data-responsive_offset=\"on\"\r\n                   style=\"z-index: 6; color: #fff; white-space: nowrap;border-radius: 5px;padding:5px 10px 5px 5px;background-color: rgba(0,0,0,0.6);margin-top: 120px;\"><b>Parallax Website Templates | E-Commerce Websites | Responsive Web Development</b>\r\n               </div>\r\n\r\n               <!-- LAYER NR. 3 -->\r\n               <div class=\"tp-caption tp-resizeme\"\r\n                   data-x=\"['left','left','center','center']\" data-hoffset=\"['20','20','0','0']\"\r\n                   data-y=\"['middle','middle','top','top']\" data-voffset=\"['100','100','220','220']\"\r\n                   data-width=\"none\"\r\n                   data-height=\"none\"\r\n                   data-whitespace=\"nowrap\"\r\n                   data-transform_idle=\"o:1;\"\r\n                   data-style_hover=\"cursor:default;\"\r\n                   data-transform_in=\"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:600;e:Power4.easeInOut;\"\r\n                   data-transform_out=\"y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;\"\r\n                   data-mask_in=\"x:0px;y:[100%];s:inherit;e:inherit;\"\r\n                   data-mask_out=\"x:inherit;y:inherit;s:inherit;e:inherit;\"\r\n                   data-start=\"1200\"\r\n                   data-splitin=\"none\"\r\n                   data-splitout=\"none\"\r\n                   data-responsive_offset=\"on\"\r\n                   style=\"z-index: 7; white-space: nowrap;margin-top: 50px;\">\r\n               </div>\r\n\r\n               <!-- LAYER NR. 4 -->\r\n               <div class=\"tp-caption tp-resizeme\"\r\n                   data-basealign=\"slide\"\r\n                   data-x=\"['right','right','center','center']\" data-hoffset=\"['0','0','0','0']\"\r\n                   data-y=\"['bottom','bottom','bottom','bottom']\" data-voffset=\"['0','0','0','0']\"\r\n                   data-transform_idle=\"o:1;\"\r\n                   data-transform_in=\"y:30px;opacity:0;s:600;e:Power2.easeOut;\"\r\n                   data-transform_out=\"opacity:0;s:1000;e:Power4.easeIn;s:1500;e:Power4.easeIn;\"\r\n                   data-start=\"1500\"\r\n                   style=\"z-index: 8;margin-bottom: 30px;margin-right: 80px;\">\r\n                   <div>\r\n                       <img src=\"assets/img/seo/slider/mobile_application_development_graph_bigperl.png\" alt=\"graph_bigperl\">\r\n                   </div>\r\n               </div>\r\n           </li>\r\n           <!-- slide 3 end -->\r\n\r\n\r\n         </ul>\r\n        </div><!-- end revolution slider -->\r\n \r\n       </section><!-- end of slider wrapper -->\r\n\r\n       <section class=\"section-padding\">\r\n           <div class=\"container\">\r\n               <div class=\"text-center mb-80 padding-top-20\">\r\n                   <h2 class=\"section-title text-uppercase\">What We Do</h2>\r\n               </div>\r\n               <div class=\"portfolio portfolio-with-title col-4 gutter\">\r\n                   <div class=\"portfolio-item\">\r\n                       <div class=\"featured-item seo-service\">\r\n                           <div class=\"icon\">\r\n                               <img class=\"img-responsive\" src=\"assets/img/seo/ios_app_development_bigperl.jpg\" alt=\"ios_app_development_bigperl\">\r\n                           </div>\r\n                           <div class=\"desc\">\r\n                               <h2 class=\"text-center\">IOS App<br>Development</h2>\r\n                               <ul class=\"list-icon1 text-center\">\r\n                                   <li>iPhone App Development</li>\r\n                                   <li>iPad Apps Development</li>\r\n                                   <li>Wearable Apps</li>\r\n                                   <li>tvOS Apps</li>\r\n                               </ul>\r\n                               <div class=\"bg-overlay\"></div>\r\n                               <p><a class=\"learn-more\" routerLink=\"/iphone-app-development\">Learn More <i class=\"fa fa-long-arrow-right\"></i></a></p>\r\n                           </div>\r\n                       </div>\r\n                   </div><!-- /.portfolio-item -->\r\n               </div>\r\n               <div class=\"portfolio portfolio-with-title col-4 gutter\">\r\n                   <div class=\"portfolio-item\">\r\n                       <div class=\"featured-item seo-service\">\r\n                           <div class=\"icon\">\r\n                               <img class=\"img-responsive\" src=\"assets/img/seo/android_app_development_bigperl.jpg\" alt=\"android_app_development_bigperl\">\r\n                           </div>\r\n                           <div class=\"desc\">\r\n                               <h2 class=\"text-center\">Android App Development</h2>\r\n                               <ul class=\"list-icon1 text-center\">\r\n                                   <li>Android Game Development</li>\r\n                                   <li>Wearable App development</li>\r\n                                   <li>Android Mobile Apps</li>\r\n                                   <li>Android Tab Apps</li>\r\n                               </ul>\r\n                               <div class=\"bg-overlay\"></div>\r\n                               <p><a class=\"learn-more\" routerLink=\"/android-app-development\">Learn More <i class=\"fa fa-long-arrow-right\"></i></a></p>\r\n                           </div>\r\n                       </div>\r\n                   </div><!-- /.portfolio-item -->\r\n               </div>\r\n               <div class=\"portfolio portfolio-with-title col-4 gutter\">\r\n                   <div class=\"portfolio-item\">\r\n                       <div class=\"featured-item seo-service\">\r\n                           <div class=\"icon\">\r\n                               <img class=\"img-responsive\" src=\"assets/img/seo/hybrid_app_development_bigperl.jpg\" alt=\"hybrid_app_development_bigperl\">\r\n                           </div>\r\n                           <div class=\"desc\">\r\n                               <h2 class=\"text-center\">Hybrid App Development</h2>\r\n                               <ul class=\"list-icon1 text-center\">\r\n                                   <li>PhoneGap App Development</li>\r\n                                   <li>IONIC Apps Development</li>\r\n                                   <li>Cocoon Framework</li>\r\n                                   <li>Mobile Angular UI</li>\r\n                               </ul>\r\n                               <div class=\"bg-overlay\"></div>\r\n                               <p><a class=\"learn-more\" routerLink=\"/hybrid-app-development\">Learn More <i class=\"fa fa-long-arrow-right\"></i></a></p>\r\n                           </div>\r\n                       </div>\r\n                   </div><!-- /.portfolio-item -->\r\n               </div>\r\n               <div class=\"portfolio portfolio-with-title col-4 gutter\">\r\n                   <div class=\"portfolio-item\">\r\n                       <div class=\"featured-item seo-service\">\r\n                           <div class=\"icon\">\r\n                               <img class=\"img-responsive\" src=\"assets/img/seo/xamarin_app_development_bigperl.jpg\" alt=\"xamarin_app_development_bigperl\">\r\n                           </div>\r\n                           <div class=\"desc\">\r\n                               <h2 class=\"text-center\">Xamarin App Development</h2>\r\n                               <ul class=\"list-icon1 text-center\">\r\n                                   <li>Xamarin Integration Services</li>\r\n                                   <li>Xamarin App Development</li>\r\n                                   <li>Xamarin Maintenance</li>\r\n                                   <li>Support Services</li>\r\n                               </ul>\r\n                               <div class=\"bg-overlay\"></div>\r\n                               <p><a class=\"learn-more\" routerLink=\"/xamarin-development.php\">Learn More <i class=\"fa fa-long-arrow-right\"></i></a></p>\r\n                           </div>\r\n                       </div>\r\n                   </div><!-- /.portfolio-item -->\r\n               </div>\r\n           </div>\r\n       </section>\r\n\r\n       <section class=\"section-padding dark-bg lighten-4\">\r\n           <div class=\"container\">\r\n             <div class=\"row\">\r\n               <div class=\"col-md-7 light-grey-text padding-top-30\">\r\n                   <h2 class=\"font-40 mb-30 white-text\">What is unique in BigPerl</h2>\r\n                   <p>Technology always changes very frequently, expectations evolved itself from traditional websites to multi screen size mobile app to BigData and Machine Learning.Gaming changed from Computers to mobiles and now Virtual Reality ,Augmented Reality and Mixed Reality.All in just ten years.We are early adopters on new innovations in technology.</p>\r\n\r\n                   <ul class=\"list-icon mb-15 extendleft\">\r\n                       <li class=\"intended\"><i class=\"material-icons\">done</i>One stop solution for complete digital need.</li>\r\n                       <li class=\"intended\"><i class=\"material-icons\">done</i>Dedicated R&D team to explore on new technologies.</li>\r\n                       <li class=\"intended\"><i class=\"material-icons\">done</i>Unique payment structure , Pay As You Go, Subscription (SaaS) & T&M</li>\r\n                       <li class=\"intended\"><i class=\"material-icons\">done</i>Best in class personalised, post - production support for Entrepreneurs & Enterprise Customers.</li>\r\n                       <li class=\"intended\"><i class=\"material-icons\">done</i>30 + Unique technology supported.</li>\r\n                       <li>\r\n                               <button type=\"button\" class=\"waves-effect waves-light btn blue\" data-toggle=\"modal\" data-target=\"#myModal\" >\r\n                                 Get A Quote\r\n                               </button>\r\n                               <div class=\"modal fade\" id=\"myModal\" tabindex=\"-1\" style=\"width: 100%;min-height: 100%;background-color: transparent;\">\r\n                                 <div class=\"modal-dialog\" >\r\n                                   <div class=\"modal-content\">\r\n                                     <div class=\"modal-header\">\r\n                                       <button type=\"button\" class=\"close\" data-dismiss=\"modal\"   ><span   >&times;</span></button>\r\n                                       <h4 class=\"modal-title\">Contact Us</h4>\r\n                                     </div>\r\n                                     <div class=\"modal-body\">\r\n                                           <div class=\"row\">\r\n                                           <div class=\"col-md-12\">\r\n                                               <form method=\"post\" id=\"contactForm\" (ngSubmit)=\"login(); contactform.reset(); Success()\" #contactform=\"ngForm\" style=\"border:none\">\r\n                                                 <div class=\"row\">\r\n                                                   <div class=\"col-md-6\">\r\n                                                     <div class=\"input-field\">\r\n                                                       <i class=\"fa fa-user prefix fontcolors\"></i>\r\n                                                       <input type=\"text\" name=\"name\" id=\"name\" style=\"height:20px\" [(ngModel)]='name' >\r\n                                                       <label for=\"name\">Name</label>\r\n                                                     </div>\r\n                                                   </div><!-- /.col-md-6 -->\r\n                                                   <div class=\"col-md-6\">\r\n                                                     <div class=\"input-field\">\r\n                                                       <i class=\"fa fa-envelope prefix fontcolors\"></i>\r\n                                                       <input id=\"email\" type=\"email\" name=\"email\" class=\"validate\" style=\"height:20px\"  [(ngModel)]='email'>\r\n                                                       <label for=\"email\" data-error=\"wrong\" data-success=\"right\">Email</label>\r\n                                                     </div>\r\n                                                   </div><!-- /.col-md-6 -->\r\n                                                 </div><!-- /.row -->\r\n\r\n                                                 <div class=\"row\">\r\n                                                   <div class=\"col-md-12\">\r\n                                                     <div class=\"input-field\">\r\n                                                       <i class=\"fa fa-book prefix fontcolors\"></i>\r\n                                                       <input id=\"subject\" type=\"tel\" name=\"subject\" class=\"validate\" style=\"height:20px\" [(ngModel)]='subject' >\r\n                                                       <label for=\"subject\">Subject</label>\r\n                                                     </div>\r\n                                                   </div><!-- /.col-md-6 -->\r\n                                                   </div>\r\n                                                 <div class=\"input-field\">\r\n                                                   <i class=\"fa fa-commenting-o prefix fontcolors\"></i>\r\n                                                   <textarea class=\"form-control\" name=\"message\"  id=\"message\"  [(ngModel)]='message' style=\"border:none\" ></textarea>\r\n                                                   <label for=\"message\">Message</label>\r\n                                                 </div>\r\n                                                 <button type=\"submit\" name=\"submit\" class=\"btn-sm btnclass\"  [disabled]=\"!contactform.form.valid\" >Send Message</button>\r\n                                                 </form>\r\n                                                 </div>\r\n                                     </div>\r\n                                   </div>\r\n                                 </div>\r\n                               </div>\r\n                               </div>\r\n\r\n                       </li>\r\n                   </ul>\r\n\r\n               </div><!-- /.col-md-7 -->\r\n\r\n               <div class=\"col-md-5 mt-sm-30\">\r\n                            <img src=\"assets/img/seo/mobile_application_development_seo_bigperl_new2.png\" alt=\"mobile_application_development_seo_bigperl\" class=\"img-responsive\">\r\n               </div><!-- /.col-md-5 -->\r\n             </div><!-- /.row -->\r\n           </div><!-- /.container -->\r\n       </section>\r\n\r\n\r\n       <section class=\"section-padding\" style=\"padding-top: 10px;\">\r\n           <div class=\"container\">\r\n             <div class=\"text-center mb-80 padding-top-20 ptb-30\">\r\n                 <h2 class=\"section-title text-uppercase\">Why Choose Us</h2>\r\n                 <p class=\"section-sub\">BigPerl Solutions is one of the fastest growing Mobile Application Development company. we are specialised in providing best in class technology and support solution to cater growing business needs.</p>\r\n             </div>\r\n\r\n\r\n               <div class=\"row\">\r\n                   <div class=\"col-md-4 mb-50\">\r\n                       <div class=\"featured-item feature-icon icon-hover icon-hover-brand icon-outline\">\r\n                           <div class=\"icon\">\r\n                               <i class=\"material-icons colored brand-icon\">library_books</i>\r\n                           </div>\r\n                           <div class=\"desc\">\r\n                               <h2>Mobile App Development</h2>\r\n                               <p>Building right app stately is key. Our consultants work with you to create successful statigy.</p>\r\n                           </div>\r\n                       </div><!-- /.featured-item -->\r\n                   </div><!-- /.col-md-4 -->\r\n\r\n                   <div class=\"col-md-4 mb-50\">\r\n                       <div class=\"featured-item feature-icon icon-hover icon-hover-brand icon-outline\">\r\n                           <div class=\"icon\">\r\n                               <i class=\"material-icons colored brand-icon\">equalizer</i>\r\n                           </div>\r\n                           <div class=\"desc\">\r\n                               <h2>Android App Development</h2>\r\n                               <p>We have team of 40+ Native Android developers along with R&D team which works on upcoming technologies like kotlin</p>\r\n                           </div>\r\n                       </div><!-- /.featured-item -->\r\n                   </div><!-- /.col-md-4 -->\r\n\r\n                   <div class=\"col-md-4 mb-50\">\r\n                       <div class=\"featured-item feature-icon icon-hover icon-hover-brand icon-outline\">\r\n                           <div class=\"icon\">\r\n                               <i class=\"material-icons colored brand-icon\">share</i>\r\n                           </div>\r\n                           <div class=\"desc\">\r\n                               <h2>iOS App Development</h2>\r\n                               <p>We have 60+ iOS developers working on Xcode, Swift 3 & Objective-C. We have dedicated R&D team, which experiments new features and releases.</p>\r\n                           </div>\r\n                       </div><!-- /.featured-item -->\r\n                   </div><!-- /.col-md-4 -->\r\n\r\n                   <div class=\"col-md-4 mb-50\">\r\n                       <div class=\"featured-item feature-icon icon-hover icon-hover-brand icon-outline\">\r\n                           <div class=\"icon\">\r\n                               <i class=\"material-icons colored brand-icon\">build</i>\r\n                           </div>\r\n                           <div class=\"desc\">\r\n                               <h2>Windows App Development</h2>\r\n                               <p>We have Dedicated team of xamarin app developers which works on windows native app development.\r\n</p>\r\n                           </div>\r\n                       </div><!-- /.featured-item -->\r\n                   </div><!-- /.col-md-4 -->\r\n\r\n                   <div class=\"col-md-4 mb-50\">\r\n                       <div class=\"featured-item feature-icon icon-hover icon-hover-brand icon-outline\">\r\n                           <div class=\"icon\">\r\n                               <i class=\"material-icons colored brand-icon\">youtube_searched_for</i>\r\n                           </div>\r\n                           <div class=\"desc\">\r\n                               <h2>Hybrid App Development</h2>\r\n                               <p>Hybrid Mobile Apps are picking up well in app stores. We have dedicated team of 25+ developers work on PhoneGap, Ionic & AngularJs frameworks.\r\n</p>\r\n                           </div>\r\n                       </div><!-- /.featured-item -->\r\n                   </div><!-- /.col-md-4 -->\r\n\r\n                   <div class=\"col-md-4 mb-50\">\r\n                       <div class=\"featured-item feature-icon icon-hover icon-hover-brand icon-outline\">\r\n                           <div class=\"icon\">\r\n                               <i class=\"material-icons colored brand-icon\">person</i>\r\n                           </div>\r\n                           <div class=\"desc\">\r\n                               <h2>E-Commerse Web Development</h2>\r\n                               <p>Building E-commerse web is always a key, Our team works on most of leading technologies e.g. : AngularJs, NodeJS, Php , Python, ROR development stack.</p>\r\n                           </div>\r\n                       </div><!-- /.featured-item -->\r\n                   </div><!-- /.col-md-4 -->\r\n             </div><!-- /.row -->\r\n             </div>\r\n       </section>\r\n\r\n       <section class=\"padding-top-20 padding-bottom-10 brand-bg\">\r\n         <div class=\"container\">\r\n           <div class=\"text-center\">\r\n               <button class=\"waves-effect waves-light btn-large blue\" id=\"btnwidth\" data-toggle=\"modal\" data-target=\"#myModals\"><img src=\"assets/img/ico/trending_up_bigperl.png\" alt=\"trending_up_bigperl\" style=\"margin-right: 13px;\">Get free consultation from our Experts <img src=\"assets/img/ico/trending_up_bigperl.png\" alt=\"trending_up_bigperl\" style=\"margin-left: 10px;\"></button>\r\n               <div class=\"modal fade\" id=\"myModals\" tabindex=\"-1\" style=\"width: 100%;min-height: 100%;background-color: transparent;\">\r\n                                 <div class=\"modal-dialog\" >\r\n                                   <div class=\"modal-content\">\r\n                                     <div class=\"modal-header\">\r\n                                       <button type=\"button\" class=\"close\" data-dismiss=\"modal\"   ><span   >&times;</span></button>\r\n                                       <h4 class=\"modal-title\">Contact Us</h4>\r\n                                     </div>\r\n                                     <div class=\"modal-body\">\r\n                                           <div class=\"row\">\r\n                                           <div class=\"col-md-12\">\r\n                                            <form method=\"post\" id=\"contactForm2\" (ngSubmit)=\"login(); contactform.reset(); Success() \" #contactform=\"ngForm\"  >\r\n                                                <div class=\"row\">\r\n                                                    <div class=\"col-md-6\">\r\n                                                      <div class=\"input-field\">\r\n                                                        <i class=\"fa fa-user prefix fontcolors\"></i>\r\n                                                        <input type=\"text\" name=\"name\" id=\"name\" style=\"height:20px\" [(ngModel)]='name' >\r\n                                                        <label for=\"name\">Name</label>\r\n                                                      </div>\r\n                                                    </div><!-- /.col-md-6 -->\r\n                                                    <div class=\"col-md-6\">\r\n                                                      <div class=\"input-field\">\r\n                                                        <i class=\"fa fa-envelope prefix fontcolors\"></i>\r\n                                                        <input id=\"email\" type=\"email\" name=\"email\" class=\"validate\" style=\"height:20px\"  [(ngModel)]='email'>\r\n                                                        <label for=\"email\" data-error=\"wrong\" data-success=\"right\">Email</label>\r\n                                                      </div>\r\n                                                    </div><!-- /.col-md-6 -->\r\n                                                  </div><!-- /.row -->\r\n \r\n                                                  <div class=\"row\">\r\n                                                    <div class=\"col-md-12\">\r\n                                                      <div class=\"input-field\">\r\n                                                        <i class=\"fa fa-book prefix fontcolors\"></i>\r\n                                                        <input id=\"subject\" type=\"tel\" name=\"subject\" class=\"validate\" style=\"height:20px\" [(ngModel)]='subject' >\r\n                                                        <label for=\"subject\">Subject</label>\r\n                                                      </div>\r\n                                                    </div><!-- /.col-md-6 -->\r\n                                                    </div>\r\n                                                  <div class=\"input-field\">\r\n                                                    <i class=\"fa fa-commenting-o prefix fontcolors\"></i>\r\n                                                    <textarea  name=\"message\"  id=\"message\"  [(ngModel)]='message'  ></textarea>\r\n                                                    <label for=\"message\">Message</label>\r\n                                                  </div>\r\n                                                  <button type=\"submit\" name=\"submit\" class=\"btn-sm btnclass\"  [disabled]=\"!contactform.form.valid\" >Send Message</button>\r\n                                                  </form>\r\n                                   \r\n                                           </div>\r\n                                     </div>\r\n                                   </div>\r\n                                 </div>\r\n                               </div>\r\n                               </div>\r\n           </div>\r\n         </div>\r\n       </section>\r\n\r\n\r\n\r\n   \r\n\r\n\r\n\r\n\r\n         <app-footer3></app-footer3>\r\n\r\n\r\n\r\n\r\n\r\n \r\n         \r\n         \r\n         \r\n         \r\n         <section class=\"section-padding\">\r\n            <div class=\"container\">\r\n\r\n              <div class=\"text-center mb-80\">\r\n                  <h1 class=\"text-uppercase\">Our Successful Clients</h1>\r\n                  <p class=\"section-sub\">Our client trust us to help them in building high performing software solutions. We have awesome list of global clients working with us for 5+ years.</p>\r\n              </div>\r\n              <div class=\"clients-grid gutter\">\r\n                <div class=\"row\">\r\n                    <div class=\"owl-carousel owl-theme\" id=\"customers-carousel2\">\r\n                        <div class=\"item active\">\r\n                            <div class=\"border-box\">\r\n                                <a href=\"#\">\r\n                                    <img src=\"assets/img/client-logo/our_clients_akg_bigperl_2.png\" alt=\"our_clients_akg_bigperl\">\r\n                                </a>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"item\">\r\n                            <div class=\"border-box\">\r\n                                <a href=\"#\">\r\n                                    <img src=\"assets/img/client-logo/our_clients_ang_bigperl_2.png\" alt=\"our_clients_ang_bigperl\">\r\n                                </a>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"item\">\r\n                            <div class=\"border-box\">\r\n                                <a href=\"#\">\r\n                                    <img src=\"assets/img/client-logo/our_clients_archi_bigperl_2.png\" alt=\"our_clients_archi_bigperl\">\r\n                                </a>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"item\">\r\n                            <div class=\"border-box\">\r\n                                <a href=\"#\">\r\n                                    <img src=\"assets/img/client-logo/our_clients_arfma_bigperl_2.png\" alt=\"our_clients_arfma_bigperl\">\r\n                                </a>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"item\">\r\n                            <div class=\"border-box\">\r\n                                <a href=\"#\">\r\n                                    <img src=\"assets/img/client-logo/our_clients_barma_bigperl_2.jpg\" alt=\"our_clients_barma_bigperl\">\r\n                                </a>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"item\">\r\n                            <div class=\"border-box\">\r\n                                <a href=\"#\">\r\n                                    <img src=\"assets/img/client-logo/our_clients_ccat_bigperl_2.png\" alt=\"our_clients_ccat_bigperl\">\r\n                                </a>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"item\">\r\n                            <div class=\"border-box\">\r\n                                <a href=\"#\">\r\n                                    <img src=\"assets/img/client-logo/our_clients_kvrc_bigperl_2.png\" alt=\"our_clients_kvrc_bigperl\">\r\n                                </a>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"item\">\r\n                            <div class=\"border-box\">\r\n                                <a href=\"#\">\r\n                                    <img src=\"assets/img/client-logo/our_clients_fashion_bigperl_2.png\" alt=\"our_clients_fashion_bigperl\">\r\n                                </a>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"item\">\r\n                            <div class=\"border-box\">\r\n                                <a href=\"#\">\r\n                                    <img src=\"assets/img/client-logo/our_clients_gp_bigperl_2.png\" alt=\"our_clients_gp_bigperl\">\r\n                                </a>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"item\">\r\n                            <div class=\"border-box\">\r\n                                <a href=\"#\">\r\n                                    <img src=\"assets/img/client-logo/our_clients_he_bigperl_2.png\" alt=\"he_bigperl\">\r\n                                </a>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"item\">\r\n                            <div class=\"border-box\">\r\n                                <a href=\"#\">\r\n                                    <img src=\"assets/img/client-logo/our_clients_jms_bigperl_2.png\" alt=\"our_clients_jms_bigperl\">\r\n                                </a>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"item\">\r\n                            <div class=\"border-box\">\r\n                                <a href=\"#\">\r\n                                    <img src=\"assets/img/client-logo/our_clients_kuch_bigperl_2.png\" alt=\"our_clients_kuch_bigperl\">\r\n                                </a>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"item\">\r\n                            <div class=\"border-box\">\r\n                                <a href=\"#\">\r\n                                    <img src=\"assets/img/client-logo/our_clients_kvrc_bigperl_2.png\" alt=\"our_clients_kvrc_bigperl\">\r\n                                </a>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div><!-- /.row -->\r\n              </div><!-- /.clients-grid -->\r\n            </div><!-- /.container -->\r\n        </section><br>\r\n\r\n          <div class=\"padding-top-20 padding-bottom-10 brand-bg\">\r\n            <div class=\"text-center\">\r\n                <form method=\"post\" id=\"contactForm2\" (ngSubmit)=\"subscribe(); subscribeform.reset(); Info() \" #subscribeform=\"ngForm\">\r\n                    <input type=\"email\" name=\"email\" placeholder=\"Subscribe to Newsletter\" id=\"inputs\" style=\"height:10px\" [(ngModel)]='email' >\r\n                    <button class=\"btn btn-sm indigo\" name=\"submit\" id=\"inputbtnstyle\"  [disabled]=\"!subscribeform.form.valid\" ><i class=\"material-icons\">send</i></button>\r\n                </form>\r\n            </div>\r\n          </div><!-- /.container -->\r\n\r\n        <section class=\"section-padding featured-list-news\">\r\n            <div class=\"container\">\r\n\r\n              <div class=\"text-center mb-80 padding-top-20\">\r\n                  <h2 class=\"section-title text-uppercase\">Latest Blog</h2>\r\n                  <p class=\"section-sub\">We are living in digital world. Most of business already understood and adopted the digital curve. This starts from paperless offices till live monintiering of crops using mobile app and iOT solutions. </p>\r\n              </div>\r\n\r\n              <div class=\"row\">\r\n                <div class=\"col-md-6\">\r\n                  <div class=\"post-wrapper featured-news\">\r\n\r\n                    <div class=\"thumb-wrapper waves-effect waves-block waves-light\">\r\n                      <a href=\"#\"><img src=\"assets/img/blog/roleofmobileapp_bigperl_2.jpeg\" class=\"img-responsive\" alt=\"blog_12_bigperl\" style=\"height:228px;width:557px;\"></a>\r\n                    </div><!-- .post-thumb -->\r\n\r\n                    <div class=\"blog-content\">\r\n                      <header class=\"entry-header-wrapper\">\r\n                        <div class=\"entry-header\">\r\n                          <h2 class=\"entry-title\"><a href=\"#\">Role of mobile apps in business functions</a></h2>\r\n\r\n                          <div class=\"entry-meta\">\r\n                              <ul class=\"list-inline\">\r\n                                  <li>\r\n                                      By <a href=\"#\">Kiran</a>\r\n                                  </li>\r\n\r\n                                  <li>\r\n                                      In <a href=\"#\">Technology</a>\r\n                                  </li>\r\n\r\n                                  <li>\r\n                                      At <a href=\"#\">2018</a>\r\n                                  </li>\r\n                              </ul>\r\n                          </div><!-- .entry-meta -->\r\n                        </div>\r\n                      </header><!-- /.entry-header-wrapper -->\r\n\r\n                      <div class=\"entry-content\">\r\n                        <p>Mobile apps have revolutionized the way businesses function. On the consumer level, they have allowed us to break down the barriers of boundaries and stay connected from anywhere in the world. Now you might have noticed that many apps ask you for your location.</p>\r\n\r\n                        <a routerLink=\"/location-based-technology-for-mobile-app\" class=\"readmore\">Read Full Post <i class=\"fa fa-long-arrow-right\"></i></a>\r\n                      </div><!-- .entry-content -->\r\n                    </div>\r\n\r\n                </div><!-- /.post-wrapper -->\r\n                </div>\r\n                <div class=\"col-md-6\">\r\n\r\n                    <div class=\"post-wrapper list-article\">\r\n\r\n                      <div class=\"hover-overlay brand-bg\"></div>\r\n\r\n                      <div class=\"thumb-wrapper waves-effect waves-block waves-light\">\r\n                        <a href=\"#\"><img src=\"assets/img/blog/appindexing_bigperl_2.jpeg\" class=\"img-responsive\" alt=\"blog_22_bigperl\" style=\"height:220px;width:180px\" ></a>\r\n                      </div><!-- .post-thumb -->\r\n\r\n                      <div class=\"blog-content\">\r\n                        <header class=\"entry-header-wrapper\">\r\n                          <div class=\"entry-header\">\r\n                            <h2 class=\"entry-title\"><a href=\"#\">How App Indexing and Deep linking</a></h2>\r\n\r\n                            <div class=\"entry-meta\">\r\n                                <ul class=\"list-inline\">\r\n                                    <li>\r\n                                        By <a href=\"#\">Jams</a>\r\n                                    </li>\r\n\r\n                                    <li>\r\n                                        In <a href=\"#\">Technology</a>\r\n                                    </li>\r\n\r\n                                    <li>\r\n                                        At <a href=\"#\">Feb,2018</a>\r\n                                    </li>\r\n                                </ul>\r\n                            </div><!-- .entry-meta -->\r\n                          </div><!-- /.entry-header -->\r\n                        </header><!-- /.entry-header-wrapper -->\r\n\r\n                        <div class=\"entry-content\">\r\n                          <p>There are millions of apps present in Google’s Playstore and in Apple’s iTune. All these apps aim for maximum downloads by providing rich content.</p>\r\n                          <a routerLink=\"/how-indexing-and-deep-linking-help\" class=\"readmore\">Read Full Post <i class=\"fa fa-long-arrow-right\"></i></a>\r\n                        </div><!-- .entry-content -->\r\n                      </div>\r\n\r\n                    </div><!-- /.post-wrapper -->\r\n\r\n                    <div class=\"post-wrapper list-article\">\r\n\r\n                      <div class=\"hover-overlay brand-bg\"></div>\r\n                       \r\n\r\n                      <div class=\"thumb-wrapper thumb-wrap3 waves-effect waves-block waves-light\">\r\n                        <a href=\"#\"><img src=\"assets/img/blog/xamarian_img_bigperl_2.jpeg\" class=\"img-responsive\" alt=\"blog_23_bigperl\" style=\"height:220px;width:180px\"></a>\r\n                      </div><!-- .post-thumb -->\r\n\r\n                      <div class=\"blog-content\">\r\n                        <header class=\"entry-header-wrapper\">\r\n                          <div class=\"entry-header\">\r\n                            <h2 class=\"entry-title\"><a href=\"#\">Cross Platform Mobile Application Development with Xamarin</a></h2>\r\n\r\n                            <div class=\"entry-meta\">\r\n                                <ul class=\"list-inline\">\r\n                                    <li>\r\n                                        By <a href=\"#\">BigPerl</a>\r\n                                    </li>\r\n\r\n                                    <li>\r\n                                        In <a href=\"#\">Technology</a>\r\n                                    </li>\r\n\r\n                                    <li>\r\n                                        At <a href=\"#\">Jan 15, 2016</a>\r\n                                    </li>\r\n                                </ul>\r\n                            </div><!-- .entry-meta -->\r\n                          </div><!-- /.entry-header -->\r\n                        </header><!-- /.entry-header-wrapper -->\r\n\r\n                        <div class=\"entry-content\">\r\n                          <p>Xamarin is a cross-platform tool used for the purpose of mobile application development.</p>\r\n                          <a routerLink=\"/xamarin-development\" class=\"readmore\">Read Full Post <i class=\"fa fa-long-arrow-right\"></i></a>\r\n                        </div><!-- .entry-content -->\r\n                      </div>\r\n\r\n                    </div><!-- /.post-wrapper -->\r\n                    <!-- <a id=\"back2Top\" title=\"Back to top\" href=\"#\">&#10148;</a> -->\r\n                </div><!-- /.col-md-6 -->\r\n              </div><!-- /.row -->\r\n             \r\n            </div><!-- /.container -->\r\n           \r\n\r\n\r\n        </section>\r\n      "

/***/ }),

/***/ "./src/app/home1/home1.component.ts":
/*!******************************************!*\
  !*** ./src/app/home1/home1.component.ts ***!
  \******************************************/
/*! exports provided: Home1Component */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Home1Component", function() { return Home1Component; });
/* harmony import */ var _toaster_service_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./../toaster-service.service */ "./src/app/toaster-service.service.ts");
/* harmony import */ var _subscribe_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../subscribe.service */ "./src/app/subscribe.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _user_model__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../user.model */ "./src/app/user.model.ts");
/* harmony import */ var _login_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../login.service */ "./src/app/login.service.ts");
/* harmony import */ var _subscribe_model__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../subscribe.model */ "./src/app/subscribe.model.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var Home1Component = /** @class */ (function () {
    function Home1Component(router, _loginService, _subscribeService, toasterService) {
        this.router = router;
        this._loginService = _loginService;
        this._subscribeService = _subscribeService;
        this.toasterService = toasterService;
        this.name = "";
        this.email = "";
        this.subject = "";
        this.message = "";
    }
    Home1Component.prototype.Success = function () {
        this.toasterService.Success("Your message has been sent successfully.");
    };
    Home1Component.prototype.Info = function () {
        this.toasterService.Info("Subscribed");
    };
    Home1Component.prototype.ngOnInit = function () {
        jQuery(document).ready(function () { jQuery(".materialize-slider").revolution({ sliderType: "standard", sliderLayout: "fullwidth", delay: 8000, navigation: { keyboardNavigation: "on", keyboard_direction: "horizontal", mouseScrollNavigation: "off", onHoverStop: "on", touch: { touchenabled: "on", swipe_threshold: 75, swipe_min_touches: 1, swipe_direction: "horizontal", drag_block_vertical: !1 }, arrows: { style: "gyges", enable: !0, hide_onmobile: !1, hide_onleave: !0, tmp: '', left: { h_align: "left", v_align: "center", h_offset: 10, v_offset: 0 }, right: { h_align: "right", v_align: "center", h_offset: 10, v_offset: 0 } } }, responsiveLevels: [1240, 1024, 778, 480], gridwidth: [1140, 1024, 778, 480], gridheight: [400, 300, 500, 500], disableProgressBar: "on", parallax: { type: "mouse", origo: "slidercenter", speed: 2000, levels: [2, 3, 4, 5, 6, 7, 12, 16, 10, 50], } }); });
        //   /*Scroll to top when arrow up clicked BEGIN*/
        // $(window).scroll(function() {
        //     var height = $(window).scrollTop();
        //     if (height > 1000) {
        //         $('#back2Top').fadeIn();
        //     } else {
        //         $('#back2Top').fadeOut();
        //     }
        // });
        // $(document).ready(function() {
        //     $("#back2Top").click(function(event) {
        //         event.preventDefault();
        //         $("html, body").animate({ scrollTop: 0 }, "slow");
        //         return false;
        //     });
        // });
        //  /*Scroll to top when arrow up clicked END*/
    };
    Home1Component.prototype.login = function () {
        var _this = this;
        var user = new _user_model__WEBPACK_IMPORTED_MODULE_4__["User"]('', '', '', '');
        user.name = this.name;
        user.email = this.email;
        user.subject = this.subject;
        user.message = this.message;
        var data = [
            { 'name': user.name, 'email': user.email, 'subject': user.subject, 'message': user.message }
        ];
        this._loginService.sendLogin({ data: data })
            .subscribe(function (response) { return _this.handleResponse(response); }, function (error) { return _this.handleResponse(error); });
    };
    // handleResponse(response) {
    //   if (response.success) {
    //     console.log("success");
    //   } else if (response.error) {
    //     console.log("errror");
    //   } else {
    //   }
    // }
    Home1Component.prototype.subscribe = function () {
        var _this = this;
        var subscribe = new _subscribe_model__WEBPACK_IMPORTED_MODULE_6__["Subscribe"]('');
        subscribe.email = this.email;
        var data = [
            { 'email': subscribe.email }
        ];
        this._subscribeService.sendSubscribe({ data: data })
            .subscribe(function (response) { return _this.handleResponse(response); }, function (error) { return _this.handleResponse(error); });
        console.log(data);
    };
    Home1Component.prototype.handleResponse = function (response) {
        if (response.success) {
            console.log("success");
        }
        else if (response.error) {
            console.log("errror");
        }
        else {
        }
    };
    Home1Component = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-home1',
            template: __webpack_require__(/*! ./home1.component.html */ "./src/app/home1/home1.component.html"),
            styles: [__webpack_require__(/*! ./home1.component.css */ "./src/app/home1/home1.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _login_service__WEBPACK_IMPORTED_MODULE_5__["LoginService"], _subscribe_service__WEBPACK_IMPORTED_MODULE_1__["SubscribeService"],
            _toaster_service_service__WEBPACK_IMPORTED_MODULE_0__["ToasterService"]])
    ], Home1Component);
    return Home1Component;
}());



/***/ }),

/***/ "./src/app/login.service.ts":
/*!**********************************!*\
  !*** ./src/app/login.service.ts ***!
  \**********************************/
/*! exports provided: LoginService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginService", function() { return LoginService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var rxjs_add_operator_catch__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/add/operator/catch */ "./node_modules/rxjs-compat/_esm5/add/operator/catch.js");
/* harmony import */ var rxjs_add_operator_map__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/add/operator/map */ "./node_modules/rxjs-compat/_esm5/add/operator/map.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var LoginService = /** @class */ (function () {
    function LoginService(_http) {
        this._http = _http;
        this._contactUrl = 'https://localhost/webservices/index.php/api/contact';
    }
    LoginService.prototype.sendLogin = function (value) {
        var body = new _angular_http__WEBPACK_IMPORTED_MODULE_1__["URLSearchParams"](value);
        body.set('name', value.data[0].name);
        body.set('email', value.data[0].email);
        body.set('subject', value.data[0].subject);
        body.set('message', value.data[0].message);
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_1__["Headers"]();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        return this._http.post(this._contactUrl, body, {
            headers: headers
        }).map(function (res) { return res.json(); });
    };
    LoginService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_1__["Http"]])
    ], LoginService);
    return LoginService;
}());



/***/ }),

/***/ "./src/app/portfolio/portfolio.component.css":
/*!***************************************************!*\
  !*** ./src/app/portfolio/portfolio.component.css ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/portfolio/portfolio.component.html":
/*!****************************************************!*\
  !*** ./src/app/portfolio/portfolio.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "  <!--page title start-->\r\n  <section class=\"page-title ptb-30\">\r\n    <div class=\"container padding-top-40\">\r\n        <div class=\"row\">\r\n            <div class=\"col-md-12\">\r\n                <h6 class=\"pagetitles\">Portfolio</h6>\r\n                <ol class=\"breadcrumb\">\r\n                    <li><a href=\"#\">Home</a></li>\r\n                    <li><a href=\"#\">portfolio</a></li>\r\n                </ol>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</section>\r\n<!--page title end-->\r\n<section class=\"section-padding\">\r\n  <div class=\"container\">\r\n    <div class=\"text-center mb-10\">\r\n        <h2 class=\"section-title text-uppercase\">Works</h2>\r\n        <p class=\"section-sub\">Quisque non erat mi. Etiam congue et augue sed tempus. Aenean sed ipsum luctus, scelerisque ipsum nec, iaculis justo. Sed at vestibulum purus, sit amet vived at vestibulum purus erra at vestibulum purus diam. Nulla ac nisi rhoncus,</p>\r\n    </div>\r\n\r\n\r\n    <div class=\"portfolio-container text-center\">\r\n        <ul class=\"portfolio-filter brand-filter\">\r\n            <li class=\"active waves-effect waves-light\" data-group=\"all\">All</li>\r\n            <li class=\"waves-effect waves-light\" data-group=\"websites\">Websites</li>\r\n            <li class=\"waves-effect waves-light\" data-group=\"branding\">Branding</li>\r\n            <li class=\"waves-effect waves-light\" data-group=\"marketing\">Marketing</li>\r\n            <li class=\"waves-effect waves-light\" data-group=\"photography\">Photography</li>\r\n        </ul>\r\n\r\n\r\n        <div class=\"portfolio col-4 mtb-30 gutter\">\r\n            <!-- remove \"gutter\" class for no spacing -->\r\n\r\n            <div class=\"portfolio-item\" data-groups='[\"all\", \"branding\", \"photography\"]'>\r\n                <div class=\"portfolio-wrapper\">\r\n\r\n                  <div class=\"thumb\">\r\n                      <div class=\"bg-overlay\"></div>\r\n\r\n                      <img src=\"assets/img/portfolio/portfolio_1_bigperl.jpg\" alt=\"portfolio_1_bigperl\">\r\n\r\n                      <div class=\"portfolio-intro\">\r\n                        <div class=\"action-btn\">\r\n                          <a href=\"assets/img/portfolio/portfolio_5_bigperl.jpg\" class=\"tt-lightbox\" title=\"\"><i class=\"fa fa-search\"></i></a>\r\n                        </div>\r\n                        <h2><a href=\"#\">Portfolio Title</a></h2>\r\n                        <p><a href=\"#\">Branding</a></p>\r\n                      </div>\r\n\r\n                  </div><!-- thumb -->\r\n\r\n                </div><!-- /.portfolio-wrapper -->\r\n            </div><!-- /.portfolio-item -->\r\n\r\n            <div class=\"portfolio-item\" data-groups='[\"all\", \"marketing\", \"websites\"]'> \r\n              <div class=\"portfolio-wrapper\">\r\n\r\n                <div class=\"thumb\">\r\n                    <div class=\"bg-overlay\"></div>\r\n                    <img src=\"assets/img/portfolio/portfolio_2_bigperl.jpg\" alt=\"portfolio_2_bigperl\">\r\n\r\n                    <div class=\"portfolio-intro\">\r\n                      <div class=\"action-btn\">\r\n                        <a href=\"#\"> <i class=\"fa fa-link\"></i> </a>\r\n                      </div>\r\n                      <h2><a href=\"#\">Portfolio Title</a></h2>\r\n                      <p><a href=\"#\">Marketing</a></p>\r\n                    </div>\r\n                </div><!-- thumb -->\r\n\r\n              </div><!-- /.portfolio-wrapper -->\r\n            </div><!-- /.portfolio-item -->\r\n\r\n            <div class=\"portfolio-item\" data-groups='[\"all\", \"photography\", \"branding\"]'>\r\n\r\n              <div class=\"portfolio-wrapper\">\r\n                <div class=\"thumb\">\r\n                    <div class=\"bg-overlay\"></div>\r\n\r\n                    <div class=\"portfolio-slider\" data-direction=\"vertical\">\r\n                        <ul class=\"slides\">\r\n                            <li>\r\n                                <a href=\"assets/img/portfolio/portfolio_9_bigperl.jpg\" title=\"materialize Unique Design\">\r\n                                  <img src=\"assets/img/portfolio/portfolio_9_bigperl.jpg\" alt=\"portfolio_9_bigperl\">\r\n                                </a>\r\n                            </li>\r\n                             <li>\r\n                                <a href=\"assets/img/portfolio/portfolio_8_bigperl.jpg\" title=\"materialize Awesome Template\">\r\n                                  <img src=\"assets/img/portfolio/portfolio_8_bigperl.jpg\" alt=\"portfolio_8_bigperl\">\r\n                                </a>\r\n                            </li>\r\n                            <li>\r\n                                <a href=\"assets/img/portfolio/portfolio_7_bigperl.jpg\" title=\"materialize Clean and Elegant\">\r\n                                  <img src=\"assets/img/portfolio/portfolio_7_bigperl.jpg\" alt=\"portfolio_7_bigperl\">\r\n                                </a>\r\n                            </li>\r\n                        </ul>\r\n                    </div>\r\n\r\n                    <div class=\"portfolio-intro\">\r\n                      <div class=\"action-btn\">\r\n                          <a href=\"#\"> <i class=\"fa fa-search\"></i>  </a>\r\n                      </div>\r\n                      <h2><a href=\"#\">Portfolio Title</a></h2>\r\n                      <p><a href=\"#\">Gallery</a></p>\r\n                    </div>\r\n\r\n                </div><!-- thumb -->\r\n\r\n              </div><!-- /.portfolio-wrapper -->\r\n            </div><!-- /.portfolio-item -->\r\n\r\n            <div class=\"portfolio-item\" data-groups='[\"all\", \"websites\", \"branding\"]'>\r\n              <div class=\"portfolio-wrapper\">\r\n                <div class=\"thumb\">\r\n                    <div class=\"bg-overlay\"></div>\r\n                    <img src=\"assets/img/portfolio/portfolio_4_bigperl.jpg\" alt=\"portfolio_4_bigperl\">\r\n\r\n                    <div class=\"portfolio-intro\">\r\n                      <div class=\"action-btn\">\r\n                          <a class=\"popup-video\" href=\"https://www.youtube.com/watch?v=XVfOe5mFbAE\"> <i class=\"fa fa-play\"></i>  </a>\r\n                      </div>\r\n                      <h2><a href=\"#\">Portfolio Title</a></h2>\r\n                      <p><a href=\"#\">Video</a></p>\r\n                    </div>\r\n                </div><!-- thumb -->\r\n\r\n              </div><!-- /.portfolio-wrapper -->\r\n            </div><!-- /.portfolio-item -->\r\n\r\n            <div class=\"portfolio-item\" data-groups='[\"all\", \"photography\", \"marketing\"]'>\r\n              <div class=\"portfolio-wrapper\">\r\n                <div class=\"thumb\">\r\n                    <div class=\"bg-overlay\"></div>\r\n                    <img src=\"assets/img/portfolio/portfolio_5_bigperl.jpg\" alt=\"portfolio_5_bigperl\">\r\n\r\n                    <div class=\"portfolio-intro\">\r\n                      <div class=\"action-btn\">\r\n                        <a href=\"assets/img/portfolio/portfolio_5_bigperl.jpg\" class=\"tt-lightbox\" title=\"\"> <i class=\"fa fa-search\"></i> </a>\r\n                      </div>\r\n                      <h2><a href=\"#\">Portfolio Title</a></h2>\r\n                      <p><a href=\"#\">Branding</a></p>\r\n                    </div>\r\n                </div><!-- /.thumb -->\r\n\r\n              </div><!-- /.portfolio-wrapper -->\r\n            </div><!-- /.portfolio-item -->\r\n\r\n            <div class=\"portfolio-item\" data-groups='[\"all\", \"websites\",  \"marketing\"]'>\r\n              <div class=\"portfolio-wrapper\">\r\n                <div class=\"thumb\">\r\n                    <div class=\"bg-overlay\"></div>\r\n                    <img src=\"assets/img/portfolio/portfolio_6_bigperl.jpg\" alt=\"portfolio_6_bigperl\">\r\n\r\n                    <div class=\"portfolio-intro\">\r\n                      <div class=\"action-btn\">\r\n                        <a href=\"#\"> <i class=\"fa fa-link\"></i></a>\r\n                      </div>\r\n                      <h2><a href=\"#\">Portfolio Title</a></h2>\r\n                      <p><a href=\"#\">Marketing</a></p>\r\n                    </div>\r\n                </div><!-- /.thumb -->\r\n\r\n              </div><!-- /.portfolio-wrapper -->\r\n            </div><!-- /.portfolio-item -->\r\n\r\n            <div class=\"portfolio-item\" data-groups='[\"all\", \"websites\", \"photography\"]'>\r\n                <div class=\"portfolio-wrapper\">\r\n\r\n                  <div class=\"thumb\">\r\n                      <div class=\"bg-overlay\"></div>\r\n\r\n                      <img src=\"assets/img/portfolio/portfolio_7_bigperl.jpg\" alt=\"portfolio_7_bigperl\">\r\n\r\n                      <div class=\"portfolio-intro\">\r\n                        <div class=\"action-btn\">\r\n                          <a href=\"assets/img/portfolio/portfolio_7_bigperl.jpg\" class=\"tt-lightbox\" title=\"\"><i class=\"fa fa-search\"></i></a>\r\n                        </div>\r\n                        <h2><a href=\"#\">Portfolio Title</a></h2>\r\n                        <p><a href=\"#\">Branding</a></p>\r\n                      </div>\r\n\r\n                  </div><!-- thumb -->\r\n\r\n                </div><!-- /.portfolio-wrapper -->\r\n            </div><!-- /.portfolio-item -->\r\n\r\n            <div class=\"portfolio-item\" data-groups='[\"all\", \"branding\", \"marketing\"]'>\r\n                <div class=\"portfolio-wrapper\">\r\n\r\n                  <div class=\"thumb\">\r\n                      <div class=\"bg-overlay\"></div>\r\n\r\n                      <img src=\"assets/img/portfolio/portfolio_8_bigperl.jpg\" alt=\"portfolio_8_bigperl\">\r\n\r\n                      <div class=\"portfolio-intro\">\r\n                        <div class=\"action-btn\">\r\n                          <a href=\"assets/img/portfolio/portfolio_6_bigperl.jpg\" class=\"tt-lightbox\" title=\"\"><i class=\"fa fa-search\"></i></a>\r\n                        </div>\r\n                        <h2><a href=\"#\">Portfolio Title</a></h2>\r\n                        <p><a href=\"#\">Branding</a></p>\r\n                      </div>\r\n\r\n                  </div><!-- thumb -->\r\n\r\n                </div><!-- /.portfolio-wrapper -->\r\n            </div><!-- /.portfolio-item -->\r\n            \r\n        </div><!-- /.portfolio -->\r\n\r\n    </div><!-- portfolio-container -->\r\n  </div>\r\n</section>\r\n\r\n\r\n"

/***/ }),

/***/ "./src/app/portfolio/portfolio.component.ts":
/*!**************************************************!*\
  !*** ./src/app/portfolio/portfolio.component.ts ***!
  \**************************************************/
/*! exports provided: PortfolioComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PortfolioComponent", function() { return PortfolioComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var PortfolioComponent = /** @class */ (function () {
    function PortfolioComponent() {
    }
    PortfolioComponent.prototype.ngOnInit = function () {
        "use strict";
        $(document).ready(function () { $(".portfolio-container").each(function (e, t) { var i = $(this).find(".portfolio"), o = this; i.shuffle({ itemSelector: ".portfolio-item" }), $(this).find(".portfolio-filter li").on("click", function (e) { e.preventDefault(), $(o).find(".portfolio-filter li").removeClass("active"), $(this).addClass("active"); var t = $(this).attr("data-group"); i.shuffle("shuffle", t); }); }), $(".portfolio-slider").length > 0 && $(".portfolio-wrapper").each(function (e, t) { var i = $(this).find(".portfolio-slider"), o = i.attr("data-direction"); i.flexslider({ animation: "slide", direction: o, slideshowSpeed: 3e3, start: function () { imagesLoaded($(".portfolio"), function () { setTimeout(function () { $(".portfolio-filter li:eq(0)").trigger("click"); }, 500); }); } }); }), $(".portfolio-slider").each(function () { for (var e = $(this).find("li > a"), t = [], i = 0; i < e.length; i++)
            t.push({ src: $(e[i]).attr("href"), title: $(e[i]).attr("title") }); $(this).parent().find(".action-btn").magnificPopup({ items: t, type: "image", mainClass: "mfp-fade", removalDelay: 160, fixedContentPos: !1, gallery: { enabled: !0 } }); }); });
    };
    PortfolioComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-portfolio',
            template: __webpack_require__(/*! ./portfolio.component.html */ "./src/app/portfolio/portfolio.component.html"),
            styles: [__webpack_require__(/*! ./portfolio.component.css */ "./src/app/portfolio/portfolio.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], PortfolioComponent);
    return PortfolioComponent;
}());



/***/ }),

/***/ "./src/app/servicessection/adf-development/adf-development.component.css":
/*!*******************************************************************************!*\
  !*** ./src/app/servicessection/adf-development/adf-development.component.css ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/servicessection/adf-development/adf-development.component.html":
/*!********************************************************************************!*\
  !*** ./src/app/servicessection/adf-development/adf-development.component.html ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n\r\n\r\n        <!--page title start-->\r\n        <section class=\"page-title ptb-30\">\r\n            <div class=\"container padding-top-40\">\r\n                <div class=\"row\">\r\n                    <div class=\"col-md-12\">\r\n                        <h6 class=\"pagetitles\">Services</h6>\r\n                        <ol class=\"breadcrumb\">\r\n                            <li><a href=\"#\">Home</a></li>\r\n                            <li><a href=\"#\">Services</a></li>\r\n                            <li class=\"active\">adf developer</li>\r\n                        </ol>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </section>\r\n        <!--page title end-->\r\n\r\n\t\t    <img src=\"assets/img/banners/adf_and_webcenter_developer_bigperl_banner.png\" alt=\"adf_and_webcenter_developer_bigperl_banner\" class=\"fullwidth\">\r\n\r\n              <div class=\"container padding-top-20\">\r\n                  <div class=\"col-md-7\">\r\n                      <h2 class=\"text-bold mb-10\">ADF Developer</h2>\r\n                      <img src=\"assets/img/adf_architecture_bigperl.jpg\" class=\"img-responsive \" alt=\"adf_architecture_bigperl\">\r\n                  </div>\r\n\r\n                  <div class=\"col-md-5\">\r\n                      <img src=\"assets/img/services/adf_developer_Bigperl.png\" class=\"img-responsive \" alt=\"adf_developer_Bigperl\">\r\n                  </div>\r\n            \r\n            </div><!-- /.container -->\r\n        <!-- </section> -->\r\n\r\n        <app-footer5></app-footer5>\r\n           \r\n        <app-footer2></app-footer2>"

/***/ }),

/***/ "./src/app/servicessection/adf-development/adf-development.component.ts":
/*!******************************************************************************!*\
  !*** ./src/app/servicessection/adf-development/adf-development.component.ts ***!
  \******************************************************************************/
/*! exports provided: AdfDevelopmentComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdfDevelopmentComponent", function() { return AdfDevelopmentComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AdfDevelopmentComponent = /** @class */ (function () {
    function AdfDevelopmentComponent() {
    }
    AdfDevelopmentComponent.prototype.ngOnInit = function () {
    };
    AdfDevelopmentComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-adf-development',
            template: __webpack_require__(/*! ./adf-development.component.html */ "./src/app/servicessection/adf-development/adf-development.component.html"),
            styles: [__webpack_require__(/*! ./adf-development.component.css */ "./src/app/servicessection/adf-development/adf-development.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], AdfDevelopmentComponent);
    return AdfDevelopmentComponent;
}());



/***/ }),

/***/ "./src/app/servicessection/adf-webcenter-development/adf-webcenter-development.component.css":
/*!***************************************************************************************************!*\
  !*** ./src/app/servicessection/adf-webcenter-development/adf-webcenter-development.component.css ***!
  \***************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/servicessection/adf-webcenter-development/adf-webcenter-development.component.html":
/*!****************************************************************************************************!*\
  !*** ./src/app/servicessection/adf-webcenter-development/adf-webcenter-development.component.html ***!
  \****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n        <!--page title start-->\r\n        <section class=\"page-title ptb-30\">\r\n          <div class=\"container padding-top-40\">\r\n              <div class=\"row\">\r\n                  <div class=\"col-md-12\">\r\n                      <h6 class=\"pagetitles\">Services</h6>\r\n                      <ol class=\"breadcrumb\">\r\n                          <li><a href=\"#\">Home</a></li>\r\n                          <li><a href=\"#\">Services</a></li>\r\n                          <li class=\"active\">adf and webcenter development</li>\r\n                      </ol>\r\n                  </div>\r\n              </div>\r\n          </div>\r\n      </section>\r\n      <!--page title end-->\r\n\r\n      <img src=\"assets/img/banners/adf_and_webcenter_development_bigperl_banner.png\" alt=\"adf_and_webcenter_development_bigperl_banner\" class=\"fullwidth\">\r\n\r\n            <div class=\"container padding-top-20\">\r\n                <div class=\"col-md-7\">\r\n                    <h2 class=\"text-bold mb-10\">ADF and Webcenter Development</h2>\r\n                    <p class=\"text-justified\">Built on top of the MVC-based JavaServer Faces framework, Oracle Application Development Framework (ADF) forms the foundation for WebCenter Portal’s components and services. ADF is an innovative, yet mature Java EE development framework available from Oracle, and, unlike most other frameworks, is directly supported and enabled by the award winning development environment, Oracle JDeveloper 11g.</p>\r\n                    <p class=\"text-justified\">We at Bigperl dedicatedly work on various verticals of ADF and Webcebter development to deliver the business solution to enterprises of all the sizes. Talented pool of programmers at our company is enriched with several years of experience in ADF development We are adroit at delivering remarkable and competitive solutions to precisely in line with clients’ needs.</p>\r\n                </div>\r\n\r\n                <div class=\"col-md-5\">\r\n                    <img src=\"assets/img/services/ADF_Web_development_bigperl.png\" class=\"img-responsive \" alt=\"ADF_Web_development_bigperl\">\r\n                </div>\r\n            </div>\r\n\r\n            <app-footer5></app-footer5>\r\n           \r\n            <app-footer2></app-footer2>"

/***/ }),

/***/ "./src/app/servicessection/adf-webcenter-development/adf-webcenter-development.component.ts":
/*!**************************************************************************************************!*\
  !*** ./src/app/servicessection/adf-webcenter-development/adf-webcenter-development.component.ts ***!
  \**************************************************************************************************/
/*! exports provided: AdfWebcenterDevelopmentComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdfWebcenterDevelopmentComponent", function() { return AdfWebcenterDevelopmentComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AdfWebcenterDevelopmentComponent = /** @class */ (function () {
    function AdfWebcenterDevelopmentComponent() {
    }
    AdfWebcenterDevelopmentComponent.prototype.ngOnInit = function () {
    };
    AdfWebcenterDevelopmentComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-adf-webcenter-development',
            template: __webpack_require__(/*! ./adf-webcenter-development.component.html */ "./src/app/servicessection/adf-webcenter-development/adf-webcenter-development.component.html"),
            styles: [__webpack_require__(/*! ./adf-webcenter-development.component.css */ "./src/app/servicessection/adf-webcenter-development/adf-webcenter-development.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], AdfWebcenterDevelopmentComponent);
    return AdfWebcenterDevelopmentComponent;
}());



/***/ }),

/***/ "./src/app/servicessection/android-app-development/android-app-development.component.css":
/*!***********************************************************************************************!*\
  !*** ./src/app/servicessection/android-app-development/android-app-development.component.css ***!
  \***********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/servicessection/android-app-development/android-app-development.component.html":
/*!************************************************************************************************!*\
  !*** ./src/app/servicessection/android-app-development/android-app-development.component.html ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n        <!--page title start-->\r\n        <section class=\"page-title ptb-30\">\r\n          <div class=\"container padding-top-40\">\r\n              <div class=\"row\">\r\n                  <div class=\"col-md-12\">\r\n                      <h6 class=\"pagetitles\">Services</h6>\r\n                      <ol class=\"breadcrumb\">\r\n                          <li><a href=\"#\">Home</a></li>\r\n                          <li><a href=\"#\">Services</a></li>\r\n                          <li class=\"active\">android application development</li>\r\n                      </ol>\r\n                  </div>\r\n              </div>\r\n          </div>\r\n      </section>\r\n      <!--page title end-->\r\n\r\n      <img src=\"assets/img/banners/android_app_development_bigperl_banner.png\" alt=\"android_app_development_bigperl_banner\" class=\"fullwidth\">\r\n\r\n            <div class=\"container padding-top-20\">\r\n                <div class=\"col-md-7\">\r\n                    <h2 class=\"text-bold mb-10\">Android App Development</h2>\r\n                    \r\n                    <p>Android Mobile Operating system is one of the best operating systems in the world. Alone it is holding around 85% global of market share and is available on multiple devices like Mobiles, Tabs, watches, cars etc. It is gaining popularity on each update.</p>\r\n\r\n<p>Best part of android is its usability flexibility, which makes android as preferred operating system among major device manufacturers like Samsung, Motorola and many more.\r\n\r\nWe at BigPerl Solutions Pvt Ltd, are one of very few top mobile app development company provides mobile application development solutions in India , USA , United Kingdom and multiple other countries across the globe. We already have excellent track record for delivering 60 + high performing android apps development solutions on multiple devices in travel , tourism , logistics and automobile industry in past five plus years.\r\n\r\nHaving full fledged development centers in India and United States we provide end to end Mobile app development solutions to our clients which starts from building solution and goes till user engagement , analytics and marketing of solution.</p>\r\n\r\n<p>Our average developer experience on android development team in 7+ Years. We make sure no compromise on development quality and securityparameters.  We believe on quick go to market and on time solution delivery.\r\n\r\nAs top android application development company we believe communication is key and effective communication improves the relationship , resolve conflicts and helps building great products.</p>\r\n                </div>\r\n\r\n                <div class=\"col-md-5\">\r\n                    <img src=\"assets/img/services/Android_app_development_bigperl.png\" class=\"img-responsive \" alt=\"Android_app_development_bigperl\">\r\n                </div>\r\n            </div>\r\n\r\n      \r\n            <app-footer5></app-footer5>\r\n           \r\n            <app-footer2></app-footer2>"

/***/ }),

/***/ "./src/app/servicessection/android-app-development/android-app-development.component.ts":
/*!**********************************************************************************************!*\
  !*** ./src/app/servicessection/android-app-development/android-app-development.component.ts ***!
  \**********************************************************************************************/
/*! exports provided: AndroidAppDevelopmentComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AndroidAppDevelopmentComponent", function() { return AndroidAppDevelopmentComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AndroidAppDevelopmentComponent = /** @class */ (function () {
    function AndroidAppDevelopmentComponent() {
    }
    AndroidAppDevelopmentComponent.prototype.ngOnInit = function () {
    };
    AndroidAppDevelopmentComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-android-app-development',
            template: __webpack_require__(/*! ./android-app-development.component.html */ "./src/app/servicessection/android-app-development/android-app-development.component.html"),
            styles: [__webpack_require__(/*! ./android-app-development.component.css */ "./src/app/servicessection/android-app-development/android-app-development.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], AndroidAppDevelopmentComponent);
    return AndroidAppDevelopmentComponent;
}());



/***/ }),

/***/ "./src/app/servicessection/android-developer/android-developer.component.css":
/*!***********************************************************************************!*\
  !*** ./src/app/servicessection/android-developer/android-developer.component.css ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/servicessection/android-developer/android-developer.component.html":
/*!************************************************************************************!*\
  !*** ./src/app/servicessection/android-developer/android-developer.component.html ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n        <!--page title start-->\r\n        <section class=\"page-title ptb-30\">\r\n          <div class=\"container padding-top-40\">\r\n              <div class=\"row\">\r\n                  <div class=\"col-md-12\">\r\n                      <h6 class=\"pagetitles\">Services</h6>\r\n                      <ol class=\"breadcrumb\">\r\n                          <li><a href=\"#\">Home</a></li>\r\n                          <li><a href=\"#\">Services</a></li>\r\n                          <li class=\"active\">android developer</li>\r\n                      </ol>\r\n                  </div>\r\n              </div>\r\n          </div>\r\n      </section>\r\n      <!--page title end-->\r\n\r\n      <img src=\"assets/img/banners/android_developer_bigperl_banner.png\" alt=\"android_developer_bigperl_banner\" class=\"fullwidth\">\r\n\r\n            <div class=\"container padding-top-20\">\r\n                <div class=\"col-md-7\">\r\n                    <h2 class=\"text-bold mb-10\">Android Developer</h2>\r\n                    <p class=\"text-justified\">Android Application is the world’s most popular open source operating system for mobile devices. Android is an open source and the Software Development Kit (SDK) and BigPerl Android application developer can help you convert your dreams into reality.</p>\r\n                    <p class=\"text-justified\">BigPerl developers can build effective and attractive mobile applications for the android operating system.</p>\r\n                    <p>Android expertise are deviant skilled to create, develop and deploy mobile applications and we build all kinds of Android Application system. Our Android apps bring you prodigious value in the business. For new start-up company a unique mobile application can take business to the next level.</p>\r\n                    <p class=\"text-justified\">We compromise end-to-end Android application development solution in Android Marketplace with GPS and Wi-Fi, Email and SMS campaigns, Multimedia, Google Maps, and Browser etc.</p>\r\n                    <p><b>Why BigPerl is the perfect choice for Android development?</b></p>\r\n                    <ol>\r\n                      <li>Best Services in Affordable Cost – Cost effective</li>\r\n                      <li>We offer Experts Hands-on Android Development.</li>\r\n                      <li>Fast deliver to customer that can positively affect customer satisfaction.</li>\r\n                      <li>We provide High quality Mobile Application development services.</li>\r\n                    </ol>\r\n                </div>\r\n\r\n                <div class=\"col-md-5\">\r\n                    <img src=\"assets/img/services/android_developer_bigperl.png\" class=\"img-responsive \" alt=\"android_developer_bigperl\">\r\n                </div>\r\n            </div>\r\n\r\n\r\n            <app-footer5></app-footer5>\r\n           \r\n            <app-footer2></app-footer2>"

/***/ }),

/***/ "./src/app/servicessection/android-developer/android-developer.component.ts":
/*!**********************************************************************************!*\
  !*** ./src/app/servicessection/android-developer/android-developer.component.ts ***!
  \**********************************************************************************/
/*! exports provided: AndroidDeveloperComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AndroidDeveloperComponent", function() { return AndroidDeveloperComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AndroidDeveloperComponent = /** @class */ (function () {
    function AndroidDeveloperComponent() {
    }
    AndroidDeveloperComponent.prototype.ngOnInit = function () {
    };
    AndroidDeveloperComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-android-developer',
            template: __webpack_require__(/*! ./android-developer.component.html */ "./src/app/servicessection/android-developer/android-developer.component.html"),
            styles: [__webpack_require__(/*! ./android-developer.component.css */ "./src/app/servicessection/android-developer/android-developer.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], AndroidDeveloperComponent);
    return AndroidDeveloperComponent;
}());



/***/ }),

/***/ "./src/app/servicessection/blackberry-app-development/blackberry-app-development.component.css":
/*!*****************************************************************************************************!*\
  !*** ./src/app/servicessection/blackberry-app-development/blackberry-app-development.component.css ***!
  \*****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/servicessection/blackberry-app-development/blackberry-app-development.component.html":
/*!******************************************************************************************************!*\
  !*** ./src/app/servicessection/blackberry-app-development/blackberry-app-development.component.html ***!
  \******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n        <!--page title start-->\r\n        <section class=\"page-title ptb-30\">\r\n          <div class=\"container padding-top-40\">\r\n              <div class=\"row\">\r\n                  <div class=\"col-md-12\">\r\n                      <h6 class=\"pagetitles\">Services</h6>\r\n                      <ol class=\"breadcrumb\">\r\n                          <li><a href=\"#\">Home</a></li>\r\n                          <li><a href=\"#\">Services</a></li>\r\n                          <li class=\"active\">blackberry app development</li>\r\n                      </ol>\r\n                  </div>\r\n              </div>\r\n          </div>\r\n      </section>\r\n      <!--page title end-->\r\n\r\n      <img src=\"assets/img/banners/blackberry_app_development_bigperl_banner.png\" alt=\"blackberry_app_development_bigperl_banner\" class=\"fullwidth\">\r\n\r\n            <div class=\"container padding-top-20\">\r\n                <div class=\"col-md-7\">\r\n                    <h2 class=\"text-bold mb-10\">Blackberry App Development</h2>\r\n                    <p class=\"text-justified\">Blackberry is known for its security on mobility. This is the reason blackberry is used specifically for business and more commonly known as “Business Phone” . It is very important to deliver top most quality oriented apps for business users.</p>\r\n                    <p>BlackBerry app developers at Bigperl can create professional, custom, high-performing and feature-packed blackberry apps to meet your business, technology and user needs.</p>\r\n                    <p class=\"text-justified\">Our Team at Bigperl has an expertise over BlackBerry Apps development. We have successfully satisfied customers by designing and developing engaging mobile apps for them to serve their business needs better. Our rich BlackBerry Applications development experience, flexible business model with attractive pricing options, and fine-tuned business processes translates into a viable business proposition for you.</p>\r\n                    <p class=\"text-justified\">Bigperl has an expert team of BlackBerry Apps development professionals who offer their dedicated services in the following crucial areas:</p>\r\n                    <ul>\r\n                      <li>Blackberry custom application development services</li>\r\n                      <li>Blackberry software QA/testing services</li>\r\n                      <li>Blackberry software support and maintenance</li>\r\n                    </ul>\r\n                </div>\r\n\r\n                <div class=\"col-md-5\">\r\n                    <img src=\"assets/img/services/BlackBerry_app_development_bigperl.png\" class=\"img-responsive \" alt=\"BlackBerry_app_development_bigperl\">\r\n                </div>\r\n            </div>\r\n\r\n            <app-footer5></app-footer5>\r\n           \r\n            <app-footer2></app-footer2>"

/***/ }),

/***/ "./src/app/servicessection/blackberry-app-development/blackberry-app-development.component.ts":
/*!****************************************************************************************************!*\
  !*** ./src/app/servicessection/blackberry-app-development/blackberry-app-development.component.ts ***!
  \****************************************************************************************************/
/*! exports provided: BlackberryAppDevelopmentComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BlackberryAppDevelopmentComponent", function() { return BlackberryAppDevelopmentComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var BlackberryAppDevelopmentComponent = /** @class */ (function () {
    function BlackberryAppDevelopmentComponent() {
    }
    BlackberryAppDevelopmentComponent.prototype.ngOnInit = function () {
    };
    BlackberryAppDevelopmentComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-blackberry-app-development',
            template: __webpack_require__(/*! ./blackberry-app-development.component.html */ "./src/app/servicessection/blackberry-app-development/blackberry-app-development.component.html"),
            styles: [__webpack_require__(/*! ./blackberry-app-development.component.css */ "./src/app/servicessection/blackberry-app-development/blackberry-app-development.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], BlackberryAppDevelopmentComponent);
    return BlackberryAppDevelopmentComponent;
}());



/***/ }),

/***/ "./src/app/servicessection/cake-php-development/cake-php-development.component.css":
/*!*****************************************************************************************!*\
  !*** ./src/app/servicessection/cake-php-development/cake-php-development.component.css ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/servicessection/cake-php-development/cake-php-development.component.html":
/*!******************************************************************************************!*\
  !*** ./src/app/servicessection/cake-php-development/cake-php-development.component.html ***!
  \******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "  <!--page title start-->\r\n  <section class=\"page-title ptb-30\">\r\n    <div class=\"container padding-top-40\">\r\n        <div class=\"row\">\r\n            <div class=\"col-md-12\">\r\n                <h6 class=\"pagetitles\">Services</h6>\r\n                <ol class=\"breadcrumb\">\r\n                    <li><a href=\"#\">Home</a></li>\r\n                    <li><a href=\"#\">Services</a></li>\r\n                    <li class=\"active\">cakephp development</li>\r\n                </ol>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</section>\r\n<!--page title end-->\r\n\r\n<img src=\"assets/img/banners/cakephp_development_bigperl_banner.png\" alt=\"cakephp_development_bigperl_banner\" class=\"fullwidth\">\r\n\r\n      <div class=\"container padding-top-20\">\r\n          <div class=\"col-md-7\">\r\n              <h2 class=\"text-bold mb-10\">CakePHP Development</h2>\r\n              <p class=\"text-justified\">Simple to learn, code and read, CakePHP frameworks from Multidots are compatible with HTML Forms, Java Scripts, AJAX and other development tools.</p>\r\n              <p class=\"text-justified\">Bigperl Solutions Pvt Ltd is a renowned CakePHP development company offering an all-inclusive skill set in the CakePHP Customization and CakePHP web development. With our diligent CakePHP Developers and fully-furnished development infrastructure we have maintained the best of CakePHP development standards and comprehensively handled big client projects.</p>\r\n              <p class=\"text-justified\">Our business-driven CakePHP development approach and our end-to-end CakePHP solutions have served different businesses with most compliant source of platform to put them across with effective interface and leveraging communication potential. We have transformed the way people perceive a CakePHP solution through our fine array of service in CakePHP development.</p>\r\n              <ul>\r\n                <li><b>Services offered:</b></li>\r\n                <li>Bespoke Web Development processes and tools for secure and high quality web applications.</li>\r\n                <li>CakePHP shopping cart and CMS development.</li>\r\n                <li>CakePHP customization, maintenance, integration and up gradation.</li>\r\n                <li>CakePHP extendibles and applications for faster E-Commerce growth and profitability.</li>\r\n              </ul>\r\n          </div>\r\n\r\n          <div class=\"col-md-5\">\r\n              <img src=\"assets/img/services/cakephp_development_bigperl.png\" class=\"img-responsive \" alt=\"cakephp_development_bigperl\">\r\n          </div>\r\n      </div>\r\n      <app-footer5></app-footer5>\r\n           \r\n      <app-footer2></app-footer2>"

/***/ }),

/***/ "./src/app/servicessection/cake-php-development/cake-php-development.component.ts":
/*!****************************************************************************************!*\
  !*** ./src/app/servicessection/cake-php-development/cake-php-development.component.ts ***!
  \****************************************************************************************/
/*! exports provided: CakePhpDevelopmentComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CakePhpDevelopmentComponent", function() { return CakePhpDevelopmentComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var CakePhpDevelopmentComponent = /** @class */ (function () {
    function CakePhpDevelopmentComponent() {
    }
    CakePhpDevelopmentComponent.prototype.ngOnInit = function () {
    };
    CakePhpDevelopmentComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-cake-php-development',
            template: __webpack_require__(/*! ./cake-php-development.component.html */ "./src/app/servicessection/cake-php-development/cake-php-development.component.html"),
            styles: [__webpack_require__(/*! ./cake-php-development.component.css */ "./src/app/servicessection/cake-php-development/cake-php-development.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], CakePhpDevelopmentComponent);
    return CakePhpDevelopmentComponent;
}());



/***/ }),

/***/ "./src/app/servicessection/cmc-development/cmc-development.component.css":
/*!*******************************************************************************!*\
  !*** ./src/app/servicessection/cmc-development/cmc-development.component.css ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/servicessection/cmc-development/cmc-development.component.html":
/*!********************************************************************************!*\
  !*** ./src/app/servicessection/cmc-development/cmc-development.component.html ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = " <!--page title start-->\r\n <section class=\"page-title ptb-30\">\r\n  <div class=\"container padding-top-40\">\r\n      <div class=\"row\">\r\n          <div class=\"col-md-12\">\r\n              <h6 class=\"pagetitles\">Services</h6>\r\n              <ol class=\"breadcrumb\">\r\n                  <li><a href=\"#\">Home</a></li>\r\n                  <li><a href=\"#\">Services</a></li>\r\n                  <li class=\"active\">cms development</li>\r\n              </ol>\r\n          </div>\r\n      </div>\r\n  </div>\r\n</section>\r\n<!--page title end-->\r\n\r\n<img src=\"assets/img/banners/cms_development_bigperl_banner.png\" alt=\"cms_development_bigperl_banner\" class=\"fullwidth\">\r\n\r\n    <div class=\"container padding-top-20\">\r\n        <div class=\"col-md-7\">\r\n            <h2 class=\"text-bold mb-10\">CMS Development</h2>\r\n            <p class=\"text-justified\">Content Management System (CMS) is a simple system that enables you to manage the content of a website. Using CMS you can create edit and modify content, add and remove images, update and manage unlimited content pages, and then publish the content. Once published, content can be easily updated and republished. It also allows you to maintain the entire content from a single back-end central interface.</p>\r\n            <p class=\"text-justified\">CMS provides you with processes which you can use to manage workflow in a collaborative domain. You can therefore control and monitor the content within your website without any technical training.</p>\r\n            <p class=\"text-justified\">Bigperl offers you a diverse set of services such as CMS Development, Open Source CMS Development and CMS Portal Development that is tailored to your needs. We provide customized services at affordable rates. Our team is experienced in developing the perfect CMS that contains all the necessaryspecifications to meet all your requirements.</p>\r\n        </div>\r\n\r\n        <div class=\"col-md-5\">\r\n            <img src=\"assets/img/services/CMS_development_bigperl.png\" class=\"img-responsive \" alt=\"CMS_development_bigperl\">\r\n        </div>\r\n    </div>\r\n\r\n    <app-footer5></app-footer5>\r\n           \r\n    <app-footer2></app-footer2>"

/***/ }),

/***/ "./src/app/servicessection/cmc-development/cmc-development.component.ts":
/*!******************************************************************************!*\
  !*** ./src/app/servicessection/cmc-development/cmc-development.component.ts ***!
  \******************************************************************************/
/*! exports provided: CmcDevelopmentComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CmcDevelopmentComponent", function() { return CmcDevelopmentComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var CmcDevelopmentComponent = /** @class */ (function () {
    function CmcDevelopmentComponent() {
    }
    CmcDevelopmentComponent.prototype.ngOnInit = function () {
    };
    CmcDevelopmentComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-cmc-development',
            template: __webpack_require__(/*! ./cmc-development.component.html */ "./src/app/servicessection/cmc-development/cmc-development.component.html"),
            styles: [__webpack_require__(/*! ./cmc-development.component.css */ "./src/app/servicessection/cmc-development/cmc-development.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], CmcDevelopmentComponent);
    return CmcDevelopmentComponent;
}());



/***/ }),

/***/ "./src/app/servicessection/codeigniter-development/codeigniter-development.component.css":
/*!***********************************************************************************************!*\
  !*** ./src/app/servicessection/codeigniter-development/codeigniter-development.component.css ***!
  \***********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/servicessection/codeigniter-development/codeigniter-development.component.html":
/*!************************************************************************************************!*\
  !*** ./src/app/servicessection/codeigniter-development/codeigniter-development.component.html ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--page title start-->\r\n<section class=\"page-title ptb-30\">\r\n  <div class=\"container padding-top-40\">\r\n      <div class=\"row\">\r\n          <div class=\"col-md-12\">\r\n              <h6 class=\"pagetitles\">Services</h6>\r\n              <ol class=\"breadcrumb\">\r\n                  <li><a href=\"#\">Home</a></li>\r\n                  <li><a href=\"#\">Services</a></li>\r\n                  <li class=\"active\">codeigniter development</li>\r\n              </ol>\r\n          </div>\r\n      </div>\r\n  </div>\r\n</section>\r\n<!--page title end-->\r\n\r\n<img src=\"assets/img/banners/codeigniter_development_bigperl_banner.png\" alt=\"codeigniter_development_bigperl_banner\" class=\"fullwidth\">\r\n\r\n    <div class=\"container padding-top-20\">\r\n        <div class=\"col-md-7\">\r\n            <h2 class=\"text-bold mb-10\">Codeigniter Development</h2>\r\n            <p class=\"text-justified\">CodeIgniter is the open source software which is used in creating promising and effective websites with the use of PHP framework. The main attribute of CodeIgniter develoment is developing and creating websites seamlessly with a rapid pace.CodeIgniter accredits programmers and developers for creating gen-next encouraging web applications. This wonderful web application platform helps the developers in developing IT web projects rapidly, by providing set of libraries for commonly used tasks instead of writing code from the scratch.</p>\r\n            <p class=\"text-justified\">BigPerl Solutions Pvt Ltd is the leading mobile and web development company, having 5+ years of wealthy experience in software development and designing work. We provide best in class CodeIgniter web development solutions at affordable prices.</p>\r\n            <p>In past years, we have built various partnership with various software development and designing field. Our company developed a reputation and worked to the best effort and ability for receiving positive feedback. We are able to provide best software solution at affordable price.</p>\r\n        </div>\r\n\r\n        <div class=\"col-md-5\">\r\n            <img src=\"assets/img/services/codeigniter_development_bigperl.png\" class=\"img-responsive \" alt=\"codeigniter_development_bigperl\">\r\n        </div>\r\n    </div>\r\n\r\n    <app-footer5></app-footer5>\r\n           \r\n    <app-footer2></app-footer2>"

/***/ }),

/***/ "./src/app/servicessection/codeigniter-development/codeigniter-development.component.ts":
/*!**********************************************************************************************!*\
  !*** ./src/app/servicessection/codeigniter-development/codeigniter-development.component.ts ***!
  \**********************************************************************************************/
/*! exports provided: CodeigniterDevelopmentComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CodeigniterDevelopmentComponent", function() { return CodeigniterDevelopmentComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var CodeigniterDevelopmentComponent = /** @class */ (function () {
    function CodeigniterDevelopmentComponent() {
    }
    CodeigniterDevelopmentComponent.prototype.ngOnInit = function () {
    };
    CodeigniterDevelopmentComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-codeigniter-development',
            template: __webpack_require__(/*! ./codeigniter-development.component.html */ "./src/app/servicessection/codeigniter-development/codeigniter-development.component.html"),
            styles: [__webpack_require__(/*! ./codeigniter-development.component.css */ "./src/app/servicessection/codeigniter-development/codeigniter-development.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], CodeigniterDevelopmentComponent);
    return CodeigniterDevelopmentComponent;
}());



/***/ }),

/***/ "./src/app/servicessection/drupal-development/drupal-development.component.css":
/*!*************************************************************************************!*\
  !*** ./src/app/servicessection/drupal-development/drupal-development.component.css ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/servicessection/drupal-development/drupal-development.component.html":
/*!**************************************************************************************!*\
  !*** ./src/app/servicessection/drupal-development/drupal-development.component.html ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--page title start-->\r\n<section class=\"page-title ptb-30\">\r\n  <div class=\"container padding-top-40\">\r\n      <div class=\"row\">\r\n          <div class=\"col-md-12\">\r\n              <h6 class=\"pagetitles\">Services</h6>\r\n              <ol class=\"breadcrumb\">\r\n                  <li><a href=\"#\">Home</a></li>\r\n                  <li><a href=\"#\">Services</a></li>\r\n                  <li class=\"active\">drupal development</li>\r\n              </ol>\r\n          </div>\r\n      </div>\r\n  </div>\r\n</section>\r\n<!--page title end-->\r\n\r\n<img src=\"assets/img/banners/drupal_development_bigperl_banner.png\" alt=\"drupal_development_bigperl_banner\" class=\"fullwidth\">\r\n\r\n    <div class=\"container padding-top-20\">\r\n        <div class=\"col-md-7\">\r\n            <h2 class=\"text-bold mb-10\">Drupal Development</h2>\r\n            <p class=\"text-justified\">Drupal is a free Content Management System (CMS) that enables you to organize, manage, and publish your content, with ease. Drupal is an open source CMS that is developed and maintained by a huge community of developers and users. Written in PHP, Drupal is an excellent platform to build and publish complex dynamic websites, portals, online communities, and the like. As it contains an open development model, Drupal can be easily customized to power millions of unique websites and applications, across the globe. <br>What We Offer You?</p>\r\n            <p class=\"text-justified\">Bigperl provides you with unbeatable Drupal Development services such as Drupal CMS Solution, Drupal Portal Development, and Drupal Module Development. As a Drupal Web Development company, We ensure that you receive the best Drupal CMS solution that will customize your website according to your requirements.</p>\r\n            <p class=\"text-justified\">Our team consists of skilled and professional Drupal developers who designs perfect Drupal themes and templates based on the specifications provided by our clients. We take pride in the fact that all our services are priced reasonably, and we have met all our delivery dates. Our clients have therefore shown satisfaction with our Drupal Development services.</p>\r\n        </div>\r\n\r\n        <div class=\"col-md-5\">\r\n            <img src=\"assets/img/services/drupal_development_bigperl.png\" class=\"img-responsive \" alt=\"drupal_development_bigperl\">\r\n        </div>\r\n    </div>\r\n\r\n    <app-footer5></app-footer5>\r\n           \r\n    <app-footer2></app-footer2>"

/***/ }),

/***/ "./src/app/servicessection/drupal-development/drupal-development.component.ts":
/*!************************************************************************************!*\
  !*** ./src/app/servicessection/drupal-development/drupal-development.component.ts ***!
  \************************************************************************************/
/*! exports provided: DrupalDevelopmentComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DrupalDevelopmentComponent", function() { return DrupalDevelopmentComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var DrupalDevelopmentComponent = /** @class */ (function () {
    function DrupalDevelopmentComponent() {
    }
    DrupalDevelopmentComponent.prototype.ngOnInit = function () {
    };
    DrupalDevelopmentComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-drupal-development',
            template: __webpack_require__(/*! ./drupal-development.component.html */ "./src/app/servicessection/drupal-development/drupal-development.component.html"),
            styles: [__webpack_require__(/*! ./drupal-development.component.css */ "./src/app/servicessection/drupal-development/drupal-development.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], DrupalDevelopmentComponent);
    return DrupalDevelopmentComponent;
}());



/***/ }),

/***/ "./src/app/servicessection/ecommerce-developer/ecommerce-developer.component.css":
/*!***************************************************************************************!*\
  !*** ./src/app/servicessection/ecommerce-developer/ecommerce-developer.component.css ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/servicessection/ecommerce-developer/ecommerce-developer.component.html":
/*!****************************************************************************************!*\
  !*** ./src/app/servicessection/ecommerce-developer/ecommerce-developer.component.html ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--page title start-->\r\n<section class=\"page-title ptb-30\">\r\n  <div class=\"container padding-top-40\">\r\n      <div class=\"row\">\r\n          <div class=\"col-md-12\">\r\n              <h6 class=\"pagetitles\">Services</h6>\r\n              <ol class=\"breadcrumb\">\r\n                  <li><a href=\"#\">Home</a></li>\r\n                  <li><a href=\"#\">Services</a></li>\r\n                  <li class=\"active\">e-commerce developer</li>\r\n              </ol>\r\n          </div>\r\n      </div>\r\n  </div>\r\n</section>\r\n<!--page title end-->\r\n\r\n<img src=\"assets/img/banners/ecommerce_developer_bigperl_banner.png\" alt=\"ecommerce_developer_bigperl_banner\" class=\"fullwidth\">\r\n\r\n    <div class=\"container padding-top-20\">\r\n        <div class=\"col-md-7\">\r\n            <h2 class=\"text-bold mb-10\">E-Commerce Developer</h2>\r\n            <p class=\"text-justified\">To have a successful online presence, an appealing and effective E-commerce website is needed. The term E-commerce refers to Electronic commerce i.e. any transactions or conducting business activities while employing the means of any electronic medium which can be any online portal like websites or any social media websites.</p>\r\n            <p class=\"text-justified\">BigPerl located in fast developing Bangalore city in the world with businesses and service providers. People of Bangalore in busy life styles give immense importance for the businesses providing online services. BigPerl is building wonderful websites for ecommerce companies in Bangalore.</p>\r\n            <p class=\"text-justified\">Our Technical specializes develops all types of eCommerce that include Oscommerce, Magento, OpenCart, VirtueMart, Protestant, XCart, ZenCart, CS-Cart and Shopify among many others.</p>\r\n            <p><b>Why BigPerl is the perfect choice for eCommerce development?</b></p>\r\n            <ol>\r\n              <li>We provide complete customization and User-Friendly Browsing and designs.</li>\r\n              <li>Our SEO-friendly eCommerce solution will provide you increased visibility on Web Traffic.</li>\r\n              <li>Fast, Cost-effective and On-Time Project Delivery.</li>\r\n              <li>We offer Experts Hands-on eCommerce Development.</li>\r\n            </ol>\r\n        </div>\r\n\r\n        <div class=\"col-md-5\">\r\n            <img src=\"assets/img/services/ecommerce_developer_bigperl.png\" class=\"img-responsive \" alt=\"ecommerce_developer_bigperl\">\r\n        </div>\r\n    </div>\r\n\r\n    <app-footer5></app-footer5>\r\n           \r\n    <app-footer2></app-footer2>"

/***/ }),

/***/ "./src/app/servicessection/ecommerce-developer/ecommerce-developer.component.ts":
/*!**************************************************************************************!*\
  !*** ./src/app/servicessection/ecommerce-developer/ecommerce-developer.component.ts ***!
  \**************************************************************************************/
/*! exports provided: EcommerceDeveloperComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EcommerceDeveloperComponent", function() { return EcommerceDeveloperComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var EcommerceDeveloperComponent = /** @class */ (function () {
    function EcommerceDeveloperComponent() {
    }
    EcommerceDeveloperComponent.prototype.ngOnInit = function () {
    };
    EcommerceDeveloperComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-ecommerce-developer',
            template: __webpack_require__(/*! ./ecommerce-developer.component.html */ "./src/app/servicessection/ecommerce-developer/ecommerce-developer.component.html"),
            styles: [__webpack_require__(/*! ./ecommerce-developer.component.css */ "./src/app/servicessection/ecommerce-developer/ecommerce-developer.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], EcommerceDeveloperComponent);
    return EcommerceDeveloperComponent;
}());



/***/ }),

/***/ "./src/app/servicessection/ecommerce-development/ecommerce-development.component.css":
/*!*******************************************************************************************!*\
  !*** ./src/app/servicessection/ecommerce-development/ecommerce-development.component.css ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/servicessection/ecommerce-development/ecommerce-development.component.html":
/*!********************************************************************************************!*\
  !*** ./src/app/servicessection/ecommerce-development/ecommerce-development.component.html ***!
  \********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--page title start-->\r\n<section class=\"page-title ptb-30\">\r\n  <div class=\"container padding-top-40\">\r\n      <div class=\"row\">\r\n          <div class=\"col-md-12\">\r\n              <h6 class=\"pagetitles\">Services</h6>\r\n              <ol class=\"breadcrumb\">\r\n                  <li><a href=\"#\">Home</a></li>\r\n                  <li><a href=\"#\">Services</a></li>\r\n                  <li class=\"active\">e-commerce development</li>\r\n              </ol>\r\n          </div>\r\n      </div>\r\n  </div>\r\n</section>\r\n<!--page title end-->\r\n\r\n<img src=\"assets/img/banners/ecommerce_development_bigperl_banner.png\" alt=\"ecommerce_development_bigperl_banner\" class=\"fullwidth\">\r\n\r\n    <div class=\"container padding-top-20\">\r\n        <div class=\"col-md-7\">\r\n            <h2 class=\"text-bold mb-10\">E-Commerce Development</h2>\r\n            <p class=\"text-justified\">e-commerce web development has indeed become a necessity instead of a means to drive greater traffic. We, at Bigperl Private Solutions, one of the pioneered web development companies,offer efficacious and effectual e-commerce web design, plug-in & module development solutions for small and medium level enterprises. Having intense love for web designing and development, our skilled and talented professionals remain abreast with the current industry trends to deliver prolific results. With the ineffable experience, our competent developers create customized e-commerce website to promote your brand effectively while bringing success as well as online sales.</p>\r\n            <h3><b>OUR FEAT IN ECOMMERCE INDUSTRY</b></h3>\r\n            <p class=\"text-justified\">Our years of industry experience makes us capable enough to handle various aspects related to your online business, enabling you to reach out to the global audience. We create and implement end-to-end e-commerce solutions that are integrated with your business website impeccably. We firmly believe in timely delivery & cost-efficient solutions along with consistency and premium quality. Our strong determination and passion towards web development have inspired us to offer state-of-the-art e-commerce web development services to the global clients, including 9100+ satisfied customers.</p>\r\n            <h3>Each website is created with usability and flexibility in mind while providing seamless navigational experience.</h3>\r\n            <ol>\r\n            <li class=\"text-justified\">Our development team evaluates your online business objectives and offers100% unique solutions tailored to meet your exact e-commerce website requirements.</li>\r\n            <li class=\"text-justified\">We deliver full-fledged e-commerce sites that are integrated with a number of elegant features, including taxation programs, shipping services, payment gateways, making your online business experience more convenient.</li>\r\n            <li class=\"text-justified\">Cater exclusive solutions by implementing innovative ideas along with neat and clear coding under W3C standards. Guaranteed customer satisfaction with 24X7 availability through seamless communication channels.</li>\r\n            </ol>\r\n        </div>\r\n\r\n        <div class=\"col-md-5\">\r\n            <img src=\"assets/img/services/ecommerce_development_bigperl.png\" class=\"img-responsive \" alt=\"ecommerce_development_bigperl\">\r\n        </div>\r\n    </div>\r\n\r\n    <app-footer5></app-footer5>\r\n           \r\n    <app-footer2></app-footer2>"

/***/ }),

/***/ "./src/app/servicessection/ecommerce-development/ecommerce-development.component.ts":
/*!******************************************************************************************!*\
  !*** ./src/app/servicessection/ecommerce-development/ecommerce-development.component.ts ***!
  \******************************************************************************************/
/*! exports provided: EcommerceDevelopmentComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EcommerceDevelopmentComponent", function() { return EcommerceDevelopmentComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var EcommerceDevelopmentComponent = /** @class */ (function () {
    function EcommerceDevelopmentComponent() {
    }
    EcommerceDevelopmentComponent.prototype.ngOnInit = function () {
    };
    EcommerceDevelopmentComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-ecommerce-development',
            template: __webpack_require__(/*! ./ecommerce-development.component.html */ "./src/app/servicessection/ecommerce-development/ecommerce-development.component.html"),
            styles: [__webpack_require__(/*! ./ecommerce-development.component.css */ "./src/app/servicessection/ecommerce-development/ecommerce-development.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], EcommerceDevelopmentComponent);
    return EcommerceDevelopmentComponent;
}());



/***/ }),

/***/ "./src/app/servicessection/facebook-app-development/facebook-app-development.component.css":
/*!*************************************************************************************************!*\
  !*** ./src/app/servicessection/facebook-app-development/facebook-app-development.component.css ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/servicessection/facebook-app-development/facebook-app-development.component.html":
/*!**************************************************************************************************!*\
  !*** ./src/app/servicessection/facebook-app-development/facebook-app-development.component.html ***!
  \**************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n        <!--page title start-->\r\n        <section class=\"page-title ptb-30\">\r\n          <div class=\"container padding-top-40\">\r\n              <div class=\"row\">\r\n                  <div class=\"col-md-12\">\r\n                      <h6 class=\"pagetitles\">Services</h6>\r\n                      <ol class=\"breadcrumb\">\r\n                          <li><a href=\"#\">Home</a></li>\r\n                          <li><a href=\"#\">Services</a></li>\r\n                          <li class=\"active\">faceebook app development</li>\r\n                      </ol>\r\n                  </div>\r\n              </div>\r\n          </div>\r\n      </section>\r\n      <!--page title end-->\r\n\r\n      <img src=\"assets/img/banners/facebook_app_development_bigperl_banner.png\" alt=\"facebook_app_development_bigperl_banner\" class=\"fullwidth\">\r\n\r\n            <div class=\"container padding-top-20\">\r\n                <div class=\"col-md-7\">\r\n                    <h2 class=\"text-bold mb-10\">Facebook App Development</h2>\r\n                    <p class=\"text-justified\">Do you want to maximize your social media presence? Go for a Facebook App! In today’s world where everyone that you know is on Facebook it becomes very much necessary to promote your business on Facebook. Nevertheless, the promotional guidelines of Facebook have made it mandatory for businesses to own Facebook Apps. Through a Facebook App you can easily conduct contests, interact with your customers and inform consumers about your business, product or services, without coming across as spam.</p>\r\n                    <p><b>Advantages of investing in a Facebook App:</b></p>\r\n                    <p class=\"text-justified\"><b>Flexibility:</b> Facebook Apps provide maximum flexibility in terms of programs and databases. You can program them in any way you want to and design your database in your desired manner. The amount of information you can extract through a Facebook App is limitless.</p>\r\n                    <p class=\"text-justified\"><b>Excellent Marketing Potential:</b> Facebook Apps offer great marketing potential as you can easily post on the walls of those who have downloaded your app. This means you can promote your business to all the friends of such users. They also offer you the option of email marketing as you will have extracted the email IDs of the users who would have downloaded your Facebook Apps.</p>\r\n                    <p class=\"text-justified\"><b>A Good way to engage your Audience:</b> While the landing page on your Facebook page may not be successful enough in engaging your audience, a Facebook App definitely will be engaging. You will be able to deliver your message, loud and clear, without any kind of distractions. You can make it as attractive as possible to engage your audience in the best possible manner.</p>\r\n                    <p class=\"text-justified\"><b>Outbound Links:</b> While Facebook Pages restrict you to comments and posts in order to post outbound links, Facebook Apps provide you the best way to post your outbound links through text, images or any kind of digital media. This definitely is a very professional way of promoting yourself.</p>\r\n                    <p class=\"text-justified\"><b>Best way to market your Facebook Page:</b> Facebook app provides you the best way to optimize your Facebook Page within the huge Facebook Market. This will definitely take you a long way in achieving success with social media marketing.</p>\r\n                    <p class=\"text-justified\">If you have made up your mind to go for a Facebook App and if you are looking out for the best Facebook App Development Company in India, you are at the right place. BigPerl is surely one of the best with its extraordinary team of app developers who have all the expertise and the experience required for the job. We have done extensive research and know all the nitty-gritties of social media marketing. We will make sure you get the maximum out of your Facebook App in the most affordable manner.</p>\r\n                </div>\r\n\r\n                <div class=\"col-md-5\">\r\n                    <img src=\"assets/img/services/facebook_app_development_bigperl.png\" class=\"img-responsive \" alt=\"facebook_app_development_bigperl\">\r\n                </div>\r\n            </div>\r\n            <app-footer5></app-footer5>\r\n           \r\n            <app-footer2></app-footer2>"

/***/ }),

/***/ "./src/app/servicessection/facebook-app-development/facebook-app-development.component.ts":
/*!************************************************************************************************!*\
  !*** ./src/app/servicessection/facebook-app-development/facebook-app-development.component.ts ***!
  \************************************************************************************************/
/*! exports provided: FacebookAppDevelopmentComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FacebookAppDevelopmentComponent", function() { return FacebookAppDevelopmentComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var FacebookAppDevelopmentComponent = /** @class */ (function () {
    function FacebookAppDevelopmentComponent() {
    }
    FacebookAppDevelopmentComponent.prototype.ngOnInit = function () {
    };
    FacebookAppDevelopmentComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-facebook-app-development',
            template: __webpack_require__(/*! ./facebook-app-development.component.html */ "./src/app/servicessection/facebook-app-development/facebook-app-development.component.html"),
            styles: [__webpack_require__(/*! ./facebook-app-development.component.css */ "./src/app/servicessection/facebook-app-development/facebook-app-development.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], FacebookAppDevelopmentComponent);
    return FacebookAppDevelopmentComponent;
}());



/***/ }),

/***/ "./src/app/servicessection/hybrid-app-development/hybrid-app-development.component.css":
/*!*********************************************************************************************!*\
  !*** ./src/app/servicessection/hybrid-app-development/hybrid-app-development.component.css ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/servicessection/hybrid-app-development/hybrid-app-development.component.html":
/*!**********************************************************************************************!*\
  !*** ./src/app/servicessection/hybrid-app-development/hybrid-app-development.component.html ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n        <!--page title start-->\r\n        <section class=\"page-title ptb-30\">\r\n          <div class=\"container padding-top-40\">\r\n              <div class=\"row\">\r\n                  <div class=\"col-md-12\">\r\n                      <h6 class=\"pagetitles\">Services</h6>\r\n                      <ol class=\"breadcrumb\">\r\n                          <li><a href=\"#\">Home</a></li>\r\n                          <li><a href=\"#\">Services</a></li>\r\n                          <li class=\"active\">hybrid application development</li>\r\n                      </ol>\r\n                  </div>\r\n              </div>\r\n          </div>\r\n      </section>\r\n      <!--page title end-->\r\n\r\n      <img src=\"assets/img/banners/hybrid_mobile_app_development_bigperl_banner.png\" alt=\"hybrid_mobile_app_development_bigperl_banner\" class=\"fullwidth\">\r\n\r\n            <div class=\"container padding-top-20\">\r\n                <div class=\"col-md-7\">\r\n                    <h2 class=\"text-bold mb-10\">Hybrid Application Development</h2>\r\n                    <p class=\"text-justified\"> Hybrid mobile applications are built using HTML, CSS and JavaScript, and than wrapped in a native container which loads most of the information on the page as the user navigates through the application. Hybrid apps wins at its portability with build once and run on any of the mobile platforms e.g. : Android , iOS & Windows which turn out to be faster go to market as well as cost effective upfront development capital.</p>\r\n                    <p class=\"text-justified\"> Benefits of hybrid app development is we can function whether or not the device is connected, Integrate with a device’s file system and Integration with Web-based services. And mainly it has an embedded browser to improve access to dynamic online content. A hybrid app is one that combines components of both native and Web applications. As Hybrid app development company, Bigperl ensure to give quality hybrid application with best user experience. The benefits to building a hybrid mobile app for your business are many: they use skills that most of the mobile app front-end developers already have, and only requires one version of app, that will run anywhere, on any device, which means it will run on any platform. As a hybrid app development company we ensure that the process of coding and deploying a hybrid app is faster, costive effective and maintainable , especially when dealing with simpler apps.</p>\r\n                    <p class=\"text-justified\"> We are having a strong experienced team of high energetic hybrid app development team, who are able to deliver best quality of code and security parameters. We are having full-fledged hybrid app development centers in india and United States. Bigperl is one of the best hybrid app development india company good in providing solutions for high performing application.</p>\r\n          <p class=\"text-justified\"> We have dedicated research and development team which keep on innovating on new upcoming technologies and frameworks to make system better. We also provide 24 X 7 technical support team for specially designed to resolve queries of  Entrepreneurs and Enterprise customers.</p>\r\n        </div>\r\n\r\n                <div class=\"col-md-5\">\r\n                    <img src=\"assets/img/services/Hybrid_app_development_bigperl.png\" class=\"img-responsive \" alt=\"Hybrid_app_development_bigperl\">\r\n                </div>\r\n            </div>\r\n\r\n            <app-footer5></app-footer5>\r\n           \r\n            <app-footer2></app-footer2>"

/***/ }),

/***/ "./src/app/servicessection/hybrid-app-development/hybrid-app-development.component.ts":
/*!********************************************************************************************!*\
  !*** ./src/app/servicessection/hybrid-app-development/hybrid-app-development.component.ts ***!
  \********************************************************************************************/
/*! exports provided: HybridAppDevelopmentComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HybridAppDevelopmentComponent", function() { return HybridAppDevelopmentComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var HybridAppDevelopmentComponent = /** @class */ (function () {
    function HybridAppDevelopmentComponent() {
    }
    HybridAppDevelopmentComponent.prototype.ngOnInit = function () {
    };
    HybridAppDevelopmentComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-hybrid-app-development',
            template: __webpack_require__(/*! ./hybrid-app-development.component.html */ "./src/app/servicessection/hybrid-app-development/hybrid-app-development.component.html"),
            styles: [__webpack_require__(/*! ./hybrid-app-development.component.css */ "./src/app/servicessection/hybrid-app-development/hybrid-app-development.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], HybridAppDevelopmentComponent);
    return HybridAppDevelopmentComponent;
}());



/***/ }),

/***/ "./src/app/servicessection/internet-of-things/internet-of-things.component.css":
/*!*************************************************************************************!*\
  !*** ./src/app/servicessection/internet-of-things/internet-of-things.component.css ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/servicessection/internet-of-things/internet-of-things.component.html":
/*!**************************************************************************************!*\
  !*** ./src/app/servicessection/internet-of-things/internet-of-things.component.html ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = " <!--page title start-->\r\n <section class=\"page-title ptb-30\">\r\n  <div class=\"container padding-top-40\">\r\n      <div class=\"row\">\r\n          <div class=\"col-md-12\">\r\n              <h6 class=\"pagetitles\">Services</h6>\r\n              <ol class=\"breadcrumb\">\r\n                  <li><a href=\"#\">Home</a></li>\r\n                  <li><a href=\"#\">Services</a></li>\r\n                  <li class=\"active\">IOT Solitions</li>\r\n              </ol>\r\n          </div>\r\n      </div>\r\n  </div>\r\n</section>\r\n<!--page title end-->\r\n\r\n<img src=\"assets/img/banners/internet-of-things_(iot)_bigperl_banner.png\" alt=\"internet-of-things_(iot)_bigperl_banner\" class=\"fullwidth\">\r\n\r\n    <div class=\"container padding-top-20\">\r\n        <div class=\"col-md-7\">\r\n            <h2 class=\"text-bold mb-10\">Harness the Power of Future Technology</h2>\r\n            <p class=\"text-justified\">As the technolgy is evolving day by day, the way of interaction between human-human, and human-devices has got advanced. That is because of the power of internet technology, particularly, Internet of Things (IoT).</p>\r\n            <h3><b>What is IOT</b></h3>\r\n            <p class=\"text-justified\">Internet of Things (IoT) is the network of physical objects or things like human, mobile phones, washing machines, lamps and wearable devices, etc. These objects are connected to the internet to communicate or sense or exchange data with each other. Here, the interaction can be happened between human-human, human-things, and things-things.</p>\r\n            <p class=\"text-justified\"><b>Simple Example:</b> You are on your way to a meeting by car, you get stuck in the traffic. If your car has access to your schedule or calendar, it can notify concerned person that you will be late. Here, everything will be automatic and instant between things-things and human-things.</p>\r\n            <h3><b>How IoT Solution is Helpful for Businesses?</b></h3>\r\n            <p class=\"text-justified\">When comes to the usage of IoT in the business sector, the possibilities are endless. The global technology research firm ‘Gartner’ says there would be over 26 billion connected iOT devices in the world by 2020. According to other recognized research firms, this number would cross 100 billion. But, here, the highest usage of IoT devices would be by businesses, as they said.</p>\r\n            <p class=\"text-justified\">Any business can use IoT to enhance work productivity as well as business performance. But, the most important is proper implementation. Especially, in the manufacturing and health industry, IoT (internet of things) solution is greatly useful for organizing tools, machines and people simultaneously and in a systematic way. Also, you can use our advanced IoT solution to safeguard your remote place and its assets through sensors and e-surveillance. The integration of IoT technolgy into wearables is the forward thinking approach followed by enterprises to save hours and millions.</p>\r\n            <h3><b>BigPerl IOT Solutions</b></h3>\r\n            <p class=\"text-justified\">We at BigPerl are the world’s fastest growing mobility company. Powered with 1000+ successful mobile, enterprise apps and game development stories, Bigperl solution – iot apps development company / IOT apps developers, is helping all sorts of businesses to enhance work productivity and business performance with IoT and Wearable solutions. Thus, enabling our customers to harness the power of future technology and to stay ahead of the competition.</p>\r\n        </div>\r\n\r\n        <div class=\"col-md-5\">\r\n            <img src=\"assets/img/services/Internet_of_Things_(IoT)_bigperl.png\" class=\"img-responsive \" alt=\"Internet_of_Things_(IoT)_bigperl\">\r\n        </div>\r\n    </div>\r\n    <app-footer5></app-footer5>\r\n           \r\n    <app-footer2></app-footer2>"

/***/ }),

/***/ "./src/app/servicessection/internet-of-things/internet-of-things.component.ts":
/*!************************************************************************************!*\
  !*** ./src/app/servicessection/internet-of-things/internet-of-things.component.ts ***!
  \************************************************************************************/
/*! exports provided: InternetOfThingsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InternetOfThingsComponent", function() { return InternetOfThingsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var InternetOfThingsComponent = /** @class */ (function () {
    function InternetOfThingsComponent() {
    }
    InternetOfThingsComponent.prototype.ngOnInit = function () {
    };
    InternetOfThingsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-internet-of-things',
            template: __webpack_require__(/*! ./internet-of-things.component.html */ "./src/app/servicessection/internet-of-things/internet-of-things.component.html"),
            styles: [__webpack_require__(/*! ./internet-of-things.component.css */ "./src/app/servicessection/internet-of-things/internet-of-things.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], InternetOfThingsComponent);
    return InternetOfThingsComponent;
}());



/***/ }),

/***/ "./src/app/servicessection/ios-developer/ios-developer.component.css":
/*!***************************************************************************!*\
  !*** ./src/app/servicessection/ios-developer/ios-developer.component.css ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/servicessection/ios-developer/ios-developer.component.html":
/*!****************************************************************************!*\
  !*** ./src/app/servicessection/ios-developer/ios-developer.component.html ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n        <!--page title start-->\r\n        <section class=\"page-title ptb-30\">\r\n          <div class=\"container padding-top-40\">\r\n              <div class=\"row\">\r\n                  <div class=\"col-md-12\">\r\n                      <h6 class=\"pagetitles\">Services</h6>\r\n                      <ol class=\"breadcrumb\">\r\n                          <li><a href=\"#\">Home</a></li>\r\n                          <li><a href=\"#\">Services</a></li>\r\n                          <li class=\"active\">ios Developer</li>\r\n                      </ol>\r\n                  </div>\r\n              </div>\r\n          </div>\r\n      </section>\r\n      <!--page title end-->\r\n\r\n      <img src=\"assets/img/banners/iOS_developer_bigperl_banner.png\" alt=\"iOS_developer_bigperl_banner\" class=\"fullwidth\">\r\n\r\n            <div class=\"container padding-top-20\">\r\n                <div class=\"col-md-7\">\r\n                    <h2 class=\"text-bold mb-10\">IOS Developer</h2>\r\n                    <p class=\"text-justified\">BigPerl iOS developers develops mobile operating system by Apple Inc. and strewn for Apple hardware. iOS for application impacts on business. iPhone is the leading mobile in the world. iOS applications preserve resilient relationship with client. We develops user friendly and convenient user interface. iPhones and its features are more in the market. Our iOS application keeps you very demandable in the business and market.</p>\r\n                    <p class=\"text-justified\">We develop more user friendly applications, which support various other Apple devices as iPad and iPod touch. iPhone application developers build customized, secure, reliable applications that enhance your brand in the business. BigPerl experienced iOS a developer understands the current trend and technology to develop also offers front-end development, back – end development, integration and deployment on server.</p>\r\n                    <p><b>Why iOS??</b></p>\r\n                    <ol>\r\n                      <li>Simple, Quick, Easy to use and Effective.</li>\r\n                      <li>Security – iOS doesn’t have any malware or bloatware which make your mobile device slow and ugly.</li>\r\n                      <li>Best-looking phones on the market and consistent user experience keeps user happy.</li>\r\n                      <li>Easy integration with other Apple devices and saves time.</li>\r\n                    </ol>\r\n                </div>\r\n\r\n                <div class=\"col-md-5\">\r\n                    <img src=\"assets/img/services/ios_developer_bigperl.png\" class=\"img-responsive \" alt=\"ios_developer_bigperl\">\r\n                </div>\r\n            </div>\r\n\r\n\r\n            <app-footer5></app-footer5>\r\n           \r\n            <app-footer2></app-footer2>"

/***/ }),

/***/ "./src/app/servicessection/ios-developer/ios-developer.component.ts":
/*!**************************************************************************!*\
  !*** ./src/app/servicessection/ios-developer/ios-developer.component.ts ***!
  \**************************************************************************/
/*! exports provided: IosDeveloperComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IosDeveloperComponent", function() { return IosDeveloperComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var IosDeveloperComponent = /** @class */ (function () {
    function IosDeveloperComponent() {
    }
    IosDeveloperComponent.prototype.ngOnInit = function () {
    };
    IosDeveloperComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-ios-developer',
            template: __webpack_require__(/*! ./ios-developer.component.html */ "./src/app/servicessection/ios-developer/ios-developer.component.html"),
            styles: [__webpack_require__(/*! ./ios-developer.component.css */ "./src/app/servicessection/ios-developer/ios-developer.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], IosDeveloperComponent);
    return IosDeveloperComponent;
}());



/***/ }),

/***/ "./src/app/servicessection/ipad-app-development/ipad-app-development.component.css":
/*!*****************************************************************************************!*\
  !*** ./src/app/servicessection/ipad-app-development/ipad-app-development.component.css ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/servicessection/ipad-app-development/ipad-app-development.component.html":
/*!******************************************************************************************!*\
  !*** ./src/app/servicessection/ipad-app-development/ipad-app-development.component.html ***!
  \******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "  <!--page title start-->\r\n  <section class=\"page-title ptb-30\">\r\n    <div class=\"container padding-top-40\">\r\n        <div class=\"row\">\r\n            <div class=\"col-md-12\">\r\n                <h6 class=\"pagetitles\">Services</h6>\r\n                <ol class=\"breadcrumb\">\r\n                    <li><a href=\"#\">Home</a></li>\r\n                    <li><a href=\"#\">Services</a></li>\r\n                    <li class=\"active\">ipad app development</li>\r\n                </ol>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</section>\r\n<!--page title end-->\r\n\r\n<img src=\"assets/img/banners/ipad_app_development_bigperl_banner.png\" alt=\"ipad_app_development_bigperl_banner\" class=\"fullwidth\">\r\n\r\n      <div class=\"container padding-top-20\">\r\n          <div class=\"col-md-7\">\r\n              <h2 class=\"text-bold mb-10\">Ipad App Development</h2>\r\n<<<<<<< HEAD\r\n    \r\n<p class=\"text-justified\"><b>iPad has became new revelunatory device after iPhone. Even though multiple other options available in tablet screen sizes iPad keep holding its top dominance due to innovation and quality.</b></p>\r\n              \r\n    <p class=\"text-justified\"> Apple launched its first iPad in April 3 2010. As of June 2017 4 Generations of iPad, and one Generation of the iPad Mini variety versions released. By January 2015, there have been over 250 million iPad sold. End users are finding using iPads more convenient and productive rather carrying laptops.</p>\r\n=======\r\n    <h4 class=\"text-bold mb-8\">iPad has became new revelunatory device after iPhone. Even though multiple other options available in tablet screen sizes iPad keep holding its top dominance due to innovation and quality.</h4>\r\n              <p class=\"text-justified\"> Apple launched its first iPad in April 3 2010. As of June 2017 4 Generations of iPad, and one Generation of the iPad Mini variety versions released. By January 2015, there have been over 250 million iPad sold. End users are finding using iPads more convenient and productive rather carrying laptops.</p>\r\n>>>>>>> b18199f708ae7f110e846ee5c11e31868612d556\r\n              <p class=\"text-justified\">Beling mobile app development company our team is highly competent in developing unique apps for iPads, which fits to customer business need as well as work on different orientation. Our team builds high performing,robust and secure iPad solutions which gives best in class usability experience to end users.. BigPerl solution Pvt Ltd is one of the iPad app development company where apps not only suits the requirements of customer business, but also meets the quality standards of iPad app development, at the same time not compromising on rich visual features. iPad app always gives a powerful way to work, learn and explore new things, and play. Take advantage of new features in iPad and create even more efficient and personal user experiences in your iPad apps. We are one of best iPad app development companies in India, USA, United Kingdom and more. We have full-fledged iOS app development centers in India and USA. BigPerl holds strong expertise and leadership among iPad developers community. we would love to be a part of making feature-rich application and have highly dedicated iPad app developer team having 5+ years of experience for research and development to explore upcoming technology and give there 100% to utilize them in your business. We provide 24 X 7 support for our customer and work along them.Our support structure is developed keeping specific needs of global entrepreneurs and enterprise customers in place.</p>\r\n    <p class=\"text-justified\"><b>Our Service offerings:</b></p>\r\n    <p class=\"text-justified\"> 1.iPhone Application Development </p>\r\n              <p class=\"text-justified\"> 2.iPad Application Development</p>\r\n              <p class=\"text-justified\"> 3.Apple Watch Applications</p>\r\n              <p class=\"text-justified\"> 4.Apple TV Applications </p>\r\n              <p class=\"text-justified\"> 5.Mobile App updates  </p>\r\n    <p class=\"text-justified\"> We follow tailored pricing model , which starts from Fixed Pricing and goes till hire a team. </p>\r\n          </div>\r\n\r\n          <div class=\"col-md-5\">\r\n              <img src=\"assets/img/services/iPad_app_development_bigperl.png\" class=\"img-responsive \" alt=\"iPad_app_development_bigperl\">\r\n          </div>\r\n      </div>\r\n\r\n      <app-footer5></app-footer5>\r\n           \r\n      <app-footer2></app-footer2>"

/***/ }),

/***/ "./src/app/servicessection/ipad-app-development/ipad-app-development.component.ts":
/*!****************************************************************************************!*\
  !*** ./src/app/servicessection/ipad-app-development/ipad-app-development.component.ts ***!
  \****************************************************************************************/
/*! exports provided: IpadAppDevelopmentComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IpadAppDevelopmentComponent", function() { return IpadAppDevelopmentComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var IpadAppDevelopmentComponent = /** @class */ (function () {
    function IpadAppDevelopmentComponent() {
    }
    IpadAppDevelopmentComponent.prototype.ngOnInit = function () {
    };
    IpadAppDevelopmentComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-ipad-app-development',
            template: __webpack_require__(/*! ./ipad-app-development.component.html */ "./src/app/servicessection/ipad-app-development/ipad-app-development.component.html"),
            styles: [__webpack_require__(/*! ./ipad-app-development.component.css */ "./src/app/servicessection/ipad-app-development/ipad-app-development.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], IpadAppDevelopmentComponent);
    return IpadAppDevelopmentComponent;
}());



/***/ }),

/***/ "./src/app/servicessection/iphone-app-development/iphone-app-development.component.css":
/*!*********************************************************************************************!*\
  !*** ./src/app/servicessection/iphone-app-development/iphone-app-development.component.css ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/servicessection/iphone-app-development/iphone-app-development.component.html":
/*!**********************************************************************************************!*\
  !*** ./src/app/servicessection/iphone-app-development/iphone-app-development.component.html ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "  <!--page title start-->\r\n  <section class=\"page-title ptb-30\">\r\n    <div class=\"container padding-top-40\">\r\n        <div class=\"row\">\r\n            <div class=\"col-md-12\">\r\n                <h6 class=\"pagetitles\">Services</h6>\r\n                <ol class=\"breadcrumb\">\r\n                    <li><a href=\"#\">Home</a></li>\r\n                    <li><a href=\"#\">Services</a></li>\r\n                    <li class=\"active\">ios application development</li>\r\n                </ol>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</section>\r\n<!--page title end-->\r\n\r\n<img src=\"assets/img/banners/iOS_app_development_bigperl_banner.png\" alt=\"iOS_app_development_bigperl_banner\" class=\"fullwidth\">\r\n\r\n      <div class=\"container padding-top-20\">\r\n          <div class=\"col-md-7\">\r\n              <h2 class=\"text-bold mb-10\">Iphone App Development(iOS,watchOS,macOS,TvOS) </h2>\r\n              <p class=\"text-justified\"> Apple Inc. devices, being always be a market leader in smart phones segment, it provides excellent variation in features and technology updates. iOS is unique platform because of its usability, design, functionality and vision of the software. Now the iPhone becomes world's most powerful personal device. iPhone taking over most of the mobile devices because of its immense demand in market. The iPhone is a revolutionary & a game-changer for the mobile phone industry, and credited with helping to make Apple one of the world's most valuable publicly traded companies by 2011. Most of the mobile app development companies around the world have chosen iPhone as a leading business standard device. </p>\r\n              <p class=\"text-justified\"> BigPerl is developing highly scalable, robust and immense iPhone apps. Since 2012 we are on mission of building high quality technology solutions and enabling them on high performing mobile apps. We are in one of fastest growing iPhone app development companies in world. BigPerl is having strong experienced iOS developers in its team, and are dedicated to deliver high quality mobile applications within a stipulated time. We have full-fledged ios app development centers in India and United States. We are having one of best team on iPhone app technology research and development, and are having excellent track record of adopting and delivering on latest technologies and its updates. This helps our engineers to provide best of its features to our clients. As top iPhone app development company we provide 24 X 7 dedicated support. Our support is designed for delivering best of solution to our global entrepreneurs and enterprise customers.</p>\r\n    <p class=\"text-justified\"> We have customised solution and pricing price offering for Entrepreneurs as well as Enterprises. Our pricing starts from Pay As you Go and available till hire a dedicated team.</p>\r\n          </div>\r\n\r\n          <div class=\"col-md-5\">\r\n              <img src=\"assets/img/services/ios_app_development_bigperl.png\" class=\"img-responsive \" alt=\"ios_app_development_bigperl\">\r\n          </div>\r\n      </div>\r\n\r\n      <app-footer5></app-footer5>\r\n           \r\n      <app-footer2></app-footer2>"

/***/ }),

/***/ "./src/app/servicessection/iphone-app-development/iphone-app-development.component.ts":
/*!********************************************************************************************!*\
  !*** ./src/app/servicessection/iphone-app-development/iphone-app-development.component.ts ***!
  \********************************************************************************************/
/*! exports provided: IphoneAppDevelopmentComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IphoneAppDevelopmentComponent", function() { return IphoneAppDevelopmentComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var IphoneAppDevelopmentComponent = /** @class */ (function () {
    function IphoneAppDevelopmentComponent() {
    }
    IphoneAppDevelopmentComponent.prototype.ngOnInit = function () {
    };
    IphoneAppDevelopmentComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-iphone-app-development',
            template: __webpack_require__(/*! ./iphone-app-development.component.html */ "./src/app/servicessection/iphone-app-development/iphone-app-development.component.html"),
            styles: [__webpack_require__(/*! ./iphone-app-development.component.css */ "./src/app/servicessection/iphone-app-development/iphone-app-development.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], IphoneAppDevelopmentComponent);
    return IphoneAppDevelopmentComponent;
}());



/***/ }),

/***/ "./src/app/servicessection/java-j2ee-development/java-j2ee-development.component.css":
/*!*******************************************************************************************!*\
  !*** ./src/app/servicessection/java-j2ee-development/java-j2ee-development.component.css ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/servicessection/java-j2ee-development/java-j2ee-development.component.html":
/*!********************************************************************************************!*\
  !*** ./src/app/servicessection/java-j2ee-development/java-j2ee-development.component.html ***!
  \********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n        <!--page title start-->\r\n        <section class=\"page-title ptb-30\">\r\n          <div class=\"container padding-top-40\">\r\n              <div class=\"row\">\r\n                  <div class=\"col-md-12\">\r\n                      <h6 class=\"pagetitles\">Services</h6>\r\n                      <ol class=\"breadcrumb\">\r\n                          <li><a href=\"#\">Home</a></li>\r\n                          <li><a href=\"#\">Services</a></li>\r\n                          <li class=\"active\">java and j2ee development</li>\r\n                      </ol>\r\n                  </div>\r\n              </div>\r\n          </div>\r\n      </section>\r\n      <!--page title end-->\r\n\r\n  <!-- <?php include 'header1.php' ?> -->\r\n\r\n            <div class=\"container padding-top-20\">\r\n                <div class=\"col-md-7\">\r\n                    <h2 class=\"text-bold mb-10\">Java and J2EE Development</h2>\r\n                    <p class=\"text-justified\">One of the most successful programming language because of its ease to use and platform independence is Java. This is infact the most widely used application programming language in today’s world.</p>\r\n                    <p class=\"text-justified\">Bigperl Solutions Pvt Ltd. is renowned as a Java development service provider that offers exceptional Java/J2EE programming services to suit the web and mobile application development requirements of the clients. Our company specifically fulfill the needs of diverse enterprises impeccably. The company aims its J2EE application development service to be scalable and easy to implement. Because of this approach, Java programming language is an obvious choice for us to handle the J2EE application development projects.</p>\r\n                    <p class=\"text-justified\">Our Java developers dedicatedly work on various verticals of Java/J2EE development to deliver the business solution to enterprises of all the sizes. Talented pool of Java programmers at our company is enriched with several years of experience in Java development services. We are adroit at delivering remarkable and competitive solutions to precisely in line with clients’ needs.</p>\r\n                </div>\r\n\r\n                <div class=\"col-md-5\">\r\n                    <img src=\"assets/img/services/java_j2ee_development_bigperl.png\" class=\"img-responsive \" alt=\"java_j2ee_development_bigperl\">\r\n                </div>\r\n            </div>\r\n            <app-footer5></app-footer5>\r\n           \r\n<app-footer2></app-footer2>\r\n"

/***/ }),

/***/ "./src/app/servicessection/java-j2ee-development/java-j2ee-development.component.ts":
/*!******************************************************************************************!*\
  !*** ./src/app/servicessection/java-j2ee-development/java-j2ee-development.component.ts ***!
  \******************************************************************************************/
/*! exports provided: JavaJ2eeDevelopmentComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "JavaJ2eeDevelopmentComponent", function() { return JavaJ2eeDevelopmentComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var JavaJ2eeDevelopmentComponent = /** @class */ (function () {
    function JavaJ2eeDevelopmentComponent() {
    }
    JavaJ2eeDevelopmentComponent.prototype.ngOnInit = function () {
    };
    JavaJ2eeDevelopmentComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-java-j2ee-development',
            template: __webpack_require__(/*! ./java-j2ee-development.component.html */ "./src/app/servicessection/java-j2ee-development/java-j2ee-development.component.html"),
            styles: [__webpack_require__(/*! ./java-j2ee-development.component.css */ "./src/app/servicessection/java-j2ee-development/java-j2ee-development.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], JavaJ2eeDevelopmentComponent);
    return JavaJ2eeDevelopmentComponent;
}());



/***/ }),

/***/ "./src/app/servicessection/joomla-development/joomla-development.component.css":
/*!*************************************************************************************!*\
  !*** ./src/app/servicessection/joomla-development/joomla-development.component.css ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/servicessection/joomla-development/joomla-development.component.html":
/*!**************************************************************************************!*\
  !*** ./src/app/servicessection/joomla-development/joomla-development.component.html ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--page title start-->\r\n<section class=\"page-title ptb-30\">\r\n  <div class=\"container padding-top-40\">\r\n      <div class=\"row\">\r\n          <div class=\"col-md-12\">\r\n              <h6 class=\"pagetitles\">Services</h6>\r\n              <ol class=\"breadcrumb\">\r\n                  <li><a href=\"#\">Home</a></li>\r\n                  <li><a href=\"#\">Services</a></li>\r\n                  <li class=\"active\">joomla development</li>\r\n              </ol>\r\n          </div>\r\n      </div>\r\n  </div>\r\n</section>\r\n<!--page title end-->\r\n\r\n<img src=\"assets/img/banners/joomla_development_bigperl_banner.png\" alt=\"joomla_development_bigperl_banner\" class=\"fullwidth\">\r\n\r\n    <div class=\"container padding-top-20\">\r\n        <div class=\"col-md-7\">\r\n            <h2 class=\"text-bold mb-10\">Joomla Development</h2>\r\n            <p class=\"text-justified\">Joomla website development is a powerful framework for website application development. Our team of highly qualified Joomla developers begins in Bigperl Based in bangalore will provide you with quality, high-end Joomla development. Our Joomla Development team consists of skills, highly proficient and experienced experts with years of experience with Joomla CMS and its latest versions including Joomla 3.0.0.</p>\r\n            <ul>\r\n              <li><b>Service Offered : </b></li>\r\n              <li>Offshore Joomla Extensions Development</li>\r\n              <li>Joomla e-Commerce website development</li>\r\n              <li>Joomla site integration & social media migration services</li>\r\n              <li>Joomla troubleshooting & support, Maintenance</li>\r\n            </ul>\r\n        </div>\r\n\r\n        <div class=\"col-md-5\">\r\n            <img src=\"assets/img/services/joomla_development_bigperl.png\" class=\"img-responsive \" alt=\"joomla_development_bigperl\">\r\n        </div>\r\n    </div>\r\n    <app-footer5></app-footer5>\r\n           \r\n    <app-footer2></app-footer2>"

/***/ }),

/***/ "./src/app/servicessection/joomla-development/joomla-development.component.ts":
/*!************************************************************************************!*\
  !*** ./src/app/servicessection/joomla-development/joomla-development.component.ts ***!
  \************************************************************************************/
/*! exports provided: JoomlaDevelopmentComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "JoomlaDevelopmentComponent", function() { return JoomlaDevelopmentComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var JoomlaDevelopmentComponent = /** @class */ (function () {
    function JoomlaDevelopmentComponent() {
    }
    JoomlaDevelopmentComponent.prototype.ngOnInit = function () {
    };
    JoomlaDevelopmentComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-joomla-development',
            template: __webpack_require__(/*! ./joomla-development.component.html */ "./src/app/servicessection/joomla-development/joomla-development.component.html"),
            styles: [__webpack_require__(/*! ./joomla-development.component.css */ "./src/app/servicessection/joomla-development/joomla-development.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], JoomlaDevelopmentComponent);
    return JoomlaDevelopmentComponent;
}());



/***/ }),

/***/ "./src/app/servicessection/laravel-development/laravel-development.component.css":
/*!***************************************************************************************!*\
  !*** ./src/app/servicessection/laravel-development/laravel-development.component.css ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/servicessection/laravel-development/laravel-development.component.html":
/*!****************************************************************************************!*\
  !*** ./src/app/servicessection/laravel-development/laravel-development.component.html ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n        <!--page title start-->\r\n        <section class=\"page-title ptb-30\">\r\n          <div class=\"container padding-top-40\">\r\n              <div class=\"row\">\r\n                  <div class=\"col-md-12\">\r\n                      <h6 class=\"pagetitles\">Services</h6>\r\n                      <ol class=\"breadcrumb\">\r\n                          <li><a href=\"#\">Home</a></li>\r\n                          <li><a href=\"#\">Services</a></li>\r\n                          <li class=\"active\">laravel development</li>\r\n                      </ol>\r\n                  </div>\r\n              </div>\r\n          </div>\r\n      </section>\r\n      <!--page title end-->\r\n\r\n      <img src=\"assets/img/banners/laravel_development_bigperl_banner.png\" alt=\"laravel_development_bigperl_banner\" class=\"fullwidth\">\r\n\r\n            <div class=\"container padding-top-20\">\r\n                <div class=\"col-md-7\">\r\n                    <h2 class=\"text-bold mb-10\">Laravel Development</h2>\r\n                    <p class=\"text-justified\">With quickly changing web development methods, we need to stay updated and keep pace with the market. </p>\r\n                    <p>Laravel is a clean and classy framework of PHP which offers you the best apps developed with simple but expressive syntax. Laravel framework eases the web application development. Laravel helps in developing high end applications with out of the box features and powerful websites.</p>\r\n                    <p>With Laravel you have the freedom to reap maximum benefits and get the best in class solutions. Laravel has the specialty of keeping all the major SQL codes in a separate model file. It comes with built-in tools for security which makes your website development more future proofed and reliable. Laravel has a modular packaging system which helps in saving time and focus on other priorities.</p>\r\n                    <ul>\r\n                      <h3><b>Services Offered:</b></h3>\r\n                      <li>Laravel Application Development</li>\r\n                      <li>Laravel Ecommerce Store Development</li>\r\n                      <li>Laravel Customization and Integration</li>\r\n                      <li>Laravel Data Migration</li>\r\n                      <li>Laravel Template Design</li>\r\n                      <li>Laravel RESTful app development</li>\r\n                    </ul>\r\n                </div>\r\n\r\n                <div class=\"col-md-5\">\r\n                    <img src=\"assets/img/services/laravel_development_bigperl.png\" class=\"img-responsive \" alt=\"laravel_development_bigperl\">\r\n                </div>\r\n            </div>\r\n            <app-footer5></app-footer5>\r\n           \r\n            <app-footer2></app-footer2>"

/***/ }),

/***/ "./src/app/servicessection/laravel-development/laravel-development.component.ts":
/*!**************************************************************************************!*\
  !*** ./src/app/servicessection/laravel-development/laravel-development.component.ts ***!
  \**************************************************************************************/
/*! exports provided: LaravelDevelopmentComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LaravelDevelopmentComponent", function() { return LaravelDevelopmentComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var LaravelDevelopmentComponent = /** @class */ (function () {
    function LaravelDevelopmentComponent() {
    }
    LaravelDevelopmentComponent.prototype.ngOnInit = function () {
    };
    LaravelDevelopmentComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-laravel-development',
            template: __webpack_require__(/*! ./laravel-development.component.html */ "./src/app/servicessection/laravel-development/laravel-development.component.html"),
            styles: [__webpack_require__(/*! ./laravel-development.component.css */ "./src/app/servicessection/laravel-development/laravel-development.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], LaravelDevelopmentComponent);
    return LaravelDevelopmentComponent;
}());



/***/ }),

/***/ "./src/app/servicessection/magento-developer/magento-developer.component.css":
/*!***********************************************************************************!*\
  !*** ./src/app/servicessection/magento-developer/magento-developer.component.css ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/servicessection/magento-developer/magento-developer.component.html":
/*!************************************************************************************!*\
  !*** ./src/app/servicessection/magento-developer/magento-developer.component.html ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "  <!--page title start-->\r\n  <section class=\"page-title ptb-30\">\r\n    <div class=\"container padding-top-40\">\r\n        <div class=\"row\">\r\n            <div class=\"col-md-12\">\r\n                <h6 class=\"pagetitles\">Services</h6>\r\n                <ol class=\"breadcrumb\">\r\n                    <li><a href=\"#\">Home</a></li>\r\n                    <li><a href=\"#\">Services</a></li>\r\n                    <li class=\"active\">magento developer</li>\r\n                </ol>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</section>\r\n<!--page title end-->\r\n\r\n<img src=\"assets/img/banners/magento_developer_bigperl_banner.png\" alt=\"magento_developer_bigperl_banner\" class=\"fullwidth\">\r\n\r\n      <div class=\"container padding-top-20\">\r\n          <div class=\"col-md-7\">\r\n              <h2 class=\"text-bold mb-10\">Magento Developer</h2>\r\n              <p class=\"text-justified\">The Magento Commerce platform provides Business owners a powerful marketing (or Advertising) tool with flexibility and control of their eCommerce website. The Magento eCommerce Application is integrated with HTML catalog that helps to gain good rankings preferred (promotes/) by search engines.</p>\r\n              <p><b>Services offered by BigPerl :</b></p>\r\n              <ol>\r\n                <li>Magento website development and customization.</li>\r\n                <li>Magento installation and configuration.</li>\r\n                <li>Magento module development.</li>\r\n                <li>Magento online shopping cart development.</li>\r\n                <li>Magento theme development.</li>\r\n                <li>Magento template integration.</li>\r\n                <li>Magento In-built SEO development.</li>\r\n                <li>Magento plug-in development.</li>\r\n                <li>Magento extensions development.</li>\r\n                <li>Magento CMS development.</li>\r\n                <li>20+ payment gateway support.</li>\r\n                <li>Compatible with SSL security.</li>\r\n              </ol>\r\n              <p>BigPerl has rich Magento Developers’ team with absolute knowledge and experience of handling online eCommerce solutions of any magnitude. Our Magento developer ensures the best usability, best practices and also optimizes for ultimate compression while delivering the best eCommerce solution for your online business to meet the expectations of the customers.</p>\r\n              <p><b>Why BigPerl is the perfect choice for Magento development?</b></p>\r\n              <ol>\r\n                <li>Fast deliver to customer that can positively affect customer satisfaction.</li>\r\n                <li>Best Services in Affordable Cost – Cost effective</li>\r\n                <li>Strong User Interface in Ecommerce.</li>\r\n                <li>High quality Application development services.</li>\r\n                <li>Magento is Powerful eCommerce websites from Web Traffic.</li>\r\n              </ol>\r\n          </div>\r\n\r\n          <div class=\"col-md-5\">\r\n              <img src=\"assets/img/services/magento_developer_bigperl.png\" class=\"img-responsive \" alt=\"magento_developer_bigperl\">\r\n          </div>\r\n      </div>\r\n      <app-footer5></app-footer5>\r\n           \r\n      <app-footer2></app-footer2>"

/***/ }),

/***/ "./src/app/servicessection/magento-developer/magento-developer.component.ts":
/*!**********************************************************************************!*\
  !*** ./src/app/servicessection/magento-developer/magento-developer.component.ts ***!
  \**********************************************************************************/
/*! exports provided: MagentoDeveloperComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MagentoDeveloperComponent", function() { return MagentoDeveloperComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var MagentoDeveloperComponent = /** @class */ (function () {
    function MagentoDeveloperComponent() {
    }
    MagentoDeveloperComponent.prototype.ngOnInit = function () {
    };
    MagentoDeveloperComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-magento-developer',
            template: __webpack_require__(/*! ./magento-developer.component.html */ "./src/app/servicessection/magento-developer/magento-developer.component.html"),
            styles: [__webpack_require__(/*! ./magento-developer.component.css */ "./src/app/servicessection/magento-developer/magento-developer.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], MagentoDeveloperComponent);
    return MagentoDeveloperComponent;
}());



/***/ }),

/***/ "./src/app/servicessection/magento-development/magento-development.component.css":
/*!***************************************************************************************!*\
  !*** ./src/app/servicessection/magento-development/magento-development.component.css ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/servicessection/magento-development/magento-development.component.html":
/*!****************************************************************************************!*\
  !*** ./src/app/servicessection/magento-development/magento-development.component.html ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n\r\n\r\n        <!--page title start-->\r\n        <section class=\"page-title ptb-30\">\r\n            <div class=\"container padding-top-40\">\r\n                <div class=\"row\">\r\n                    <div class=\"col-md-12\">\r\n                        <h6 class=\"pagetitles\">Services</h6>\r\n                        <ol class=\"breadcrumb\">\r\n                            <li><a href=\"#\">Home</a></li>\r\n                            <li><a href=\"#\">Services</a></li>\r\n                            <li class=\"active\">magento development</li>\r\n                        </ol>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </section>\r\n        <!--page title end-->\r\n\r\n\t\t    <img src=\"assets/img/banners/magento_development_bigperl_banner.png\" alt=\"magento_development_bigperl_banner\" class=\"fullwidth\">\r\n\r\n              <div class=\"container padding-top-20\">\r\n                  <div class=\"col-md-7\">\r\n                      <h2 class=\"text-bold mb-10\">Magento Development</h2>\r\n                      <p class=\"text-justified\">You have the perfect idea for an eCommerce website? You already have a store to take to the next level?</p>\r\n                      <p>Either way, be rest assured we’ll get it done. We are the master chefs of Magento having deep experience crafting beautiful and scalable shopping websites.</p>\r\n                      <p class=\"text-justified\">Bigperl believes that an E-Commerce can be successful if it’s tailored to meet the requirements of both the trading and the client’s side. The Magento website development platform gives us the freedom to customize the website in best possible manner to suit your requirements.</p>\r\n                      <p class=\"text-justified\">As professional web designers and developers we keep researching a lot on latest technologies and platforms available and Magento is also a result of this research. We at Bigperl found Magento Ecommerce development to be successful and powerful platform because of the following reasons:</p>\r\n                      <ul>\r\n                        <li>Effortlessly customizable to meet the requirements.</li>\r\n                        <li>It supports number of gateways for payment.</li>\r\n                        <li>There is an array of design and themes to choose from.</li>\r\n                        <li>One of the few open source platforms that offer compatibility with mobile phones.</li>\r\n                        <li>Also it is search engine friendly as well.</li>\r\n                        <li>Changes to previous design and addition of new features are very easy with the help of the modules that can be added or removed.</li>\r\n                      </ul>\r\n                  </div>\r\n\r\n                  <div class=\"col-md-5\">\r\n                      <img src=\"assets/img/services/magento_development_bigperl.png\" class=\"img-responsive \" alt=\"magento_development_bigperl\">\r\n                  </div>\r\n              </div>\r\n\r\n              <app-footer5></app-footer5>\r\n           \r\n              <app-footer2></app-footer2>"

/***/ }),

/***/ "./src/app/servicessection/magento-development/magento-development.component.ts":
/*!**************************************************************************************!*\
  !*** ./src/app/servicessection/magento-development/magento-development.component.ts ***!
  \**************************************************************************************/
/*! exports provided: MagentoDevelopmentComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MagentoDevelopmentComponent", function() { return MagentoDevelopmentComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var MagentoDevelopmentComponent = /** @class */ (function () {
    function MagentoDevelopmentComponent() {
    }
    MagentoDevelopmentComponent.prototype.ngOnInit = function () {
    };
    MagentoDevelopmentComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-magento-development',
            template: __webpack_require__(/*! ./magento-development.component.html */ "./src/app/servicessection/magento-development/magento-development.component.html"),
            styles: [__webpack_require__(/*! ./magento-development.component.css */ "./src/app/servicessection/magento-development/magento-development.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], MagentoDevelopmentComponent);
    return MagentoDevelopmentComponent;
}());



/***/ }),

/***/ "./src/app/servicessection/phone-gap-development/phone-gap-development.component.css":
/*!*******************************************************************************************!*\
  !*** ./src/app/servicessection/phone-gap-development/phone-gap-development.component.css ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/servicessection/phone-gap-development/phone-gap-development.component.html":
/*!********************************************************************************************!*\
  !*** ./src/app/servicessection/phone-gap-development/phone-gap-development.component.html ***!
  \********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--page title start-->\r\n<section class=\"page-title ptb-30\">\r\n  <div class=\"container padding-top-40\">\r\n      <div class=\"row\">\r\n          <div class=\"col-md-12\">\r\n              <h6 class=\"pagetitles\">Services</h6>\r\n              <ol class=\"breadcrumb\">\r\n                  <li><a href=\"#\">Home</a></li>\r\n                  <li><a href=\"#\">Services</a></li>\r\n                  <li class=\"active\">phonegap developer</li>\r\n              </ol>\r\n          </div>\r\n      </div>\r\n  </div>\r\n</section>\r\n<!--page title end-->\r\n\r\n<img src=\"assets/img/banners/phonegap_developer_bigperl_banner.png\" alt=\"phonegap_developer_bigperl_banner\" class=\"fullwidth\">\r\n\r\n    <div class=\"container padding-top-20\">\r\n        <div class=\"col-md-7\">\r\n            <h2 class=\"text-bold mb-10\">Phone Gap Developer</h2>\r\n            <p class=\"text-justified\">PhoneGap is a mobile application development platform developed by Apache Software Foundation. PhoneGap App Development is a simplified technique of developing applications for platforms, i.e. Windows, Android, iPhone and iPads, Palm, BlackBerry or iOS. With lesser and defined tools, mobile application development on PhoneGap can be developed using only a single code, allowing diverse compatibility for different mobile phones and platforms.</p>\r\n            <p class=\"text-justified\">BigPerl is the top PhoneGap development company in India bringing you the best Application in your budget. Feel free to contact us and get the best support.</p>\r\n            <p><b>Benefits with PhoneGap development services:</b></p>\r\n            <ol>\r\n              <li>Quick Development Process.</li>\r\n              <li>Cross Platform Quality.</li>\r\n              <li>Using Native API’s.</li>\r\n              <li>Open Source Development.</li>\r\n            </ol>\r\n            <p><b>Why BigPerl is the perfect choice for PhoneGap development?</b></p>\r\n            <ol>\r\n              <li>Cost-effective Mobile Application Development.</li>\r\n              <li>Flexible Client’s Requirements and Custom PhoneGap development</li>\r\n              <li>Experience Hands-on PhoneGap Development.</li>\r\n              <li>On-Time Project Delivery.</li>\r\n            </ol>\r\n        </div>\r\n\r\n        <div class=\"col-md-5\">\r\n            <img src=\"assets/img/services/phonegap_developer_bigperl.png\" class=\"img-responsive \" alt=\"phonegap_developer_bigperl\">\r\n        </div>\r\n    </div>\r\n    <app-footer5></app-footer5>\r\n           \r\n    <app-footer2></app-footer2>"

/***/ }),

/***/ "./src/app/servicessection/phone-gap-development/phone-gap-development.component.ts":
/*!******************************************************************************************!*\
  !*** ./src/app/servicessection/phone-gap-development/phone-gap-development.component.ts ***!
  \******************************************************************************************/
/*! exports provided: PhoneGapDevelopmentComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PhoneGapDevelopmentComponent", function() { return PhoneGapDevelopmentComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var PhoneGapDevelopmentComponent = /** @class */ (function () {
    function PhoneGapDevelopmentComponent() {
    }
    PhoneGapDevelopmentComponent.prototype.ngOnInit = function () {
    };
    PhoneGapDevelopmentComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-phone-gap-development',
            template: __webpack_require__(/*! ./phone-gap-development.component.html */ "./src/app/servicessection/phone-gap-development/phone-gap-development.component.html"),
            styles: [__webpack_require__(/*! ./phone-gap-development.component.css */ "./src/app/servicessection/phone-gap-development/phone-gap-development.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], PhoneGapDevelopmentComponent);
    return PhoneGapDevelopmentComponent;
}());



/***/ }),

/***/ "./src/app/servicessection/php-developer/php-developer.component.css":
/*!***************************************************************************!*\
  !*** ./src/app/servicessection/php-developer/php-developer.component.css ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/servicessection/php-developer/php-developer.component.html":
/*!****************************************************************************!*\
  !*** ./src/app/servicessection/php-developer/php-developer.component.html ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n        <!--page title start-->\r\n        <section class=\"page-title ptb-30\">\r\n          <div class=\"container padding-top-40\">\r\n              <div class=\"row\">\r\n                  <div class=\"col-md-12\">\r\n                      <h6 class=\"pagetitles\">Services</h6>\r\n                      <ol class=\"breadcrumb\">\r\n                          <li><a href=\"#\">Home</a></li>\r\n                          <li><a href=\"#\">Services</a></li>\r\n                          <li class=\"active\">php developer</li>\r\n                      </ol>\r\n                  </div>\r\n              </div>\r\n          </div>\r\n      </section>\r\n      <!--page title end-->\r\n\r\n      <img src=\"assets/img/banners/php_developer_bigperl_banner.png\" alt=\"php_developer_bigperl_banner\" class=\"fullwidth\">\r\n\r\n            <div class=\"container padding-top-20\">\r\n                <div class=\"col-md-7\">\r\n                    <h2 class=\"text-bold mb-10\">PHP Developer</h2>\r\n                    <p class=\"text-justified\">PHP is a dynamic, open source scripting language that can be directly embedded into HTML code for corporate applications and web development. Specially intended for server side web development where it runs on web server.</p>\r\n                    <p class=\"text-justified\">BigPerl PHP developer originates UI (user interface) and braces with MYSQL and Database in the back end development using techniques as MYSQL, ZEND, CAKE PHP and many others. We provide efficient, reliable product with speed. PHP helps us to create thumbnail images, crop and resize images, add watermarks and gallery for photos.</p>\r\n                    <p class=\"text-justified\">We have 50 plus tremendous work experienced developers with quality code. Our code bolsters you in the business retail. We make your website effectively for your business. PHP developer’s implement programs with well advanced technology such as CodeIgnitor. PHP development provides highly customized and competent MYSQL database management with greater efficiency, flexible, highly secured websites.</p>\r\n                    <p><b>Why BigPerl is the perfect choice for PHP development?</b></p>\r\n                    <ul>\r\n                      <li>Best Services in Affordable Cost – Cost effective.</li>\r\n                      <li>User friendly and Security.</li>\r\n                      <li>Experience Hands-on on PHP Development.</li>\r\n                    </ul>\r\n                </div>\r\n\r\n                <div class=\"col-md-5\">\r\n                    <img src=\"assets/img/services/php_developer_bigperl.png\" class=\"img-responsive \" alt=\"php_developer_bigperl\">\r\n                </div>\r\n            </div>\r\n            <app-footer5></app-footer5>\r\n           \r\n            <app-footer2></app-footer2>"

/***/ }),

/***/ "./src/app/servicessection/php-developer/php-developer.component.ts":
/*!**************************************************************************!*\
  !*** ./src/app/servicessection/php-developer/php-developer.component.ts ***!
  \**************************************************************************/
/*! exports provided: PhpDeveloperComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PhpDeveloperComponent", function() { return PhpDeveloperComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var PhpDeveloperComponent = /** @class */ (function () {
    function PhpDeveloperComponent() {
    }
    PhpDeveloperComponent.prototype.ngOnInit = function () {
    };
    PhpDeveloperComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-php-developer',
            template: __webpack_require__(/*! ./php-developer.component.html */ "./src/app/servicessection/php-developer/php-developer.component.html"),
            styles: [__webpack_require__(/*! ./php-developer.component.css */ "./src/app/servicessection/php-developer/php-developer.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], PhpDeveloperComponent);
    return PhpDeveloperComponent;
}());



/***/ }),

/***/ "./src/app/servicessection/php-development/php-development.component.css":
/*!*******************************************************************************!*\
  !*** ./src/app/servicessection/php-development/php-development.component.css ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/servicessection/php-development/php-development.component.html":
/*!********************************************************************************!*\
  !*** ./src/app/servicessection/php-development/php-development.component.html ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "  <!--page title start-->\r\n  <section class=\"page-title ptb-30\">\r\n    <div class=\"container padding-top-40\">\r\n        <div class=\"row\">\r\n            <div class=\"col-md-12\">\r\n                <h6 class=\"pagetitles\">Services</h6>\r\n                <ol class=\"breadcrumb\">\r\n                    <li><a href=\"#\">Home</a></li>\r\n                    <li><a href=\"#\">Services</a></li>\r\n                    <li class=\"active\">php development</li>\r\n                </ol>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</section>\r\n<!--page title end-->\r\n\r\n<img src=\"assets/img/banners/php_development_bigperl_banner.png\" alt=\"php_development_bigperl_banner\" class=\"fullwidth\">\r\n\r\n      <div class=\"container padding-top-20\">\r\n          <div class=\"col-md-7\">\r\n              <h2 class=\"text-bold mb-10\">PHP Development</h2>\r\n              <p>PHP is being used on 244+ million websites and 2.1 million web servers the world over.</p>\r\n              <p class=\"text-justified\">There are several key reasons to consider adopting our PHP development service as an implementation tool for your website. For starters, it is completely dynamic and comes with an extremely large number of libraries, which means more functionality and versatility. Additionally, thanks to its massive popularity, support for PHP development is almost never an issue. So, if you feel the need for a website to front your business in the Internet world, PHP is definitely the way to go.</p>\r\n              <p class=\"text-justified\">BigPerl Solutions Pvt Ltd. is a leading PHP development company offering comprehensive range of custom of PHP application development services that steer your business to the next level, streamline processes and enhance growth.</p>\r\n              <p class=\"text-justified\">We have successfully leveraged the power of the simplified code of this server side scripting language to develop highly dynamic websites, complex web applications and database-driven websites. Our PHP experts adhere to industry best practices to implement mature PHP solutions that align with our clients’ business strategies.</p>\r\n          </div>\r\n\r\n          <div class=\"col-md-5\">\r\n              <img src=\"assets/img/services/php_development_bigperl.png\" class=\"img-responsive \" alt=\"php_development_bigperl\">\r\n          </div>\r\n      </div>\r\n\r\n      <app-footer5></app-footer5>\r\n           \r\n      <app-footer2></app-footer2>"

/***/ }),

/***/ "./src/app/servicessection/php-development/php-development.component.ts":
/*!******************************************************************************!*\
  !*** ./src/app/servicessection/php-development/php-development.component.ts ***!
  \******************************************************************************/
/*! exports provided: PhpDevelopmentComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PhpDevelopmentComponent", function() { return PhpDevelopmentComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var PhpDevelopmentComponent = /** @class */ (function () {
    function PhpDevelopmentComponent() {
    }
    PhpDevelopmentComponent.prototype.ngOnInit = function () {
    };
    PhpDevelopmentComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-php-development',
            template: __webpack_require__(/*! ./php-development.component.html */ "./src/app/servicessection/php-development/php-development.component.html"),
            styles: [__webpack_require__(/*! ./php-development.component.css */ "./src/app/servicessection/php-development/php-development.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], PhpDevelopmentComponent);
    return PhpDevelopmentComponent;
}());



/***/ }),

/***/ "./src/app/servicessection/python-developer/python-developer.component.css":
/*!*********************************************************************************!*\
  !*** ./src/app/servicessection/python-developer/python-developer.component.css ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/servicessection/python-developer/python-developer.component.html":
/*!**********************************************************************************!*\
  !*** ./src/app/servicessection/python-developer/python-developer.component.html ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n        <!--page title start-->\r\n        <section class=\"page-title ptb-30\">\r\n          <div class=\"container padding-top-40\">\r\n              <div class=\"row\">\r\n                  <div class=\"col-md-12\">\r\n                      <h6 class=\"pagetitles\">Services</h6>\r\n                      <ol class=\"breadcrumb\">\r\n                          <li><a href=\"#\">Home</a></li>\r\n                          <li><a href=\"#\">Services</a></li>\r\n                          <li class=\"active\">python developer</li>\r\n                      </ol>\r\n                  </div>\r\n              </div>\r\n          </div>\r\n      </section>\r\n      <!--page title end-->\r\n\r\n      <img src=\"assets/img/banners/python_developer_bigperl_banner.png\" alt=\"python_developer_bigperl_banner\" class=\"fullwidth\">\r\n\r\n            <div class=\"container padding-top-20\">\r\n                <div class=\"col-md-7\">\r\n                    <h2 class=\"text-bold mb-10\">Python Developer</h2>\r\n                    <p class=\"text-justified\">We develop and manage applications with technologies such as MySQL, Django, frameworks PyGTK, PyQT for User Interface design, wxPython, PostgreSQL, MSSQL.</p>\r\n                    <p><b>Benefits with Python development:</b></p>\r\n                    <ol>\r\n                      <li>Easy to write, maintain and integrate.</li>\r\n                      <li>It’s free and open source with large standard libraries support.</li>\r\n                      <li>Fast and Reduced development cycle (C++: Python=10:1).</li>\r\n                      <li>Secure and Portable – works on Unix, Linux, Windows, Mac etc.</li>\r\n                    </ol>\r\n                    <p><b>Why BigPerl is the perfect choice for ADF development?</b></p>\r\n                    <ol>\r\n                      <li>We provide Quality customer service for a successful business.</li>\r\n                      <li>We understand the client needs and meet their expectations.</li>\r\n                      <li>Cost effective Python applications development solutions.</li>\r\n                      <li>Experience Hands-on on ROR Development.</li>\r\n                    </ol>\r\n                </div>\r\n\r\n                <div class=\"col-md-5\">\r\n                    <img src=\"assets/img/services/python_developer_bigperl.png\" class=\"img-responsive \" alt=\"python_developer_bigperl\">\r\n                </div>\r\n            </div>\r\n            <app-footer5></app-footer5>\r\n           \r\n            <app-footer2></app-footer2>"

/***/ }),

/***/ "./src/app/servicessection/python-developer/python-developer.component.ts":
/*!********************************************************************************!*\
  !*** ./src/app/servicessection/python-developer/python-developer.component.ts ***!
  \********************************************************************************/
/*! exports provided: PythonDeveloperComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PythonDeveloperComponent", function() { return PythonDeveloperComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var PythonDeveloperComponent = /** @class */ (function () {
    function PythonDeveloperComponent() {
    }
    PythonDeveloperComponent.prototype.ngOnInit = function () {
    };
    PythonDeveloperComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-python-developer',
            template: __webpack_require__(/*! ./python-developer.component.html */ "./src/app/servicessection/python-developer/python-developer.component.html"),
            styles: [__webpack_require__(/*! ./python-developer.component.css */ "./src/app/servicessection/python-developer/python-developer.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], PythonDeveloperComponent);
    return PythonDeveloperComponent;
}());



/***/ }),

/***/ "./src/app/servicessection/python-web-development/python-web-development.component.css":
/*!*********************************************************************************************!*\
  !*** ./src/app/servicessection/python-web-development/python-web-development.component.css ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/servicessection/python-web-development/python-web-development.component.html":
/*!**********************************************************************************************!*\
  !*** ./src/app/servicessection/python-web-development/python-web-development.component.html ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n        <!--page title start-->\r\n        <section class=\"page-title ptb-30\">\r\n          <div class=\"container padding-top-40\">\r\n              <div class=\"row\">\r\n                  <div class=\"col-md-12\">\r\n                      <h6 class=\"pagetitles\">Services</h6>\r\n                      <ol class=\"breadcrumb\">\r\n                          <li><a href=\"#\">Home</a></li>\r\n                          <li><a href=\"#\">Services</a></li>\r\n                          <li class=\"active\">python development</li>\r\n                      </ol>\r\n                  </div>\r\n              </div>\r\n          </div>\r\n      </section>\r\n      <!--page title end-->\r\n\r\n      <img src=\"assets/img/banners/python_development_bigperl_banner.png\" alt=\"python_development_bigperl_banner\" class=\"fullwidth\">\r\n\r\n            <div class=\"container padding-top-20\">\r\n                <div class=\"col-md-7\">\r\n                    <h2 class=\"text-bold mb-10\">Python Development</h2>\r\n                    <p class=\"text-justified\">Bigperl Solutions Pvt Ltd has a rich experience in developing dynamic websites, custom web applications and Desktop Applications in Python and Django. We have been in business of custom software development for 6 years and we are a specialist in Small-Team Software Development by following Agile Methods.</p>\r\n                    <p><b>Some of the services offered by Bigperl Solutions Pvt Ltd are :</b></p>\r\n                    <ul>\r\n                      <li>Python UI Design and Development using Frameworks PyGTK, PyQt, wxPython</li>\r\n                      <li>Python Web Crawler Development</li>\r\n                      <li>Python Desktop Application Development</li>\r\n                      <li>Turnkey Windows Services Development</li>\r\n                      <li>Python Custom Content Management System Development</li>\r\n                      <li>Python and Active Directory Integration Services</li>\r\n                      <li>Java and Python/Django Integration Services</li>\r\n                      <li>Python Web Application Development using Frameworks Django, Zope, CherryPy</li>\r\n                    </ul>\r\n                </div>\r\n\r\n                <div class=\"col-md-5\">\r\n                    <img src=\"assets/img/services/python_development_bigperl.png\" class=\"img-responsive \" alt=\"python_development_bigperl\">\r\n                </div>\r\n            </div>\r\n            \r\n            <app-footer5></app-footer5>\r\n           \r\n            <app-footer2></app-footer2>"

/***/ }),

/***/ "./src/app/servicessection/python-web-development/python-web-development.component.ts":
/*!********************************************************************************************!*\
  !*** ./src/app/servicessection/python-web-development/python-web-development.component.ts ***!
  \********************************************************************************************/
/*! exports provided: PythonWebDevelopmentComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PythonWebDevelopmentComponent", function() { return PythonWebDevelopmentComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var PythonWebDevelopmentComponent = /** @class */ (function () {
    function PythonWebDevelopmentComponent() {
    }
    PythonWebDevelopmentComponent.prototype.ngOnInit = function () {
    };
    PythonWebDevelopmentComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-python-web-development',
            template: __webpack_require__(/*! ./python-web-development.component.html */ "./src/app/servicessection/python-web-development/python-web-development.component.html"),
            styles: [__webpack_require__(/*! ./python-web-development.component.css */ "./src/app/servicessection/python-web-development/python-web-development.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], PythonWebDevelopmentComponent);
    return PythonWebDevelopmentComponent;
}());



/***/ }),

/***/ "./src/app/servicessection/rar-development/rar-development.component.css":
/*!*******************************************************************************!*\
  !*** ./src/app/servicessection/rar-development/rar-development.component.css ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/servicessection/rar-development/rar-development.component.html":
/*!********************************************************************************!*\
  !*** ./src/app/servicessection/rar-development/rar-development.component.html ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\r\n  rar-development works!\r\n</p>\r\n"

/***/ }),

/***/ "./src/app/servicessection/rar-development/rar-development.component.ts":
/*!******************************************************************************!*\
  !*** ./src/app/servicessection/rar-development/rar-development.component.ts ***!
  \******************************************************************************/
/*! exports provided: RarDevelopmentComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RarDevelopmentComponent", function() { return RarDevelopmentComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var RarDevelopmentComponent = /** @class */ (function () {
    function RarDevelopmentComponent() {
    }
    RarDevelopmentComponent.prototype.ngOnInit = function () {
    };
    RarDevelopmentComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-rar-development',
            template: __webpack_require__(/*! ./rar-development.component.html */ "./src/app/servicessection/rar-development/rar-development.component.html"),
            styles: [__webpack_require__(/*! ./rar-development.component.css */ "./src/app/servicessection/rar-development/rar-development.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], RarDevelopmentComponent);
    return RarDevelopmentComponent;
}());



/***/ }),

/***/ "./src/app/servicessection/responsive-web-development/responsive-web-development.component.css":
/*!*****************************************************************************************************!*\
  !*** ./src/app/servicessection/responsive-web-development/responsive-web-development.component.css ***!
  \*****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/servicessection/responsive-web-development/responsive-web-development.component.html":
/*!******************************************************************************************************!*\
  !*** ./src/app/servicessection/responsive-web-development/responsive-web-development.component.html ***!
  \******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = " <!--page title start-->\r\n <section class=\"page-title ptb-30\">\r\n  <div class=\"container padding-top-40\">\r\n      <div class=\"row\">\r\n          <div class=\"col-md-12\">\r\n              <h6 class=\"pagetitles\">Services</h6>\r\n              <ol class=\"breadcrumb\">\r\n                  <li><a href=\"#\">Home</a></li>\r\n                  <li><a href=\"#\">Services</a></li>\r\n                  <li class=\"active\">responsive web development</li>\r\n              </ol>\r\n          </div>\r\n      </div>\r\n  </div>\r\n</section>\r\n<!--page title end-->\r\n\r\n<img src=\"assets/img/banners/responsive_web_development_bigperl_banner.png\" alt=\"responsive_web_development_bigperl_banner\" class=\"fullwidth\">\r\n\r\n    <div class=\"container padding-top-20\">\r\n        <div class=\"col-md-7\">\r\n            <h2 class=\"text-bold mb-10\">Responsive Web Development</h2>\r\n            <p class=\"text-justified\">The users base for smartphones are increasing rapidly every now and then, almost every clients expects to have mobile version of their website. Within the web design and development platform, creating a website and making it compatible for website version of new devices is challenging task. Implementing responsive web design it new trend that makes the websites to be compatible and responsive hence making users to keep in a highly engaging environment. To provide responsive environment it is required to mix flexible blend of technology, to incorporate effective features. As the user switches to and fro from one device to other the design and development has to be made compatible to all devices. It is not necessary to create a compatible website individually for many devices, rather it is better to build one that can run well on all platforms and devices.</p>\r\n            <h3><b>The Core Concept of Responsive Web Design:</b></h3>\r\n            <p class=\"text-justified\">At present the responsive architecture has started to rule over the spaces that can respond to the presence of people passing through them. Users finds it very exciting to spend time with the responsive environment rather than being at an ideal still track. Responsive website development implies completely different designing version when compared to the traditional web design, the developers must know about the pros and cons of the responsive web designing. Using highly effective CSS transitions experts can make websites to be an interactive environment. One such fact are the images that serve at different resolutions, ranging from larger to smaller screens. The scaled images can be made to appear fluidly with help of updated developer tools and coding languages, making it to appear sharp on every context. The responsive web development is completely different from traditional designing in terms of creative and technical issues cautious utilization of this features can bring out wonders while designing.</p>\r\n            <h3><b>You can Expect us to deliver best services:</b></h3>\r\n            <p class=\"text-justified\">Get services from our highly committed responsive development company to experience the change. Feel free to disclose your ideas so that we can transform into a responsive design and users will anticipate for.</p>\r\n        </div>\r\n\r\n        <div class=\"col-md-5\">\r\n            <img src=\"assets/img/services/Responsive_web_development_bigperl.png\" class=\"img-responsive \" alt=\"Responsive_web_development_bigperl\">\r\n        </div>\r\n    </div>\r\n\r\n    <app-footer5></app-footer5>\r\n           \r\n    <app-footer2></app-footer2>"

/***/ }),

/***/ "./src/app/servicessection/responsive-web-development/responsive-web-development.component.ts":
/*!****************************************************************************************************!*\
  !*** ./src/app/servicessection/responsive-web-development/responsive-web-development.component.ts ***!
  \****************************************************************************************************/
/*! exports provided: ResponsiveWebDevelopmentComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ResponsiveWebDevelopmentComponent", function() { return ResponsiveWebDevelopmentComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ResponsiveWebDevelopmentComponent = /** @class */ (function () {
    function ResponsiveWebDevelopmentComponent() {
    }
    ResponsiveWebDevelopmentComponent.prototype.ngOnInit = function () {
    };
    ResponsiveWebDevelopmentComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-responsive-web-development',
            template: __webpack_require__(/*! ./responsive-web-development.component.html */ "./src/app/servicessection/responsive-web-development/responsive-web-development.component.html"),
            styles: [__webpack_require__(/*! ./responsive-web-development.component.css */ "./src/app/servicessection/responsive-web-development/responsive-web-development.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], ResponsiveWebDevelopmentComponent);
    return ResponsiveWebDevelopmentComponent;
}());



/***/ }),

/***/ "./src/app/servicessection/ruby-on-rails-developer/ruby-on-rails-developer.component.css":
/*!***********************************************************************************************!*\
  !*** ./src/app/servicessection/ruby-on-rails-developer/ruby-on-rails-developer.component.css ***!
  \***********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/servicessection/ruby-on-rails-developer/ruby-on-rails-developer.component.html":
/*!************************************************************************************************!*\
  !*** ./src/app/servicessection/ruby-on-rails-developer/ruby-on-rails-developer.component.html ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = " <!--page title start-->\r\n <section class=\"page-title ptb-30\">\r\n  <div class=\"container padding-top-40\">\r\n      <div class=\"row\">\r\n          <div class=\"col-md-12\">\r\n              <h6 class=\"pagetitles\">Services</h6>\r\n              <ol class=\"breadcrumb\">\r\n                  <li><a href=\"#\">Home</a></li>\r\n                  <li><a href=\"#\">Services</a></li>\r\n                  <li class=\"active\">ruby on rails development</li>\r\n              </ol>\r\n          </div>\r\n      </div>\r\n  </div>\r\n</section>\r\n<!--page title end-->\r\n\r\n<img src=\"assets/img/banners/ROR_developer_bigperl_banner.png\" alt=\"ROR_developer_bigperl_banner\" class=\"fullwidth\">\r\n\r\n    <div class=\"container padding-top-20\">\r\n        <div class=\"col-md-7\">\r\n            <h2 class=\"text-bold mb-10\">ROR Developer</h2>\r\n            <p class=\"text-justified\">Ruby on Rails, often referred as ROR, is a best known open-source framework that is extensively used for building database driven web apps. ROR is one of the best platforms to develop cloud-ready applications. It is time saving as it is quicker to build Application.</p>\r\n            <p class=\"text-justified\">Using ROR, we can build and launch a prototype application in an extremely short time. Compared to projects developed using ASP, JSP or PHP, we are able to attain a much higher level of productivity with Rails</p>\r\n            <p class=\"text-justified\">ROR development has three in-built mechanisms, which make it one of the fastest application development platforms, available today. They are known as convention over configuration (CoC), don’t repeat yourself (DRY), and active record pattern.</p>\r\n            <p><b>Benefits with ROR development services:</b></p>\r\n            <ol>\r\n              <li>Easy and Faster Development.</li>\r\n              <li>Errors are quickly reported and fixed during programming. This way high code quality is ensured.</li>\r\n              <li>Integrated javascript framework – Create interactive interfaces.</li>\r\n            </ol>\r\n            <p><b>Why BigPerl is the perfect choice for ROR development?</b></p>\r\n            <ol>\r\n              <li>Flexible in Client’s Requirements.</li>\r\n              <li>Experience Hands-on on ROR Development.</li>\r\n              <li>Fast, Cost-effective and On-Time Project Delivery.</li>\r\n            </ol>\r\n        </div>\r\n\r\n        <div class=\"col-md-5\">\r\n            <img src=\"assets/img/services/ROR_developer_bigperl.png\" class=\"img-responsive \" alt=\"ROR_developer_bigperl\">\r\n        </div>\r\n    </div>\r\n\r\n    <app-footer5></app-footer5>\r\n           \r\n    <app-footer2></app-footer2>\r\n"

/***/ }),

/***/ "./src/app/servicessection/ruby-on-rails-developer/ruby-on-rails-developer.component.ts":
/*!**********************************************************************************************!*\
  !*** ./src/app/servicessection/ruby-on-rails-developer/ruby-on-rails-developer.component.ts ***!
  \**********************************************************************************************/
/*! exports provided: RubyOnRailsDeveloperComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RubyOnRailsDeveloperComponent", function() { return RubyOnRailsDeveloperComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var RubyOnRailsDeveloperComponent = /** @class */ (function () {
    function RubyOnRailsDeveloperComponent() {
    }
    RubyOnRailsDeveloperComponent.prototype.ngOnInit = function () {
    };
    RubyOnRailsDeveloperComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-ruby-on-rails-developer',
            template: __webpack_require__(/*! ./ruby-on-rails-developer.component.html */ "./src/app/servicessection/ruby-on-rails-developer/ruby-on-rails-developer.component.html"),
            styles: [__webpack_require__(/*! ./ruby-on-rails-developer.component.css */ "./src/app/servicessection/ruby-on-rails-developer/ruby-on-rails-developer.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], RubyOnRailsDeveloperComponent);
    return RubyOnRailsDeveloperComponent;
}());



/***/ }),

/***/ "./src/app/servicessection/ruby-on-rails-development/ruby-on-rails-development.component.css":
/*!***************************************************************************************************!*\
  !*** ./src/app/servicessection/ruby-on-rails-development/ruby-on-rails-development.component.css ***!
  \***************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/servicessection/ruby-on-rails-development/ruby-on-rails-development.component.html":
/*!****************************************************************************************************!*\
  !*** ./src/app/servicessection/ruby-on-rails-development/ruby-on-rails-development.component.html ***!
  \****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n        <!--page title start-->\r\n        <section class=\"page-title ptb-30\">\r\n          <div class=\"container padding-top-40\">\r\n              <div class=\"row\">\r\n                  <div class=\"col-md-12\">\r\n                      <h6 class=\"pagetitles\">Services</h6>\r\n                      <ol class=\"breadcrumb\">\r\n                          <li><a href=\"#\">Home</a></li>\r\n                          <li><a href=\"#\">Services</a></li>\r\n                          <li class=\"active\">ruby on rails development</li>\r\n                      </ol>\r\n                  </div>\r\n              </div>\r\n          </div>\r\n      </section>\r\n      <!--page title end-->\r\n\r\n      <img src=\"assets/img/banners/ROR_development_bigperl_banner.png\" alt=\"ROR_development_bigperl_banner\" class=\"fullwidth\">\r\n\r\n            <div class=\"container padding-top-20\">\r\n                <div class=\"col-md-7\">\r\n                    <h2 class=\"text-bold mb-10\">Ruby on Rails Development</h2>\r\n                    <h5><b>Your goals are ours</b></h5>\r\n                    <p class=\"text-justified\">We relate with your vision and understand the problem you are solving. We take up work we believe in. Your goals becomes ours</p>\r\n                    <h5><b>We understand product development cycle</b></h5>\r\n                    <p class=\"text-justified\">We have worked with founders right from ideation to development to support and seen them raise various rounds of funding and exit as well. So we understand what it takes to build a successful product</p>\r\n                    <h5><b>Solid Tech</b></h5>\r\n                    <p class=\"text-justified\">You will be working with people who are hackers at heart. Once we understand the problem we make sure we architect a solution which encompasses the latest tech. We are known as the best Ruby on Rails Developers in Bangalore.</p>\r\n                    <h5><b>Clients all around the world</b></h5>\r\n                    <p class=\"text-justified\">We've worked with startups, recently funded and enterprise customers in various industries. We understand what you are trying to solve and work with a product mindset.</p>\r\n                </div>\r\n\r\n                <div class=\"col-md-5\">\r\n                    <img src=\"assets/img/services/ROR_development_bigperl.png\" class=\"img-responsive \" alt=\"ROR_development_bigperl\">\r\n                </div>\r\n            </div>\r\n\r\n\r\n            <app-footer5></app-footer5>\r\n           \r\n<app-footer2></app-footer2>"

/***/ }),

/***/ "./src/app/servicessection/ruby-on-rails-development/ruby-on-rails-development.component.ts":
/*!**************************************************************************************************!*\
  !*** ./src/app/servicessection/ruby-on-rails-development/ruby-on-rails-development.component.ts ***!
  \**************************************************************************************************/
/*! exports provided: RubyOnRailsDevelopmentComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RubyOnRailsDevelopmentComponent", function() { return RubyOnRailsDevelopmentComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var RubyOnRailsDevelopmentComponent = /** @class */ (function () {
    function RubyOnRailsDevelopmentComponent() {
    }
    RubyOnRailsDevelopmentComponent.prototype.ngOnInit = function () {
    };
    RubyOnRailsDevelopmentComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-ruby-on-rails-development',
            template: __webpack_require__(/*! ./ruby-on-rails-development.component.html */ "./src/app/servicessection/ruby-on-rails-development/ruby-on-rails-development.component.html"),
            styles: [__webpack_require__(/*! ./ruby-on-rails-development.component.css */ "./src/app/servicessection/ruby-on-rails-development/ruby-on-rails-development.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], RubyOnRailsDevelopmentComponent);
    return RubyOnRailsDevelopmentComponent;
}());



/***/ }),

/***/ "./src/app/servicessection/servicessection.component.css":
/*!***************************************************************!*\
  !*** ./src/app/servicessection/servicessection.component.css ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/servicessection/servicessection.component.html":
/*!****************************************************************!*\
  !*** ./src/app/servicessection/servicessection.component.html ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\r\n  servicessection works!\r\n</p>\r\n"

/***/ }),

/***/ "./src/app/servicessection/servicessection.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/servicessection/servicessection.component.ts ***!
  \**************************************************************/
/*! exports provided: ServicessectionComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ServicessectionComponent", function() { return ServicessectionComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ServicessectionComponent = /** @class */ (function () {
    function ServicessectionComponent() {
    }
    ServicessectionComponent.prototype.ngOnInit = function () {
    };
    ServicessectionComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-servicessection',
            template: __webpack_require__(/*! ./servicessection.component.html */ "./src/app/servicessection/servicessection.component.html"),
            styles: [__webpack_require__(/*! ./servicessection.component.css */ "./src/app/servicessection/servicessection.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], ServicessectionComponent);
    return ServicessectionComponent;
}());



/***/ }),

/***/ "./src/app/servicessection/wearable-devices/wearable-devices.component.css":
/*!*********************************************************************************!*\
  !*** ./src/app/servicessection/wearable-devices/wearable-devices.component.css ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/servicessection/wearable-devices/wearable-devices.component.html":
/*!**********************************************************************************!*\
  !*** ./src/app/servicessection/wearable-devices/wearable-devices.component.html ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n        <!--page title start-->\r\n        <section class=\"page-title ptb-30\">\r\n          <div class=\"container padding-top-40\">\r\n              <div class=\"row\">\r\n                  <div class=\"col-md-12\">\r\n                      <h6 class=\"pagetitles\">Services</h6>\r\n                      <ol class=\"breadcrumb\">\r\n                          <li><a href=\"#\">Home</a></li>\r\n                          <li><a href=\"#\">Services</a></li>\r\n                          <li class=\"active\">wearable devices</li>\r\n                      </ol>\r\n                  </div>\r\n              </div>\r\n          </div>\r\n      </section>\r\n      <!--page title end-->\r\n\r\n      <img src=\"assets/img/banners/wearable_devices_bigperl_banner.png\" alt=\"wearable_devices_bigperl_banner\" class=\"fullwidth\">\r\n\r\n            <div class=\"container padding-top-20\">\r\n                <div class=\"col-md-7\">\r\n                    <h2 class=\"text-bold mb-10\"><b>Wearable Solutions</b></h2>\r\n                    <p class=\"text-justified\">Today, the whole world is excited hoping the remarkable changes that wearable technology could bring. The giant technology companies, Google, Apple and Samsung, have continued putting a considerable effort to harness the power wearables solutions</p>\r\n                    <h3><b>What is Wearables?</b></h3>\r\n                    <p class=\"text-justified\">Wearables or Wearable Gadgets are technology devices used in the form of smartwatch, eyeglass, bracelet and fitness band. Wearable device is worn by a consumer to measure his health and fitness, to take photos by synchronizing it with smartphones and to make business tasks easier.</p>\r\n                    <h3><b>Which are On Demand Wearable Devices?</b></h3>\r\n                    <p class=\"text-justified\">Smartwatch is one of the most popular Wearables. When comes to example, Samsung Galaxy Gear and Apple Watch are the best examples that you can understand what is wearable device. Google Glass and Sony SmartEyeglass are the top sold smart glass category Wearables. Wearable Puma Hooded Sweatshirts and Puma Miscellaneous Wrist Band are the popular wearable clothes that you can use to measure the number of steps you’ve walked, to measure heart rate and to get known how many calories you’ve burned.</p>\r\n                    <h3><b>How Wearables are Helpful for Businesses?</b></h3>\r\n                    <p class=\"text-justified\">When comes to the usage of Wearables in the business sector, opportunities are abundant. Wearables can be used to effectively enhance work productivity and the overall efficiency of any sort and any size business, though healthcare industry is expected to be the highest utilizer of wearable technology. More specifically, wearable devices can be used to look inside patients’ veins in the hospitals, likewise, to easily see inside piping and walls in the construction works.</p>\r\n                    <p class=\"text-justified\">But, the proper implementation of wearable technology decides the success of your wearable effort. Most importantly, you need a skilled wearable solution provider who can understand your business better and help you to implement your wearable ideas. Since wearable technology is in budding stage, you could be a trend setter.</p>\r\n                    <p class=\"text-justified\"><b>Simple example for the power of wearable solutions:</b> If a traffic police wears wearable smart glass, he no need to hold the camera and set it to capture the photos of people who violate traffic rules. He can capture photos of violator as he sees violator. Likewise, you can use our customizable wearable solution to solve your business issues faster, which can save your millions.</p>\r\n                    <h3><b>Bigperl solution Wearable Solutions</b></h3>\r\n                    <h4><b>How Bigperl solution helps to enhance your business performance with wearable solution?</b></h4>\r\n                    <p class=\"text-justified\">Bigperl Solution is the world’s fastest growing mobility complny , developed more than 1000 result-oriented mobile apps and games for businesses of all sorts and all scales across the world. Following successfully delivering cloud computing solutions, Bigperl solutions has stepped forward to create innovative wearable solutions to enhance your business performance.</p>\r\n                    <p class=\"text-justified\">We build robust, customizable wearable platform that can be used for developing business apps for Android, Apple, Sony, Google, Pebble, Puma, ARM, Fitbit, Philips, Samsung and other wearable devices. As an emerging wearable solution provider in India and the world, Bigperl Solutions delivers technology-driven wearable solutions to bring your business to the next level.</p>\r\n                </div>\r\n\r\n                <div class=\"col-md-5\">\r\n                    <img src=\"assets/img/services/wearable_solutions_bigperl.png\" class=\"img-responsive \" alt=\"wearable_solutions_bigperl\">\r\n                </div>\r\n            </div>\r\n\r\n\r\n            <app-footer5></app-footer5>\r\n           \r\n            <app-footer2></app-footer2>"

/***/ }),

/***/ "./src/app/servicessection/wearable-devices/wearable-devices.component.ts":
/*!********************************************************************************!*\
  !*** ./src/app/servicessection/wearable-devices/wearable-devices.component.ts ***!
  \********************************************************************************/
/*! exports provided: WearableDevicesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WearableDevicesComponent", function() { return WearableDevicesComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var WearableDevicesComponent = /** @class */ (function () {
    function WearableDevicesComponent() {
    }
    WearableDevicesComponent.prototype.ngOnInit = function () {
    };
    WearableDevicesComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-wearable-devices',
            template: __webpack_require__(/*! ./wearable-devices.component.html */ "./src/app/servicessection/wearable-devices/wearable-devices.component.html"),
            styles: [__webpack_require__(/*! ./wearable-devices.component.css */ "./src/app/servicessection/wearable-devices/wearable-devices.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], WearableDevicesComponent);
    return WearableDevicesComponent;
}());



/***/ }),

/***/ "./src/app/servicessection/web-application-development/web-application-development.component.css":
/*!*******************************************************************************************************!*\
  !*** ./src/app/servicessection/web-application-development/web-application-development.component.css ***!
  \*******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/servicessection/web-application-development/web-application-development.component.html":
/*!********************************************************************************************************!*\
  !*** ./src/app/servicessection/web-application-development/web-application-development.component.html ***!
  \********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "  <!--page title start-->\r\n  <section class=\"page-title ptb-30\">\r\n    <div class=\"container padding-top-40\">\r\n        <div class=\"row\">\r\n            <div class=\"col-md-12\">\r\n                <h6 class=\"pagetitles\">Services</h6>\r\n                <ol class=\"breadcrumb\">\r\n                    <li><a href=\"#\">Home</a></li>\r\n                    <li><a href=\"#\">Services</a></li>\r\n                    <li class=\"active\">web app developer</li>\r\n                </ol>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</section>\r\n<!--page title end-->\r\n\r\n<img src=\"assets/img/banners/web_application_developer_bigperl_banner.png\" alt=\"web_application_developer_bigperl_banner\" class=\"fullwidth\">\r\n\r\n      <div class=\"container padding-top-20\">\r\n          <div class=\"col-md-7\">\r\n              <h2 class=\"text-bold mb-10\">Web Application Developer</h2>\r\n              <p class=\"text-justified\">Web Application Development is the creation and maintenance of application software that resides on servers and is delivered to the end user device over the Internet used on the World Wide Web.</p>\r\n              <p class=\"text-justified\">The Web Application development process includes Web design, Web content development, client-side/server-side scripting and network security configuration, among other tasks. BigPerl team of Web Application Development developers have good experience and knowledge to understand the importance of unified marketing strategies and designing effective web application that live on remote servers and are delivered to the user’s device over the Internet.</p>\r\n              <p class=\"text-justified\">We develop websites for hosting via internet includes web design, client- server side scripting and network configuration security. Our programmers can design optimized, customized and user friendly applications.</p>\r\n              <ul>\r\n                <li><b>Web Application Technology:</b></li>\r\n                <li>js</li>\r\n                <li>Javascript</li>\r\n                <li>JQuery</li>\r\n                <li>HTML</li>\r\n                <li>PHP</li>\r\n              </ul>\r\n              <ul>\r\n                <li><b>Why BigPerl is the perfect choice for Web Development?</b></li>\r\n                <li>Strong, talented and experienced in-house team.</li>\r\n                <li>Fast, cost-effective and on-time project delivery.</li>\r\n                <li>We provide complete customization and User-Friendly browsing and designs.</li>\r\n              </ul>\r\n          </div>\r\n\r\n          <div class=\"col-md-5\">\r\n              <img src=\"assets/img/services/web_application_developer_bigperl.png\" class=\"img-responsive \" alt=\"web_application_developer_bigperl\">\r\n          </div>\r\n      </div>\r\n\r\n\r\n    <app-footer5></app-footer5>\r\n           \r\n    <app-footer2></app-footer2>\r\n\r\n"

/***/ }),

/***/ "./src/app/servicessection/web-application-development/web-application-development.component.ts":
/*!******************************************************************************************************!*\
  !*** ./src/app/servicessection/web-application-development/web-application-development.component.ts ***!
  \******************************************************************************************************/
/*! exports provided: WebApplicationDevelopmentComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WebApplicationDevelopmentComponent", function() { return WebApplicationDevelopmentComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var WebApplicationDevelopmentComponent = /** @class */ (function () {
    function WebApplicationDevelopmentComponent() {
    }
    WebApplicationDevelopmentComponent.prototype.ngOnInit = function () {
    };
    WebApplicationDevelopmentComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-web-application-development',
            template: __webpack_require__(/*! ./web-application-development.component.html */ "./src/app/servicessection/web-application-development/web-application-development.component.html"),
            styles: [__webpack_require__(/*! ./web-application-development.component.css */ "./src/app/servicessection/web-application-development/web-application-development.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], WebApplicationDevelopmentComponent);
    return WebApplicationDevelopmentComponent;
}());



/***/ }),

/***/ "./src/app/servicessection/windows-app-development/windows-app-development.component.css":
/*!***********************************************************************************************!*\
  !*** ./src/app/servicessection/windows-app-development/windows-app-development.component.css ***!
  \***********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/servicessection/windows-app-development/windows-app-development.component.html":
/*!************************************************************************************************!*\
  !*** ./src/app/servicessection/windows-app-development/windows-app-development.component.html ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n        <!--page title start-->\r\n        <section class=\"page-title ptb-30\">\r\n          <div class=\"container padding-top-40\">\r\n              <div class=\"row\">\r\n                  <div class=\"col-md-12\">\r\n                      <h6 class=\"pagetitles\">Services</h6>\r\n                      <ol class=\"breadcrumb\">\r\n                          <li><a href=\"#\">Home</a></li>\r\n                          <li><a href=\"#\">Services</a></li>\r\n                          <li class=\"active\">windows app development</li>\r\n                      </ol>\r\n                  </div>\r\n              </div>\r\n          </div>\r\n      </section>\r\n      <!--page title end-->\r\n\r\n      <img src=\"assets/img/banners/windows_app_development_bigperl_banner.png\" alt=\"windows_app_development_bigperl_banner\" class=\"fullwidth\">\r\n\r\n            <div class=\"container padding-top-20\">\r\n                <div class=\"col-md-7\">\r\n                    <h2 class=\"text-bold mb-10\">Windows Application Development</h2>\r\n                    <p class=\"text-justified\"> Microsoft Windows one of the top desktop operating system used by 95% of global users.With proven it dominance and already used by billion and a half people having its large-scale market place thought the world, now MicroSoft is started expending this operating system to mobile platform as well. With huge innovations coming in from microsoft towards its mobile platform as well as big aquasations like Nokia, market share of windows based mobile is growing exponentially   Because of its flexibility and versatility Windows apps has increasing its user base. Keeping this high increasing user base, it is not viable to ignore the platform if you are aiming at maximizing your business reach.</p>\r\n                    <p class=\"text-justified\"> Windows platform is the compact version of the powerful desktop Windows operating system, it remains the most preferred platform for both technical and non-technical users.</p>\r\n                    <p class=\"text-justified\"> As a Windows app development company, Bigperl solution Pvt Ltd Windows developers team is highly proficient in developing apps that need syncing applications that runs in Windows platform. If you are at making the choice for developing apps best option is Windows App development. We have deep experience on building mobile application over all major available mobile operating systems today. Our engineers has knowledge to develop amazing Windows applications, since we are having strong experienced team in windows app development, but the best part is that our windows app developer work towards your vision of the mobile app by constantly interacting with you, as of we have dedicated 24 X 7 customer support for specially designed for entrepreneurs and enterprise.</p>\r\n                    <p class=\"text-justified\"> Windows App Development can open up your business to a new audience, since it has versatile and user friendly platform. Since new and better versions of the operating systems are reaching customers in quick time, it is better to be go with windows mobile app development. We are having windows app development companies in india, USA, UK.</p>\r\n                </div>\r\n\r\n                <div class=\"col-md-5\">\r\n                    <img src=\"assets/img/services/windows_app_development_bigperl.png\" class=\"img-responsive \" alt=\"windows_app_development_bigperl\">\r\n                </div>\r\n            </div>\r\n\r\n            <app-footer5></app-footer5>\r\n           \r\n            <app-footer2></app-footer2>\r\n"

/***/ }),

/***/ "./src/app/servicessection/windows-app-development/windows-app-development.component.ts":
/*!**********************************************************************************************!*\
  !*** ./src/app/servicessection/windows-app-development/windows-app-development.component.ts ***!
  \**********************************************************************************************/
/*! exports provided: WindowsAppDevelopmentComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WindowsAppDevelopmentComponent", function() { return WindowsAppDevelopmentComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var WindowsAppDevelopmentComponent = /** @class */ (function () {
    function WindowsAppDevelopmentComponent() {
    }
    WindowsAppDevelopmentComponent.prototype.ngOnInit = function () {
    };
    WindowsAppDevelopmentComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-windows-app-development',
            template: __webpack_require__(/*! ./windows-app-development.component.html */ "./src/app/servicessection/windows-app-development/windows-app-development.component.html"),
            styles: [__webpack_require__(/*! ./windows-app-development.component.css */ "./src/app/servicessection/windows-app-development/windows-app-development.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], WindowsAppDevelopmentComponent);
    return WindowsAppDevelopmentComponent;
}());



/***/ }),

/***/ "./src/app/servicessection/word-press-development/word-press-development.component.css":
/*!*********************************************************************************************!*\
  !*** ./src/app/servicessection/word-press-development/word-press-development.component.css ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/servicessection/word-press-development/word-press-development.component.html":
/*!**********************************************************************************************!*\
  !*** ./src/app/servicessection/word-press-development/word-press-development.component.html ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n\r\n\r\n        <!--page title start-->\r\n        <section class=\"page-title ptb-30\">\r\n          <div class=\"container padding-top-40\">\r\n              <div class=\"row\">\r\n                  <div class=\"col-md-12\">\r\n                      <h6 class=\"pagetitles\">Services</h6>\r\n                      <ol class=\"breadcrumb\">\r\n                          <li><a href=\"#\">Home</a></li>\r\n                          <li><a href=\"#\">Services</a></li>\r\n                          <li class=\"active\">wordpress development</li>\r\n                      </ol>\r\n                  </div>\r\n              </div>\r\n          </div>\r\n      </section>\r\n      <!--page title end-->\r\n\r\n      <img src=\"assets/img/banners/wordpress_development_bigperl_banner.png\" alt=\"wordpress_development_bigperl_banner\" class=\"fullwidth\">\r\n\r\n            <div class=\"container padding-top-20\">\r\n                <div class=\"col-md-7\">\r\n                    <h2 class=\"text-bold mb-10\">Wordpress Development</h2>\r\n                    <p class=\"text-justified\">WordPress is thought to be the most capable Tool for improvement of sites and online journals, which has begun its prevalence since 2003. What’s more, it has become the biggest self-facilitated blogging instrument on the planet, utilized on a large number of locales and saw by a huge number of individuals consistently. WordPress has been utilized limitlessly as a substance administration framework.</p>\r\n                    <p class=\"text-justified\">Bigperl is one of the innovative WordPress site outline organization in Bangalore by offering mind blowing WordPress web plans and topics by encasing all the most recent web gages and movements in our web structure work that ascents up out of the social event.</p>\r\n                    <p class=\"text-justified\">We give complete site or topic improvement in WordPress. We guarantee all the WordPress topics are versatile perfect and responsive. Our WordPress Web outlining organization Bangalore conveys quality, better than average and utilitarian sites, as well as backings in the establishment and setup, backing and support, custom module improvement, blog advancement and a great deal more. Our get-together of magnificent wordpress site designers in Bangalore first studies your business and after that proposes a few styles to look over their idea presentation report. We take after the best strategy in WordPress coding, and never hack the center records as some different organizations do. This guarantees you can undoubtedly redesign WordPress and strengthen security of your website.</p>\r\n                    <ul>\r\n                      <h3><b>Services Offered:</b></h3>\r\n                      <li>WordPress Blog Development</li>\r\n                      <li>Theme Installation of Customer choice</li>\r\n                      <li>PSD to WordPress conversion</li>\r\n                      <li>Redesigning your existing WordPress website</li>\r\n                      <li>WordPress website maintenance and Services</li>\r\n                      <li>WordPress Integration with Existing Website into New</li>\r\n                      <li>WordPress Website customization</li>\r\n                      <li>Customization WordPress Theme Design</li>\r\n                      <li>Customization Templates design and development</li>\r\n                    </ul>\r\n                </div>\r\n\r\n                <div class=\"col-md-5\">\r\n                    <img src=\"assets/img/services/wordpress_development_bigperl.png\" class=\"img-responsive \" alt=\"Image\">\r\n                </div>\r\n            </div>\r\n\r\n      \r\n\r\n\r\n<app-footer5></app-footer5>\r\n           \r\n<app-footer2></app-footer2>"

/***/ }),

/***/ "./src/app/servicessection/word-press-development/word-press-development.component.ts":
/*!********************************************************************************************!*\
  !*** ./src/app/servicessection/word-press-development/word-press-development.component.ts ***!
  \********************************************************************************************/
/*! exports provided: WordPressDevelopmentComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WordPressDevelopmentComponent", function() { return WordPressDevelopmentComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var WordPressDevelopmentComponent = /** @class */ (function () {
    function WordPressDevelopmentComponent() {
    }
    WordPressDevelopmentComponent.prototype.ngOnInit = function () {
    };
    WordPressDevelopmentComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-word-press-development',
            template: __webpack_require__(/*! ./word-press-development.component.html */ "./src/app/servicessection/word-press-development/word-press-development.component.html"),
            styles: [__webpack_require__(/*! ./word-press-development.component.css */ "./src/app/servicessection/word-press-development/word-press-development.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], WordPressDevelopmentComponent);
    return WordPressDevelopmentComponent;
}());



/***/ }),

/***/ "./src/app/servicessection/xcart-development/xcart-development.component.css":
/*!***********************************************************************************!*\
  !*** ./src/app/servicessection/xcart-development/xcart-development.component.css ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/servicessection/xcart-development/xcart-development.component.html":
/*!************************************************************************************!*\
  !*** ./src/app/servicessection/xcart-development/xcart-development.component.html ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n        <!--page title start-->\r\n        <section class=\"page-title ptb-30\">\r\n          <div class=\"container padding-top-40\">\r\n              <div class=\"row\">\r\n                  <div class=\"col-md-12\">\r\n                      <h6 class=\"pagetitles\">Services</h6>\r\n                      <ol class=\"breadcrumb\">\r\n                          <li><a href=\"#\">Home</a></li>\r\n                          <li><a href=\"#\">Services</a></li>\r\n                          <li class=\"active\">x-cart development</li>\r\n                      </ol>\r\n                  </div>\r\n              </div>\r\n          </div>\r\n      </section>\r\n      <!--page title end-->\r\n\r\n      <img src=\"assets/img/banners/xcart_development_bigperl_banner.png\" alt=\"xcart_development_bigperl_banner\" class=\"fullwidth\">\r\n\r\n            <div class=\"container padding-top-20\">\r\n                <div class=\"col-md-7\">\r\n                    <h2 class=\"text-bold mb-10\">X-Cart Development</h2>\r\n                    <p class=\"text-justified\">As professional web designers and developers we keep researching a lot on latest technologies and platforms available and Magento is also a result of this research. We at Bigperl found Magento Ecommerce development to be successful and powerful platform because of the following reasons:</p>\r\n                    <ul>\r\n                      <li>Effortlessly customizable to meet the requirements.</li>\r\n                      <li>It supports number of gateways for payment.</li>\r\n                      <li>There is an array of design and themes to choose from.</li>\r\n                      <li>One of the few open source platforms that offer compatibility with mobile phones.</li>\r\n                      <li>Also it is search engine friendly as well.</li>\r\n                      <li>Changes to previous design and addition of new features are very easy with the help of the modules that can be added or removed.</li>\r\n                    </ul>\r\n                    <p>Some reasons why you should power your e-commerce with X-Cart:</p>\r\n                    <p class=\"text-justified\">It is highly user friendly and flexible Well organized orders, customers, products management Enables easy inventory management Provides multi-language support Exhaustive report management system What services we offer under the X-cart umbrella?</p>\r\n                    <ul>\r\n                      <li>X-Cart website development</li>\r\n                      <li>X-Cart template installation</li>\r\n                      <li>X-Cart customization</li>\r\n                      <li>X-Cart module integration</li>\r\n                      <li>X-Cart Payment Gateway Setup</li>\r\n                      <li>X-cart store maintenance</li>\r\n                      <li>X-Cart store migration from one server to another</li>\r\n                    </ul>\r\n                </div>\r\n\r\n                <div class=\"col-md-5\">\r\n                    <img src=\"assets/img/services/xcart_development_bigperl.png\" class=\"img-responsive \" alt=\"xcart_development_bigperl\">\r\n                </div>\r\n            </div>\r\n\r\n\r\n\r\n            <app-footer5></app-footer5>\r\n           \r\n            <app-footer2></app-footer2>\r\n"

/***/ }),

/***/ "./src/app/servicessection/xcart-development/xcart-development.component.ts":
/*!**********************************************************************************!*\
  !*** ./src/app/servicessection/xcart-development/xcart-development.component.ts ***!
  \**********************************************************************************/
/*! exports provided: XCartDevelopmentComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "XCartDevelopmentComponent", function() { return XCartDevelopmentComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var XCartDevelopmentComponent = /** @class */ (function () {
    function XCartDevelopmentComponent() {
    }
    XCartDevelopmentComponent.prototype.ngOnInit = function () {
    };
    XCartDevelopmentComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-xcart-development',
            template: __webpack_require__(/*! ./xcart-development.component.html */ "./src/app/servicessection/xcart-development/xcart-development.component.html"),
            styles: [__webpack_require__(/*! ./xcart-development.component.css */ "./src/app/servicessection/xcart-development/xcart-development.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], XCartDevelopmentComponent);
    return XCartDevelopmentComponent;
}());



/***/ }),

/***/ "./src/app/subscribe.model.ts":
/*!************************************!*\
  !*** ./src/app/subscribe.model.ts ***!
  \************************************/
/*! exports provided: Subscribe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Subscribe", function() { return Subscribe; });
var Subscribe = /** @class */ (function () {
    function Subscribe(email) {
        this.email = email;
    }
    return Subscribe;
}());



/***/ }),

/***/ "./src/app/subscribe.service.ts":
/*!**************************************!*\
  !*** ./src/app/subscribe.service.ts ***!
  \**************************************/
/*! exports provided: SubscribeService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SubscribeService", function() { return SubscribeService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var rxjs_add_operator_catch__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/add/operator/catch */ "./node_modules/rxjs-compat/_esm5/add/operator/catch.js");
/* harmony import */ var rxjs_add_operator_map__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/add/operator/map */ "./node_modules/rxjs-compat/_esm5/add/operator/map.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var SubscribeService = /** @class */ (function () {
    function SubscribeService(_http) {
        this._http = _http;
        this._contactUrl1 = 'https://localhost/webservices/index.php/api/subscribe';
    }
    SubscribeService.prototype.sendSubscribe = function (value) {
        var body = new _angular_http__WEBPACK_IMPORTED_MODULE_1__["URLSearchParams"](value);
        body.set('email', value.data[0].email);
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_1__["Headers"]();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        return this._http.post(this._contactUrl1, body, {
            headers: headers
        }).map(function (res) { return res.json(); });
    };
    SubscribeService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_1__["Http"]])
    ], SubscribeService);
    return SubscribeService;
}());



/***/ }),

/***/ "./src/app/toaster-service.service.ts":
/*!********************************************!*\
  !*** ./src/app/toaster-service.service.ts ***!
  \********************************************/
/*! exports provided: ToasterService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ToasterService", function() { return ToasterService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ToasterService = /** @class */ (function () {
    function ToasterService() {
        this.setting();
    }
    ToasterService.prototype.Success = function (title, meassage) {
        toastr.success(title, meassage);
    };
    ToasterService.prototype.Info = function (title, message) {
        toastr.info(title, message);
    };
    ToasterService.prototype.setting = function () {
        toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-center",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "2000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };
    };
    ToasterService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [])
    ], ToasterService);
    return ToasterService;
}());



/***/ }),

/***/ "./src/app/user.model.ts":
/*!*******************************!*\
  !*** ./src/app/user.model.ts ***!
  \*******************************/
/*! exports provided: User */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "User", function() { return User; });
var User = /** @class */ (function () {
    function User(name, email, subject, message) {
        this.name = name;
        this.email = email;
        this.subject = subject;
        this.message = message;
    }
    return User;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\Sunil Yadav\Desktop\Desk\aba\bigperlwebsite_angular\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map