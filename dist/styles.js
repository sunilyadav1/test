(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["styles"],{

/***/ "./node_modules/raw-loader/index.js!./node_modules/postcss-loader/lib/index.js??embedded!./src/styles.css":
/*!**********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./node_modules/postcss-loader/lib??embedded!./src/styles.css ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n@import url(https://fonts.googleapis.com/css?family=Roboto:400,300,500,700,900);\r\n@import url(https://fonts.googleapis.com/css?family=Great+Vibes);\r\n/*\r\n====================================\r\n[ CSS TABLE CONTENT ]\r\n-----------------------------------\r\n    1.0 - General\r\n    2.0 - Typography\r\n    3.0 - Global Style\r\n    4.0 - Color Presets\r\n    5.0 - Overlay css\r\n    6.0 - Hero css\r\n    7.0 - Background Banner\r\n    8.0 - 404 page\r\n    9.0 - Author Wrapper\r\n    10.0 - Progress bar\r\n    11.0 - Clients\r\n    12.0 - Gallery Thumb\r\n    13.0 - Material CSS Customization\r\n    14.0 - Social Icon Animation\r\n    15.0 - device mockup carousel\r\n    16.0 - Search Style\r\n    17.0 - Preloader\r\n\r\n-------------------------------------\r\n[ END CSS TABLE CONTENT ]\r\n=====================================\r\n*/\r\n/* Import css files */\r\n@font-face {\r\n\tfont-family: \"Flaticon\";\r\n\tsrc: url('flaticon.eot');\r\n\tsrc: url('flaticon.eot#iefix') format(\"embedded-opentype\"),\r\n\turl('flaticon.woff') format(\"woff\"),\r\n\turl('flaticon.ttf') format(\"truetype\"),\r\n\turl('flaticon.svg') format(\"svg\");\r\n\tfont-weight: normal;\r\n\tfont-style: normal;\r\n}\r\n[class^=\"flaticon-\"]:before, [class*=\" flaticon-\"]:before,\r\n[class^=\"flaticon-\"]:after, [class*=\" flaticon-\"]:after {   \r\n\tfont-family: Flaticon;\r\n        font-size: 20px;\r\nfont-style: normal;\r\nmargin-left: 20px;\r\n}\r\n.flaticon-bag of flour:before {\r\n\tcontent: \"\\e000\";\r\n}\r\n.flaticon-baked1:before {\r\n\tcontent: \"\\e001\";\r\n}\r\n.flaticon-bakery11:before {\r\n\tcontent: \"\\e002\";\r\n}\r\n.flaticon-bakery8:before {\r\n\tcontent: \"\\e003\";\r\n}\r\n.flaticon-bakery9:before {\r\n\tcontent: \"\\e004\";\r\n}\r\n.flaticon-barbecue2:before {\r\n\tcontent: \"\\e005\";\r\n}\r\n.flaticon-barbecue9:before {\r\n\tcontent: \"\\e006\";\r\n}\r\n.flaticon-bread1:before {\r\n\tcontent: \"\\e007\";\r\n}\r\n.flaticon-bread:before {\r\n\tcontent: \"\\e008\";\r\n}\r\n.flaticon-cake28:before {\r\n\tcontent: \"\\e009\";\r\n}\r\n.flaticon-cake29:before {\r\n\tcontent: \"\\e00a\";\r\n}\r\n.flaticon-cheese12:before {\r\n\tcontent: \"\\e00b\";\r\n}\r\n.flaticon-cheese:before {\r\n\tcontent: \"\\e00c\";\r\n}\r\n.flaticon-chinese food1:before {\r\n\tcontent: \"\\e00d\";\r\n}\r\n.flaticon-chinese food2:before {\r\n\tcontent: \"\\e00e\";\r\n}\r\n.flaticon-chinese food3:before {\r\n\tcontent: \"\\e00f\";\r\n}\r\n.flaticon-chinese food4:before {\r\n\tcontent: \"\\e010\";\r\n}\r\n.flaticon-chinese food5:before {\r\n\tcontent: \"\\e011\";\r\n}\r\n.flaticon-coffee3:before {\r\n\tcontent: \"\\e012\";\r\n}\r\n.flaticon-cooking27:before {\r\n\tcontent: \"\\e013\";\r\n}\r\n.flaticon-cooking28:before {\r\n\tcontent: \"\\e014\";\r\n}\r\n.flaticon-cooking29:before {\r\n\tcontent: \"\\e015\";\r\n}\r\n.flaticon-cooking30:before {\r\n\tcontent: \"\\e016\";\r\n}\r\n.flaticon-corndog:before {\r\n\tcontent: \"\\e017\";\r\n}\r\n.flaticon-crustacean:before {\r\n\tcontent: \"\\e018\";\r\n}\r\n.flaticon-disposable1:before {\r\n\tcontent: \"\\e019\";\r\n}\r\n.flaticon-drink2:before {\r\n\tcontent: \"\\e01a\";\r\n}\r\n.flaticon-drink68:before {\r\n\tcontent: \"\\e01b\";\r\n}\r\n.flaticon-drink69:before {\r\n\tcontent: \"\\e01c\";\r\n}\r\n.flaticon-drink74:before {\r\n\tcontent: \"\\e01d\";\r\n}\r\n.flaticon-drink76:before {\r\n\tcontent: \"\\e01e\";\r\n}\r\n.flaticon-drinking6:before {\r\n\tcontent: \"\\e01f\";\r\n}\r\n.flaticon-earn:before {\r\n\tcontent: \"\\e020\";\r\n}\r\n.flaticon-egg14:before {\r\n\tcontent: \"\\e021\";\r\n}\r\n.flaticon-eggplant:before {\r\n\tcontent: \"\\e022\";\r\n}\r\n.flaticon-egg:before {\r\n\tcontent: \"\\e023\";\r\n}\r\n.flaticon-fork2:before {\r\n\tcontent: \"\\e024\";\r\n}\r\n.flaticon-fruit31:before {\r\n\tcontent: \"\\e025\";\r\n}\r\n.flaticon-fruit32:before {\r\n\tcontent: \"\\e026\";\r\n}\r\n.flaticon-fruit33:before {\r\n\tcontent: \"\\e027\";\r\n}\r\n.flaticon-fruit34:before {\r\n\tcontent: \"\\e028\";\r\n}\r\n.flaticon-fruit35:before {\r\n\tcontent: \"\\e029\";\r\n}\r\n.flaticon-fruit37:before {\r\n\tcontent: \"\\e02a\";\r\n}\r\n.flaticon-fruit38:before {\r\n\tcontent: \"\\e02b\";\r\n}\r\n.flaticon-fruit39:before {\r\n\tcontent: \"\\e02c\";\r\n}\r\n.flaticon-fruit40:before {\r\n\tcontent: \"\\e02d\";\r\n}\r\n.flaticon-fruit41:before {\r\n\tcontent: \"\\e02e\";\r\n}\r\n.flaticon-gastronomy:before {\r\n\tcontent: \"\\e02f\";\r\n}\r\n.flaticon-glass51:before {\r\n\tcontent: \"\\e030\";\r\n}\r\n.flaticon-glove10:before {\r\n\tcontent: \"\\e031\";\r\n}\r\n.flaticon-goodies1:before {\r\n\tcontent: \"\\e032\";\r\n}\r\n.flaticon-grains:before {\r\n\tcontent: \"\\e033\";\r\n}\r\n.flaticon-grater2:before {\r\n\tcontent: \"\\e034\";\r\n}\r\n.flaticon-hat27:before {\r\n\tcontent: \"\\e035\";\r\n}\r\n.flaticon-healthy food3:before {\r\n\tcontent: \"\\e036\";\r\n}\r\n.flaticon-herbal tea:before {\r\n\tcontent: \"\\e037\";\r\n}\r\n.flaticon-hot food:before {\r\n\tcontent: \"\\e038\";\r\n}\r\n.flaticon-hot:before {\r\n\tcontent: \"\\e039\";\r\n}\r\n.flaticon-ice cream4:before {\r\n\tcontent: \"\\e03a\";\r\n}\r\n.flaticon-ice cubes:before {\r\n\tcontent: \"\\e03b\";\r\n}\r\n.flaticon-italian food:before {\r\n\tcontent: \"\\e03c\";\r\n}\r\n.flaticon-jar20:before {\r\n\tcontent: \"\\e03d\";\r\n}\r\n.flaticon-kitchen82:before {\r\n\tcontent: \"\\e03e\";\r\n}\r\n.flaticon-kitchen83:before {\r\n\tcontent: \"\\e03f\";\r\n}\r\n.flaticon-kitchen84:before {\r\n\tcontent: \"\\e040\";\r\n}\r\n.flaticon-kitchen85:before {\r\n\tcontent: \"\\e041\";\r\n}\r\n.flaticon-lunch3:before {\r\n\tcontent: \"\\e042\";\r\n}\r\n.flaticon-lunch5:before {\r\n\tcontent: \"\\e043\";\r\n}\r\n.flaticon-meat11:before {\r\n\tcontent: \"\\e044\";\r\n}\r\n.flaticon-meat1:before {\r\n\tcontent: \"\\e045\";\r\n}\r\n.flaticon-mixer:before {\r\n\tcontent: \"\\e046\";\r\n}\r\n.flaticon-paper cup:before {\r\n\tcontent: \"\\e047\";\r\n}\r\n.flaticon-pine11:before {\r\n\tcontent: \"\\e048\";\r\n}\r\n.flaticon-pizza1:before {\r\n\tcontent: \"\\e049\";\r\n}\r\n.flaticon-plate23:before {\r\n\tcontent: \"\\e04a\";\r\n}\r\n.flaticon-quesadilla:before {\r\n\tcontent: \"\\e04b\";\r\n}\r\n.flaticon-scale21:before {\r\n\tcontent: \"\\e04c\";\r\n}\r\n.flaticon-sea17:before {\r\n\tcontent: \"\\e04d\";\r\n}\r\n.flaticon-shroom:before {\r\n\tcontent: \"\\e04e\";\r\n}\r\n.flaticon-snack1:before {\r\n\tcontent: \"\\e04f\";\r\n}\r\n.flaticon-soft3:before {\r\n\tcontent: \"\\e050\";\r\n}\r\n.flaticon-spicy1:before {\r\n\tcontent: \"\\e051\";\r\n}\r\n.flaticon-sprig1:before {\r\n\tcontent: \"\\e052\";\r\n}\r\n.flaticon-steamed egg:before {\r\n\tcontent: \"\\e053\";\r\n}\r\n.flaticon-suckers:before {\r\n\tcontent: \"\\e054\";\r\n}\r\n.flaticon-sundae:before {\r\n\tcontent: \"\\e055\";\r\n}\r\n.flaticon-tetra brik:before {\r\n\tcontent: \"\\e056\";\r\n}\r\n.flaticon-tray25:before {\r\n\tcontent: \"\\e057\";\r\n}\r\n.flaticon-turkey13:before {\r\n\tcontent: \"\\e058\";\r\n}\r\n.flaticon-turkey leg:before {\r\n\tcontent: \"\\e059\";\r\n}\r\n.flaticon-vegetable3:before {\r\n\tcontent: \"\\e05a\";\r\n}\r\n.flaticon-vegetable4:before {\r\n\tcontent: \"\\e05b\";\r\n}\r\n.flaticon-vegetable5:before {\r\n\tcontent: \"\\e05c\";\r\n}\r\n.flaticon-vegetable6:before {\r\n\tcontent: \"\\e05d\";\r\n}\r\n.flaticon-vegetable7:before {\r\n\tcontent: \"\\e05e\";\r\n}\r\n.flaticon-vegetable8:before {\r\n\tcontent: \"\\e05f\";\r\n}\r\n.flaticon-water69:before {\r\n\tcontent: \"\\e060\";\r\n}\r\n.flaticon-wedding11:before {\r\n\tcontent: \"\\e061\";\r\n}\r\n.flaticon-wheels2:before {\r\n\tcontent: \"\\e062\";\r\n}\r\n.flaticon-wine1:before {\r\n\tcontent: \"\\e063\";\r\n}\r\n/* Google Web Fonts */\r\n/*Careers page starts*/\r\nbody .container{\r\n    zoom: 96%;\r\n}\r\n* {\r\n  box-sizing: border-box;\r\n}\r\n.pagetitles{\r\n    float: right;\r\n    margin-bottom: -5px;\r\n    font-size: 13px;\r\n}\r\n.colorwhite{\r\n    color: white;\r\n    font-size: 34px;\r\n}\r\n.intended{\r\n    text-indent: -24px;\r\n}\r\n.extendleft{\r\n    margin-left: 45px;\r\n}\r\n.fontcolors{\r\n    color: black;\r\n    font-size: 25px !important;\r\n    margin-left: -6px;\r\n}\r\n.fontcolors1 {\r\n    color: black;\r\n    font-size: 23px !important;\r\n    left: 23.5%;\r\n    margin-top: 6px;\r\n    padding: 0px;\r\n}\r\n#myInput {\r\n  background-position: 10px 12px;\r\n  background-repeat: no-repeat;\r\n  width: 50%;\r\n  font-size: 16px;\r\n  padding: 12px 20px 12px 40px;\r\n  border: 1px solid #ddd;\r\n  margin-bottom: 12px;\r\n  border-radius: 5px;\r\n  background-color: white;\r\n}\r\n#myUL {\r\n    list-style-type: none;\r\n    padding: 0;\r\n    margin: 0;\r\n    width: 100%;\r\n    max-height: 320px;\r\n    line-height: 2em;\r\n}\r\ndiv ::-webkit-scrollbar { \r\n    display: none; \r\n}\r\n#myUL li a {\r\n  border-bottom: 1px solid #000;\r\n  margin-top: 0px;\r\n  padding: 12px;\r\n  text-decoration: none;\r\n  font-size: 18px;\r\n  color: #2263a3;\r\n  display: block;\r\n  -webkit-transition: all 0.3s ease;\r\n}\r\n#myUL li a i{\r\n    position: relative;\r\n    top: 5px;\r\n}\r\n#myUL li a:hover{\r\n    color: #227BC2;\r\n    font-size: 19px;\r\n    background: rgba(225,225,225,0.3);\r\n}\r\n#myUL li a.header {\r\n  background-color: #F0F0F0;\r\n  cursor: default;\r\n}\r\n#myUL li:nth-child(odd) {\r\n    background: white;\r\n}\r\n#myUL li:nth-child(even) {\r\n    background: rgba(225,225,225,0.7);\r\n}\r\n/*careers page ends*/\r\n#customers-carousel2.owl-theme .owl-controls .owl-nav [class*=owl-] {\r\n    color: black;\r\n    font-size: 18px;\r\n    font-weight: lighter;\r\n    margin: 5px;\r\n    padding: 8px 12px 8px 12px;\r\n    background: #d6d6d6;\r\n    display: inline-block;\r\n    cursor: pointer;\r\n    border-radius: 3px;\r\n    margin-top: -5.5em;\r\n}\r\n.borderright{\r\n    border-right: 1px solid #D4D4D4;\r\n}\r\n.fullmap{\r\n    width: auto;\r\n    height: 280px;\r\n}\r\n#liststyle{\r\n    list-style: none;\r\n}\r\n#liststyle li:before{\r\n    content: \"\\00BB \\0020\";\r\n}\r\n#btnpad{\r\n    line-height: 3.5px;\r\n    padding: 28px;\r\n    padding-left: 20px;\r\n}\r\n/* ================= General ==================== */\r\n.btnclass{\r\n    background-color: #1976d2;\r\n    color: white;\r\n    border: 1px solid white;\r\n    padding: 10px;\r\n    float: right;\r\n}\r\n.button-style .btn-floating i {\r\n    height: auto !important;\r\n}\r\n#inputs {\r\n    width: 36%;\r\n    background-color: lightcyan;\r\n    padding: 21px;\r\n    color: black;\r\n    border-radius: 5px;\r\n}\r\n#inputs1 {\r\n    width: 36%;\r\n    background-color: lightcyan;\r\n    padding: 21px;\r\n    color: black;\r\n    border-radius: 5px;\r\n}\r\n::-webkit-input-placeholder {\r\n  text-align: center;\r\n}\r\n#inputs:focus{\r\n    border: hidden !important;\r\n}\r\n#inputbtnstyle{\r\n    position: absolute;\r\n    margin-left: -62px;\r\n    margin-top: 1px;\r\n}\r\n#btnwidth{\r\n    width: 50%;\r\n}\r\n.tex{\r\n    padding: 5px;\r\n}\r\n.position5{\r\n    position:fixed;\r\n    width:400px;\r\n    padding: 1em;\r\n    right: 0;\r\n    bottom: 0;\r\n    z-index: 10000;\r\n    margin-bottom: 4em;\r\n\r\n}\r\n.position2{\r\n    position:fixed;\r\n    right: 0;\r\n    bottom: 0;\r\n    z-index: 10000;\r\n    margin: 5px;\r\n}\r\n.owl-prev {\r\n    margin-left: -3em !important;\r\n    float: left;\r\n}\r\n.owl-dots{\r\n    display: none !important;\r\n}\r\n.owl-next {\r\n    margin-right: -3em !important;\r\n    float: right;\r\n}\r\n.fullwidth{\r\n    height: auto;\r\n    width: 100%;\r\n}\r\n#floating {\r\n    float: right;\r\n    margin-top: 15px;\r\n}\r\n.glyphicon-chevron-left:before {\r\n    content: \"\\e079\";\r\n    color: black;\r\n}\r\n.glyphicon-chevron-right:before {\r\n    content: \"\\e080\";\r\n    color: black;\r\n}\r\n.carousel-control .glyphicon-chevron-left, .carousel-control .icon-prev {\r\n    left: 0;\r\n}\r\n.carousel-control .glyphicon-chevron-right, .carousel-control .icon-next {\r\n    right: 0;\r\n}\r\n.carousel-inner .active.left { left: -25%; }\r\n.carousel-inner .next        { left:  25%; }\r\n.carousel-inner .prev        { left: -25%; }\r\n.carousel-control.left,.carousel-control.right {background-image:none;}\r\n.item:not(.prev) {visibility: visible;}\r\n.item.right:not(.prev) {visibility: hidden;}\r\n.rightest{ visibility: visible;}\r\n#popup-bg{\r\n    position: fixed;\r\n    top: 0;\r\n    left: 0;\r\n    background-color: rgba(0,0,0,0.7);\r\n    width: 100%;\r\n    height: 100%;\r\n    display: none;\r\n}\r\n#popup-main-div{\r\n    position: fixed;\r\n    width: 600px;\r\n    height: 500px;\r\n    border: 2px solid black;\r\n    border-radius: 5px;\r\n    background-color: white;\r\n    margin-left: 400px;\r\n    margin-top: -250px;\r\n    top: 50%;\r\n}\r\n#close-popup-div{\r\n    position: absolute;\r\n    width: 25px;\r\n    height: 25px;\r\n    border-radius: 25px;\r\n    border: 2px solid black;\r\n    right: 5px;\r\n    top: 5px;\r\n    text-align: center;\r\n}\r\n#close-popup-div p{\r\n    margin-top: -3px;\r\n    cursor: pointer;\r\n    \r\n}\r\nbody{\r\n    font-family: 'Raleway', sans-serif !important;\r\n    font-size: 14px;\r\n    line-height: 29px;\r\n    font-weight: 500;\r\n    color: black;\r\n    background-color: #fff;\r\n    overflow-x: hidden;\r\n    -webkit-text-size-adjust: 100%;\r\n    -webkit-overflow-scrolling: touch;\r\n    -webkit-font-smoothing: antialiased !important;\r\n}\r\nhtml {\r\n    font-family: 'Raleway', sans-serif!important;\r\n}\r\nhtml,\r\nbody {\r\n    width: 100%;\r\n    height: 100%;\r\n}\r\n/* Link style\r\n/* ------------------------------ */\r\na {\r\n    color: #ed145b;\r\n}\r\na,\r\na > * {\r\n    outline: none;\r\n    cursor: pointer;\r\n    text-decoration: none;\r\n}\r\na:focus,\r\na:hover {\r\n    color: #03a9f4;\r\n    outline: none;\r\n    text-decoration: none;\r\n}\r\n/* Transition elements\r\n/* ------------------------------ */\r\na,\r\n.navbar a,\r\n.form-control {\r\n    transition: all 0.3s ease; \r\n}\r\n/* ================ Typography ================== */\r\nh1, h2, h3, h4, h5, h6{\r\n    font-family: 'Raleway', sans-serif!important;\r\n    font-weight: 400;\r\n    color: #202020;\r\n    margin: 0 0 15px;\r\n}\r\nh1 {\r\n    font-size: 40px;\r\n}\r\nh2 {\r\n    font-size: 20px;\r\n}\r\nh3 {\r\n    font-size: 18px;\r\n}\r\nh4 {\r\n    font-size: 16px;\r\n}\r\nh5 {\r\n    font-size: 14px;\r\n}\r\nh6 {\r\n    font-size: 12px;\r\n}\r\np img { \r\n    margin: 0; \r\n}\r\np {\r\n    margin: 0px;\r\n    font-size: 15px;\r\n    line-height: 30px;\r\n}\r\nhr {\r\n    margin: 0;\r\n}\r\n/* Material Button Style\r\n/* ------------------------------ */\r\n.btn {\r\n    height: 50px;\r\n    padding: 12px 20px;\r\n    font-size: 16px;\r\n    line-height: 26px;\r\n    border-radius: 2px;\r\n    background-color: #03a9f4; /*brand primary color*/\r\n    border: 0;\r\n}\r\n.btn-lg {\r\n    padding: 12px 38px;\r\n}\r\n/*button size in extra small devices*/\r\n@media screen and (max-width: 767px) {\r\n    .btn-lg {\r\n        height: auto;\r\n        font-size: 14px;\r\n        padding: 8px 25px;\r\n    }\r\n}\r\n.btn:hover{\r\n    background-color: #03a9f4;\r\n}\r\n.btn.active, \r\n.btn:active {\r\n    box-shadow: 0 5px 11px 0 rgba(0, 0, 0, 0.18), 0 4px 15px 0 rgba(0, 0, 0, 0.15);\r\n}\r\n.btn.white,\r\n.btn.white:hover,\r\n.btn.white:focus {\r\n    color: #03a9f4;\r\n}\r\n.btn i.material-icons {\r\n    font-size: 20px;\r\n}\r\n.btn.focus, \r\n.btn:focus, \r\n.btn:hover {\r\n    color: #fff;\r\n}\r\n.btn:focus,\r\nbutton:focus {\r\n    outline: none !important;\r\n}\r\n/*Download Button*/\r\n.btn-download {\r\n    padding: 16px 38px;\r\n    text-align: left;\r\n    line-height: 22px;\r\n    height: 75px;\r\n}\r\n.btn-download i{\r\n    font-size: 30px;\r\n    line-height: 48px;\r\n}\r\n.btn-download span{\r\n    display: block!important;\r\n    overflow: hidden;\r\n}\r\n.btn-download strong{\r\n    display: block;\r\n    font-size: 20px;\r\n    font-weight: 900;\r\n    text-transform: uppercase;\r\n}\r\naddress {\r\n    margin: 30px 0 0;\r\n    font-style: normal;\r\n    line-height: 25px;\r\n}\r\naddress hr {\r\n    margin-top: 20px;\r\n    margin-bottom: 20px; \r\n}\r\nthead {\r\n    color: #272829;\r\n    background-color: #f5f5f5;\r\n    border-bottom: 1px solid #eee;\r\n}\r\n.table>thead>tr>th {\r\n    padding: 12px 8px;\r\n}\r\n.table>thead>tr>th {\r\n    border-bottom: 0;\r\n}\r\n.table>tbody>tr>td,\r\n.table>tbody>tr>th {\r\n    color: #666;\r\n    padding: 15px 8px;\r\n    border-top: 1px solid #eee;\r\n}\r\n.contact-info i {\r\n    font-size: 30px;\r\n    line-height: 38px;\r\n    float: left;\r\n    margin-right: 18px;\r\n    padding-left: 2px;\r\n}\r\n.contact-info .address,\r\n.contact-info .phone,\r\n.contact-info .mail{\r\n    overflow: hidden;\r\n}\r\n.contact-info .mail a {\r\n    color: #999;\r\n}\r\n.contact-info .mail a:hover {\r\n    color: #03a9f4;\r\n}\r\n#mapcontent p {\r\n    margin: 0;\r\n}\r\n/*Contact Form*/\r\n.contact-form-wrapper {\r\n    background: url('vactor-map.png') no-repeat center center;\r\n}\r\n.contact-form-bg {\r\n    background: url('vactor-map.png') no-repeat center center;\r\n    background-size: contain;\r\n}\r\n@media (min-width: 992px) {\r\n    #contactForm .submit-button {\r\n        float: right;\r\n    }\r\n}\r\n/* Customize Material Form style \r\n/* ------------------------------------ */\r\n.form-control {\r\n    border-radius: 0;\r\n}\r\n.input-field {\r\n    margin-top: 5px;\r\n}\r\n.input-field label {\r\n    color: #999;\r\n    top: 0;\r\n    left: 0;\r\n    font-size: 14px;\r\n    line-height: 16px;\r\n    font-weight: 400;\r\n    margin: 0;\r\n}\r\n.input-field label.active {\r\n    font-size: 12px;\r\n}\r\n.alert {\r\n    padding: 12px 15px;\r\n}\r\n.overflow-hidden {\r\n\toverflow: hidden;\r\n}\r\n/* ================= Global Classes ==================== */\r\n.no-margin {\r\n    margin: 0 !important;\r\n}\r\n.no-gutter > [class*='col-'] {\r\n    padding-right: 0;\r\n    padding-left: 0;\r\n}\r\n.no-padding {\r\n    padding: 0 !important;\r\n}\r\n.section-padding {\r\n    padding: 0px 0;\r\n}\r\n/*margin top*/\r\n.mt-0 {\r\n    margin-top: 0px;\r\n}\r\n.mt-10 {\r\n    margin-top: 10px;\r\n}\r\n.mt-15 {\r\n    margin-top: 15px;\r\n}\r\n.mt-20 {\r\n    margin-top: 20px;\r\n}\r\n.mt-30 {\r\n    margin-top: 30px;\r\n}\r\n.mt-40 {\r\n    margin-top: 40px;\r\n}\r\n.mt-50 {\r\n    margin-top: 50px;\r\n}\r\n.mt-80 {\r\n    margin-top: 80px;\r\n}\r\n.mt-100 {\r\n    margin-top: 20px;\r\n}\r\n/*margin bottom*/\r\n.mb-10 {\r\n    margin-bottom: 10px !important;\r\n}\r\n.mb-15 {\r\n    margin-bottom: 15px !important;\r\n}\r\n.mb-20 {\r\n    margin-bottom: 20px !important;\r\n}\r\n.mb-30 {\r\n    margin-bottom: 30px !important;\r\n}\r\n.mb-40 {\r\n    margin-bottom: 40px !important;\r\n}\r\n.mb-50 {\r\n    margin-bottom: 50px !important;\r\n}\r\n.mb-80 {\r\n    margin-bottom: 0px !important;\r\n}\r\n.mb-100 {\r\n    margin-bottom: 100px !important;\r\n}\r\n.mtb-50 {\r\n    margin: 50px 0 !important;\r\n}\r\n/*margin right*/\r\n.mr-10 {\r\n    margin-right: 10px;\r\n}\r\n.mr-20 {\r\n    margin-right: 20px;\r\n}\r\n/*margin left*/\r\n.ml-10 {\r\n    margin-left: 10px;\r\n}\r\n.ml-20 {\r\n    margin-left: 20px;\r\n}\r\n/*padding-left*/\r\n.padding-left-0{\r\n    padding-left: 0;\r\n}\r\n.padding-right-0{\r\n    padding-right: 0;\r\n}\r\n/*padding-right*/\r\n/*padding-top*/\r\n.padding-top-20 {\r\n    padding-top: 20px;\r\n}\r\n.padding-top-30 {\r\n    padding-top: 30px;\r\n}\r\n.padding-top-40 {\r\n    padding-top: 40px;\r\n}\r\n.padding-top-50 {\r\n    padding-top: 50px;\r\n}\r\n.padding-top-70 {\r\n    padding-top: 70px;\r\n}\r\n.padding-top-90 {\r\n    padding-top: 90px;\r\n}\r\n.padding-top-100 {\r\n    padding-top: 100px;\r\n}\r\n.padding-top-110 {\r\n    padding-top: 110px;\r\n}\r\n.padding-top-120 {\r\n    padding-top: 120px;\r\n}\r\n.padding-top-160 {\r\n    padding-top: 160px;\r\n}\r\n.padding-top-220 {\r\n    padding-top: 220px;\r\n}\r\n@media screen and (max-width: 768px) {\r\n    .padding-top-220 {\r\n        padding-top: 170px;\r\n    }\r\n}\r\n/*padding-bottom*/\r\n.padding-bottom-10 {\r\n    padding-bottom: 10px;\r\n}\r\n.padding-bottom-20 {\r\n    padding-bottom: 20px;\r\n}\r\n.padding-bottom-30 {\r\n    padding-bottom: 30px;\r\n}\r\n.padding-bottom-40 {\r\n    padding-bottom: 40px;\r\n}\r\n.padding-bottom-50 {\r\n    padding-bottom: 50px;\r\n}\r\n.padding-bottom-70 {\r\n    padding-bottom: 70px;\r\n}\r\n.padding-bottom-80 {\r\n    padding-bottom: 80px;\r\n}\r\n.padding-bottom-90 {\r\n    padding-bottom: 90px;\r\n}\r\n.padding-bottom-100 {\r\n    padding-bottom: 100px;\r\n}\r\n.padding-bottom-110 {\r\n    padding-bottom: 110px;\r\n}\r\n.padding-bottom-120 {\r\n    padding-bottom: 120px;\r\n}\r\n.padding-bottom-190 {\r\n    padding-bottom: 190px;\r\n}\r\n@media screen and (min-width: 992px) {\r\n    /*padding left*/\r\n    .pl-100 {\r\n        padding-left: 100px;\r\n    }\r\n}\r\n/*padding top bottom*/\r\n.ptb-30 {\r\n    padding: 20px 0;\r\n}\r\n.ptb-40 {\r\n    padding: 40px 0;\r\n}\r\n.ptb-50 {\r\n    padding: 50px 0;\r\n}\r\n.ptb-70 {\r\n    padding: 70px 0;\r\n}\r\n.ptb-90 {\r\n    padding-top: 90px;\r\n    padding-bottom: 90px;\r\n}\r\n.ptb-110 {\r\n    padding-top: 110px;\r\n    padding-bottom: 110px;\r\n}\r\n.ptb-120 {\r\n    padding-top: 120px;\r\n    padding-bottom: 120px;\r\n}\r\n.ptb-150 {\r\n    padding: 150px 0;\r\n}\r\n.ptb-190 {\r\n    padding: 190px 0;\r\n}\r\n/*Margin for small devices*/\r\n@media screen and (max-width: 991px) {\r\n    /*margin top*/\r\n    .mt-sm-30 {\r\n        margin-top: 30px;\r\n    }\r\n    .mt-sm-50 {\r\n        margin-top: 50px;\r\n    }\r\n    \r\n    /*margin-bottom*/\r\n    .mb-sm-30 {\r\n        margin-bottom: 30px;\r\n    }\r\n    .mb-sm-50 {\r\n        margin-bottom: 50px;\r\n    }\r\n}\r\n@media screen and (max-width: 768px) {\r\n    .mt-xs-30 {\r\n        margin-top: 30px;\r\n    }\r\n    .mt-xs-46 {\r\n        margin-top: 46px;\r\n    }\r\n}\r\n/*Border Radious*/\r\n.radius-2 {\r\n    border-radius: 2px;\r\n}\r\n.radius-3 {\r\n    border-radius: 3px;\r\n}\r\n.radius-4 {\r\n    border-radius: 4px;\r\n}\r\n/*Font Family*/\r\n.font-roboto {\r\n    font-family: 'Roboto', sans-serif !important;\r\n}\r\n.font-greatvibes {\r\n    font-family: 'Great Vibes', cursive !important;\r\n}\r\n/*Font Size*/\r\n.font-20 {\r\n    font-size: 20px\r\n}\r\n.font-25 {\r\n    font-size: 25px\r\n}\r\n.font-30 {\r\n    font-size: 30px\r\n}\r\n.font-35 {\r\n    font-size: 35px\r\n}\r\n.font-40 {\r\n    font-size: 40px\r\n}\r\n/*font size in extra small devices*/\r\n@media screen and (max-width: 767px) {\r\n\r\n    .font-30 {\r\n        font-size: 25px\r\n    }\r\n\r\n    h1,\r\n    .font-35,\r\n    .font-40 {\r\n        font-size: 30px\r\n    }\r\n}\r\n/*Line Height*/\r\n.line-height-40 {\r\n    line-height: 40px;\r\n}\r\n.line-height-50 {\r\n    line-height: 50px;\r\n}\r\n.text-light {\r\n    font-weight: 300 !important;\r\n}\r\n.text-regular {\r\n    font-weight: 400 !important;\r\n}\r\n.text-medium {\r\n    font-weight: 500 !important;\r\n}\r\n.text-bold {\r\n    font-weight: 700 !important;\r\n}\r\n.text-extrabold {\r\n    font-weight: 900 !important;\r\n}\r\n.dark-text {\r\n    color: #202020 !important;\r\n}\r\n.light-grey-text {\r\n    color: #dedede !important;\r\n}\r\n/*List Style*/\r\n.list-icon li{\r\n    font-size: 17px;\r\n    line-height: 30px;\r\n}\r\n.list-icon li .material-icons {\r\n    position: relative;\r\n    top: 5px;\r\n}\r\n.list-icon1 li{\r\n    font-size: 11px;\r\n    line-height: 30px;\r\n}\r\n.list-icon1 li i{\r\n    font-size: 15px;\r\n}\r\n.list-icon1 li .material-icons {\r\n    position: relative;\r\n    top: 5px;\r\n}\r\n/*height*/\r\n.height-200 {\r\n    height: 200px!important;\r\n}\r\n.height-350 {\r\n    height: 350px!important;\r\n}\r\n.height-450 {\r\n    height: 450px!important;\r\n}\r\n.height-650 {\r\n    height: 650px!important;\r\n}\r\n/* ================= Color Presets ==================== */\r\n.brand-color {\r\n    color: #03a9f4 !important; /*theme primary color*/\r\n}\r\n.brand-color.darken-2 {\r\n    color: #0288d1 !important;\r\n}\r\n.brand-bg {\r\n    background-color: #03a9f4 !important; /*theme primary background color*/\r\n}\r\n.brand-bg.darken-2 {\r\n    background-color: #0288d1 !important;\r\n}\r\n.brand-hover:hover {\r\n\tbackground-color: #03a9f4 !important; /*theme primary hover color*/\r\n}\r\n.green-bg {\r\n    background-color: #71c44c !important;\r\n}\r\n.green-color {\r\n    color: #71c44c !important;\r\n}\r\n.pink {\r\n    background-color: #ed145b !important;\r\n}\r\n.white-bg {\r\n    background-color: #fff !important;\r\n}\r\n.gray-bg {\r\n    background-color: #dddddd!important;\r\n}\r\n.light-gray-bg {\r\n    background-color: #f3f3f3 !important;\r\n}\r\n.light-pink-bg {\r\n    background: #fff8f1 !important;\r\n}\r\n.dark-bg.darken-1 {\r\n    background: #101010 !important;\r\n}\r\n.dark-bg {\r\n    background: #202020 !important;\r\n}\r\n.dark-bg.lighten-1 {\r\n    background-color: #1e262a !important;\r\n}\r\n.dark-bg.lighten-2 {\r\n    background-color: #303b41 !important;\r\n}\r\n.dark-bg.lighten-3 {\r\n    background-color: #2c2c2c !important;\r\n}\r\n.dark-bg.lighten-4 {\r\n    background-color: #373a3d !important;\r\n}\r\n.border-top {\r\n    border-top: 1px solid #eee;\r\n}\r\n.border-tb {\r\n    border-top: 1px solid #eee;\r\n    border-bottom: 1px solid #eee;\r\n}\r\n/* ================= Pre Defined Overlay ==================== */\r\n.overlay,\r\n.overlay .container {\r\n  position: relative;\r\n}\r\n.overlay:before {\r\n    content: \"\";\r\n    width: 100%;\r\n    height: 100%;\r\n    position: absolute;\r\n    top: 0;\r\n    left: 0;\r\n    background: rgba(0, 0, 0, 0.2); /*fallback overlay*/\r\n}\r\n/* dark overlay */\r\n.overlay.dark-0:before {\r\n    background-color: rgba(0,0,0,0);\r\n}\r\n.overlay.dark-1:before {\r\n    background-color: rgba(0,0,0,.1);\r\n}\r\n.overlay.dark-2:before {\r\n    background-color: rgba(0,0,0,.2);\r\n}\r\n.overlay.dark-3:before {\r\n    background-color: rgba(0,0,0,.3);\r\n}\r\n.overlay.dark-4:before {\r\n    background-color: rgba(0,0,0,.4);\r\n}\r\n.overlay.dark-5:before {\r\n    background-color: rgba(0,0,0,.5);\r\n}\r\n.overlay.dark-6:before {\r\n    background-color: rgba(0,0,0,.6);\r\n}\r\n.overlay.dark-7:before {\r\n    background-color: rgba(0,0,0,.7);\r\n}\r\n.overlay.dark-8:before {\r\n    background-color: rgba(0,0,0,.8);\r\n}\r\n.overlay.dark-9:before {\r\n    background-color: rgba(0,0,0,.9);\r\n}\r\n.overlay.dark-10:before {\r\n    background-color: rgba(0,0,0,1);\r\n}\r\n/* light overlay */\r\n.overlay.light-0:before {\r\n    background-color: rgba(255,255,255,0);\r\n}\r\n.overlay.light-1:before {\r\n    background-color: rgba(255,255,255,.1);\r\n}\r\n.overlay.light-2:before {\r\n    background-color: rgba(255,255,255,.2);\r\n}\r\n.overlay.light-3:before {\r\n    background-color: rgba(255,255,255,.3);\r\n}\r\n.overlay.light-4:before {\r\n    background-color: rgba(255,255,255,.4);\r\n}\r\n.overlay.light-5:before {\r\n    background-color: rgba(255,255,255,.5);\r\n}\r\n.overlay.light-6:before {\r\n    background-color: rgba(255,255,255,.6);\r\n}\r\n.overlay.light-7:before {\r\n    background-color: rgba(255,255,255,.7);\r\n}\r\n.overlay.light-8:before {\r\n    background-color: rgba(255,255,255,.8);\r\n}\r\n.overlay.light-9:before {\r\n    background-color: rgba(255,255,255,.9);\r\n}\r\n.overlay.light-10:before {\r\n    background-color: rgba(255,255,255,1);\r\n}\r\n.full-height {\r\n  height: 100vh;\r\n  width: 100%;\r\n}\r\n.half-height {\r\n  height: 78vh !important;\r\n  width: 100%;\r\n}\r\n/*Verticle Aligne Middle for equel height*/\r\n.valign-wrapper {\r\n    display: table;\r\n    width: 100%;\r\n}\r\n.valign-cell {\r\n    display: table-cell;\r\n    vertical-align: middle;\r\n}\r\n/*equal height wrapper*/\r\n.equal-wrapper {}\r\n.equal-wrapper img {\r\n    width: 100%;\r\n}\r\n.equal-wrapper-content {\r\n    padding: 0 120px;\r\n}\r\n.equal-wrapper-content .featured-item .icon {\r\n    float: left;\r\n    margin-right: 35px;\r\n    color: #60e3f8;\r\n}\r\n@media screen and (max-width: 991px) {\r\n    .equal-wrapper-content {\r\n        padding: 50px 30px;\r\n    }\r\n}\r\n/* ================= Hero Unit ==================== */\r\n.intro-title {\r\n    font-size: 60px;\r\n    font-weight: 900;\r\n}\r\n.sub-intro {\r\n    display: block;\r\n    margin: 0 auto 30px;\r\n}\r\n@media (min-width: 768px){\r\n    .sub-intro {\r\n        width: 60%;\r\n    }\r\n}\r\n.section-title {\r\n    font-size: 40px;\r\n    margin-bottom: 5px;\r\n    padding-top: 5px;\r\n}\r\n.section-sub {\r\n    margin: 0 auto;\r\n    padding-bottom: 10px;\r\n}\r\n@media (min-width: 992px){\r\n    .section-sub {\r\n        width: 60%;\r\n    }\r\n}\r\n.width-60 {\r\n    width: 60%;\r\n    margin: 0 auto;\r\n}\r\n@media screen and (max-width: 767px) {\r\n\r\n    .intro-title {\r\n        font-size: 35px;\r\n    }\r\n    .section-title {\r\n        font-size: 30px;\r\n    }\r\n    .section-title p {\r\n        width: 100%;\r\n    }\r\n    .width-60 {\r\n        width: 100%;\r\n    }\r\n\r\n    #btnwidth{\r\n    width: 100%;\r\n    height: auto;\r\n}\r\n\r\n}\r\n/*Hero Clouds*/\r\n.hero-clouds {\r\n  position: absolute;\r\n  top: 0;\r\n  left: 0;\r\n  width: 250.625em;\r\n  height: 100vh;\r\n  /* background: url('assets/img/banner/clouds.png') 0px 100% repeat-x; */\r\n  -webkit-animation: cloudLoop 80s linear infinite;\r\n          animation: cloudLoop 80s linear infinite;\r\n}\r\n/*keyframes for cloud*/\r\n@-webkit-keyframes cloudLoop {\r\n  0% {\r\n    -webkit-transform: translate3d(0, 0, 0);\r\n            transform: translate3d(0, 0, 0);\r\n  }\r\n  100% {\r\n    -webkit-transform: translate3d(-50%, 0, 0);\r\n            transform: translate3d(-50%, 0, 0);\r\n  }\r\n}\r\n@keyframes cloudLoop {\r\n  0% {\r\n    -webkit-transform: translate3d(0, 0, 0);\r\n            transform: translate3d(0, 0, 0);\r\n  }\r\n  100% {\r\n    -webkit-transform: translate3d(-50%, 0, 0);\r\n            transform: translate3d(-50%, 0, 0);\r\n  }\r\n}\r\n/*Mouse animate icon*/\r\n.mouse-icon {\r\n    position: absolute;\r\n    left: 50%;\r\n    bottom: 40px;\r\n    border: 2px solid #fff;\r\n    border-radius: 16px;\r\n    height: 40px;\r\n    width: 24px;\r\n    margin-left: -15px;\r\n    display: block;\r\n    z-index: 10;\r\n}\r\n.mouse-icon .wheel {\r\n    -webkit-animation-name: drop;\r\n    -webkit-animation-duration: 1s;\r\n    -webkit-animation-timing-function: linear;\r\n    -webkit-animation-delay: 0s;\r\n    -webkit-animation-iteration-count: infinite;\r\n    -webkit-animation-play-state: running;\r\n    animation-name: drop;\r\n    animation-duration: 1s;\r\n    animation-timing-function: linear;\r\n    animation-delay: 0s;\r\n    animation-iteration-count: infinite;\r\n    animation-play-state: running;\r\n}\r\n.mouse-icon .wheel {\r\n    position: relative;\r\n    border-radius: 10px;\r\n    background: #fff;\r\n    width: 2px;\r\n    height: 6px;\r\n    top: 4px;\r\n    margin-left: auto;\r\n    margin-right: auto;\r\n}\r\n@-webkit-keyframes drop {\r\n    0%   { top:5px;  opacity: 0;}\r\n    30%  { top:10px; opacity: 1;}\r\n    100% { top:25px; opacity: 0;}\r\n}\r\n@keyframes drop {\r\n    0%   { top:5px;  opacity: 0;}\r\n    30%  { top:10px; opacity: 1;}\r\n    100% { top:25px; opacity: 0;}\r\n}\r\n/*Video Introduction*/\r\n.video-intro {\r\n    position: relative;\r\n}\r\n.video-intro .external-link {\r\n    position: absolute;\r\n    top: 50%;\r\n    left: 50%;\r\n    margin-top: -30px;\r\n    margin-left: -30px;\r\n}\r\n.video-intro .external-link .material-icons {\r\n    font-size: 60px;\r\n    color: rgba(255, 255, 255, 0.4);\r\n    transition: color 0.3s ease;\r\n}\r\n.video-intro:hover .external-link .material-icons {\r\n    color: rgba(255, 255, 255, 0.9);\r\n}\r\n.video-trigger i.material-icons {\r\n    font-size: 68px;\r\n    color: #fff;\r\n    margin: 0 20px;\r\n    position: relative;\r\n    top: 24px;\r\n}\r\n.mocup-wrapper {\r\n    position: relative;\r\n}\r\n.mocup-wrapper img {\r\n    max-width: 100%;\r\n}\r\n@media screen and (max-width: 991px) {\r\n   .mocup-wrapper-sm img{\r\n        width: 100%;\r\n   } \r\n}\r\n/* ================= Banner Background ==================== */\r\n.bg-cover,\r\n[class*='banner-'] {\r\n    background-size: cover !important;\r\n}\r\n/* \r\n.banner-1 {\r\n    background-image: url(\"assets/img/banner/banner-1.jpg\");  \r\n}\r\n.banner-2 {\r\n    background-image: url(\"assets/img/banner/banner-2.jpg\");  \r\n}\r\n.banner-3 {\r\n    background-image: url(\"assets/img/banner/banner-3.jpg\");  \r\n}\r\n.banner-4 {\r\n    background-image: url(\"assets/img/banner/banner-4.jpg\");  \r\n}\r\n.banner-5 {\r\n    background-image: url(\"assets/img/banner/banner-5.jpg\");\r\n}\r\n.banner-6 {\r\n    background-image: url(\"assets/img/about_us_banner_6_bigperl.jpg\");  \r\n}\r\n.banner-7 {\r\n    background-image: url(\"assets/img/banner/banner-7.jpg\");  \r\n}\r\n.banner-8 {\r\n    background-image: url(\"assets/img/banner/banner-8.jpg\");\r\n}\r\n.banner-9 {\r\n    background-image: url(\"assets/img/banner/banner-9.jpg\");  \r\n}\r\n.banner-10 {\r\n    background-image: url(\"assets/img/banner_10_bigperl.jpg\");  \r\n}\r\n.banner-11 {\r\n    background-image: url(\"assets/img/banner/banner-11.jpg\");  \r\n}\r\n.banner-12 {\r\n    background-image: url(\"assets/img/banner/banner-12.jpg\");  \r\n}\r\n.banner-13 {\r\n    background-image: url(\"assets/img/banner/banner-13.jpg\");  \r\n}\r\n.banner-14 {\r\n    background-image: url(\"assets/img/banner/banner-14.jpg\"); \r\n}\r\n.banner-14.overlay::before  {\r\n    background-color: rgba(246, 72, 63, 0.9);\r\n}\r\n.banner-15 {\r\n    background-image: url(\"assets/img/banner/banner-15.jpg\");  \r\n}\r\n.banner-16 {\r\n    background-image: url(\"assets/img/banner/banner-16.jpg\");  \r\n}\r\n.banner-17 {\r\n    background-image: url(\"assets/img/banner/banner-17.jpg\");  \r\n}\r\n.banner-18 {\r\n    background-image: url(\"assets/img/banner/banner-18.jpg\");  \r\n}\r\n.banner-19 {\r\n    background-image: url(\"assets/img/banner/banner-19.jpg\");  \r\n}\r\n.banner-20 {\r\n    background-image: url(\"assets/img/banner/banner-20.jpg\");  \r\n} */\r\n@media screen and (max-width: 767px) {\r\n    .banner-wrapper {\r\n        margin-top: 45px;\r\n    }\r\n}\r\n@media (min-width: 992px) {\r\n    .bg-fixed {\r\n        background-attachment: fixed;\r\n    }\r\n}\r\n/* ================= 404 Page ==================== */\r\n.error-wrapper {\r\n    padding: 50px 0;\r\n}\r\n.error-wrapper i {\r\n    font-size: 130px;\r\n    line-height: 170px;\r\n    text-align: center;\r\n    display: block;\r\n    color: #dadada;\r\n}\r\n.error-info {\r\n    padding-left: 60px;\r\n    border-left: 1px solid #eee;\r\n}\r\n.error-info h1{\r\n    color: #fff;\r\n    font-size: 130px;\r\n    line-height: 100px;\r\n    font-weight: 700;\r\n    text-shadow: 5px 5px 0 #dadada, -1px -1px 0 #dadada, 1px -1px 0 #dadada, -1px 1px 0 #dadada, 1px 1px 0 #dadada;\r\n}\r\n.ie9 .error-info h1,\r\n.ie9 .error-wrapper-alt h1{\r\n    color: #999;\r\n}\r\n.error-sub {\r\n    display: block;\r\n    font-size: 30px;\r\n    line-height: 45px;\r\n    font-weight: 700;\r\n    text-transform: uppercase;\r\n}\r\n@media screen and (max-width: 767px) {\r\n    .error-info {\r\n        padding-left: 0;\r\n        border-left: 0;\r\n        text-align: center;\r\n    }\r\n}\r\n@media (min-width: 992px) {\r\n    .error-wrapper {\r\n        padding: 200px 0;\r\n    }\r\n}\r\n.error-wrapper-alt h1 {\r\n    color: #fff;\r\n    font-size: 130px;\r\n    line-height: 130px;\r\n    font-weight: 700;\r\n    text-shadow: 5px 5px 0 #dadada, -1px -1px 0 #dadada, 1px -1px 0 #dadada, -1px 1px 0 #dadada, 1px 1px 0 #dadada;\r\n}\r\n/*Magnific Popup Close Button*/\r\n.mfp-image-holder .mfp-close, \r\n.mfp-iframe-holder .mfp-close {\r\n    right: -10px;\r\n    padding-right: 0;\r\n    width: 40px;\r\n    text-align: center;\r\n}\r\nbutton.mfp-close:focus,\r\nbutton.mfp-arrow {\r\n    background-color: transparent;\r\n}\r\n.mfp-bg{\r\n    z-index: 1055;\r\n}\r\n.mfp-wrap {\r\n    z-index: 1056;\r\n}\r\n.mfp-image-holder .mfp-close:hover, \r\n.mfp-iframe-holder .mfp-close:hover{\r\n    cursor: pointer;\r\n}\r\n.mfp-zoom-out-cur{\r\n    cursor: default;\r\n}\r\n/* Onepage*/\r\n.box-padding {\r\n    padding: 60px 40px;\r\n}\r\n/*Author Wrapper*/\r\n.profile .author-cover {\r\n    position: relative;\r\n}\r\n.profile .author-cover img{\r\n    width: 100%;\r\n}\r\n.author-wrapper.profile .author-avatar {\r\n    position: relative;\r\n    padding: 15px 20px 30px 130px;\r\n}\r\n.author-wrapper.profile .author-avatar img {\r\n    width: 100px;\r\n    height: 100px;\r\n    border-radius: 50%;\r\n    background: #fff;\r\n    padding: 5px;\r\n    position: absolute;\r\n    top: -30px;\r\n    left: 15px;\r\n    z-index: 20;\r\n}\r\n.profile .author-meta {\r\n    padding: 0 40px;\r\n    overflow: hidden;\r\n}\r\n.profile .author-meta li {\r\n    margin: 10px 0;\r\n    font-weight: 500;\r\n}\r\n.profile .author-meta li .title {\r\n    display: inline-block;\r\n    width: 135px;\r\n    color: #202020;\r\n}\r\n.profile .author-meta li .address {\r\n    float: right;\r\n    width: 50%;\r\n}\r\n.profile .available {\r\n    display: block;\r\n    padding: 10px;\r\n    text-align: center;\r\n}\r\n.profile .available a{\r\n    display: inline-block;\r\n    color: #999;\r\n}\r\n.profile .available a:hover{\r\n    color: #03a9f4;\r\n}\r\n@media screen and (max-width: 991px) {\r\n    .author-wrapper.profile {\r\n        margin-top: 30px;\r\n    }  \r\n}\r\n@media screen and (max-width: 479px) {\r\n    .box-padding .cta-button .btn {\r\n        display: block;\r\n        margin-top: 10px;\r\n    }\r\n    .profile .author-meta li .title {\r\n        width: 80px;\r\n    }\r\n    .profile .author-meta li .address {\r\n        width: 60%;\r\n    }\r\n\r\n}\r\n/* ================= Progress bar ==================== */\r\n.progress-section {\r\n    position: relative;\r\n}\r\n.progress-title {\r\n    display: block;\r\n    margin-bottom: 8px;\r\n}\r\n.progress {\r\n    box-shadow: none;\r\n    background-color: #eee;\r\n    height: 5px;\r\n    overflow: visible;\r\n    border-radius: 0;\r\n    margin-bottom: 30px;\r\n}\r\n.progress-bar {\r\n    box-shadow: none;\r\n    text-align: right;\r\n}\r\n.progress-bar span {\r\n    position: absolute;\r\n    top: -32px;\r\n    color: #999;\r\n    font-size: 14px;\r\n    display: inline-block;\r\n}\r\n/*IE Fixing*/\r\n.ie11 .progress-bar span,\r\n.ie10 .progress-bar span,\r\n.ie9 .progress-bar span {\r\n  top: -25px;\r\n  right: 0;\r\n}\r\n.progress .progress-bar.six-sec-ease-in-out {\r\n  transition:  width 2s ease-in-out;\r\n}\r\n/*Progress Dot Style*/\r\n.progress-dot {\r\n    position: relative;\r\n}\r\n.progress-dot::before {\r\n    content: \"\";\r\n    width: 20px;\r\n    height: 20px;\r\n    background-color: #03a9f4;\r\n    border-radius: 50%;\r\n    position: absolute;\r\n    top: 0;\r\n    margin-top: -8px;\r\n    right: -1px;\r\n}\r\n/* ================= Clients ==================== */\r\n.clients-grid .border-box {\r\n    border: 1px solid #eee;\r\n    margin-left: -1px;\r\n    margin-bottom: -1px;\r\n    transition: box-shadow 0.3s;\r\n}\r\n.clients-grid .border-box a{\r\n    display: block;\r\n    margin-left: auto;\r\n    margin-right: auto;\r\n}\r\n.clients-grid .border-box img {\r\n    padding: 30px 50px;\r\n    display: block;\r\n    margin-left: auto;\r\n    margin-right: auto;\r\n    width: 100%;\r\n    -webkit-filter: grayscale(100%); /* Chrome, Safari, Opera */\r\n    filter: grayscale(100%);\r\n    transition: all 0.3s;\r\n}\r\n.clients-grid .border-box img:hover {\r\n    -webkit-filter: grayscale(0%);\r\n    filter: grayscale(0%);\r\n}\r\n.clients-grid .border-box:hover{\r\n    box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);\r\n}\r\n/*\r\nGrid Gutter Style\r\n---------------------------------------------------------------------------*/\r\n.clients-grid.grid-gutter .border-box {\r\n    margin-bottom: 30px;\r\n}\r\n/*\r\nGutter Style\r\n---------------------------------------------------------------------------*/\r\n@media (max-width: 991px) {\r\n    .clients-grid.gutter .border-box {\r\n        margin-bottom: 30px;\r\n    } \r\n}\r\n/*\r\nGallery Thumb\r\n-----------------------------------------------------*/\r\n.gallery-thumb .flex-viewport li img {\r\n    width: 100%;\r\n}\r\n.gallery-thumb .flex-control-thumbs {\r\n    margin: -35px 0 0;\r\n}\r\n.gallery-thumb .flex-control-thumbs li {\r\n    width: 70px;\r\n    float: none;\r\n    margin: 0 5px;\r\n}\r\n.gallery-thumb .flex-control-thumbs img {\r\n    width: 70px;\r\n    height: 70px!important;\r\n    border-radius: 50%;\r\n    background: #fff;\r\n    padding: 5px;\r\n    opacity: 1;\r\n    z-index: 100;\r\n    position: relative;\r\n}\r\n.gallery-thumb .flex-direction-nav a {\r\n    opacity: 1;\r\n    top: auto;\r\n    bottom: 45px;\r\n    text-align: center;\r\n}\r\n.gallery-thumb .flex-direction-nav .flex-prev {\r\n    left: 15px;\r\n}\r\n.gallery-thumb .flex-direction-nav .flex-next {\r\n    right: 15px;\r\n}\r\n.gallery-thumb  .flex-direction-nav a.flex-prev::before,\r\n.gallery-thumb  .flex-direction-nav a.flex-next::before {\r\n    font-family: 'Material Icons';\r\n    font-size: 20px;\r\n    color: #fff;  \r\n}\r\n.gallery-thumb  .flex-direction-nav a.flex-prev::before {\r\n    content: 'arrow_back';\r\n}\r\n.gallery-thumb  .flex-direction-nav a.flex-next::before {\r\n    content: 'arrow_forward';\r\n}\r\n@media screen and (max-width: 370px){\r\n    .gallery-thumb .flex-control-thumbs li {\r\n        width: 50px;\r\n    }\r\n\r\n    .gallery-thumb .flex-control-thumbs img {\r\n        width: 50px;\r\n        height: 50px!important;\r\n    }\r\n\r\n}\r\n/* ====== Food Menu CSS for restaurant and coffee shop ====== */\r\n/*you can remove this css if you not use restaurant demo or coffee shop demo*/\r\n.food-menu-category {\r\n    padding-bottom: 35px;\r\n}\r\n.food-menu-category .food-menu-wrapper {\r\n    transition: box-shadow .3s ease-out;\r\n}\r\n.food-menu-category:hover .food-menu-wrapper {\r\n    box-shadow: 0 8px 17px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);\r\n}\r\n.food-menu-intro {\r\n    padding: 24px;\r\n}\r\n.food-menu-intro .material-icons{\r\n    font-size: 40px;\r\n}\r\n.food-menu-list {\r\n    padding: 30px;\r\n}\r\n.food-menu {\r\n    border-bottom: 1px dotted rgba(255, 255, 255, 0.5);\r\n    padding: 0 0 15px;\r\n    margin: 0 0 15px;\r\n}\r\n.food-menu-title {\r\n    margin-bottom: 5px;\r\n}\r\n.food-menu-detail {\r\n    line-height: 20px;\r\n}\r\n.food-menu-price {\r\n    font-style: italic;\r\n}\r\n.food-menu-price-detail {\r\n    position: relative;\r\n    text-align: right;\r\n}\r\n.food-menu-label {\r\n    background: #fff8f1;\r\n    color: #03a9f4;\r\n    display: inline-block;\r\n    padding: 0 10px;\r\n    font-style: italic;\r\n    font-size: 13px;\r\n   // float: left;\r\n    position: relative;\r\n}\r\n/*Flat Icon Setup*/\r\ni[class^=\"flaticon-\"] {\r\n    line-height: 50px;\r\n    display: inline-block;\r\n}\r\ni[class^=\"flaticon-\"]:before {\r\n    font-size: 45px;\r\n    margin-left: 0;\r\n}\r\n/* ================= Material CSS Customization ==================== */\r\nnav {\r\n    background-color: transparent;\r\n}\r\nnav ul,\r\nnav ul li {\r\n    float: none;\r\n}\r\nnav ul li:hover, \r\nnav ul li.active {\r\n    background-color: transparent;\r\n}\r\nnav ul a {\r\n    font-size: inherit;\r\n}\r\n@media only screen and (min-width: 1200px){\r\n  .container {\r\n    width: 1170px;\r\n  }\r\n}\r\n@media only screen and (max-width: 749px) {\r\n    .container {\r\n        width: 100%; \r\n    }\r\n}\r\n@media only screen and (max-width: 601px) {\r\n    .container {\r\n         width: 90%; \r\n    }\r\n}\r\n.container .row,\r\n.container-fluid .row {\r\n    margin-bottom: 0;\r\n}\r\n.container .row {\r\n    margin-right: -15px;\r\n    margin-left: -15px;\r\n}\r\n.row .col {\r\n    padding-right: 15px;\r\n    padding-left: 15px;\r\n}\r\n.carousel {\r\n    height: auto;\r\n}\r\n#owl-demo .section img{\r\n    display: block;\r\n    width: 100%;\r\n    height: auto;\r\n}\r\n/* Button Style In Shortcode Page\r\n/* ---------------------------------- */\r\n.button-style .btn,\r\n.button-style .btn-large {\r\n    margin-right: 30px;\r\n    margin-bottom: 30px;\r\n}\r\n.button-style .btn-floating i {\r\n    height: auto !important;\r\n}\r\n/*Equal Height Columns*/\r\n@media (max-width: 767px) {\r\n    .equal-height-column {\r\n        height: auto !important;\r\n    }\r\n}\r\n/*TT Animate CSS*/\r\n.tt-animate i {\r\n    text-align         : center;\r\n    overflow           : hidden;\r\n    transition         : all 0.5s;\r\n}\r\n.tt-animate i::before {\r\n    speak                       : none;\r\n    display                     : block;\r\n    -webkit-font-smoothing      : subpixel-antialiased !important;\r\n    -webkit-backface-visibility : hidden;\r\n    -moz-backface-visibility    : hidden;\r\n    -ms-backface-visibility     : hidden;\r\n}\r\n/*Left To Right*/\r\n.tt-animate.ltr i:hover::before {\r\n    -webkit-animation : LeftToRight 0.3s forwards;\r\n    animation         : LeftToRight 0.3s forwards;\r\n}\r\n.tt-animate.ltr i:before {\r\n    -webkit-animation : RightToLeft 0.3s forwards;\r\n    animation         : RightToLeft 0.3s forwards;\r\n}\r\n/*Right To Left*/\r\n.tt-animate.btt i:hover::before {\r\n    -webkit-animation : BottomToTop 0.3s forwards;\r\n    animation         : BottomToTop 0.3s forwards;\r\n}\r\n.tt-animate.btt i:before {\r\n    -webkit-animation : TopToBottom 0.3s forwards;\r\n    animation         : TopToBottom 0.3s forwards;\r\n}\r\n/*---------------------------------------------------------\r\nKEY FRAME \r\n-----------------------------------------------------------*/\r\n/* Left to Right key frame*/\r\n@-webkit-keyframes LeftToRight {\r\n    49% {\r\n        -webkit-transform : translate(100%);\r\n    }\r\n    50% {\r\n        opacity           : 0;\r\n        -webkit-transform : translate(-100%);\r\n    }\r\n    51% {\r\n        opacity : 1;\r\n    }\r\n}\r\n@keyframes LeftToRight {\r\n    49% {\r\n        -webkit-transform : translate(100%);\r\n                transform : translate(100%);\r\n    }\r\n    50% {\r\n        opacity   : 0;\r\n        -webkit-transform : translate(-100%);\r\n                transform : translate(-100%);\r\n    }\r\n    51% {\r\n        opacity : 1;\r\n    }\r\n}\r\n/* Right to Left key frame*/\r\n@-webkit-keyframes RightToLeft {\r\n\r\n    49% {\r\n        -webkit-transform : translate(100%);\r\n    }\r\n    50% {\r\n        opacity           : 0;\r\n        -webkit-transform : translate(-100%);\r\n    }\r\n    51% {\r\n        opacity : 1;\r\n    }\r\n\r\n}\r\n@keyframes RightToLeft {\r\n    49% {\r\n        -webkit-transform : translate(100%);\r\n    }\r\n    50% {\r\n        opacity           : 0;\r\n        -webkit-transform : translate(-100%);\r\n    }\r\n    51% {\r\n        opacity : 1;\r\n    }\r\n}\r\n/* Bottom to Top key frame*/\r\n@-webkit-keyframes BottomToTop {\r\n    49% {\r\n        -webkit-transform : translateY(-100%);\r\n        }\r\n    50% {\r\n        opacity           : 0;\r\n        -webkit-transform : translateY(100%);\r\n        }\r\n    51% {\r\n        opacity : 1;\r\n        }\r\n    }\r\n@keyframes BottomToTop {\r\n    49% {\r\n        -webkit-transform : translateY(-100%);\r\n                transform : translateY(-100%);\r\n    }\r\n    50% {\r\n        opacity   : 0;\r\n        -webkit-transform : translateY(100%);\r\n                transform : translateY(100%);\r\n    }\r\n    51% {\r\n        opacity : 1;\r\n    }\r\n}\r\n/* Top to Bottom key frame*/\r\n@-webkit-keyframes TopToBottom {\r\n    49% {\r\n        -webkit-transform : translateY(-100%);\r\n        }\r\n    50% {\r\n        opacity           : 0;\r\n        -webkit-transform : translateY(100%);\r\n        }\r\n    51% {\r\n        opacity : 1;\r\n        }\r\n    }\r\n@keyframes TopToBottom {\r\n    49% {\r\n        -webkit-transform : translateY(-100%);\r\n                transform : translateY(-100%);\r\n    }\r\n    50% {\r\n        opacity   : 0;\r\n        -webkit-transform : translateY(100%);\r\n                transform : translateY(100%);\r\n    }\r\n    51% {\r\n        opacity : 1;\r\n    }\r\n}\r\n/*device-mockup*/\r\n.device-mockup {\r\n  position: relative;\r\n  width: 100%;\r\n  padding-bottom: 61.775701%;\r\n}\r\n.device-mockup > .device {\r\n  position: absolute;\r\n  top: 0;\r\n  bottom: 0;\r\n  left: 0;\r\n  right: 0;\r\n  width: 100%;\r\n  height: 100%;\r\n  background-repeat: no-repeat;\r\n  background-size: contain;\r\n  /* background-image: url(\"assets/img/device-mockups/macbook.png\"); */\r\n}\r\n.device-mockup > .device > .screen {\r\n  background-color: #000;\r\n  -webkit-transform: translateZ(0);\r\n  transform: translateZ(0);\r\n  position: absolute;\r\n  top: 11.0438729%;\r\n  bottom: 14.6747352%;\r\n  left: 13.364486%;\r\n  right: 13.364486%;\r\n  overflow: hidden;\r\n}\r\n.device-mockup[data-device=\"ipad\"],\r\n.device-mockup[data-device=\"ipad\"][data-orientation=\"portrait\"] {\r\n  padding-bottom: 128.406276%;\r\n}\r\n.device-mockup[data-device=\"ipad\"][data-orientation=\"landscape\"] {\r\n  padding-bottom: 79.9086758%;\r\n}\r\n.device-mockup[data-device=\"ipad\"] > .device,\r\n.device-mockup[data-device=\"ipad\"][data-color=\"black\"] > .device,\r\n/* .device-mockup[data-device=\"ipad\"][data-orientation=\"portrait\"][data-color=\"black\"] > .device {\r\n  background-image: url(\"assets/img/device-mockups/ipad_port_black.png\");\r\n}\r\n.device-mockup[data-device=\"ipad\"][data-color=\"white\"] > .device,\r\n.device-mockup[data-device=\"ipad\"][data-orientation=\"portrait\"][data-color=\"white\"] > .device {\r\n  background-image: url(\"assets/img/device-mockups/ipad_port_white.png\");\r\n}\r\n.device-mockup[data-device=\"ipad\"][data-orientation='landscape'] > .device,\r\n.device-mockup[data-device=\"ipad\"][data-orientation=\"landscape\"][data-color=\"black\"] > .device {\r\n  background-image: url(\"assets/img/device-mockups/ipad_land_black.png\");\r\n}\r\n.device-mockup[data-device=\"ipad\"][data-orientation=\"landscape\"][data-color=\"white\"] > .device {\r\n  background-image: url(\"assets/img/device-mockups/ipad_land_white.png\");\r\n} */\r\n.device-mockup[data-device=\"ipad\"] > .device > .screen,\r\n.device-mockup[data-device=\"ipad\"][data-orientation=\"portrait\"] > .device > .screen {\r\n  top: 12.025723%;\r\n  bottom: 12.154341%;\r\n  left: 13.45995%;\r\n  right: 13.45995%;\r\n}\r\n.device-mockup[data-device=\"ipad\"][data-orientation=\"landscape\"] > .device > .screen {\r\n  top: 13.87755102%;\r\n  bottom: 13.87755102%;\r\n  left: 11.5459883%;\r\n  right: 11.5459883%;\r\n}\r\n/*screenshot-carousel*/\r\n.screenshot-carousel-wrapper .carousel-control {\r\n    top: 50%;\r\n    margin-top: -35px;\r\n    text-shadow: none;\r\n    filter: alpha(opacity=100);\r\n    opacity: 1;\r\n    width: 70px;\r\n    height: 70px;\r\n    font-size: 40px;\r\n    line-height: 70px;\r\n    background: #fff;\r\n    background-image: none !important;\r\n    border-radius: 50%;\r\n    box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);\r\n    transition: all 0.3s ease;\r\n}\r\n.screenshot-carousel-wrapper .carousel-control:hover {\r\n    color: #fff;\r\n    background: #81c784;\r\n    box-shadow: 0 8px 17px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);\r\n}\r\n@media screen and (min-width: 850px) {\r\n    .screenshot-carousel-wrapper .carousel-control.left {\r\n        left: -70px;\r\n    }\r\n    .screenshot-carousel-wrapper .carousel-control.right {\r\n        right: -70px;\r\n    }\r\n}\r\n@media screen and (max-width: 767px) {\r\n    .screenshot-carousel-wrapper .carousel-control {\r\n        margin-top: -20px;\r\n        width: 40px;\r\n        height: 40px;\r\n        font-size: 24px;\r\n        line-height: 40px;\r\n    }\r\n}\r\n/* ========== Newsletter Subscription ============== */\r\n.subscription-success {\r\n    color: #fff;\r\n    line-height: 24px;\r\n    margin-top: 20px;\r\n}\r\n/* ================= Search Style ==================== */\r\n.has-header-search .menuzord-menu{\r\n    margin-right: 0px;\r\n}\r\n.search-trigger {\r\n    position: absolute;\r\n    right: 10px;\r\n    top: 0;\r\n    cursor: pointer;\r\n    z-index: 1;\r\n}\r\n.search-trigger:focus {\r\n    outline: none\r\n}\r\n.search-trigger i {\r\n    font-size: 18px;\r\n    line-height: 102px;\r\n    color: #202020;\r\n    position: absolute;\r\n    top: 0;\r\n    left: 0;\r\n    right: 0;\r\n    text-align: center;\r\n    margin: 0px auto;\r\n    transition: all 0.3s ease; \r\n}\r\n@media (min-width: 769px) {\r\n    .search-trigger.light i {\r\n        color: #fff;\r\n    }\r\n    .search-trigger.dark i {\r\n        color: #202020;\r\n    }\r\n    .sticky .search-trigger.light i {\r\n        color: #999;\r\n    }\r\n    .sticky .search-trigger.light.semidark i {\r\n        color: #fff;\r\n    }\r\n\r\n\r\n}\r\n.search-trigger i:hover {\r\n    color: #03a9f4;\r\n}\r\n.tt-nav.sticky .search-trigger i.material-icons {\r\n    line-height: 62px;\r\n}\r\n.search-trigger .search-btn {\r\n    padding: 0;\r\n    outline: 0;\r\n    width: 40px;\r\n    height: 40px;\r\n    margin-top: -35px;\r\n    border-radius: 50%;\r\n    box-sizing: border-box;\r\n    -webkit-transform-origin: 50%;\r\n            transform-origin: 50%;\r\n    transition: all 0.7s cubic-bezier(0.4, 0, 0.2, 1);\r\n}\r\n.search-trigger .search-btn:active,\r\n.search-trigger .search-btn:focus {\r\n    outline: none\r\n}\r\n.search-trigger .icon-search,\r\n.search-trigger .search-close {\r\n    transition: 0.2s ease-in-out;\r\n}\r\n.icon-search {\r\n    position: absolute;\r\n    top: 18px;\r\n    left: 20px;\r\n    font-size: 28px\r\n}\r\n.search-close {\r\n    position: fixed;\r\n    top: 35px;\r\n    right: 35px;\r\n    color: rgba(255, 255, 255, 0.9);\r\n    font-size: 30px;\r\n    visibility: hidden;\r\n    -webkit-transform: translate(10px, 0) rotate(90deg);\r\n    transform: translate(10px, 0) rotate(90deg);\r\n    transition: all 0.3s ease-in-out;\r\n    z-index: 1001;\r\n    cursor: pointer\r\n}\r\n.search-close:hover {\r\n    color: #fff;\r\n}\r\n.search-form-wrapper{\r\n    position: relative;\r\n}\r\n.search-form-wrapper .search-button{\r\n    position: absolute;\r\n    top: 30px;\r\n    right: 0;\r\n    border-radius: 50%;\r\n    width: 40px;\r\n    height: 40px;\r\n    font-size: 14px;\r\n    text-align: center;\r\n    line-height: 40px;\r\n    padding: 0;\r\n}\r\n.search-form-wrapper .input-field label,\r\n.search-form-wrapper .input-field input[type=text] {\r\n    color: #fff;\r\n    font-size: 17px;\r\n}\r\n.search-form-wrapper .input-field label.active {\r\n    font-size: 12px;\r\n}\r\n.search-form-wrapper form{\r\n    position: absolute;\r\n    top: 150px;\r\n    left: 0;\r\n    right: 0;\r\n    bottom: 0;\r\n    display: block;\r\n    pointer-events: none;\r\n    -moz-opacity: 0;\r\n    -webkit-opacity: 0;\r\n    opacity: 0;    \r\n    visibility: hidden;\r\n    -webkit-transform: translate(40px, 0);\r\n    transform: translate(40px, 0);\r\n    transition: all 0.3s ease-in-out;\r\n    z-index: 1000;\r\n}\r\n.active-search {\r\n    overflow: hidden\r\n}\r\n.active-search .search-form-wrapper form,\r\n.active-search .search-close {\r\n    -moz-opacity: 1;\r\n    -webkit-opacity: 1;\r\n    opacity: 1;\r\n    visibility: visible;\r\n    pointer-events: all;\r\n    -webkit-transform: none;\r\n    transform: none;\r\n}\r\n.active-search .search-trigger .icon-search {\r\n    -moz-opacity: 0;\r\n    -webkit-opacity: 0;\r\n    opacity: 0;\r\n    visibility: hidden\r\n}\r\n.active-search .search-trigger .search-btn {\r\n    position: relative;\r\n    cursor: default;\r\n    z-index: 300;\r\n    background-color: rgba(3, 169, 244, 0.9);\r\n    -webkit-transform: scale(90);\r\n    transform: scale(90);\r\n}\r\n@media screen and (max-width: 768px) {\r\n    .search-trigger i {\r\n        line-height: 48px;\r\n    }\r\n\r\n    .borderright{\r\n        border-right: none;\r\n    }\r\n    .search-trigger .search-btn {\r\n        margin-top: 6px;\r\n    }\r\n    .search-trigger {\r\n        position: absolute;\r\n        right: 10px;\r\n        top: 0;\r\n        cursor: pointer;\r\n        z-index: 0;\r\n    }\r\n    .search-trigger .search-btn {\r\n        padding: 0;\r\n        outline: 0;\r\n        width: 40px;\r\n        height: 40px;\r\n        margin-top: 10px;\r\n        border-radius: 50%;\r\n        box-sizing: border-box;\r\n        -webkit-transform-origin: 50%;\r\n                transform-origin: 50%;\r\n        transition: all 0.7s cubic-bezier(0.4, 0, 0.2, 1);\r\n    }\r\n    .btn {\r\n        height: 50px;\r\n        padding: 12px 12px;\r\n        font-size: 16px;\r\n        line-height: 26px;\r\n        border-radius: 2px;\r\n        background-color: #03a9f4;\r\n        border: 0;\r\n    }\r\n    .btn {\r\n        font-size: 11px;\r\n    }\r\n    .owl-dots{\r\n    display: block !important;\r\n}\r\n.position5{\r\n    position:fixed;\r\n    width:300px;\r\n    padding: 1em;\r\n    right: 0;\r\n    bottom: 0;\r\n    z-index: 10000;\r\n    margin-bottom: 2.5em;\r\n\r\n}\r\n\r\n.list-icon1 li {\r\n    font-size: 10px;\r\n    line-height: 30px;\r\n}\r\n\r\n}\r\n/* ================= Preloader ==================== */\r\n#preloader {\r\n    position: fixed;\r\n    top: 0;\r\n    left: 0;\r\n    height: 100%;\r\n    width: 100%;\r\n    background: #fff;\r\n    z-index: 999999;\r\n    text-align:center;\r\n}\r\n#preloader .preloader-position{\r\n    width:100%;\r\n    margin:0 auto;\r\n    left: 50%;\r\n    position: absolute;\r\n    top: 50%;\r\n    -webkit-transform: translate(-50%, -50%);\r\n    transform: translate(-50%, -50%);\r\n}\r\n#preloader .progress{\r\n    height: 2px;\r\n    margin: 35px 0;\r\n}\r\n#preloader .progress .indeterminate{\r\n    background:#03a9f4;\r\n}\r\n.xamarianSec1 img{\r\n    height: 160px;\r\n    width: 600px;\r\n    margin: 0 auto;\r\n}\r\n.xamarianSec img{\r\n    height: 360px;\r\n    width: 600px;\r\n    margin: 0 auto;\r\n}\r\n.bigperlArticle_3a img{\r\n    height: 540px;\r\n    width: 940px;\r\n    margin: 0 auto;\r\n}\r\n.bigperlArticle_3b img{\r\n    height: 570px;\r\n    width: 420px;\r\n    margin: 0 auto;\r\n}\r\n.location-based-technology ul {\r\n  margin: 0;\r\n}\r\n.location-based-technology ul li{\r\n  margin: 0;\r\n}\r\n.location-based-technology ul.dashed  {\r\n list-style-type: none;\r\n}\r\n.location-based-technology ul.dashed  > li {\r\n  text-indent: 20px;\r\n}\r\n.location-based-technology ul.dashed > li:before {\r\n  content: \" - \\00a0 \\00a0 \\00a0 \\00a0 \\00a0 \";\r\n  text-indent: 20px;\r\n\r\n}\r\n.location-based-technology1 img{\r\n    height: 440px;\r\n    width: 900px;\r\n    margin: 0 auto;\r\n}\r\n.location-based-technology2 img{\r\n    height: 460px;\r\n    width: 500px;\r\n    margin: 0 auto;\r\n}\r\n.location-based-technology img{\r\n    height: 380px;\r\n    width: 740px;\r\n    margin: 0 auto;\r\n}\r\n.blog .well{\r\nmargin: 0 auto;\r\npadding: 0;\r\n}\r\n.blog .media img{\r\nheight: 150px;\r\nwidth:100%;\r\n}\r\n.blog  .img-heading{\r\nwidth: 100%;\r\nbackground-color: #38B7EE;\r\n\r\n   \r\n}\r\n.blog  .img-heading h5{\r\ncolor: white;\r\npadding: 5px;\r\n\r\n   \r\n}\r\n.pagination1 {\r\n      text-align: centre;\r\n}\r\n/*.blog .add-on .input-group-btn > .btn {\r\n  border-left-width:0;left:-2px;\r\n  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);\r\n  box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);\r\n}\r\n/* stop the glowing blue shadow */\r\n/*.blog .add-on .form-control:focus {\r\n box-shadow:none;\r\n -webkit-box-shadow:none; \r\n border-color:#cccccc; \r\n}\r\n.blog .form-control{width:20%}\r\n.navbar-nav > li > a {\r\n  border-right: 1px solid #ddd;\r\n  padding-bottom: 15px;\r\n  padding-top: 15px;\r\n}\r\n.navbar-nav:last-child{ border-right:0}*/\r\n*/\r\n\r\n/*\r\n   .input-group-btn .btn-group {\r\n    display: flex !important;\r\n}\r\n.btn-group .btn {\r\n    border-radius: 0;\r\n    margin-left: -1px;\r\n}\r\n.btn-group .btn:last-child {\r\n    border-top-right-radius: 4px;\r\n    border-bottom-right-radius: 4px;\r\n}\r\n.btn-group .form-horizontal .btn[type=\"submit\"] {\r\n  border-top-left-radius: 4px;\r\n  border-bottom-left-radius: 4px;\r\n}\r\n.form-horizontal .form-group {\r\n    margin-left: 0;\r\n    margin-right: 0;\r\n}\r\n.form-group .form-control:last-child {\r\n    border-top-left-radius: 4px;\r\n    border-bottom-left-radius: 4px;\r\n}\r\n\r\n\r\n*/\r\n .  btn-a{\r\n    \r\n      height: 50px;\r\n    padding: 12px 20px;\r\n    font-size: 16px;\r\n    line-height: 26px;\r\n    border-radius: 2px;\r\n    background-color: #03a9f4;\r\n    border: 0;\r\n\r\n }\r\n.btn-a{\r\n    display: inline-block;\r\n    padding: 6px 12px;\r\n    margin-bottom: 0;\r\n    font-size: 14px;\r\n    font-weight: 400;\r\n    line-height: 1.42857143;\r\n    text-align: center;\r\n    white-space: nowrap;\r\n    vertical-align: middle;\r\n    touch-action: manipulation;\r\n    cursor: pointer;\r\n    -webkit-user-select: none;\r\n    -moz-user-select: none;\r\n    -ms-user-select: none;\r\n    user-select: none;\r\n    background-image: none;\r\n    border: 1px solid transparent;\r\n    border-radius: 4px;\r\n}\r\n\r\n"

/***/ }),

/***/ "./node_modules/style-loader/lib/addStyles.js":
/*!****************************************************!*\
  !*** ./node_modules/style-loader/lib/addStyles.js ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/

var stylesInDom = {};

var	memoize = function (fn) {
	var memo;

	return function () {
		if (typeof memo === "undefined") memo = fn.apply(this, arguments);
		return memo;
	};
};

var isOldIE = memoize(function () {
	// Test for IE <= 9 as proposed by Browserhacks
	// @see http://browserhacks.com/#hack-e71d8692f65334173fee715c222cb805
	// Tests for existence of standard globals is to allow style-loader
	// to operate correctly into non-standard environments
	// @see https://github.com/webpack-contrib/style-loader/issues/177
	return window && document && document.all && !window.atob;
});

var getTarget = function (target) {
  return document.querySelector(target);
};

var getElement = (function (fn) {
	var memo = {};

	return function(target) {
                // If passing function in options, then use it for resolve "head" element.
                // Useful for Shadow Root style i.e
                // {
                //   insertInto: function () { return document.querySelector("#foo").shadowRoot }
                // }
                if (typeof target === 'function') {
                        return target();
                }
                if (typeof memo[target] === "undefined") {
			var styleTarget = getTarget.call(this, target);
			// Special case to return head of iframe instead of iframe itself
			if (window.HTMLIFrameElement && styleTarget instanceof window.HTMLIFrameElement) {
				try {
					// This will throw an exception if access to iframe is blocked
					// due to cross-origin restrictions
					styleTarget = styleTarget.contentDocument.head;
				} catch(e) {
					styleTarget = null;
				}
			}
			memo[target] = styleTarget;
		}
		return memo[target]
	};
})();

var singleton = null;
var	singletonCounter = 0;
var	stylesInsertedAtTop = [];

var	fixUrls = __webpack_require__(/*! ./urls */ "./node_modules/style-loader/lib/urls.js");

module.exports = function(list, options) {
	if (typeof DEBUG !== "undefined" && DEBUG) {
		if (typeof document !== "object") throw new Error("The style-loader cannot be used in a non-browser environment");
	}

	options = options || {};

	options.attrs = typeof options.attrs === "object" ? options.attrs : {};

	// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
	// tags it will allow on a page
	if (!options.singleton && typeof options.singleton !== "boolean") options.singleton = isOldIE();

	// By default, add <style> tags to the <head> element
        if (!options.insertInto) options.insertInto = "head";

	// By default, add <style> tags to the bottom of the target
	if (!options.insertAt) options.insertAt = "bottom";

	var styles = listToStyles(list, options);

	addStylesToDom(styles, options);

	return function update (newList) {
		var mayRemove = [];

		for (var i = 0; i < styles.length; i++) {
			var item = styles[i];
			var domStyle = stylesInDom[item.id];

			domStyle.refs--;
			mayRemove.push(domStyle);
		}

		if(newList) {
			var newStyles = listToStyles(newList, options);
			addStylesToDom(newStyles, options);
		}

		for (var i = 0; i < mayRemove.length; i++) {
			var domStyle = mayRemove[i];

			if(domStyle.refs === 0) {
				for (var j = 0; j < domStyle.parts.length; j++) domStyle.parts[j]();

				delete stylesInDom[domStyle.id];
			}
		}
	};
};

function addStylesToDom (styles, options) {
	for (var i = 0; i < styles.length; i++) {
		var item = styles[i];
		var domStyle = stylesInDom[item.id];

		if(domStyle) {
			domStyle.refs++;

			for(var j = 0; j < domStyle.parts.length; j++) {
				domStyle.parts[j](item.parts[j]);
			}

			for(; j < item.parts.length; j++) {
				domStyle.parts.push(addStyle(item.parts[j], options));
			}
		} else {
			var parts = [];

			for(var j = 0; j < item.parts.length; j++) {
				parts.push(addStyle(item.parts[j], options));
			}

			stylesInDom[item.id] = {id: item.id, refs: 1, parts: parts};
		}
	}
}

function listToStyles (list, options) {
	var styles = [];
	var newStyles = {};

	for (var i = 0; i < list.length; i++) {
		var item = list[i];
		var id = options.base ? item[0] + options.base : item[0];
		var css = item[1];
		var media = item[2];
		var sourceMap = item[3];
		var part = {css: css, media: media, sourceMap: sourceMap};

		if(!newStyles[id]) styles.push(newStyles[id] = {id: id, parts: [part]});
		else newStyles[id].parts.push(part);
	}

	return styles;
}

function insertStyleElement (options, style) {
	var target = getElement(options.insertInto)

	if (!target) {
		throw new Error("Couldn't find a style target. This probably means that the value for the 'insertInto' parameter is invalid.");
	}

	var lastStyleElementInsertedAtTop = stylesInsertedAtTop[stylesInsertedAtTop.length - 1];

	if (options.insertAt === "top") {
		if (!lastStyleElementInsertedAtTop) {
			target.insertBefore(style, target.firstChild);
		} else if (lastStyleElementInsertedAtTop.nextSibling) {
			target.insertBefore(style, lastStyleElementInsertedAtTop.nextSibling);
		} else {
			target.appendChild(style);
		}
		stylesInsertedAtTop.push(style);
	} else if (options.insertAt === "bottom") {
		target.appendChild(style);
	} else if (typeof options.insertAt === "object" && options.insertAt.before) {
		var nextSibling = getElement(options.insertInto + " " + options.insertAt.before);
		target.insertBefore(style, nextSibling);
	} else {
		throw new Error("[Style Loader]\n\n Invalid value for parameter 'insertAt' ('options.insertAt') found.\n Must be 'top', 'bottom', or Object.\n (https://github.com/webpack-contrib/style-loader#insertat)\n");
	}
}

function removeStyleElement (style) {
	if (style.parentNode === null) return false;
	style.parentNode.removeChild(style);

	var idx = stylesInsertedAtTop.indexOf(style);
	if(idx >= 0) {
		stylesInsertedAtTop.splice(idx, 1);
	}
}

function createStyleElement (options) {
	var style = document.createElement("style");

	if(options.attrs.type === undefined) {
		options.attrs.type = "text/css";
	}

	addAttrs(style, options.attrs);
	insertStyleElement(options, style);

	return style;
}

function createLinkElement (options) {
	var link = document.createElement("link");

	if(options.attrs.type === undefined) {
		options.attrs.type = "text/css";
	}
	options.attrs.rel = "stylesheet";

	addAttrs(link, options.attrs);
	insertStyleElement(options, link);

	return link;
}

function addAttrs (el, attrs) {
	Object.keys(attrs).forEach(function (key) {
		el.setAttribute(key, attrs[key]);
	});
}

function addStyle (obj, options) {
	var style, update, remove, result;

	// If a transform function was defined, run it on the css
	if (options.transform && obj.css) {
	    result = options.transform(obj.css);

	    if (result) {
	    	// If transform returns a value, use that instead of the original css.
	    	// This allows running runtime transformations on the css.
	    	obj.css = result;
	    } else {
	    	// If the transform function returns a falsy value, don't add this css.
	    	// This allows conditional loading of css
	    	return function() {
	    		// noop
	    	};
	    }
	}

	if (options.singleton) {
		var styleIndex = singletonCounter++;

		style = singleton || (singleton = createStyleElement(options));

		update = applyToSingletonTag.bind(null, style, styleIndex, false);
		remove = applyToSingletonTag.bind(null, style, styleIndex, true);

	} else if (
		obj.sourceMap &&
		typeof URL === "function" &&
		typeof URL.createObjectURL === "function" &&
		typeof URL.revokeObjectURL === "function" &&
		typeof Blob === "function" &&
		typeof btoa === "function"
	) {
		style = createLinkElement(options);
		update = updateLink.bind(null, style, options);
		remove = function () {
			removeStyleElement(style);

			if(style.href) URL.revokeObjectURL(style.href);
		};
	} else {
		style = createStyleElement(options);
		update = applyToTag.bind(null, style);
		remove = function () {
			removeStyleElement(style);
		};
	}

	update(obj);

	return function updateStyle (newObj) {
		if (newObj) {
			if (
				newObj.css === obj.css &&
				newObj.media === obj.media &&
				newObj.sourceMap === obj.sourceMap
			) {
				return;
			}

			update(obj = newObj);
		} else {
			remove();
		}
	};
}

var replaceText = (function () {
	var textStore = [];

	return function (index, replacement) {
		textStore[index] = replacement;

		return textStore.filter(Boolean).join('\n');
	};
})();

function applyToSingletonTag (style, index, remove, obj) {
	var css = remove ? "" : obj.css;

	if (style.styleSheet) {
		style.styleSheet.cssText = replaceText(index, css);
	} else {
		var cssNode = document.createTextNode(css);
		var childNodes = style.childNodes;

		if (childNodes[index]) style.removeChild(childNodes[index]);

		if (childNodes.length) {
			style.insertBefore(cssNode, childNodes[index]);
		} else {
			style.appendChild(cssNode);
		}
	}
}

function applyToTag (style, obj) {
	var css = obj.css;
	var media = obj.media;

	if(media) {
		style.setAttribute("media", media)
	}

	if(style.styleSheet) {
		style.styleSheet.cssText = css;
	} else {
		while(style.firstChild) {
			style.removeChild(style.firstChild);
		}

		style.appendChild(document.createTextNode(css));
	}
}

function updateLink (link, options, obj) {
	var css = obj.css;
	var sourceMap = obj.sourceMap;

	/*
		If convertToAbsoluteUrls isn't defined, but sourcemaps are enabled
		and there is no publicPath defined then lets turn convertToAbsoluteUrls
		on by default.  Otherwise default to the convertToAbsoluteUrls option
		directly
	*/
	var autoFixUrls = options.convertToAbsoluteUrls === undefined && sourceMap;

	if (options.convertToAbsoluteUrls || autoFixUrls) {
		css = fixUrls(css);
	}

	if (sourceMap) {
		// http://stackoverflow.com/a/26603875
		css += "\n/*# sourceMappingURL=data:application/json;base64," + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + " */";
	}

	var blob = new Blob([css], { type: "text/css" });

	var oldSrc = link.href;

	link.href = URL.createObjectURL(blob);

	if(oldSrc) URL.revokeObjectURL(oldSrc);
}


/***/ }),

/***/ "./node_modules/style-loader/lib/urls.js":
/*!***********************************************!*\
  !*** ./node_modules/style-loader/lib/urls.js ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {


/**
 * When source maps are enabled, `style-loader` uses a link element with a data-uri to
 * embed the css on the page. This breaks all relative urls because now they are relative to a
 * bundle instead of the current page.
 *
 * One solution is to only use full urls, but that may be impossible.
 *
 * Instead, this function "fixes" the relative urls to be absolute according to the current page location.
 *
 * A rudimentary test suite is located at `test/fixUrls.js` and can be run via the `npm test` command.
 *
 */

module.exports = function (css) {
  // get current location
  var location = typeof window !== "undefined" && window.location;

  if (!location) {
    throw new Error("fixUrls requires window.location");
  }

	// blank or null?
	if (!css || typeof css !== "string") {
	  return css;
  }

  var baseUrl = location.protocol + "//" + location.host;
  var currentDir = baseUrl + location.pathname.replace(/\/[^\/]*$/, "/");

	// convert each url(...)
	/*
	This regular expression is just a way to recursively match brackets within
	a string.

	 /url\s*\(  = Match on the word "url" with any whitespace after it and then a parens
	   (  = Start a capturing group
	     (?:  = Start a non-capturing group
	         [^)(]  = Match anything that isn't a parentheses
	         |  = OR
	         \(  = Match a start parentheses
	             (?:  = Start another non-capturing groups
	                 [^)(]+  = Match anything that isn't a parentheses
	                 |  = OR
	                 \(  = Match a start parentheses
	                     [^)(]*  = Match anything that isn't a parentheses
	                 \)  = Match a end parentheses
	             )  = End Group
              *\) = Match anything and then a close parens
          )  = Close non-capturing group
          *  = Match anything
       )  = Close capturing group
	 \)  = Match a close parens

	 /gi  = Get all matches, not the first.  Be case insensitive.
	 */
	var fixedCss = css.replace(/url\s*\(((?:[^)(]|\((?:[^)(]+|\([^)(]*\))*\))*)\)/gi, function(fullMatch, origUrl) {
		// strip quotes (if they exist)
		var unquotedOrigUrl = origUrl
			.trim()
			.replace(/^"(.*)"$/, function(o, $1){ return $1; })
			.replace(/^'(.*)'$/, function(o, $1){ return $1; });

		// already a full url? no change
		if (/^(#|data:|http:\/\/|https:\/\/|file:\/\/\/|\s*$)/i.test(unquotedOrigUrl)) {
		  return fullMatch;
		}

		// convert the url to a full url
		var newUrl;

		if (unquotedOrigUrl.indexOf("//") === 0) {
		  	//TODO: should we add protocol?
			newUrl = unquotedOrigUrl;
		} else if (unquotedOrigUrl.indexOf("/") === 0) {
			// path should be relative to the base url
			newUrl = baseUrl + unquotedOrigUrl; // already starts with '/'
		} else {
			// path should be relative to current directory
			newUrl = currentDir + unquotedOrigUrl.replace(/^\.\//, ""); // Strip leading './'
		}

		// send back the fixed url(...)
		return "url(" + JSON.stringify(newUrl) + ")";
	});

	// send back the fixed css
	return fixedCss;
};


/***/ }),

/***/ "./src/styles.css":
/*!************************!*\
  !*** ./src/styles.css ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../node_modules/raw-loader!../node_modules/postcss-loader/lib??embedded!./styles.css */ "./node_modules/raw-loader/index.js!./node_modules/postcss-loader/lib/index.js??embedded!./src/styles.css");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ 2:
/*!******************************!*\
  !*** multi ./src/styles.css ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\Sunil Yadav\Desktop\Desk\aba\bigperlwebsite_angular\src\styles.css */"./src/styles.css");


/***/ })

},[[2,"runtime"]]]);
//# sourceMappingURL=styles.js.map